var VialSoftClinic = VialSoftClinic || {};

VialSoftClinic.Config = function () {
    var bingMapsKey = '76x5fueKlPdZdpORSfpc~bSxz_DgcqA6Qptt3Y9Kalg~AuRtAguH6QYFfxt0f4HIZ1evLKLxM0STU0A2psSuyCnzewq-o8a_qxw3EgqA2XNz',
        infobBoxCompanyAddress = 'VialSoft',
        companyLocation = {
            Latitude: -32.949979,
            Longitude: -60.655932
        };

    return {
        bingMapsKey: bingMapsKey,
        infoBoxCompanyAddress: infobBoxCompanyAddress,
        companyLocation: companyLocation
    };
}();
