﻿var ModuleName = "VialSoft";

import SharedModuleName from "./components/shared/shared.module";
import DashboardModuleName from "./components/dashboard/dashboard.module";
import CustomersModuleName from "./components/customers/customers.module";
import WorksModuleName from "./components/works/works.module";
import AssetsModuleName from "./components/assets/assets.module";
import AccessoriesModuleName from "./components/accessories/accessories.module";
import TenantsModuleName from "./components/tenants/tenants.module";
import PurchasesModuleName from "./components/purchases/purchases.module";
import TransfersWorksModuleName from "./components/transfersworks/transfersworks.module";
import ProvidersModuleName from "./components/providers/providers.module";
import MaintenancecardsModuleName from "./components/maintenancecards/maintenancecards.module";
import CosteAssetModuleName from "./components/costeasset/costeasset.module";


var app = angular.module(ModuleName, ["ui.router", "ngAnimate", SharedModuleName, DashboardModuleName,
                         CustomersModuleName, WorksModuleName, AssetsModuleName, AccessoriesModuleName,
                         TenantsModuleName, PurchasesModuleName, TransfersWorksModuleName,
                         ProvidersModuleName, MaintenancecardsModuleName,
                         CosteAssetModuleName]);

app.config(Config);

function Config($stateProvider, $urlRouterProvider, $compileProvider) {

    const defaultUrl = "/";

    $compileProvider.debugInfoEnabled(false);

    $urlRouterProvider.when("", defaultUrl);

    $urlRouterProvider.otherwise('/404');

    $stateProvider
        .state("dashboard", {
            url: "/",
            templateUrl: "/app/components/dashboard/views/main.html",
            controller:"dashboardController"
        })
        .state("customers", {
            url: "/customers",
            templateUrl: "/app/components/customers/views/main.html",
            controller:"customersController"
        })
        .state("customer", {
            url: "/customer?id",
            templateUrl: "/app/components/customers/views/detail.html",
            controller:"customersdetailController"
        })
        .state("works", {
            url: "/works",
            templateUrl: "/app/components/works/views/main.html",
            controller:"worksController"
        })
        .state("work", {
            url: "/work?id",
            templateUrl: "/app/components/works/views/detail.html",
            controller:"worksdetailController"
        })
        .state("assets", {
            url: "/assets",
            templateUrl: "/app/components/assets/views/main.html",
            controller:"assetsController"
        })
        .state("asset", {
            url: "/asset?id",
            templateUrl: "/app/components/assets/views/detail.html",
            controller:"assetsdetailController"
        })
        .state("accessories", {
            url: "/accessories?id",
            templateUrl: "/app/components/accessories/views/main.html",
            controller:"accessoriesController"
        })
        .state("accessory", {
            url: "/accessory?id",
            templateUrl: "/app/components/accessories/views/detail.html",
            controller:"accessoriesdetailController"
        })
        .state("tenants", {
            url: "/tenants",
            templateUrl: "/app/components/tenants/views/main.html",
            controller:"tenantsController"
        })
        .state("tenant", {
            url: "/tenant?id",
            templateUrl: "/app/components/tenants/views/detail.html",
            controller:"tenantsdetailController"
        })
        .state("purchases", {
            url: "/purchases",
            templateUrl: "/app/components/purchases/views/main.html",
            controller:"purchasesController"
        })
        .state("purchase", {
            url: "/purchase?id",
            templateUrl: "/app/components/purchases/views/detail.html",
            controller:"purchasesdetailController"
        })
        .state("transfersworks", {
            url: "/transfersworks",
            templateUrl: "/app/components/transfersworks/views/main.html",
            controller:"transfersworksController"
        })
        .state("transferwork", {
            url: "/transferwork?id",
            templateUrl: "/app/components/transfersworks/views/detail.html",
            controller:"transfersworksdetailController"
        })
        .state("providers", {
            url: "/providers",
            templateUrl: "/app/components/providers/views/main.html",
            controller:"providersController"
        })
        .state("provider", {
            url: "/provider?id",
            templateUrl: "/app/components/providers/views/detail.html",
            controller:"providersdetailController"
        })
        .state("maintenancecards", {
            url: "/maintenancecards?id",
            templateUrl: "/app/components/maintenancecards/views/main.html",
            controller:"maintenancecardsController"
        })
        .state("maintenancecard", {
            url: "/maintenancecard?id",
            templateUrl: "/app/components/maintenancecards/views/detail.html",
            controller:"maintenancecardsdetailController"
        })
        .state("costeassets", {
            url: "/costeassets",
            templateUrl: "/app/components/costeasset/views/main.html",
            controller:"costeassetController"
        })
        .state("costeasset", {
            url: "/costeasset?id",
            templateUrl: "/app/components/costeasset/views/detail.html",
            controller:"costeassetdetailController"
        })
        .state("error", {
            url: "/404",
            templateUrl: "/app/components/shared/views/error.html"
        });
}

export default ModuleName;