﻿function MaintenanceCardsService($http) {
    "use strict";

    var maintenancecards;

    return {
        getJob,
        getJobs,
        getPeriod,
        getTenant,
        getMaintenanceCards,
        getList,
        add,
        remove
    };

    function getJob(){
        return $http({
            method: "GET",
            url: "/api/Jobs"
        });
    }

    function getJobs(periodId) {
        return $http({
            method: "GET",
            url: `/api/Jobs/${periodId}`
        });
    }

    function getPeriod() {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/Periods`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });


    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount, assetId) {
        let handleSuccess = (response) => {
            maintenancecards = response.data;
            return maintenancecards;
        };
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = `/api/maintenancecards`;
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount,
                    assetId: assetId
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getMaintenanceCards(maintenancecardsId) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/maintenancecards/${maintenancecardsId}`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(maintenancecards) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/maintenancecards/";
            return $http({
                method: "POST",
                url: url,
                data: maintenancecards,
                headers: {
                    TenantId: tenantId                 
                }
            });
        });
    }

    function remove(mantenanceCardId) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            let url = `/api/maintenancecards/${mantenanceCardId}`;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
    
}

export default MaintenanceCardsService;