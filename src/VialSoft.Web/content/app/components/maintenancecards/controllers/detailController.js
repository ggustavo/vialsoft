﻿class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, maintenancecardsService, toasterService, modalService) {

        $("select").select2({ width: "100%" });

        var maintenancecardsId = $stateParams.id;
        var tenantId;
        $scope.editMode = false;
        $scope.showDetail = false;

        $scope.maintenancecards = {
            TenantId: 0,
            AssetId: maintenancecardsId,
            MaintenanceCardDetail: []
        };

        maintenancecardsService.getPeriod()
            .then((response) => {
                $scope.periods = response.data;
            });

        maintenancecardsService.getJob()
            .then((response) => {
                $scope.jobs = response.data;
            });

        $scope.getJobs = () => {
            $scope.jobs = null;
            maintenancecardsService.getJobs($scope.maintenancecard.PeriodId)
                .then((response) => {
                    $scope.jobs = response.data;
                    $("#job").select2({
                        width: "100%"
                    });
                });
        }

        $scope.addMaintenanceCardDetail = () => {
            var detail = {
                PeriodId :$scope.maintenancecard.PeriodId,
                JobId: $scope.maintenancecard.JobId,
                Replacement: $scope.maintenancecard.Replacement             
            };
            $scope.maintenancecards.MaintenanceCardDetail.push(detail);
            $scope.showDetail = true;
        }

        if ($scope.editMode) {
            $rootScope.loading = true;
            maintenancecardsService.getmaintenancecards(maintenancecardsId)
                .then((response) => {
                    $scope.maintenancecards = response.data;
                    $scope.showDetail = true;

                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        }

        $scope.removeAsset = (assetItem) => {
            let index = $scope.maintenancecards.Details.indexOf(assetItem);
            $scope.maintenancecards.Details.splice(index, 1);
        }

        $scope.navigateBack = () => {
            $state.transitionTo("maintenancecards", {id : maintenancecardsId});
        };

        $scope.save = () => {
            $rootScope.loading = true;
            maintenancecardsService.getTenant()
                .then(function(response) {
                    tenantId = response.data;

                    $scope.maintenancecards.TenantId = tenantId;

                    maintenancecardsService.add($scope.maintenancecards)
                        .then((responseAdd) => {
                            if (responseAdd.status === 200) {
                                $scope.navigateBack();
                            }
                        });

                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });

        };

    }
}

export default DetailController;