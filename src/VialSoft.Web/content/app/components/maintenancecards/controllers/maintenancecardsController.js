﻿class MaintenanceCardsController {
    constructor($scope, $rootScope, $stateParams, $state, maintenancecardsService, toasterService, modalService) {

        var assetId = $stateParams.id;
        const pageSize = 6;
        var pageCount = 0;
        $scope.newMaintenanceCard = true;
        $scope.AssetId = assetId;
        $scope.maintenancecards = [];

        $scope.getList = () => {
            $rootScope.loading = true;
            maintenancecardsService.getList(pageSize, pageCount, assetId)
                .then((maintenanceCards) => {

                    if (maintenanceCards.length > 0){
                        $scope.newMaintenanceCard = false;
                    }

                    if (maintenanceCards.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    maintenanceCards.forEach((maintenanceCard) => {
                        $scope.maintenancecards.push(maintenanceCard);
                    });
                    pageCount ++;                   
                    $scope.refreshSelectedItems();                    
                    if (!$scope.maintenancecards.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = () => {
            $state.transitionTo("maintenancecard", { id: $scope.AssetId });
        };

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.maintenancecards.forEach((maintenanceCard) => {
                    maintenanceCard.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.maintenancecards.some((maintenanceCard) => {
                return maintenanceCard.selected;
            });

            $scope.everySelected = $scope.maintenancecards.every((maintenanceCard) => {
                return maintenanceCard.selected;
            });
        };

        $scope.navigateBackAsset = () => {
            $state.transitionTo("asset", {id : $scope.AssetId});
        };

        $scope.remove = (maintenanceCard) => {
            modalService.showConfirmModal({
                messages: {
                    title: `Eliminar item`,
                    body: `Esta seguro que desea eliminar el item`,
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
            .then(() => {                
                maintenancecardsService.remove(maintenanceCard)
                        .then((response) => {
                            if (response.status === 200) {
                                $scope.maintenancecards.forEach((maintenanceCardItem) => {
                                    if (maintenanceCard === maintenanceCardItem.MaintenanceCardDetailId) {
                                        let index = $scope.maintenancecards.indexOf(maintenanceCardItem);
                                        $scope.maintenancecards.splice(index, 1);
                                    }
                                });
                            }
                        })
                        .catch((error) => {
                            toasterService.showServerError(error);
                });

                $scope.refreshSelectedItems();

            });
        };

        $scope.getList();
    }
}

export default MaintenanceCardsController;