﻿var ModuleName = "VialSoft.maintenancecards";

import MaintenancecardsController from "./controllers/maintenancecardsController";
import DetailController from "./controllers/detailController";
import MaintenancecardsService from "./services/maintenancecardsService";

angular.module(ModuleName, []).
    controller("maintenancecardsController", MaintenancecardsController).
    controller("maintenancecardsdetailController", DetailController).
    service("maintenancecardsService", MaintenancecardsService);

export default ModuleName;