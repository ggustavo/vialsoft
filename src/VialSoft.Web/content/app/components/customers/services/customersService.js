﻿function CustomersService($http) {
    "use strict";

    var customers;

    return {
        enumEntity,
        getTenant,
        getCustomer,
        getList,
        add,
        update,
        remove
    };

    function enumEntity() {
        return $http({
            method: "GET",
            url: "/api/customers/EnumEntity"
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        let handleSuccess = (response) => {
            customers = response.data;
            return customers;
        };

        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/customers";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount:pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getCustomer(customerId) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/customers/${customerId}`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(customer) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/customers/";
            return $http({
                method: "POST",
                url: url,
                data: customer,
                headers: {
                    TenantId: tenantId                 
                }
            });
        });
    }

    function update(customer) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/customers/";
            return $http({
                method: "PUT",
                url: url,
                data: customer,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(customerId) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            let url = `/api/customers/${customerId}`;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

export default CustomersService;