﻿var ModuleName = "VialSoft.customers";

import CustomersController from "./controllers/customersController";
import DetailController from "./controllers/detailController";
import CustomersService from "./services/customersService";
import FileBase64 from "./directives/fileDirective";

angular.module(ModuleName, []).
    controller("customersController", CustomersController).
    controller("customersdetailController", DetailController).
    service("customersService", CustomersService).
    directive("fileBase64", FileBase64);

export default ModuleName;