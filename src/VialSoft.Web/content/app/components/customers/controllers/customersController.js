﻿class CustomersController {
    constructor($scope, $rootScope, $state, customersService, toasterService, modalService) {

        const pageSize = 6;
        var pageCount = 0;

        $scope.customers = [];

        $scope.getList = () => {
            $rootScope.loading = true;
            customersService.getList(pageSize, pageCount)
                .then((customers) => {
                    if (customers.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    customers.forEach((customer) => {
                        $scope.customers.push(customer);
                    });
                    pageCount ++;
                    $scope.refreshSelectedItems();

                    if (!$scope.customers.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = (customerId) => {
            customerId ? $state.transitionTo("customer", { id: customerId }) : $state.transitionTo("customer");
        };

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.customers.forEach((customer) => {
                    customer.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.customers.some((customer) => {
                return customer.selected;
            });

            $scope.everySelected = $scope.customers.every((customer) => {
                return customer.selected;
            });
        };

        $scope.remove = (customer) => {

            modalService.showConfirmModal({
                messages: {
                    title: `Eliminar cliente`,
                    body: `Esta seguro que desea eliminar el cliente`,
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
            .then(() => {
                var customerIdList;
                if (customer) {
                    customerIdList = [customer.CustomerId];
                } else {
                    customerIdList = $scope.customers
                        .map((customerItem) => {
                            if (customerItem.selected) {                             
                                return customerItem.CustomerId;
                            }
                            return null;
                        });
                }
                
                customerIdList.forEach((customerId) => {
                    customersService.remove(customerId)
                        .then((response) => {
                            if (response.status === 200) {
                                $scope.customers.forEach((customerItem) => {
                                    if (customerId === customerItem.CustomerId) {
                                        let index = $scope.customers.indexOf(customerItem);
                                        $scope.customers.splice(index, 1);
                                    }
                                });
                            }
                        })
                        .catch((error) => {
                            toasterService.showServerError(error);
                        });
                });

                $scope.refreshSelectedItems();

            });
        };

        $scope.getList();
    }

}

export default CustomersController;