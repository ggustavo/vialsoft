﻿"use strict";

class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, customersService, toasterService, modalService) {

        $("select").select2({width: "100%"});

        var customerId = $stateParams.id;
        $scope.editMode = customerId !== undefined;
        var tenantId;

        $scope.customer = {
            CreatedAt: new Date()
        };

        customersService.enumEntity()
            .then((response) => {
                $scope.options = response.data;
            });

        if ($scope.editMode) {
            $rootScope.loading = true;
            customersService.getCustomer(customerId)
                .then((response) => {
                    $scope.customer = response.data;
                    customersService.enumEntity()
                        .then((responseEntity) => {
                            $scope.options = responseEntity.data;
                            $("#entity").select2({ width: "100%" });
                        });
                   
                    $scope.customer.Picture = `data:image/png;base64,${$scope.customer.Picture}`;
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        } else {
            $rootScope.loading = true;
            customersService.getTenant()
                .then(function(response) {
                    tenantId = response.data;
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        }

        $scope.navigateBack = () => {
            $state.transitionTo("customers");
        };

        $scope.removeCustomer = () => {
            modalService.showConfirmModal({
                messages: {
                    title: "Eliminar Cliente",
                    body: "Esta seguro que desea eliminar el cliente?",
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
           .then(() => {
               $rootScope.loading = true;
               customersService.remove(customerId)
                   .then((response) => {
                       if (response.status === 200) {
                           $state.transitionTo("customers");
                       }
                   })
                   .catch((error) => {
                       toasterService.showServerError(error);
                   })
                   .finally(() => {
                       $rootScope.loading = false;
                   });
           });
        };

        $scope.save = () => {
            if ($scope.customer.Picture) {
                $scope.customer.Picture = $scope.customer.Picture.split(",")[1];
            }

            if (!$scope.editMode) {
                $scope.customer.TenantId = tenantId;
                $rootScope.loading = true;

                customersService.add($scope.customer)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            } else {
                $rootScope.loading = true;
                customersService.update($scope.customer)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            }
        };
    }
}
export default DetailController;