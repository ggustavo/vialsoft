﻿var ModuleName = "VialSoft.transfersworks";

import TransfersWorksController from "./controllers/transfersworksController";
import DetailController from "./controllers/detailController";
import TransfersWorksService from "./services/transfersworksService";

angular.module(ModuleName, []).
    controller("transfersworksController", TransfersWorksController).
    controller("transfersworksdetailController", DetailController).
    service("transfersworksService", TransfersWorksService);
export default ModuleName;