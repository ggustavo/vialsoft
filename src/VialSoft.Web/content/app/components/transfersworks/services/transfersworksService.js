﻿function TransfersWorksService($http) {
    "use strict";

    var transfersWorks;

    return {
        enumStatus,
        getCategory,
        getSubCategory,
        getSubCategoryAll,
        getAsset,
        getProvider,
        getTypeReceipt,
        getCustomer,
        getTenant,
        getTransferWork,
        getList,
        getWork,
        add,
        update
    };

    function getWork(){
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/works/getAll`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getCustomer(){
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/customers/getAll`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function enumStatus() {
        return $http({
            method: "GET",
            url: "/api/transfersWorks/EnumStatus"
        });
    }

    function getCategory() {
        return $http({
            method: "GET",
            url: "/api/Categories"
        });
    }

    function getSubCategoryAll() {
        return $http({
            method: "GET",
            url: `/api/SubCategories`
        });
    }

    function getSubCategory(categoryId) {
        return $http({
            method: "GET",
            url: `/api/SubCategories/${categoryId}`
        });
    }

    function getTypeReceipt() {
        return $http({
            method: "GET",
            url: "/api/transfersWorks/TypeReceipts"
        });
    }

    function getProvider(){
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/providers/getAll`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getAsset(id) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/assets/GetAsset`;
            return $http({
                method: "GET",
                url: url,
                params: {
                    Id: id
                },
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        let handleSuccess = (response) => {
            transfersWorks = response.data;
            return transfersWorks;
        };

        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/transfersWorks";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount:pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getTransferWork(transferworkId) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/transfersWorks/${transferworkId}`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(transferwork) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/transfersWorks/";
            return $http({
                method: "POST",
                url: url,
                data: transferwork,
                headers: {
                    TenantId: tenantId                 
                }
            });
        });
    }

    function update(transferwork) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/transfersWorks/";
            return $http({
                method: "PUT",
                url: url,
                data: transferwork,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
    
}

export default TransfersWorksService;