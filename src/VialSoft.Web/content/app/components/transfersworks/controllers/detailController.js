﻿class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, transfersworksService, toasterService, modalService) {

        $("select").select2({ width: "100%" });

        var transferworkId = $stateParams.id;
        $scope.editMode = transferworkId !== undefined;
        $scope.searchMode = false;
        $scope.showDetail = false;
        $scope.countTotal = 0;
        var tenantId;

        $scope.asset = {};

        $scope.transferworkDetail = {
            AssetId: null,
            WorkId: null
        };

        $scope.transferwork = {
            CreatedAt: new Date(),
            TenantId: 0,
            Details: []
        };

        transfersworksService.getCategory()
            .then((response) => {
                $scope.categories = response.data;
            });

        transfersworksService.getSubCategoryAll()
            .then((response) => {
                $scope.subcategories = response.data;
            });

        $scope.getSubCategory = () => {
            $scope.subcategories = null;
            transfersworksService.getSubCategory($scope.asset.CategoryId)
                .then((response) => {
                    $scope.subcategories = response.data;
                    $("#subcategories").select2({
                        width: "100%"
                    });
                });
        }

        transfersworksService.getProvider()
            .then((response) => {
                $scope.providers = response.data;
            });

        transfersworksService.getTypeReceipt()
            .then((response) => {
                $scope.typeReceipts = response.data;
            });

        $scope.search = () => {
            transfersworksService.getAsset($scope.asset.Id)
                .then((response) => {
                    $scope.searchMode = true;
                    $scope.asset = response.data;
                });
        }

        transfersworksService.getWork()
            .then((response) => {
                $scope.works = response.data;
            });

        $scope.addTransferWorkDetail = () => {
            var detail = {
                AssetId: $scope.asset.AssetId,
                WorkId: $scope.asset.WorkId
            };
            $scope.countTotal ++;
            $scope.transferwork.Details.push(detail);
            $scope.showDetail = true;
        }

        if ($scope.editMode) {
            $rootScope.loading = true;
            transfersworksService.getTransferWork(transferworkId)
                .then((response) => {
                    $scope.transferwork = response.data;
                    $scope.showDetail = true;

                    $scope.transferwork.Details.forEach(() => {
                        $scope.countTotal ++;
                    });

                    transfersworksService.getTypeReceipt()
                        .then((responseType) => {
                            $scope.typeReceipts = responseType.data;
                            $("#typeReceipt").select2({ width: "100%" });
                        });
    
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        }

        $scope.removeAsset = (assetItem) => {
            let index = $scope.transferwork.Details.indexOf(assetItem);
            $scope.countTotal --;
            $scope.transferwork.Details.splice(index, 1);
        }

        $scope.navigateBack = () => {
            $state.transitionTo("transfersworks");
        };

        $scope.save = () => {
            $rootScope.loading = true;
            transfersworksService.getTenant()
                .then(function(response) {
                    tenantId = response.data;

                    $scope.transferwork.TenantId = tenantId;
                    transfersworksService.add($scope.transferwork)
                        .then((responseAdd) => {
                            if (responseAdd.status === 200) {
                                $scope.navigateBack();
                            }
                        });

                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
            
        };

    }
}

export default DetailController;