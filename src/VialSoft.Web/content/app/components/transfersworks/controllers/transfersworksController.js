﻿class TransferWorksController {
    constructor($scope, $rootScope, $state, transfersworksService, toasterService) {

        const pageSize = 6;
        var pageCount = 0;

        $scope.transfersworks = [];

        $scope.getList = () => {

            $rootScope.loading = true;
            
            transfersworksService.getList(pageSize, pageCount)
                .then((transfersworks) => {
                    if (transfersworks.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    transfersworks.forEach((transferWork) => {
                        $scope.transfersworks.push(transferWork);
                    });
                    pageCount ++;                   
                    $scope.refreshSelectedItems();                    
                    if (!$scope.transfersworks.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = (transferWorkId) => {
            transferWorkId ? $state.transitionTo("transferwork", { id: transferWorkId }) : $state.transitionTo("transferwork");
        };

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.transfersworks.forEach((transferWork) => {
                    transferWork.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.transfersworks.some((transferWork) => {
                return transferWork.selected;
            });

            $scope.everySelected = $scope.transfersworks.every((transferWork) => {
                return transferWork.selected;
            });
        };

        $scope.getList();
    }
}

export default TransferWorksController;