﻿function ToasterService(toaster) {
    "use strict";

    return {
        showServerError
    };

    function showServerError() {
        toaster.pop("error", "Error", "Oops! Ocurrio un problema!");
    }

}

export default ToasterService;