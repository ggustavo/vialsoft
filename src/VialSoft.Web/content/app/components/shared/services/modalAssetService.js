﻿function ModalAssetService($modal) {
    "use strict";

    return {
        showConfirmModal
    };
   

    function showConfirmModal() {

        return $modal.open({
            templateUrl: "/app/components/shared/views/confirmAssetModal.html",
            controller: ["$scope", "$modalInstance", "purchasesService", "assetsService",
                function ($scope, $modalInstance, purchasesService, assetsService) {

                 $scope.AssetId = 0;

                var tenantId;
                $scope.editMode = false;
                $scope.ownAsset = true;

                $scope.asset = {
                    CreatedAt: new Date(),
                    StatusAsset: 1
                };

                assetsService.enumStatus()
                    .then((response) => {
                        $scope.options = response.data;
                    });

                purchasesService.getCategory()
                    .then((response) => {
                        $scope.categories = response.data;
                        $("#categories").select2({ width: "100%" });
                    });

                assetsService.getSubCategory($scope.asset.CategoryId)
                    .then((responsesubCategory) => {
                        $scope.subcategories = responsesubCategory.data;
                        $("#subcategories").select2({ width: "100%" });
                    });

                $scope.getSubCategory = () => {
                    $scope.subcategories = null;
                    assetsService.getSubCategory($scope.asset.CategoryId)
                        .then((response) => {
                            $scope.subcategories = response.data;
                            $("#subcategories").select2({
                                width: "100%"
                            });
                        });
                }


                assetsService.getWork()
                    .then((response) => {
                        $scope.works = response.data;
                        $("#works").select2({ width: "100%" });
                    });

                assetsService.enumStatus()
                    .then((responseStatus) => {
                        $scope.options = responseStatus.data;
                        $("#property").select2({ width: "100%" });
                    });

                assetsService.getTenant()
                    .then(function(response) {
                        tenantId = response.data;
                    });

                $scope.changeProperty = () => {
                    if ($scope.asset.StatusAsset === 1) {
                        $scope.ownAsset = true;
                    } else {
                        $scope.ownAsset = false;

                        $("#customers").select2({
                            width: "100%"
                        });
                    }
                }

                assetsService.getCustomer()
                    .then((response) => {
                        $scope.customers = response.data;
                        $scope.asset.CustomerId = 1;
                    });

                $scope.save = () => {

                    if ($scope.asset.Picture != null) {
                        $scope.asset.Picture = $scope.asset.Picture.split(",")[1];
                    }
                    $scope.asset.TenantId = tenantId;
                    assetsService.add($scope.asset)
                        .then((response) => {
                            if (response.status === 200) {
                                $modalInstance.close(response.data);
                            }
                        });
                }

                    $scope.cancel = function() {
                        $modalInstance.dismiss("cancel");
                    };
                }],
            size: "lg"
        }).result.then(function(result) {
            return result;
        });
    }
}

export default ModalAssetService;