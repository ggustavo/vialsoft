﻿const state = new WeakMap();

class HeaderController {
    constructor($state, $rootScope, $http, $timeout) {
        var vm = this;
        state.set(this, $state);

        $timeout(() => {
            this.title = $state.current.name;
            vm.viewName = $state.current.name;
        }, 10);

        $rootScope.$on("$stateChangeStart",
            (e, toState) => {
                this.title = toState.name;
                vm.viewName = toState.name;
                
                if (toState.name === "customers" || toState.name === "customer")
                    this.title = "Clientes";

                if (toState.name === "works" || toState.name === "work")
                    this.title = "Obras";

                if (toState.name === "assets" || toState.name === "asset")
                    this.title = "Bienes";

                if (toState.name === "accessories" || toState.name === "accessory")
                    this.title = "Accesorios";

                if (toState.name === "tenants" || toState.name === "tenant")
                    this.title = "Propietarios";

                if (toState.name === "purchases" || toState.name === "purchase")
                    this.title = "Compras";

                if (toState.name === "transfersworks" || toState.name === "transferwork")
                    this.title = "Traslados";

                if (toState.name === "providers" || toState.name === "provider")
                    this.title = "Proveedores";

                if (toState.name === "costeassets" || toState.name === "costeasset")
                    this.title = "Costos";

                $rootScope.menuOpen = false;
            });

        $http({
            method: "GET",
            url: "/api/users/current/user"
        }).then((response) => {
            vm.userName = response.data;
        });

    }
}

HeaderController.$inject = ["$state", "$rootScope", "$http", "$timeout"];
export default HeaderController;