﻿var ModuleName = "VialSoft.providers";

import ProvidersController from "./controllers/providersController";
import DetailController from "./controllers/detailController";
import ProvidersService from "./services/providersService";

angular.module(ModuleName, []).
    controller("providersController", ProvidersController).
    controller("providersdetailController", DetailController).
    service("providersService", ProvidersService);

export default ModuleName;