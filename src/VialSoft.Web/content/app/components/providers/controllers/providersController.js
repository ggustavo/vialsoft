﻿class ProvidersController {
    constructor($scope, $rootScope, $state, providersService, toasterService, modalService) {

        const pageSize = 6;
        var pageCount = 0;

        $scope.providers = [];

        $scope.getList = () => {
            $rootScope.loading = true;
            providersService.getList(pageSize, pageCount)
                .then((providers) => {
                    if (providers.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    providers.forEach((provider) => {
                        $scope.providers.push(provider);
                    });
                    pageCount ++;
                    $scope.refreshSelectedItems();

                    if (!$scope.providers.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = (providerId) => {
            providerId ? $state.transitionTo("provider", { id: providerId }) : $state.transitionTo("provider");
        };

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.providers.forEach((provider) => {
                    provider.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.providers.some((provider) => {
                return provider.selected;
            });

            $scope.everySelected = $scope.providers.every((provider) => {
                return provider.selected;
            });
        };

        $scope.remove = (provider) => {

            modalService.showConfirmModal({
                messages: {
                    title: `Eliminar proveedor`,
                    body: `Esta seguro que desea eliminar al proveedor`,
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
            .then(() => {
                var providerIdList;
                if (provider) {
                    providerIdList = [provider.ProviderId];
                } else {
                    providerIdList = $scope.providers
                        .map((providerItem) => {
                            if (providerItem.selected) {                             
                                return providerItem.ProviderId;
                            }
                            return null;
                        });
                }
                
                providerIdList.forEach((providerId) => {
                    providersService.remove(providerId)
                        .then((response) => {
                            if (response.status === 200) {
                                $scope.providers.forEach((providerItem) => {
                                    if (providerId === providerItem.ProviderId) {
                                        let index = $scope.providers.indexOf(providerItem);
                                        $scope.providers.splice(index, 1);
                                    }
                                });
                            }
                        })
                        .catch((error) => {
                            toasterService.showServerError(error);
                        });
                });

                $scope.refreshSelectedItems();

            });
        };

        $scope.getList();
    }

}

export default ProvidersController;