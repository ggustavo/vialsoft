﻿"use strict";

class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, providersService, toasterService, modalService) {

        $("select").select2({width: "100%"});

        var providerId = $stateParams.id;
        $scope.editMode = providerId !== undefined;
        var tenantId;

        $scope.provider = {
            CreatedAt: new Date()
        };

        providersService.getTypeReceipt()
            .then((response) => {
                $scope.typeReceipts = response.data;
            });

        if ($scope.editMode) {
            $rootScope.loading = true;
            providersService.getProvider(providerId)
                .then((response) => {
                    $scope.provider = response.data;

                    providersService.getTypeReceipt()
                        .then((responseType) => {
                            $scope.typeReceipts = responseType.data;

                            $("#typeReceipt").select2({ width: "100%" });
                        });
                    if ($scope.provider.Picture != null) {
                        $scope.provider.Picture = `data:image/png;base64,${$scope.provider.Picture}`;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        } else {
            $rootScope.loading = true;
            providersService.getTenant()
                .then(function(response) {
                    tenantId = response.data;
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        }

        $scope.navigateBack = () => {
            $state.transitionTo("providers");
        };

        $scope.removeProvider = () => {
            modalService.showConfirmModal({
                messages: {
                    title: "Eliminar Proveedor",
                    body: "Esta seguro que desea eliminar el proveedor?",
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
           .then(() => {
               $rootScope.loading = true;
               providersService.remove(providerId)
                   .then((response) => {
                       if (response.status === 200) {
                           $state.transitionTo("providers");
                       }
                   })
                   .catch((error) => {
                       toasterService.showServerError(error);
                   })
                   .finally(() => {
                       $rootScope.loading = false;
                   });
           });
        };

        $scope.save = () => {

            if ($scope.provider.Picture != null) {
                $scope.provider.Picture = $scope.provider.Picture.split(",")[1];
            }

            if (!$scope.editMode) {
                $scope.provider.TenantId = tenantId;
                $rootScope.loading = true;

                providersService.add($scope.provider)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            } else {
                $rootScope.loading = true;
                providersService.update($scope.provider)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            }
        };
    }
}
export default DetailController;