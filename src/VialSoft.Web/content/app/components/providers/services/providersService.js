﻿function ProvidersService($http) {
    "use strict";

    var providers;

    return {
        getTypeReceipt,
        getTenant,
        getProvider,
        getList,
        add,
        update,
        remove
    };

    function getTypeReceipt() {
        return $http({
            method: "GET",
            url: "/api/purchases/TypeReceipts"
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        let handleSuccess = (response) => {
            providers = response.data;
            return providers;
        };

        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/providers";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount:pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getProvider(providerId) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/providers/${providerId}`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(provider) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/providers/";
            return $http({
                method: "POST",
                url: url,
                data: provider,
                headers: {
                    TenantId: tenantId                 
                }
            });
        });
    }

    function update(provider) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/providers/";
            return $http({
                method: "PUT",
                url: url,
                data: provider,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(providerId) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            let url = `/api/providers/${providerId}`;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

export default ProvidersService;