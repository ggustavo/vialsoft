﻿class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, purchasesService, toasterService, modalService, modalAssetService) {

        $("select").select2({ width: "100%" });

        var purchaseId = $stateParams.id;
        $scope.editMode = purchaseId !== undefined;
        $scope.searchMode = false;
        $scope.showDetail = false;
        $scope.priceTotal = 0;
        var tenantId;

        $scope.asset = {};

        $scope.purchaseDetail = {
            AssetId: null,
            Price: null
        };

        $scope.purchase = {
            CreatedAt: new Date(),
            TenantId: 0,
            Details: []
        };

        purchasesService.getCategory()
            .then((response) => {
                $scope.categories = response.data;
            });

        purchasesService.getSubCategoryAll()
            .then((response) => {
                $scope.subcategories = response.data;
            });

        $scope.getSubCategory = () => {
            $scope.subcategories = null;
            purchasesService.getSubCategory($scope.asset.CategoryId)
                .then((response) => {
                    $scope.subcategories = response.data;
                    $("#subcategories").select2({
                        width: "100%"
                    });
                });
        }

        purchasesService.getProvider()
            .then((response) => {
                $scope.providers = response.data;
            });

        $scope.search = () => {
            $rootScope.loading = true;
            purchasesService.getAssetById($scope.asset.Id)
                .then((response) => {
                    $scope.searchMode = true;
                    $scope.asset = response.data;
                }).catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        }

        $scope.addPurchaceDetail = () => {
            var detail = {
                Price: $scope.purchase.Price,
                AssetId: $scope.asset.AssetId
            };
            $scope.priceTotal += $scope.purchase.Price;
            $scope.purchase.Details.push(detail);
            $scope.showDetail = true;
        }

        if ($scope.editMode) {
            $rootScope.loading = true;
            purchasesService.getPurchase(purchaseId)
                .then((response) => {
                    $scope.purchase = response.data;
                    $scope.showDetail = true;

                    $scope.purchase.Details.forEach((detail) => {
                        $scope.priceTotal += detail.Price;
                    });
                    purchasesService.getProvider()
                        .then((responseProvider) => {
                            $scope.providers = responseProvider.data;
                            $("#provider").select2({ width: "100%" });
                        });

                    purchasesService.getTypeReceipt()
                        .then((responseType) => {
                            $scope.typeReceipts = responseType.data;
                            $("#typeReceipt").select2({ width: "100%" });
                        });

                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        }

        $scope.removeAsset = (assetItem) => {
            let index = $scope.purchase.Details.indexOf(assetItem);
            $scope.priceTotal -= assetItem.Price;
            $scope.purchase.Details.splice(index, 1);
        }

        $scope.navigateBack = () => {
            $state.transitionTo("purchases");
        };

        $scope.nagivateToAsset = () => {
            modalAssetService.showConfirmModal()
                .then(function(response) {

                    purchasesService.getAsset(response)
                        .then((responseAsset) => {
                            $scope.searchMode = true;
                            $scope.asset = responseAsset.data;
                        });

                });
        };

        $scope.save = () => {
            $rootScope.loading = true;
            purchasesService.getTenant()
                .then(function(response) {
                    tenantId = response.data;

                    $scope.purchase.TenantId = tenantId;
                    purchasesService.add($scope.purchase)
                        .then((responseAdd) => {
                            if (responseAdd.status === 200) {
                                $scope.navigateBack();
                            }
                        });

                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });

        };

    }
}

export default DetailController;