﻿class PurchasesController {
    constructor($scope, $rootScope, $state, purchasesService, toasterService, modalService) {

        const pageSize = 6;
        var pageCount = 0;

        $scope.purchases = [];

        $scope.getList = () => {

            $rootScope.loading = true;
            
            purchasesService.getList(pageSize, pageCount)
                .then((purchases) => {
                    if (purchases.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    purchases.forEach((purchase) => {
                        $scope.purchases.push(purchase);
                    });
                    pageCount ++;                   
                    $scope.refreshSelectedItems();                    
                    if (!$scope.purchases.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = (purchaseId) => {
            purchaseId ? $state.transitionTo("purchase", { id: purchaseId }) : $state.transitionTo('purchase');
        };

        $scope.nagivateToAsset = () => {
            $state.transitionTo("asset");
        };

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.purchases.forEach((purchase) => {
                    purchase.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.purchases.some((purchase) => {
                return purchase.selected;
            });

            $scope.everySelected = $scope.purchases.every((purchase) => {
                return purchase.selected;
            });
        };

        $scope.getList();
    }
}

export default PurchasesController;