﻿function PurchasesService($http) {
    "use strict";

    var purchases;

    return {
        getCategory,
        getSubCategory,
        getSubCategoryAll,
        getAssetById,
        getAsset,
        getProvider,
        getTypeReceipt,
        getCustomer,
        getTenant,
        getPurchase,
        getList,
        add,
        update
    };

    function getCustomer(){
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/customers/getAll`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getCategory() {
        return $http({
            method: "GET",
            url: "/api/Categories"
        });
    }

    function getSubCategoryAll() {
        return $http({
            method: "GET",
            url: `/api/SubCategories`
        });
    }

    function getSubCategory(categoryId) {
        return $http({
            method: "GET",
            url: `/api/SubCategories/${categoryId}`
        });
    }

    function getTypeReceipt() {
        return $http({
            method: "GET",
            url: "/api/purchases/TypeReceipts"
        });
    }

    function getProvider(){
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/providers/getAll`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
    function getAsset(assetId) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/assets/${assetId}`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getAssetById(id) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/assets/GetAsset`;
            return $http({
                method: "GET",
                url: url,
                params: {
                    Id: id
                },
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        let handleSuccess = (response) => {
            purchases = response.data;
            return purchases;
        };

        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/purchases";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount:pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getPurchase(purchaseId) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/purchases/${purchaseId}`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(purchase) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/purchases/";
            return $http({
                method: "POST",
                url: url,
                data: purchase,
                headers: {
                    TenantId: tenantId                 
                }
            });
        });
    }

    function update(purchase) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/purchases/";
            return $http({
                method: "PUT",
                url: url,
                data: purchase,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
    
}

export default PurchasesService;