﻿var ModuleName = "VialSoft.purchases";

import PurchasesController from "./controllers/purchasesController";
import DetailController from "./controllers/detailController";
import PurchasesService from "./services/purchasesService";

angular.module(ModuleName, []).
    controller("purchasesController", PurchasesController).
    controller("purchasesdetailController", DetailController).
    service("purchasesService", PurchasesService);
export default ModuleName;