﻿var ModuleName = "VialSoft.tenants";

import TenantsController from "./controllers/tenantsController";
import DetailController from "./controllers/detailController";
import TenantsService from "./services/tenantsService";

angular.module(ModuleName, []).
    controller("tenantsController", TenantsController).
    controller("tenantsdetailController", DetailController).
    service("tenantsService", TenantsService);   

export default ModuleName;