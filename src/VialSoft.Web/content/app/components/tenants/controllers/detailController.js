﻿class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, tenantsService, toasterService, modalService) {

        var tenantId = $stateParams.id;
        $scope.editMode = tenantId !== undefined;

        if ($scope.editMode) {
            $rootScope.loading = true;
            tenantsService.getTenant(tenantId)
                .then((response) => {
                    $scope.tenant = response.data;
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        }

        $scope.navigateBack = () => {
            $state.transitionTo('tenants');
        };

        $scope.removetenant = () => {
            modalService.showConfirmModal({
                messages: {
                    title: "Eliminar Propietario",
                    body: "Esta seguro que desea eliminar el propietario?",
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
           .then(() => {
               $rootScope.loading = true;
               tenantsService.remove(tenantId)
                   .then((response) => {
                       if (response.status === 200) {
                           $state.transitionTo('tenants');
                       }
                   })
                   .catch((error) => {
                       toasterService.showServerError(error);
                   })
                   .finally(() => {
                       $rootScope.loading = false;
                   });
           });
        };

        $scope.save = () => {
            if (!$scope.editMode) {
                $scope.tenant.TenantId = tenantId;
                $rootScope.loading = true;

                tenantsService.add($scope.tenant)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            } else {
                $rootScope.loading = true;
                tenantsService.update($scope.tenant)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            }
        };
    }
}

export default DetailController;