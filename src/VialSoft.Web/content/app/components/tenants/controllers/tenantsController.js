﻿class TenantsController {
    constructor($scope, $rootScope, $state, tenantsService, toasterService, modalService) {

        const pageSize = 6;
        var pageCount = 0;

        $scope.tenants = [];

        $scope.getList = () => {
            $rootScope.loading = true;
            tenantsService.getList(pageSize, pageCount)
                .then((tenants) => {
                    if (tenants.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    tenants.forEach((tenant) => {
                        $scope.tenants.push(tenant);
                    });
                    pageCount ++;
                    $scope.refreshSelectedItems();

                    if (!$scope.tenants.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = (tenantId) => {
            tenantId ? $state.transitionTo("tenant", { id: tenantId }) : $state.transitionTo("tenant");
        };

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.tenants.forEach((tenant) => {
                    tenant.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.tenants.some((tenant) => {
                return tenant.selected;
            });

            $scope.everySelected = $scope.tenants.every((tenant) => {
                return tenant.selected;
            });
        };

        $scope.remove = (tenant) => {

            modalService.showConfirmModal({
                messages: {
                    title: `Eliminar Propietario`,
                    body: `Esta seguro que desea eliminar el propietario`,
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
            .then(() => {
                var tenantIdList;
                if (tenant) {
                    tenantIdList = [tenant.TenantId];
                } else {
                    tenantIdList = $scope.tenants
                        .map((tenantItem) => {
                            if (tenantItem.selected) {                             
                                return tenantItem.TenantId;
                            }
                            return null;
                        });
                }
                
                tenantIdList.forEach((tenantId) => {
                    tenantsService.remove(tenantId)
                        .then((response) => {
                            if (response.status === 200) {
                                $scope.tenants.forEach((tenantItem) => {
                                    if (tenantId === tenantItem.TenantId) {
                                        let index = $scope.tenants.indexOf(tenantItem);
                                        $scope.tenants.splice(index, 1);
                                    }
                                });
                            }
                        })
                        .catch((error) => {
                            toasterService.showServerError(error);
                        });
                });

                $scope.refreshSelectedItems();

            });
        };

        $scope.getList();
    }

}

export default TenantsController;