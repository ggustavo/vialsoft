﻿function TenantsService($http) {
    "use strict";

    var tenants;

    return {
        getTenant,
        getList,
        add,
        update,
        remove
    };

    function getList(pageSize, pageCount) {
        let handleSuccess = (response) => {
            tenants = response.data;
            return tenants;
        };
        var url = '/api/tenants';
        return $http({
            method: 'GET',
            url: url,
            params: {
                pageSize: pageSize,
                pageCount:pageCount
            }
        }).then(handleSuccess);
    }

    function getTenant(tenantId) {
        let url = `/api/tenants/${tenantId}`;
        return $http({
            method: "GET",
            url: url
        });
    }

    function add(tenant) {
        var url = '/api/tenants/';
        return $http({
            method: 'POST',
            url: url,
            data: tenant
        });
    }

    function update(tenant) {
        var url = "/api/tenants/";
        return $http({
            method: "PUT",
            url: url,
            data: tenant
        });
    }

    function remove(tenantId) {
        let url = `/api/tenants/${tenantId}`;
        return $http({
            method: "DELETE",
            url: url
        });
    }
}

export default TenantsService;