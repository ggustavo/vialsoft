﻿class CosteAssetController {
    constructor($scope, $rootScope, $state, costeassetService, toasterService) {

        const pageSize = 6;
        var pageCount = 0;

        $scope.costeassets = [];

        $scope.getList = () => {

            $rootScope.loading = true;
            
            costeassetService.getList(pageSize, pageCount)
                .then((costeassets) => {
                    if (costeassets.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    costeassets.forEach((costeAsset) => {
                        $scope.costeassets.push(costeAsset);
                    });
                    pageCount ++;                   
                    $scope.refreshSelectedItems();                    
                    if (!$scope.costeassets.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = (costeId) => {
            costeId ? $state.transitionTo("costeasset", { id: costeId }) : $state.transitionTo("costeasset");
        };

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.costeassets.forEach((costeAsset) => {
                    costeAsset.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.costeassets.some((costeAsset) => {
                return costeAsset.selected;
            });

            $scope.everySelected = $scope.costeassets.every((costeAsset) => {
                return costeAsset.selected;
            });
        };

        $scope.getList();

    }
}

export default CosteAssetController;