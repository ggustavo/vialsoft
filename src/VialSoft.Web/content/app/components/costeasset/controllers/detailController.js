﻿class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, costeassetService, toasterService, modalService) {

        var costeassetId = $stateParams.id;
        $scope.editMode = costeassetId !== undefined;
        $scope.searchMode = false;
        $scope.asset = {};
        $scope.costeasset = {
            CreatedAt: new Date()
        };    

        $scope.search = () => {
            $rootScope.loading = true;
            costeassetService.getAssetById($scope.asset.Id)
                .then((response) => {
                    $scope.searchMode = true;
                    $scope.asset = response.data;
                }).catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        }

        $scope.navigateBack = () => {
            $state.transitionTo("costeassets");
        };

        $scope.save = () => {
            $rootScope.loading = true;
            costeassetService.getTenant()
                .then(function(response) {
                    var tenantId = response.data;

                    $scope.costeasset.TenantId = tenantId;
                    $scope.costeasset.AssetId = $scope.asset.AssetId;
                    costeassetService.add($scope.costeasset)
                        .then((responseAdd) => {
                            if (responseAdd.status === 200) {
                                $scope.navigateBack();
                            }
                        });

                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });

        };

    }
}

export default DetailController;