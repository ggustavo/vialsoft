﻿var ModuleName = "VialSoft.costeasset";

import CosteAssetController from "./controllers/costeassetController";
import DetailController from "./controllers/detailController";
import CosteAssetService from "./services/costeassetService";

angular.module(ModuleName, []).
    controller("costeassetController", CosteAssetController).
    controller("costeassetdetailController", DetailController).
    service("costeassetService", CosteAssetService);

export default ModuleName;