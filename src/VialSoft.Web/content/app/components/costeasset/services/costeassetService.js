﻿function CosteAssetService($http) {
    "use strict";

    var costeAsset;

    return {
        getAsset,
        getTenant,
        getList,
        getAssetById,
        add,
        update
    };

    function getAsset(id) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/assets/GetAsset`;
            return $http({
                method: "GET",
                url: url,
                params: {
                    Id: id
                },
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        let handleSuccess = (response) => {
            costeAsset = response.data;
            return costeAsset;
        };

        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/costeAsset";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount:pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getAssetById(id) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/assets/GetAsset`;
            return $http({
                method: "GET",
                url: url,
                params: {
                    Id: id
                },
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(costeasset) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/costeAsset/";
            return $http({
                method: "POST",
                url: url,
                data: costeasset,
                headers: {
                    TenantId: tenantId                 
                }
            });
        });
    }

    function update(costeasset) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/costeAsset/";
            return $http({
                method: "PUT",
                url: url,
                data: costeasset,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
    
}

export default CosteAssetService;