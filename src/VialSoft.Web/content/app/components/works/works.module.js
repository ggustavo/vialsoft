﻿var ModuleName = "VialSoft.works";

import WorksController from "./controllers/worksController";
import DetailController from "./controllers/detailController";
import WorksService from "./services/worksService";

angular.module(ModuleName, []).
    controller("worksController", WorksController).
    controller("worksdetailController", DetailController).
    service("worksService", WorksService);   

export default ModuleName;