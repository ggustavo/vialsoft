﻿class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, worksService, toasterService, modalService) {
        
        $("select").select2({ width: "100%" });

        var workId = $stateParams.id;
        $scope.editMode = workId !== undefined;
        var tenantId;

        $scope.work = {
            CreatedAt: new Date()
        };

        worksService.getCustomer()
            .then((response) => {
                $scope.customers = response.data;
                $scope.work.CustomerId = 1;
            });

        if ($scope.editMode) {
            $rootScope.loading = true;

            worksService.getWork(workId)
                .then((response) => {
                    $scope.work = response.data;
                    worksService.getCustomer()
                        .then((responseCustomer) => {
                            $scope.customers = responseCustomer.data;
                            $("select").select2({ width: "100%" });
                        });
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });

            
        } else {
            $rootScope.loading = true;
            worksService.getCustomer()
                .then((responseCustomer) => {
                    $scope.customers = responseCustomer.data;
                    $("select").select2({ width: "100%" });
                });
            worksService.getTenant()
                .then(function(response) {
                    tenantId = response.data;
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        
        }

        $scope.navigateBack = () => {
            $state.transitionTo("works");
        };

        $scope.removework = () => {
            modalService.showConfirmModal({
                messages: {
                    title: "Eliminar Obra",
                    body: "Esta seguro que desea eliminar la obra?",
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
           .then(() => {
               $rootScope.loading = true;
               worksService.remove(workId)
                   .then((response) => {
                       if (response.status === 200) {
                           $state.transitionTo("works");
                       }
                   })
                   .catch((error) => {
                       toasterService.showServerError(error);
                   })
                   .finally(() => {
                       $rootScope.loading = false;
                   });
           });
        };

        $scope.save = () => {

            if (!$scope.editMode) {
                $scope.work.TenantId = tenantId;
                $rootScope.loading = true;

                worksService.add($scope.work)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            } else {
                $rootScope.loading = true;
                worksService.update($scope.work)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            }
        };
    }
}

export default DetailController;