﻿class WorksController {
    constructor($scope, $rootScope, $state, worksService, toasterService, modalService) {

        const pageSize = 6;
        var pageCount = 0;

        $scope.works = [];

        $scope.getList = () => {
            $rootScope.loading = true;
            worksService.getList(pageSize, pageCount)
                .then((works) => {
                    if (works.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    works.forEach((work) => {
                        $scope.works.push(work);
                    });
                    pageCount ++;                   
                    $scope.refreshSelectedItems();                    
                    if (!$scope.works.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = (workId) => {
            workId ? $state.transitionTo("work", { id: workId }) : $state.transitionTo("work");
        };

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.works.forEach((work) => {
                    work.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.works.some((work) => {
                return work.selected;
            });

            $scope.everySelected = $scope.works.every((work) => {
                return work.selected;
            });
        };

        $scope.remove = (work) => {

            modalService.showConfirmModal({
                messages: {
                    title: `Eliminar la obra`,
                    body: `Esta seguro que desea eliminar la obra`,
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
            .then(() => {
                var workIdList;
                if (work) {
                    workIdList = [work.WorkId];
                } else {
                    workIdList = $scope.works
                        .map((workItem) => {
                            if (workItem.selected) {                             
                                return workItem.WorkId;
                            }
                            return null;
                        });
                }
                
                workIdList.forEach((workId) => {
                    worksService.remove(workId)
                        .then((response) => {
                            if (response.status === 200) {
                                $scope.works.forEach((workItem) => {
                                    if (workId === workItem.WorkId) {
                                        let index = $scope.works.indexOf(workItem);
                                        $scope.works.splice(index, 1);
                                    }
                                });
                            }
                        })
                        .catch((error) => {
                            toasterService.showServerError(error);
                        });
                });

                $scope.refreshSelectedItems();

            });
        };

        $scope.getList();
    }

}

export default WorksController;