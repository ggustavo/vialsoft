﻿function WorksService($http) {
    "use strict";

    var works;

    return {
        getCustomer,
        getTenant,
        getWork,
        getList,
        add,
        update,
        remove
    };

    function getCustomer(){
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/customers/getAll`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        let handleSuccess = (response) => {
            works = response.data;
            return works;
        };

        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/works";
            return $http({
                method: 'GET',
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount:pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getWork(worksId) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/works/${worksId}`;
            return $http({
                method: 'GET',
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(work) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/works/";
            return $http({
                method: "POST",
                url: url,
                data: work,
                headers: {
                    TenantId: tenantId   
                }
            });
        });
    }

    function update(work) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/works/";
            return $http({
                method: "PUT",
                url: url,
                data: work,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(worksId) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            let url = `/api/works/${worksId}`;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

export default WorksService;