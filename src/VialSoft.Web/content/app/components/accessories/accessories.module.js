﻿var ModuleName = "VialSoft.accessories";

import AccessoriesController from "./controllers/accessoriesController";
import DetailController from "./controllers/detailController";
import AccessoriesService from "./services/accessoriesService";

angular.module(ModuleName, []).
    controller("accessoriesController", AccessoriesController).
    controller("accessoriesdetailController", DetailController).
    service("accessoriesService", AccessoriesService);

export default ModuleName;