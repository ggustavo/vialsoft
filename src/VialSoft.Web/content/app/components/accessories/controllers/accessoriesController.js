﻿class AccessoriesController {
    constructor($scope, $rootScope, $stateParams, $state, accessoriesService, toasterService, modalService) {

        var assetId = $stateParams.id;

        const pageSize = 6;
        var pageCount = 0;

        $scope.AssetId = assetId;
        $scope.accessories = [];

        $scope.getList = () => {
            $rootScope.loading = true;
            accessoriesService.getList(pageSize, pageCount, assetId)
                .then((accessories) => {
                    if (accessories.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    accessories.forEach((accessory) => {
                        $scope.accessories.push(accessory);
                    });
                    pageCount ++;
                    $scope.refreshSelectedItems();

                    if (!$scope.accessories.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = () => {
            $state.transitionTo("accessory", { id: $scope.AssetId });
        };

        $scope.navigateBackAsset = () => {
            $state.transitionTo("asset", {id : $scope.AssetId});
        };

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.accessories.forEach((accessory) => {
                    accessory.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.accessories.some((accessory) => {
                return accessory.selected;
            });

            $scope.everySelected = $scope.accessories.every((accessory) => {
                return accessory.selected;
            });
        };

        $scope.remove = (accessory) => {

            modalService.showConfirmModal({
                messages: {
                    title: `Eliminar accesorio`,
                    body: `Esta seguro que desea eliminar el accesorio`,
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
            .then(() => {
                var accessoryIdList;
                if (accessory) {
                    accessoryIdList = [accessory.AccessoryId];
                } else {
                    accessoryIdList = $scope.accessories
                        .map((accessoryItem) => {
                            if (accessoryItem.selected) {                             
                                return accessoryItem.AccessoryId;
                            }
                            return null;
                        });
                }
                
                accessoryIdList.forEach((accessoryId) => {
                    accessoriesService.remove(accessoryId)
                        .then((response) => {
                            if (response.status === 200) {
                                $scope.accessories.forEach((accessoryItem) => {
                                    if (accessoryId === accessoryItem.AccessoryId) {
                                        let index = $scope.accessories.indexOf(accessoryItem);
                                        $scope.accessories.splice(index, 1);
                                    }
                                });
                            }
                        })
                        .catch((error) => {
                            toasterService.showServerError(error);
                        });
                });

                $scope.refreshSelectedItems();

            });
        };

        $scope.getList();
    }

}

export default AccessoriesController;