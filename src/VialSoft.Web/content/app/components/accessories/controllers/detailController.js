﻿class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, accessoriesService, toasterService, modalService) {

        var accessoryId = $stateParams.id;
        //$scope.editMode = accessoryId !== undefined;
        var tenantId;
        $scope.editMode = false;

        $scope.accessory = {
            AssetId: $stateParams.id
        };

        if ($scope.editMode) {
            $rootScope.loading = true;
            accessoriesService.getAccessory(accessoryId)
                .then((response) => {
                    $scope.accessory = response.data;
                    $scope.accessory.Picture = `data:image/png;base64,${$scope.accessory.Picture}`;
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        } else {
            $rootScope.loading = true;
            accessoriesService.getTenant()
                .then(function(response) {
                    tenantId = response.data;
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        }

        $scope.navigateBack = () => {
            $state.transitionTo("accessories", {id : $scope.accessory.AssetId});
        };

        $scope.removeaccessory = () => {
            modalService.showConfirmModal({
                messages: {
                    title: "Eliminar Accesorio",
                    body: "Esta seguro que desea eliminar el accesorio?",
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
           .then(() => {
               $rootScope.loading = true;
               accessoriesService.remove(accessoryId)
                   .then((response) => {
                       if (response.status === 200) {
                           $state.transitionTo("accessories");
                       }
                   })
                   .catch((error) => {
                       toasterService.showServerError(error);
                   })
                   .finally(() => {
                       $rootScope.loading = false;
                   });
           });
        };

        $scope.save = () => {
            if ($scope.accessory.Picture) {
                $scope.accessory.Picture = $scope.accessory.Picture.split(',')[1];
            }

            if (!$scope.editMode) {
                $scope.accessory.TenantId = tenantId;
                $rootScope.loading = true;

                accessoriesService.add($scope.accessory)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            } else {
                $rootScope.loading = true;
                accessoriesService.update($scope.accessory)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            }
        };
    }
}

export default DetailController;