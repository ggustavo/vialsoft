﻿function AccessoriesService($http) {
    "use strict";

    var accessories;

    return {
        getTenant,
        getAccessory,
        getList,
        add,
        update,
        remove
    };

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount, assetId) {
        let handleSuccess = (response) => {
            accessories = response.data;
            return accessories;
        };

        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = `/api/accessories/${assetId}`;
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount:pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getAccessory(accessoryId) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/accessories/${accessoryId}`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(accessory) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/accessories/";
            return $http({
                method: "POST",
                url: url,
                data: accessory,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function update(accessory) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/accessories/";
            return $http({
                method: "PUT",
                url: url,
                data: accessory,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(accessoryId) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            let url = `/api/accessories/${accessoryId}`;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

export default AccessoriesService;