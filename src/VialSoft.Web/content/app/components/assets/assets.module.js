﻿var ModuleName = "VialSoft.assets";

import AssetsController from "./controllers/assetsController";
import DetailController from "./controllers/detailController";
import AssetsService from "./services/assetsService";

angular.module(ModuleName, []).
    controller("assetsController", AssetsController).
    controller("assetsdetailController", DetailController).
    service("assetsService", AssetsService);

export default ModuleName;