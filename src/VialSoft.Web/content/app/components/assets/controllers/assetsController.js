﻿class AssetsController {
    constructor($scope, $rootScope, $state, assetsService, toasterService, modalService) {

        const pageSize = 6;
        var pageCount = 0;

        $scope.assets = [];

        $scope.getList = () => {

            $rootScope.loading = true;
            
            assetsService.getList(pageSize, pageCount)
                .then((assets) => {
                    if (assets.length < pageSize) {
                        $scope.noMoreData = true;
                    }
                    assets.forEach((asset) => {
                        $scope.assets.push(asset);
                    });
                    pageCount ++;                   
                    $scope.refreshSelectedItems();                    
                    if (!$scope.assets.length) {
                        $scope.noData = true;
                    }
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        };

        $scope.nagivateToDetail = (assetId) => {
            assetId ? $state.transitionTo("asset", { id: assetId }) : $state.transitionTo("asset");
        };

        $scope.nagivateToPurchase = () => {
            $state.transitionTo("purchase");
        }

        $scope.refreshSelectedItems = (all) => {
            if (all) {
                $scope.assets.forEach((asset) => {
                    asset.selected = $scope.everySelected;
                });
            }

            $scope.anySelected = $scope.assets.some((asset) => {
                return asset.selected;
            });

            $scope.everySelected = $scope.assets.every((asset) => {
                return asset.selected;
            });
        };

        $scope.remove = (asset) => {

            modalService.showConfirmModal({
                messages: {
                    title: `Eliminar el bien`,
                    body: `Esta seguro que desea eliminar el bien`,
                    ok: "Si, eliminar",
                    cancel: "Cancelar"
                }
            })
            .then(() => {
                var assetIdList;
                if (asset) {
                    assetIdList = [asset.AssetId];
                } else {
                    assetIdList = $scope.assets
                        .map((assetItem) => {
                            if (assetItem.selected) {                             
                                return assetItem.AssetId;
                            }
                            return null;
                        });
                }
                
                assetIdList.forEach((assetId) => {
                    assetsService.remove(assetId)
                        .then((response) => {
                            if (response.status === 200) {
                                $scope.assets.forEach((assetItem) => {
                                    if (assetId === assetItem.AssetId) {
                                        let index = $scope.assets.indexOf(assetItem);
                                        $scope.assets.splice(index, 1);
                                    }
                                });
                            }
                        })
                        .catch((error) => {
                            toasterService.showServerError(error);
                        });
                });

                $scope.refreshSelectedItems();

            });
        };

        $scope.getList();
    }
}

export default AssetsController;