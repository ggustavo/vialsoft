﻿class DetailController {
    constructor($scope, $rootScope, $stateParams, $state, assetsService, toasterService, modalService) {

        $("select").select2({ width: "100%" });

        var assetId = $stateParams.id;
        $scope.editMode = assetId !== undefined;
        $scope.ownAsset = true;

        var tenantId;

        $scope.asset = {
            CreatedAt: new Date(),
            StatusAsset: 1
        };

        assetsService.getCustomer()
            .then((response) => {
                $scope.customers = response.data;
                $scope.asset.CustomerId = 1;
            });

        assetsService.getCategory()
            .then((response) => {
                $scope.categories = response.data;
            });

        assetsService.getSubCategoryAll()
            .then((response) => {
                $scope.subcategories = response.data;
            });

        $scope.changeProperty = () => {
            if ($scope.asset.StatusAsset === 1) {
                $scope.ownAsset = true;
            } else {
                $scope.ownAsset = false;

                $("#customers").select2({
                    width: "100%"
                });
            }
        }

        $scope.getSubCategory = () => {
            $scope.subcategories = null;
            assetsService.getSubCategory($scope.asset.CategoryId)
                .then((response) => {
                    $scope.subcategories = response.data;
                    $("#subcategories").select2({
                        width: "100%"
                    });
                });
        }

        assetsService.enumStatus()
            .then((response) => {
                $scope.options = response.data;
            });

        assetsService.getWork()
            .then((response) => {
                $scope.works = response.data;
            });

        if ($scope.editMode) {
            $rootScope.loading = true;
            assetsService.getAsset(assetId)
                .then((response) => {
                    $scope.asset = response.data;
                    $scope.asset.Picture = `data:image/png;base64,${$scope.asset.Picture}`;
                    assetsService.getCategory()
                        .then((responseCategory) => {
                            $scope.categories = responseCategory.data;
                            $("#categories").select2({ width: "100%" });
                            $("#works").select2({ width: "100%" });
                            $("#property").select2({ width: "100%" });
                            assetsService.getSubCategory($scope.asset.CategoryId)
                                .then((responsesubCategory) => {
                                    $scope.subcategories = responsesubCategory.data;
                                    $("#subcategories").select2({ width: "100%" });
                                });
                        });

                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });
        } else {

            assetsService.getCategory()
                        .then((responseCategory) => {
                            $scope.categories = responseCategory.data;
                            $("#categories").select2({ width: "100%" });
                            $("#works").select2({ width: "100%" });
                            $("#property").select2({ width: "100%" });
                            assetsService.getSubCategory($scope.asset.CategoryId)
                                .then((responsesubCategory) => {
                                    $scope.subcategories = responsesubCategory.data;
                                    $("#subcategories").select2({ width: "100%" });
                                });
                        });

            assetsService.enumStatus()
                .then((responseStatus) => {
                    $scope.options = responseStatus.data;
                    $("#property").select2({ width: "100%" });
                });

            $rootScope.loading = true;
            assetsService.getTenant()
                .then(function(response) {
                    tenantId = response.data;
                })
                .catch((error) => {
                    toasterService.showServerError(error);
                })
                .finally(() => {
                    $rootScope.loading = false;
                });

        }

        $scope.navigateBack = () => {
            $state.transitionTo("assets");
        };

        $scope.nagivateToMaintenanceCard = (id) => {
            $state.transitionTo("maintenancecards", { id: id });
        };
        

        $scope.nagivateToAccessories = (id) => {
            $state.transitionTo("accessories", { id: id });
        };

        $scope.removeAsset = () => {
            modalService.showConfirmModal({
                    messages: {
                        title: "Eliminar el Bien",
                        body: "Esta seguro que desea eliminar el Bien?",
                        ok: "Si, eliminar",
                        cancel: "Cancelar"
                    }
                })
                .then(() => {
                    $rootScope.loading = true;
                    assetsService.remove(assetId)
                        .then((response) => {
                            if (response.status === 200) {
                                $state.transitionTo("assets");
                            }
                        })
                        .catch((error) => {
                            toasterService.showServerError(error);
                        })
                        .finally(() => {
                            $rootScope.loading = false;
                        });
                });
        };

        $scope.save = () => {
            if ($scope.asset.Picture) {
                $scope.asset.Picture = $scope.asset.Picture.split(",")[1];
            }

            if (!$scope.editMode) {
                $scope.asset.TenantId = tenantId;
                $rootScope.loading = true;

                assetsService.add($scope.asset)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            } else {
                $rootScope.loading = true;
                assetsService.update($scope.asset)
                    .then((response) => {
                        if (response.status === 200) {
                            $scope.navigateBack();
                        }
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    })
                    .finally(() => {
                        $rootScope.loading = false;
                    });
            }
        };
    }
}

export default DetailController;