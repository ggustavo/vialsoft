﻿function AssetsService($http) {
    "use strict";

    var assets;

    return {
        enumStatus,
        getCategory,
        getSubCategory,
        getSubCategoryAll,
        getCustomer,
        getTenant,
        getWork,
        getAsset,
        getList,
        add,
        update,
        remove
    };

    function getCustomer(){
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/customers/getAll`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getWork(){
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/works/getAll`;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function enumStatus() {
        return $http({
            method: "GET",
            url: "/api/assets/EnumStatus"
        });
    }

    function getCategory() {
        return $http({
            method: "GET",
            url: "/api/Categories"
        });
    }

    function getSubCategoryAll() {
        return $http({
            method: "GET",
            url: `/api/SubCategories`
        });
    }

    function getSubCategory(categoryId) {
        return $http({
            method: "GET",
            url: `/api/SubCategories/${categoryId}`
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        let handleSuccess = (response) => {
            assets = response.data;
            return assets;
        };

        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/assets";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount:pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getAsset(assetId) {
        return getTenant().then((response) => {
            var tenantId = response.data;
            let url = `/api/assets/${assetId}`;
            return $http({
                method: 'GET',
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(asset) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/assets/";
            return $http({
                method: "POST",
                url: url,
                data: asset,
                headers: {
                    TenantId: tenantId                 
                }
            });
        });
    }

    function update(asset) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            var url = "/api/assets/";
            return $http({
                method: "PUT",
                url: url,
                data: asset,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(assetId) {
        return getTenant().then(function(response) {
            var tenantId = response.data;
            let url = `/api/assets/${assetId}`;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
    
}

export default AssetsService;