﻿class DashboardController {
    constructor($scope, $rootScope, dashboardService, toasterService) {

        const year = new Date().getFullYear();
        $scope.incomesExpensesYear = year;
        $scope.currentYear = year;

        $rootScope.loading = true;
        dashboardService.getSummary()
            .then((summary) => {
                $scope.summary = summary;
            })
            .catch((error) => {
                toasterService.showServerError(error);
            })
            .finally(() => {
                $rootScope.loading = false;
            });

        $scope.addYearIncomesExpenses = () => {
            if ($scope.currentYear > $scope.incomesExpensesYear) {
                $scope.incomesExpensesYear += 1;
            }
        };

        $scope.reduceYearIncomesExpenses = () => {
            $scope.incomesExpensesYear -= 1;
        };

        $scope.correctYear = () => {
            if ($scope.currentYear < $scope.incomesExpensesYear) {
                $scope.incomesExpensesYear = $scope.currentYear;
            }
        };

        var createChartDataIncomesExpenses = (expenses, incomes) => {
            $scope.chartDataIncomesExpenses = {
                scaleLabel: function (valuePayload) {
                    return Number(valuePayload.value).toFixed.replace('.', ',') + '$';
                },
                labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Deciembre'],
                datasets: [
                    {
                        label: 'INGRESOS',
                        fillColor:  'rgba(0,216,204,0.2)',
                        strokeColor: 'rgba(0,216,204,1)',
                        pointColor: 'rgba(0,216,204,1)',
                        pointStrokeColor: 'rgba(0,216,204,1)',
                        pointHighlightFill: 'rgba(0,216,204,1)',
                        pointHighlightStroke:  '#fff',
                        data: incomes
                    },
                    {
                        label: 'GASTOS',
                        fillColor:  'rgba(255,23,112,0.2)',
                        strokeColor: 'rgba(255,23,112,1)',
                        pointColor: 'rgba(255,23,112,1)',
                        pointStrokeColor: 'rgba(255,23,112,1)',
                        pointHighlightFill: 'rgba(255,23,112,1)',
                        pointHighlightStroke:  '#fff',
                        data: expenses
                    }
                ]
            };
        };

        $scope.$watch('incomesExpensesYear', (newValue, oldValue) => {
            if (newValue || oldValue) {
                var expenses = new Array(12);
                expenses.fill(0, 0, 13);
                var incomes = new Array(12);
                incomes.fill(0, 0, 13);

                dashboardService.getExpenses($scope.incomesExpensesYear)
                    .then((allExpenses) => {
                        allExpenses.forEach((elem, index) => {
                            expenses[index] = elem.Expenses;
                            incomes[index] = elem.Incomes;
                        });

                        createChartDataIncomesExpenses(expenses, incomes);
                    })
                    .catch((error) => {
                        toasterService.showServerError(error);
                    });
            }
        });
    }
}

export default DashboardController;