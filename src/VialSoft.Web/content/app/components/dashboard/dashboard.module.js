﻿var ModuleName = "VialSoft.dashboard";

import DashboardController from "./controllers/dashboardController";
import DashboardService from "./services/dashboardService";
import MhChart from "./directives/MHChartDirective";

angular.module(ModuleName, []).
    directive("chart", MhChart).
    controller("dashboardController", DashboardController).
    service("dashboardService", DashboardService);

export default ModuleName;