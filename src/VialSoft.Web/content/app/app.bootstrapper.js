﻿import { default as VialSoftModule} from "./app.module";

angular.bootstrap(document.getElementById("app"), [VialSoftModule]);