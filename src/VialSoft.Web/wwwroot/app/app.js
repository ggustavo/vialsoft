(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _appModule = require("./app.module");

var _appModule2 = _interopRequireDefault(_appModule);

angular.bootstrap(document.getElementById("app"), [_appModule2["default"]]);

},{"./app.module":2}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _componentsSharedSharedModule = require("./components/shared/shared.module");

var _componentsSharedSharedModule2 = _interopRequireDefault(_componentsSharedSharedModule);

var _componentsDashboardDashboardModule = require("./components/dashboard/dashboard.module");

var _componentsDashboardDashboardModule2 = _interopRequireDefault(_componentsDashboardDashboardModule);

var _componentsCustomersCustomersModule = require("./components/customers/customers.module");

var _componentsCustomersCustomersModule2 = _interopRequireDefault(_componentsCustomersCustomersModule);

var _componentsWorksWorksModule = require("./components/works/works.module");

var _componentsWorksWorksModule2 = _interopRequireDefault(_componentsWorksWorksModule);

var _componentsAssetsAssetsModule = require("./components/assets/assets.module");

var _componentsAssetsAssetsModule2 = _interopRequireDefault(_componentsAssetsAssetsModule);

var _componentsAccessoriesAccessoriesModule = require("./components/accessories/accessories.module");

var _componentsAccessoriesAccessoriesModule2 = _interopRequireDefault(_componentsAccessoriesAccessoriesModule);

var _componentsTenantsTenantsModule = require("./components/tenants/tenants.module");

var _componentsTenantsTenantsModule2 = _interopRequireDefault(_componentsTenantsTenantsModule);

var _componentsPurchasesPurchasesModule = require("./components/purchases/purchases.module");

var _componentsPurchasesPurchasesModule2 = _interopRequireDefault(_componentsPurchasesPurchasesModule);

var _componentsTransfersworksTransfersworksModule = require("./components/transfersworks/transfersworks.module");

var _componentsTransfersworksTransfersworksModule2 = _interopRequireDefault(_componentsTransfersworksTransfersworksModule);

var _componentsProvidersProvidersModule = require("./components/providers/providers.module");

var _componentsProvidersProvidersModule2 = _interopRequireDefault(_componentsProvidersProvidersModule);

var _componentsMaintenancecardsMaintenancecardsModule = require("./components/maintenancecards/maintenancecards.module");

var _componentsMaintenancecardsMaintenancecardsModule2 = _interopRequireDefault(_componentsMaintenancecardsMaintenancecardsModule);

var _componentsCosteassetCosteassetModule = require("./components/costeasset/costeasset.module");

var _componentsCosteassetCosteassetModule2 = _interopRequireDefault(_componentsCosteassetCosteassetModule);

var ModuleName = "VialSoft";

var app = angular.module(ModuleName, ["ui.router", "ngAnimate", _componentsSharedSharedModule2["default"], _componentsDashboardDashboardModule2["default"], _componentsCustomersCustomersModule2["default"], _componentsWorksWorksModule2["default"], _componentsAssetsAssetsModule2["default"], _componentsAccessoriesAccessoriesModule2["default"], _componentsTenantsTenantsModule2["default"], _componentsPurchasesPurchasesModule2["default"], _componentsTransfersworksTransfersworksModule2["default"], _componentsProvidersProvidersModule2["default"], _componentsMaintenancecardsMaintenancecardsModule2["default"], _componentsCosteassetCosteassetModule2["default"]]);

app.config(Config);

function Config($stateProvider, $urlRouterProvider, $compileProvider) {

    var defaultUrl = "/";

    $compileProvider.debugInfoEnabled(false);

    $urlRouterProvider.when("", defaultUrl);

    $urlRouterProvider.otherwise('/404');

    $stateProvider.state("dashboard", {
        url: "/",
        templateUrl: "/app/components/dashboard/views/main.html",
        controller: "dashboardController"
    }).state("customers", {
        url: "/customers",
        templateUrl: "/app/components/customers/views/main.html",
        controller: "customersController"
    }).state("customer", {
        url: "/customer?id",
        templateUrl: "/app/components/customers/views/detail.html",
        controller: "customersdetailController"
    }).state("works", {
        url: "/works",
        templateUrl: "/app/components/works/views/main.html",
        controller: "worksController"
    }).state("work", {
        url: "/work?id",
        templateUrl: "/app/components/works/views/detail.html",
        controller: "worksdetailController"
    }).state("assets", {
        url: "/assets",
        templateUrl: "/app/components/assets/views/main.html",
        controller: "assetsController"
    }).state("asset", {
        url: "/asset?id",
        templateUrl: "/app/components/assets/views/detail.html",
        controller: "assetsdetailController"
    }).state("accessories", {
        url: "/accessories?id",
        templateUrl: "/app/components/accessories/views/main.html",
        controller: "accessoriesController"
    }).state("accessory", {
        url: "/accessory?id",
        templateUrl: "/app/components/accessories/views/detail.html",
        controller: "accessoriesdetailController"
    }).state("tenants", {
        url: "/tenants",
        templateUrl: "/app/components/tenants/views/main.html",
        controller: "tenantsController"
    }).state("tenant", {
        url: "/tenant?id",
        templateUrl: "/app/components/tenants/views/detail.html",
        controller: "tenantsdetailController"
    }).state("purchases", {
        url: "/purchases",
        templateUrl: "/app/components/purchases/views/main.html",
        controller: "purchasesController"
    }).state("purchase", {
        url: "/purchase?id",
        templateUrl: "/app/components/purchases/views/detail.html",
        controller: "purchasesdetailController"
    }).state("transfersworks", {
        url: "/transfersworks",
        templateUrl: "/app/components/transfersworks/views/main.html",
        controller: "transfersworksController"
    }).state("transferwork", {
        url: "/transferwork?id",
        templateUrl: "/app/components/transfersworks/views/detail.html",
        controller: "transfersworksdetailController"
    }).state("providers", {
        url: "/providers",
        templateUrl: "/app/components/providers/views/main.html",
        controller: "providersController"
    }).state("provider", {
        url: "/provider?id",
        templateUrl: "/app/components/providers/views/detail.html",
        controller: "providersdetailController"
    }).state("maintenancecards", {
        url: "/maintenancecards?id",
        templateUrl: "/app/components/maintenancecards/views/main.html",
        controller: "maintenancecardsController"
    }).state("maintenancecard", {
        url: "/maintenancecard?id",
        templateUrl: "/app/components/maintenancecards/views/detail.html",
        controller: "maintenancecardsdetailController"
    }).state("costeassets", {
        url: "/costeassets",
        templateUrl: "/app/components/costeasset/views/main.html",
        controller: "costeassetController"
    }).state("costeasset", {
        url: "/costeasset?id",
        templateUrl: "/app/components/costeasset/views/detail.html",
        controller: "costeassetdetailController"
    }).state("error", {
        url: "/404",
        templateUrl: "/app/components/shared/views/error.html"
    });
}

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./components/accessories/accessories.module":3,"./components/assets/assets.module":7,"./components/costeasset/costeasset.module":13,"./components/customers/customers.module":17,"./components/dashboard/dashboard.module":21,"./components/maintenancecards/maintenancecards.module":26,"./components/providers/providers.module":30,"./components/purchases/purchases.module":34,"./components/shared/shared.module":44,"./components/tenants/tenants.module":48,"./components/transfersworks/transfersworks.module":52,"./components/works/works.module":56}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersAccessoriesController = require("./controllers/accessoriesController");

var _controllersAccessoriesController2 = _interopRequireDefault(_controllersAccessoriesController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesAccessoriesService = require("./services/accessoriesService");

var _servicesAccessoriesService2 = _interopRequireDefault(_servicesAccessoriesService);

var ModuleName = "VialSoft.accessories";

angular.module(ModuleName, []).controller("accessoriesController", _controllersAccessoriesController2["default"]).controller("accessoriesdetailController", _controllersDetailController2["default"]).service("accessoriesService", _servicesAccessoriesService2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/accessoriesController":4,"./controllers/detailController":5,"./services/accessoriesService":6}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AccessoriesController = function AccessoriesController($scope, $rootScope, $stateParams, $state, accessoriesService, toasterService, modalService) {
    _classCallCheck(this, AccessoriesController);

    var assetId = $stateParams.id;

    var pageSize = 6;
    var pageCount = 0;

    $scope.AssetId = assetId;
    $scope.accessories = [];

    $scope.getList = function () {
        $rootScope.loading = true;
        accessoriesService.getList(pageSize, pageCount, assetId).then(function (accessories) {
            if (accessories.length < pageSize) {
                $scope.noMoreData = true;
            }
            accessories.forEach(function (accessory) {
                $scope.accessories.push(accessory);
            });
            pageCount++;
            $scope.refreshSelectedItems();

            if (!$scope.accessories.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function () {
        $state.transitionTo("accessory", { id: $scope.AssetId });
    };

    $scope.navigateBackAsset = function () {
        $state.transitionTo("asset", { id: $scope.AssetId });
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.accessories.forEach(function (accessory) {
                accessory.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.accessories.some(function (accessory) {
            return accessory.selected;
        });

        $scope.everySelected = $scope.accessories.every(function (accessory) {
            return accessory.selected;
        });
    };

    $scope.remove = function (accessory) {

        modalService.showConfirmModal({
            messages: {
                title: "Eliminar accesorio",
                body: "Esta seguro que desea eliminar el accesorio",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            var accessoryIdList;
            if (accessory) {
                accessoryIdList = [accessory.AccessoryId];
            } else {
                accessoryIdList = $scope.accessories.map(function (accessoryItem) {
                    if (accessoryItem.selected) {
                        return accessoryItem.AccessoryId;
                    }
                    return null;
                });
            }

            accessoryIdList.forEach(function (accessoryId) {
                accessoriesService.remove(accessoryId).then(function (response) {
                    if (response.status === 200) {
                        $scope.accessories.forEach(function (accessoryItem) {
                            if (accessoryId === accessoryItem.AccessoryId) {
                                var index = $scope.accessories.indexOf(accessoryItem);
                                $scope.accessories.splice(index, 1);
                            }
                        });
                    }
                })["catch"](function (error) {
                    toasterService.showServerError(error);
                });
            });

            $scope.refreshSelectedItems();
        });
    };

    $scope.getList();
};

exports["default"] = AccessoriesController;
module.exports = exports["default"];

},{}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, accessoriesService, toasterService, modalService) {
    _classCallCheck(this, DetailController);

    var accessoryId = $stateParams.id;
    //$scope.editMode = accessoryId !== undefined;
    var tenantId;
    $scope.editMode = false;

    $scope.accessory = {
        AssetId: $stateParams.id
    };

    if ($scope.editMode) {
        $rootScope.loading = true;
        accessoriesService.getAccessory(accessoryId).then(function (response) {
            $scope.accessory = response.data;
            $scope.accessory.Picture = "data:image/png;base64," + $scope.accessory.Picture;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    } else {
        $rootScope.loading = true;
        accessoriesService.getTenant().then(function (response) {
            tenantId = response.data;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    }

    $scope.navigateBack = function () {
        $state.transitionTo("accessories", { id: $scope.accessory.AssetId });
    };

    $scope.removeaccessory = function () {
        modalService.showConfirmModal({
            messages: {
                title: "Eliminar Accesorio",
                body: "Esta seguro que desea eliminar el accesorio?",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            $rootScope.loading = true;
            accessoriesService.remove(accessoryId).then(function (response) {
                if (response.status === 200) {
                    $state.transitionTo("accessories");
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        });
    };

    $scope.save = function () {
        if ($scope.accessory.Picture) {
            $scope.accessory.Picture = $scope.accessory.Picture.split(',')[1];
        }

        if (!$scope.editMode) {
            $scope.accessory.TenantId = tenantId;
            $rootScope.loading = true;

            accessoriesService.add($scope.accessory).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        } else {
            $rootScope.loading = true;
            accessoriesService.update($scope.accessory).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        }
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function AccessoriesService($http) {
    "use strict";

    var accessories;

    return {
        getTenant: getTenant,
        getAccessory: getAccessory,
        getList: getList,
        add: add,
        update: update,
        remove: remove
    };

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount, assetId) {
        var handleSuccess = function handleSuccess(response) {
            accessories = response.data;
            return accessories;
        };

        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/accessories/" + assetId;
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getAccessory(accessoryId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/accessories/" + accessoryId;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(accessory) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/accessories/";
            return $http({
                method: "POST",
                url: url,
                data: accessory,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function update(accessory) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/accessories/";
            return $http({
                method: "PUT",
                url: url,
                data: accessory,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(accessoryId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/accessories/" + accessoryId;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

exports["default"] = AccessoriesService;
module.exports = exports["default"];

},{}],7:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersAssetsController = require("./controllers/assetsController");

var _controllersAssetsController2 = _interopRequireDefault(_controllersAssetsController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesAssetsService = require("./services/assetsService");

var _servicesAssetsService2 = _interopRequireDefault(_servicesAssetsService);

var ModuleName = "VialSoft.assets";

angular.module(ModuleName, []).controller("assetsController", _controllersAssetsController2["default"]).controller("assetsdetailController", _controllersDetailController2["default"]).service("assetsService", _servicesAssetsService2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/assetsController":8,"./controllers/detailController":9,"./services/assetsService":10}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AssetsController = function AssetsController($scope, $rootScope, $state, assetsService, toasterService, modalService) {
    _classCallCheck(this, AssetsController);

    var pageSize = 6;
    var pageCount = 0;

    $scope.assets = [];

    $scope.getList = function () {

        $rootScope.loading = true;

        assetsService.getList(pageSize, pageCount).then(function (assets) {
            if (assets.length < pageSize) {
                $scope.noMoreData = true;
            }
            assets.forEach(function (asset) {
                $scope.assets.push(asset);
            });
            pageCount++;
            $scope.refreshSelectedItems();
            if (!$scope.assets.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function (assetId) {
        assetId ? $state.transitionTo("asset", { id: assetId }) : $state.transitionTo("asset");
    };

    $scope.nagivateToPurchase = function () {
        $state.transitionTo("purchase");
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.assets.forEach(function (asset) {
                asset.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.assets.some(function (asset) {
            return asset.selected;
        });

        $scope.everySelected = $scope.assets.every(function (asset) {
            return asset.selected;
        });
    };

    $scope.remove = function (asset) {

        modalService.showConfirmModal({
            messages: {
                title: "Eliminar el bien",
                body: "Esta seguro que desea eliminar el bien",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            var assetIdList;
            if (asset) {
                assetIdList = [asset.AssetId];
            } else {
                assetIdList = $scope.assets.map(function (assetItem) {
                    if (assetItem.selected) {
                        return assetItem.AssetId;
                    }
                    return null;
                });
            }

            assetIdList.forEach(function (assetId) {
                assetsService.remove(assetId).then(function (response) {
                    if (response.status === 200) {
                        $scope.assets.forEach(function (assetItem) {
                            if (assetId === assetItem.AssetId) {
                                var index = $scope.assets.indexOf(assetItem);
                                $scope.assets.splice(index, 1);
                            }
                        });
                    }
                })["catch"](function (error) {
                    toasterService.showServerError(error);
                });
            });

            $scope.refreshSelectedItems();
        });
    };

    $scope.getList();
};

exports["default"] = AssetsController;
module.exports = exports["default"];

},{}],9:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, assetsService, toasterService, modalService) {
    _classCallCheck(this, DetailController);

    $("select").select2({ width: "100%" });

    var assetId = $stateParams.id;
    $scope.editMode = assetId !== undefined;
    $scope.ownAsset = true;

    var tenantId;

    $scope.asset = {
        CreatedAt: new Date(),
        StatusAsset: 1
    };

    assetsService.getCustomer().then(function (response) {
        $scope.customers = response.data;
        $scope.asset.CustomerId = 1;
    });

    assetsService.getCategory().then(function (response) {
        $scope.categories = response.data;
    });

    assetsService.getSubCategoryAll().then(function (response) {
        $scope.subcategories = response.data;
    });

    $scope.changeProperty = function () {
        if ($scope.asset.StatusAsset === 1) {
            $scope.ownAsset = true;
        } else {
            $scope.ownAsset = false;

            $("#customers").select2({
                width: "100%"
            });
        }
    };

    $scope.getSubCategory = function () {
        $scope.subcategories = null;
        assetsService.getSubCategory($scope.asset.CategoryId).then(function (response) {
            $scope.subcategories = response.data;
            $("#subcategories").select2({
                width: "100%"
            });
        });
    };

    assetsService.enumStatus().then(function (response) {
        $scope.options = response.data;
    });

    assetsService.getWork().then(function (response) {
        $scope.works = response.data;
    });

    if ($scope.editMode) {
        $rootScope.loading = true;
        assetsService.getAsset(assetId).then(function (response) {
            $scope.asset = response.data;
            $scope.asset.Picture = "data:image/png;base64," + $scope.asset.Picture;
            assetsService.getCategory().then(function (responseCategory) {
                $scope.categories = responseCategory.data;
                $("#categories").select2({ width: "100%" });
                $("#works").select2({ width: "100%" });
                $("#property").select2({ width: "100%" });
                assetsService.getSubCategory($scope.asset.CategoryId).then(function (responsesubCategory) {
                    $scope.subcategories = responsesubCategory.data;
                    $("#subcategories").select2({ width: "100%" });
                });
            });
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    } else {

        assetsService.getCategory().then(function (responseCategory) {
            $scope.categories = responseCategory.data;
            $("#categories").select2({ width: "100%" });
            $("#works").select2({ width: "100%" });
            $("#property").select2({ width: "100%" });
            assetsService.getSubCategory($scope.asset.CategoryId).then(function (responsesubCategory) {
                $scope.subcategories = responsesubCategory.data;
                $("#subcategories").select2({ width: "100%" });
            });
        });

        assetsService.enumStatus().then(function (responseStatus) {
            $scope.options = responseStatus.data;
            $("#property").select2({ width: "100%" });
        });

        $rootScope.loading = true;
        assetsService.getTenant().then(function (response) {
            tenantId = response.data;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    }

    $scope.navigateBack = function () {
        $state.transitionTo("assets");
    };

    $scope.nagivateToMaintenanceCard = function (id) {
        $state.transitionTo("maintenancecards", { id: id });
    };

    $scope.nagivateToAccessories = function (id) {
        $state.transitionTo("accessories", { id: id });
    };

    $scope.removeAsset = function () {
        modalService.showConfirmModal({
            messages: {
                title: "Eliminar el Bien",
                body: "Esta seguro que desea eliminar el Bien?",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            $rootScope.loading = true;
            assetsService.remove(assetId).then(function (response) {
                if (response.status === 200) {
                    $state.transitionTo("assets");
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        });
    };

    $scope.save = function () {
        if ($scope.asset.Picture) {
            $scope.asset.Picture = $scope.asset.Picture.split(",")[1];
        }

        if (!$scope.editMode) {
            $scope.asset.TenantId = tenantId;
            $rootScope.loading = true;

            assetsService.add($scope.asset).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        } else {
            $rootScope.loading = true;
            assetsService.update($scope.asset).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        }
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],10:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function AssetsService($http) {
    "use strict";

    var assets;

    return {
        enumStatus: enumStatus,
        getCategory: getCategory,
        getSubCategory: getSubCategory,
        getSubCategoryAll: getSubCategoryAll,
        getCustomer: getCustomer,
        getTenant: getTenant,
        getWork: getWork,
        getAsset: getAsset,
        getList: getList,
        add: add,
        update: update,
        remove: remove
    };

    function getCustomer() {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/customers/getAll";
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getWork() {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/works/getAll";
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function enumStatus() {
        return $http({
            method: "GET",
            url: "/api/assets/EnumStatus"
        });
    }

    function getCategory() {
        return $http({
            method: "GET",
            url: "/api/Categories"
        });
    }

    function getSubCategoryAll() {
        return $http({
            method: "GET",
            url: "/api/SubCategories"
        });
    }

    function getSubCategory(categoryId) {
        return $http({
            method: "GET",
            url: "/api/SubCategories/" + categoryId
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        var handleSuccess = function handleSuccess(response) {
            assets = response.data;
            return assets;
        };

        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getAsset(assetId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets/" + assetId;
            return $http({
                method: 'GET',
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(asset) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets/";
            return $http({
                method: "POST",
                url: url,
                data: asset,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function update(asset) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets/";
            return $http({
                method: "PUT",
                url: url,
                data: asset,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(assetId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets/" + assetId;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

exports["default"] = AssetsService;
module.exports = exports["default"];

},{}],11:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CosteAssetController = function CosteAssetController($scope, $rootScope, $state, costeassetService, toasterService) {
    _classCallCheck(this, CosteAssetController);

    var pageSize = 6;
    var pageCount = 0;

    $scope.costeassets = [];

    $scope.getList = function () {

        $rootScope.loading = true;

        costeassetService.getList(pageSize, pageCount).then(function (costeassets) {
            if (costeassets.length < pageSize) {
                $scope.noMoreData = true;
            }
            costeassets.forEach(function (costeAsset) {
                $scope.costeassets.push(costeAsset);
            });
            pageCount++;
            $scope.refreshSelectedItems();
            if (!$scope.costeassets.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function (costeId) {
        costeId ? $state.transitionTo("costeasset", { id: costeId }) : $state.transitionTo("costeasset");
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.costeassets.forEach(function (costeAsset) {
                costeAsset.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.costeassets.some(function (costeAsset) {
            return costeAsset.selected;
        });

        $scope.everySelected = $scope.costeassets.every(function (costeAsset) {
            return costeAsset.selected;
        });
    };

    $scope.getList();
};

exports["default"] = CosteAssetController;
module.exports = exports["default"];

},{}],12:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, costeassetService, toasterService, modalService) {
    _classCallCheck(this, DetailController);

    var costeassetId = $stateParams.id;
    $scope.editMode = costeassetId !== undefined;
    $scope.searchMode = false;
    $scope.asset = {};
    $scope.costeasset = {
        CreatedAt: new Date()
    };

    $scope.search = function () {
        $rootScope.loading = true;
        costeassetService.getAssetById($scope.asset.Id).then(function (response) {
            $scope.searchMode = true;
            $scope.asset = response.data;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.navigateBack = function () {
        $state.transitionTo("costeassets");
    };

    $scope.save = function () {
        $rootScope.loading = true;
        costeassetService.getTenant().then(function (response) {
            var tenantId = response.data;

            $scope.costeasset.TenantId = tenantId;
            $scope.costeasset.AssetId = $scope.asset.AssetId;
            costeassetService.add($scope.costeasset).then(function (responseAdd) {
                if (responseAdd.status === 200) {
                    $scope.navigateBack();
                }
            });
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],13:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersCosteassetController = require("./controllers/costeassetController");

var _controllersCosteassetController2 = _interopRequireDefault(_controllersCosteassetController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesCosteassetService = require("./services/costeassetService");

var _servicesCosteassetService2 = _interopRequireDefault(_servicesCosteassetService);

var ModuleName = "VialSoft.costeasset";

angular.module(ModuleName, []).controller("costeassetController", _controllersCosteassetController2["default"]).controller("costeassetdetailController", _controllersDetailController2["default"]).service("costeassetService", _servicesCosteassetService2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/costeassetController":11,"./controllers/detailController":12,"./services/costeassetService":14}],14:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function CosteAssetService($http) {
    "use strict";

    var costeAsset;

    return {
        getAsset: getAsset,
        getTenant: getTenant,
        getList: getList,
        getAssetById: getAssetById,
        add: add,
        update: update
    };

    function getAsset(id) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets/GetAsset";
            return $http({
                method: "GET",
                url: url,
                params: {
                    Id: id
                },
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        var handleSuccess = function handleSuccess(response) {
            costeAsset = response.data;
            return costeAsset;
        };

        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/costeAsset";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getAssetById(id) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets/GetAsset";
            return $http({
                method: "GET",
                url: url,
                params: {
                    Id: id
                },
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(costeasset) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/costeAsset/";
            return $http({
                method: "POST",
                url: url,
                data: costeasset,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function update(costeasset) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/costeAsset/";
            return $http({
                method: "PUT",
                url: url,
                data: costeasset,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

exports["default"] = CosteAssetService;
module.exports = exports["default"];

},{}],15:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CustomersController = function CustomersController($scope, $rootScope, $state, customersService, toasterService, modalService) {
    _classCallCheck(this, CustomersController);

    var pageSize = 6;
    var pageCount = 0;

    $scope.customers = [];

    $scope.getList = function () {
        $rootScope.loading = true;
        customersService.getList(pageSize, pageCount).then(function (customers) {
            if (customers.length < pageSize) {
                $scope.noMoreData = true;
            }
            customers.forEach(function (customer) {
                $scope.customers.push(customer);
            });
            pageCount++;
            $scope.refreshSelectedItems();

            if (!$scope.customers.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function (customerId) {
        customerId ? $state.transitionTo("customer", { id: customerId }) : $state.transitionTo("customer");
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.customers.forEach(function (customer) {
                customer.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.customers.some(function (customer) {
            return customer.selected;
        });

        $scope.everySelected = $scope.customers.every(function (customer) {
            return customer.selected;
        });
    };

    $scope.remove = function (customer) {

        modalService.showConfirmModal({
            messages: {
                title: "Eliminar cliente",
                body: "Esta seguro que desea eliminar el cliente",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            var customerIdList;
            if (customer) {
                customerIdList = [customer.CustomerId];
            } else {
                customerIdList = $scope.customers.map(function (customerItem) {
                    if (customerItem.selected) {
                        return customerItem.CustomerId;
                    }
                    return null;
                });
            }

            customerIdList.forEach(function (customerId) {
                customersService.remove(customerId).then(function (response) {
                    if (response.status === 200) {
                        $scope.customers.forEach(function (customerItem) {
                            if (customerId === customerItem.CustomerId) {
                                var index = $scope.customers.indexOf(customerItem);
                                $scope.customers.splice(index, 1);
                            }
                        });
                    }
                })["catch"](function (error) {
                    toasterService.showServerError(error);
                });
            });

            $scope.refreshSelectedItems();
        });
    };

    $scope.getList();
};

exports["default"] = CustomersController;
module.exports = exports["default"];

},{}],16:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, customersService, toasterService, modalService) {
    _classCallCheck(this, DetailController);

    $("select").select2({ width: "100%" });

    var customerId = $stateParams.id;
    $scope.editMode = customerId !== undefined;
    var tenantId;

    $scope.customer = {
        CreatedAt: new Date()
    };

    customersService.enumEntity().then(function (response) {
        $scope.options = response.data;
    });

    if ($scope.editMode) {
        $rootScope.loading = true;
        customersService.getCustomer(customerId).then(function (response) {
            $scope.customer = response.data;
            customersService.enumEntity().then(function (responseEntity) {
                $scope.options = responseEntity.data;
                $("#entity").select2({ width: "100%" });
            });

            $scope.customer.Picture = "data:image/png;base64," + $scope.customer.Picture;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    } else {
        $rootScope.loading = true;
        customersService.getTenant().then(function (response) {
            tenantId = response.data;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    }

    $scope.navigateBack = function () {
        $state.transitionTo("customers");
    };

    $scope.removeCustomer = function () {
        modalService.showConfirmModal({
            messages: {
                title: "Eliminar Cliente",
                body: "Esta seguro que desea eliminar el cliente?",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            $rootScope.loading = true;
            customersService.remove(customerId).then(function (response) {
                if (response.status === 200) {
                    $state.transitionTo("customers");
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        });
    };

    $scope.save = function () {
        if ($scope.customer.Picture) {
            $scope.customer.Picture = $scope.customer.Picture.split(",")[1];
        }

        if (!$scope.editMode) {
            $scope.customer.TenantId = tenantId;
            $rootScope.loading = true;

            customersService.add($scope.customer).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        } else {
            $rootScope.loading = true;
            customersService.update($scope.customer).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        }
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],17:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersCustomersController = require("./controllers/customersController");

var _controllersCustomersController2 = _interopRequireDefault(_controllersCustomersController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesCustomersService = require("./services/customersService");

var _servicesCustomersService2 = _interopRequireDefault(_servicesCustomersService);

var _directivesFileDirective = require("./directives/fileDirective");

var _directivesFileDirective2 = _interopRequireDefault(_directivesFileDirective);

var ModuleName = "VialSoft.customers";

angular.module(ModuleName, []).controller("customersController", _controllersCustomersController2["default"]).controller("customersdetailController", _controllersDetailController2["default"]).service("customersService", _servicesCustomersService2["default"]).directive("fileBase64", _directivesFileDirective2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/customersController":15,"./controllers/detailController":16,"./directives/fileDirective":18,"./services/customersService":19}],18:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var FileBase64 = (function () {
    function FileBase64() {
        _classCallCheck(this, FileBase64);

        this.restrict = 'A';
        this.scope = {
            'b64': '='
        };
    }

    _createClass(FileBase64, [{
        key: 'link',
        value: function link(scope, element) {

            element.on('change', function () {
                var file = element.get(0).files[0];
                var reader = new FileReader();

                reader.onloadend = function () {
                    scope.$apply(function () {
                        scope.b64 = reader.result;
                    });
                };

                if (file) {
                    reader.readAsDataURL(file);
                } else {
                    scope.b64 = '';
                }
            });
        }
    }], [{
        key: 'directiveFactory',
        value: function directiveFactory() {
            FileBase64.instance = new FileBase64();
            return FileBase64.instance;
        }
    }]);

    return FileBase64;
})();

exports['default'] = FileBase64.directiveFactory;
module.exports = exports['default'];

},{}],19:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function CustomersService($http) {
    "use strict";

    var customers;

    return {
        enumEntity: enumEntity,
        getTenant: getTenant,
        getCustomer: getCustomer,
        getList: getList,
        add: add,
        update: update,
        remove: remove
    };

    function enumEntity() {
        return $http({
            method: "GET",
            url: "/api/customers/EnumEntity"
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        var handleSuccess = function handleSuccess(response) {
            customers = response.data;
            return customers;
        };

        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/customers";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getCustomer(customerId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/customers/" + customerId;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(customer) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/customers/";
            return $http({
                method: "POST",
                url: url,
                data: customer,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function update(customer) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/customers/";
            return $http({
                method: "PUT",
                url: url,
                data: customer,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(customerId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/customers/" + customerId;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

exports["default"] = CustomersService;
module.exports = exports["default"];

},{}],20:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var DashboardController = function DashboardController($scope, $rootScope, dashboardService, toasterService) {
    _classCallCheck(this, DashboardController);

    var year = new Date().getFullYear();
    $scope.incomesExpensesYear = year;
    $scope.currentYear = year;

    $rootScope.loading = true;
    dashboardService.getSummary().then(function (summary) {
        $scope.summary = summary;
    })['catch'](function (error) {
        toasterService.showServerError(error);
    })['finally'](function () {
        $rootScope.loading = false;
    });

    $scope.addYearIncomesExpenses = function () {
        if ($scope.currentYear > $scope.incomesExpensesYear) {
            $scope.incomesExpensesYear += 1;
        }
    };

    $scope.reduceYearIncomesExpenses = function () {
        $scope.incomesExpensesYear -= 1;
    };

    $scope.correctYear = function () {
        if ($scope.currentYear < $scope.incomesExpensesYear) {
            $scope.incomesExpensesYear = $scope.currentYear;
        }
    };

    var createChartDataIncomesExpenses = function createChartDataIncomesExpenses(expenses, incomes) {
        $scope.chartDataIncomesExpenses = {
            scaleLabel: function scaleLabel(valuePayload) {
                return Number(valuePayload.value).toFixed.replace('.', ',') + '$';
            },
            labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Deciembre'],
            datasets: [{
                label: 'INGRESOS',
                fillColor: 'rgba(0,216,204,0.2)',
                strokeColor: 'rgba(0,216,204,1)',
                pointColor: 'rgba(0,216,204,1)',
                pointStrokeColor: 'rgba(0,216,204,1)',
                pointHighlightFill: 'rgba(0,216,204,1)',
                pointHighlightStroke: '#fff',
                data: incomes
            }, {
                label: 'GASTOS',
                fillColor: 'rgba(255,23,112,0.2)',
                strokeColor: 'rgba(255,23,112,1)',
                pointColor: 'rgba(255,23,112,1)',
                pointStrokeColor: 'rgba(255,23,112,1)',
                pointHighlightFill: 'rgba(255,23,112,1)',
                pointHighlightStroke: '#fff',
                data: expenses
            }]
        };
    };

    $scope.$watch('incomesExpensesYear', function (newValue, oldValue) {
        if (newValue || oldValue) {
            var expenses = new Array(12);
            expenses.fill(0, 0, 13);
            var incomes = new Array(12);
            incomes.fill(0, 0, 13);

            dashboardService.getExpenses($scope.incomesExpensesYear).then(function (allExpenses) {
                allExpenses.forEach(function (elem, index) {
                    expenses[index] = elem.Expenses;
                    incomes[index] = elem.Incomes;
                });

                createChartDataIncomesExpenses(expenses, incomes);
            })['catch'](function (error) {
                toasterService.showServerError(error);
            });
        }
    });
};

exports['default'] = DashboardController;
module.exports = exports['default'];

},{}],21:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersDashboardController = require("./controllers/dashboardController");

var _controllersDashboardController2 = _interopRequireDefault(_controllersDashboardController);

var _servicesDashboardService = require("./services/dashboardService");

var _servicesDashboardService2 = _interopRequireDefault(_servicesDashboardService);

var _directivesMHChartDirective = require("./directives/MHChartDirective");

var _directivesMHChartDirective2 = _interopRequireDefault(_directivesMHChartDirective);

var ModuleName = "VialSoft.dashboard";

angular.module(ModuleName, []).directive("chart", _directivesMHChartDirective2["default"]).controller("dashboardController", _controllersDashboardController2["default"]).service("dashboardService", _servicesDashboardService2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/dashboardController":20,"./directives/MHChartDirective":22,"./services/dashboardService":23}],22:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var FILTER = new WeakMap();

var MhChart = (function () {
    function MhChart($filter) {
        _classCallCheck(this, MhChart);

        this.restrict = 'A';
        this.scope = {
            'chartdata': '=',
            'kind': '@'
        };
        FILTER.set(this, $filter);
    }

    _createClass(MhChart, [{
        key: 'link',
        value: function link(scope, element) {
            var numberFilter = FILTER.get(MhChart.instance)('number');

            var options = {
                scaleShowGridLines: true,
                scaleGridLineColor: 'rgba(0,0,0,.05)',
                scaleGridLineWidth: 1,
                scaleShowHorizontalLines: true,
                scaleShowVerticalLines: false,
                scaleLabel: function scaleLabel(valuePayload) {
                    return numberFilter(valuePayload.value);
                },
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: false,
                pointDotRadius: 3,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                tooltipFontColor: '#7c7c81',
                maintainAspectRatio: true,
                responsive: true,
                animation: true,
                animationEasing: 'easeOutQuint',
                customTooltips: function customTooltips(tooltip) {

                    var $tooltip = $('#chart-customtooltip');

                    if (!$tooltip[0]) {
                        $('body').append('<div id="chart-customtooltip" class="chart-customtooltip"></div>');
                        $tooltip = $('#chartjs-customtooltip');
                    }

                    if (!tooltip) {
                        $tooltip.css({
                            opacity: 0
                        });
                        return;
                    }

                    $tooltip.removeClass('above below no-transform');
                    if (tooltip.yAlign) {
                        $tooltip.addClass(tooltip.yAlign);
                    } else {
                        $tooltip.addClass('no-transform');
                    }

                    if (tooltip.text) {
                        $tooltip.html(tooltip.text);
                    } else {
                        var innerHtml = '<div class="title">' + tooltip.title + '</div>';
                        for (var i = 0; i < tooltip.labels.length; i++) {
                            innerHtml += ['<div class="section">', '   <span class="key" style="background-color:' + tooltip.legendColors[i].fill + '"></span>', '   <span class="value">$' + numberFilter(tooltip.labels[i]) + '</span>', '</div>'].join('');
                        }
                        $tooltip.html(innerHtml);
                    }

                    var top = 0;
                    if (tooltip.yAlign) {
                        if (tooltip.yAlign === 'above') {
                            top = tooltip.y - tooltip.caretHeight - tooltip.caretPadding;
                        } else {
                            top = tooltip.y + tooltip.caretHeight + tooltip.caretPadding;
                        }
                    }

                    var offset = $(tooltip.chart.canvas).offset();

                    $tooltip.css({
                        opacity: 1,
                        width: tooltip.width ? tooltip.width + 'px' : 'auto',
                        left: offset.left + tooltip.x + 'px',
                        top: offset.top + top + 'px',
                        fontFamily: tooltip.fontFamily,
                        fontSize: tooltip.fontSize,
                        fontStyle: tooltip.fontStyle,
                        backgroundColor: 'rgb(255, 255, 255)',
                        boxShadow: '0 2px 6px 0 rgba(0, 0, 0, .8)'
                    });
                }
            };

            var ctx = element.get(0).getContext('2d');

            scope.$watch('chartdata', function (newValue, oldValue) {

                if (newValue && !oldValue) {
                    if (scope.kind === 'line') {
                        ctx.canvas.height = 80;
                        scope.incomeExpensesChart = new Chart(ctx).Line(scope.chartdata, options);
                        var legend = scope.incomeExpensesChart.generateLegend();
                        document.getElementById('legendIncomeExpenses').innerHTML = legend;
                    }

                    if (scope.kind === 'bar') {
                        ctx.canvas.height = 80;
                        scope.patientsChart = new Chart(ctx).Bar(scope.chartdata, options);
                    }
                }

                if (newValue && oldValue) {
                    if (scope.kind === 'line') {
                        scope.chartdata.datasets[0].data.forEach(function (elem, index) {
                            scope.incomeExpensesChart.datasets[0].points[index].value = elem;
                        });

                        scope.chartdata.datasets[1].data.forEach(function (elem, index) {
                            scope.incomeExpensesChart.datasets[1].points[index].value = elem;
                        });

                        scope.incomeExpensesChart.update();
                    }

                    if (scope.kind === 'bar') {
                        scope.chartdata.datasets[0].data.forEach(function (elem, index) {
                            scope.patientsChart.datasets[0].bars[index].value = elem;
                        });
                        scope.patientsChart.update();
                    }
                }
            });

            Chart.defaults.global.responsive = true;
        }
    }], [{
        key: 'directiveFactory',
        value: function directiveFactory($filter) {
            MhChart.instance = new MhChart($filter);
            return MhChart.instance;
        }
    }]);

    return MhChart;
})();

MhChart.directiveFactory.$inject = ['$filter'];

exports['default'] = MhChart.directiveFactory;
module.exports = exports['default'];

},{}],23:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});
function DashboardService($http) {
    "use strict";

    return {
        getSummary: getSummary,
        getExpenses: getExpenses
    };

    function getSummary() {
        var handleSuccess = function handleSuccess(response) {
            var summary = response.data;
            return summary;
        };

        var tenantId = '';

        return $http({
            method: 'GET',
            url: '/api/users/current/tenant'
        }).then(function (response) {
            tenantId = response.data;
            var url = '/api/reports/assetsummary';
            return $http({
                method: 'GET',
                url: url,
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getExpenses(year) {
        var handleSuccess = function handleSuccess(response) {
            var expenses = response.data;
            return expenses;
        };

        var tenantId = '';

        return $http({
            method: 'GET',
            url: '/api/users/current/tenant'
        }).then(function (response) {
            tenantId = response.data;
            var url = '/api/reports/expenses/' + year;
            return $http({
                method: 'GET',
                url: url,
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }
}

exports['default'] = DashboardService;
module.exports = exports['default'];

},{}],24:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, maintenancecardsService, toasterService, modalService) {
    _classCallCheck(this, DetailController);

    $("select").select2({ width: "100%" });

    var maintenancecardsId = $stateParams.id;
    var tenantId;
    $scope.editMode = false;
    $scope.showDetail = false;

    $scope.maintenancecards = {
        TenantId: 0,
        AssetId: maintenancecardsId,
        MaintenanceCardDetail: []
    };

    maintenancecardsService.getPeriod().then(function (response) {
        $scope.periods = response.data;
    });

    maintenancecardsService.getJob().then(function (response) {
        $scope.jobs = response.data;
    });

    $scope.getJobs = function () {
        $scope.jobs = null;
        maintenancecardsService.getJobs($scope.maintenancecard.PeriodId).then(function (response) {
            $scope.jobs = response.data;
            $("#job").select2({
                width: "100%"
            });
        });
    };

    $scope.addMaintenanceCardDetail = function () {
        var detail = {
            PeriodId: $scope.maintenancecard.PeriodId,
            JobId: $scope.maintenancecard.JobId,
            Replacement: $scope.maintenancecard.Replacement
        };
        $scope.maintenancecards.MaintenanceCardDetail.push(detail);
        $scope.showDetail = true;
    };

    if ($scope.editMode) {
        $rootScope.loading = true;
        maintenancecardsService.getmaintenancecards(maintenancecardsId).then(function (response) {
            $scope.maintenancecards = response.data;
            $scope.showDetail = true;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    }

    $scope.removeAsset = function (assetItem) {
        var index = $scope.maintenancecards.Details.indexOf(assetItem);
        $scope.maintenancecards.Details.splice(index, 1);
    };

    $scope.navigateBack = function () {
        $state.transitionTo("maintenancecards", { id: maintenancecardsId });
    };

    $scope.save = function () {
        $rootScope.loading = true;
        maintenancecardsService.getTenant().then(function (response) {
            tenantId = response.data;

            $scope.maintenancecards.TenantId = tenantId;

            maintenancecardsService.add($scope.maintenancecards).then(function (responseAdd) {
                if (responseAdd.status === 200) {
                    $scope.navigateBack();
                }
            });
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],25:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MaintenanceCardsController = function MaintenanceCardsController($scope, $rootScope, $stateParams, $state, maintenancecardsService, toasterService, modalService) {
    _classCallCheck(this, MaintenanceCardsController);

    var assetId = $stateParams.id;
    var pageSize = 6;
    var pageCount = 0;
    $scope.newMaintenanceCard = true;
    $scope.AssetId = assetId;
    $scope.maintenancecards = [];

    $scope.getList = function () {
        $rootScope.loading = true;
        maintenancecardsService.getList(pageSize, pageCount, assetId).then(function (maintenanceCards) {

            if (maintenanceCards.length > 0) {
                $scope.newMaintenanceCard = false;
            }

            if (maintenanceCards.length < pageSize) {
                $scope.noMoreData = true;
            }
            maintenanceCards.forEach(function (maintenanceCard) {
                $scope.maintenancecards.push(maintenanceCard);
            });
            pageCount++;
            $scope.refreshSelectedItems();
            if (!$scope.maintenancecards.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function () {
        $state.transitionTo("maintenancecard", { id: $scope.AssetId });
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.maintenancecards.forEach(function (maintenanceCard) {
                maintenanceCard.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.maintenancecards.some(function (maintenanceCard) {
            return maintenanceCard.selected;
        });

        $scope.everySelected = $scope.maintenancecards.every(function (maintenanceCard) {
            return maintenanceCard.selected;
        });
    };

    $scope.navigateBackAsset = function () {
        $state.transitionTo("asset", { id: $scope.AssetId });
    };

    $scope.remove = function (maintenanceCard) {
        modalService.showConfirmModal({
            messages: {
                title: "Eliminar item",
                body: "Esta seguro que desea eliminar el item",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            maintenancecardsService.remove(maintenanceCard).then(function (response) {
                if (response.status === 200) {
                    $scope.maintenancecards.forEach(function (maintenanceCardItem) {
                        if (maintenanceCard === maintenanceCardItem.MaintenanceCardDetailId) {
                            var index = $scope.maintenancecards.indexOf(maintenanceCardItem);
                            $scope.maintenancecards.splice(index, 1);
                        }
                    });
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            });

            $scope.refreshSelectedItems();
        });
    };

    $scope.getList();
};

exports["default"] = MaintenanceCardsController;
module.exports = exports["default"];

},{}],26:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersMaintenancecardsController = require("./controllers/maintenancecardsController");

var _controllersMaintenancecardsController2 = _interopRequireDefault(_controllersMaintenancecardsController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesMaintenancecardsService = require("./services/maintenancecardsService");

var _servicesMaintenancecardsService2 = _interopRequireDefault(_servicesMaintenancecardsService);

var ModuleName = "VialSoft.maintenancecards";

angular.module(ModuleName, []).controller("maintenancecardsController", _controllersMaintenancecardsController2["default"]).controller("maintenancecardsdetailController", _controllersDetailController2["default"]).service("maintenancecardsService", _servicesMaintenancecardsService2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/detailController":24,"./controllers/maintenancecardsController":25,"./services/maintenancecardsService":27}],27:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function MaintenanceCardsService($http) {
    "use strict";

    var maintenancecards;

    return {
        getJob: getJob,
        getJobs: getJobs,
        getPeriod: getPeriod,
        getTenant: getTenant,
        getMaintenanceCards: getMaintenanceCards,
        getList: getList,
        add: add,
        remove: remove
    };

    function getJob() {
        return $http({
            method: "GET",
            url: "/api/Jobs"
        });
    }

    function getJobs(periodId) {
        return $http({
            method: "GET",
            url: "/api/Jobs/" + periodId
        });
    }

    function getPeriod() {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/Periods";
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount, assetId) {
        var handleSuccess = function handleSuccess(response) {
            maintenancecards = response.data;
            return maintenancecards;
        };
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/maintenancecards";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount,
                    assetId: assetId
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getMaintenanceCards(maintenancecardsId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/maintenancecards/" + maintenancecardsId;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(maintenancecards) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/maintenancecards/";
            return $http({
                method: "POST",
                url: url,
                data: maintenancecards,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(mantenanceCardId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/maintenancecards/" + mantenanceCardId;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

exports["default"] = MaintenanceCardsService;
module.exports = exports["default"];

},{}],28:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, providersService, toasterService, modalService) {
    _classCallCheck(this, DetailController);

    $("select").select2({ width: "100%" });

    var providerId = $stateParams.id;
    $scope.editMode = providerId !== undefined;
    var tenantId;

    $scope.provider = {
        CreatedAt: new Date()
    };

    providersService.getTypeReceipt().then(function (response) {
        $scope.typeReceipts = response.data;
    });

    if ($scope.editMode) {
        $rootScope.loading = true;
        providersService.getProvider(providerId).then(function (response) {
            $scope.provider = response.data;

            providersService.getTypeReceipt().then(function (responseType) {
                $scope.typeReceipts = responseType.data;

                $("#typeReceipt").select2({ width: "100%" });
            });
            if ($scope.provider.Picture != null) {
                $scope.provider.Picture = "data:image/png;base64," + $scope.provider.Picture;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    } else {
        $rootScope.loading = true;
        providersService.getTenant().then(function (response) {
            tenantId = response.data;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    }

    $scope.navigateBack = function () {
        $state.transitionTo("providers");
    };

    $scope.removeProvider = function () {
        modalService.showConfirmModal({
            messages: {
                title: "Eliminar Proveedor",
                body: "Esta seguro que desea eliminar el proveedor?",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            $rootScope.loading = true;
            providersService.remove(providerId).then(function (response) {
                if (response.status === 200) {
                    $state.transitionTo("providers");
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        });
    };

    $scope.save = function () {

        if ($scope.provider.Picture != null) {
            $scope.provider.Picture = $scope.provider.Picture.split(",")[1];
        }

        if (!$scope.editMode) {
            $scope.provider.TenantId = tenantId;
            $rootScope.loading = true;

            providersService.add($scope.provider).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        } else {
            $rootScope.loading = true;
            providersService.update($scope.provider).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        }
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],29:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ProvidersController = function ProvidersController($scope, $rootScope, $state, providersService, toasterService, modalService) {
    _classCallCheck(this, ProvidersController);

    var pageSize = 6;
    var pageCount = 0;

    $scope.providers = [];

    $scope.getList = function () {
        $rootScope.loading = true;
        providersService.getList(pageSize, pageCount).then(function (providers) {
            if (providers.length < pageSize) {
                $scope.noMoreData = true;
            }
            providers.forEach(function (provider) {
                $scope.providers.push(provider);
            });
            pageCount++;
            $scope.refreshSelectedItems();

            if (!$scope.providers.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function (providerId) {
        providerId ? $state.transitionTo("provider", { id: providerId }) : $state.transitionTo("provider");
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.providers.forEach(function (provider) {
                provider.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.providers.some(function (provider) {
            return provider.selected;
        });

        $scope.everySelected = $scope.providers.every(function (provider) {
            return provider.selected;
        });
    };

    $scope.remove = function (provider) {

        modalService.showConfirmModal({
            messages: {
                title: "Eliminar proveedor",
                body: "Esta seguro que desea eliminar al proveedor",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            var providerIdList;
            if (provider) {
                providerIdList = [provider.ProviderId];
            } else {
                providerIdList = $scope.providers.map(function (providerItem) {
                    if (providerItem.selected) {
                        return providerItem.ProviderId;
                    }
                    return null;
                });
            }

            providerIdList.forEach(function (providerId) {
                providersService.remove(providerId).then(function (response) {
                    if (response.status === 200) {
                        $scope.providers.forEach(function (providerItem) {
                            if (providerId === providerItem.ProviderId) {
                                var index = $scope.providers.indexOf(providerItem);
                                $scope.providers.splice(index, 1);
                            }
                        });
                    }
                })["catch"](function (error) {
                    toasterService.showServerError(error);
                });
            });

            $scope.refreshSelectedItems();
        });
    };

    $scope.getList();
};

exports["default"] = ProvidersController;
module.exports = exports["default"];

},{}],30:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersProvidersController = require("./controllers/providersController");

var _controllersProvidersController2 = _interopRequireDefault(_controllersProvidersController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesProvidersService = require("./services/providersService");

var _servicesProvidersService2 = _interopRequireDefault(_servicesProvidersService);

var ModuleName = "VialSoft.providers";

angular.module(ModuleName, []).controller("providersController", _controllersProvidersController2["default"]).controller("providersdetailController", _controllersDetailController2["default"]).service("providersService", _servicesProvidersService2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/detailController":28,"./controllers/providersController":29,"./services/providersService":31}],31:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function ProvidersService($http) {
    "use strict";

    var providers;

    return {
        getTypeReceipt: getTypeReceipt,
        getTenant: getTenant,
        getProvider: getProvider,
        getList: getList,
        add: add,
        update: update,
        remove: remove
    };

    function getTypeReceipt() {
        return $http({
            method: "GET",
            url: "/api/purchases/TypeReceipts"
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        var handleSuccess = function handleSuccess(response) {
            providers = response.data;
            return providers;
        };

        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/providers";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getProvider(providerId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/providers/" + providerId;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(provider) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/providers/";
            return $http({
                method: "POST",
                url: url,
                data: provider,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function update(provider) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/providers/";
            return $http({
                method: "PUT",
                url: url,
                data: provider,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(providerId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/providers/" + providerId;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

exports["default"] = ProvidersService;
module.exports = exports["default"];

},{}],32:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, purchasesService, toasterService, modalService, modalAssetService) {
    _classCallCheck(this, DetailController);

    $("select").select2({ width: "100%" });

    var purchaseId = $stateParams.id;
    $scope.editMode = purchaseId !== undefined;
    $scope.searchMode = false;
    $scope.showDetail = false;
    $scope.priceTotal = 0;
    var tenantId;

    $scope.asset = {};

    $scope.purchaseDetail = {
        AssetId: null,
        Price: null
    };

    $scope.purchase = {
        CreatedAt: new Date(),
        TenantId: 0,
        Details: []
    };

    purchasesService.getCategory().then(function (response) {
        $scope.categories = response.data;
    });

    purchasesService.getSubCategoryAll().then(function (response) {
        $scope.subcategories = response.data;
    });

    $scope.getSubCategory = function () {
        $scope.subcategories = null;
        purchasesService.getSubCategory($scope.asset.CategoryId).then(function (response) {
            $scope.subcategories = response.data;
            $("#subcategories").select2({
                width: "100%"
            });
        });
    };

    purchasesService.getProvider().then(function (response) {
        $scope.providers = response.data;
    });

    $scope.search = function () {
        $rootScope.loading = true;
        purchasesService.getAssetById($scope.asset.Id).then(function (response) {
            $scope.searchMode = true;
            $scope.asset = response.data;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.addPurchaceDetail = function () {
        var detail = {
            Price: $scope.purchase.Price,
            AssetId: $scope.asset.AssetId
        };
        $scope.priceTotal += $scope.purchase.Price;
        $scope.purchase.Details.push(detail);
        $scope.showDetail = true;
    };

    if ($scope.editMode) {
        $rootScope.loading = true;
        purchasesService.getPurchase(purchaseId).then(function (response) {
            $scope.purchase = response.data;
            $scope.showDetail = true;

            $scope.purchase.Details.forEach(function (detail) {
                $scope.priceTotal += detail.Price;
            });
            purchasesService.getProvider().then(function (responseProvider) {
                $scope.providers = responseProvider.data;
                $("#provider").select2({ width: "100%" });
            });

            purchasesService.getTypeReceipt().then(function (responseType) {
                $scope.typeReceipts = responseType.data;
                $("#typeReceipt").select2({ width: "100%" });
            });
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    }

    $scope.removeAsset = function (assetItem) {
        var index = $scope.purchase.Details.indexOf(assetItem);
        $scope.priceTotal -= assetItem.Price;
        $scope.purchase.Details.splice(index, 1);
    };

    $scope.navigateBack = function () {
        $state.transitionTo("purchases");
    };

    $scope.nagivateToAsset = function () {
        modalAssetService.showConfirmModal().then(function (response) {

            purchasesService.getAsset(response).then(function (responseAsset) {
                $scope.searchMode = true;
                $scope.asset = responseAsset.data;
            });
        });
    };

    $scope.save = function () {
        $rootScope.loading = true;
        purchasesService.getTenant().then(function (response) {
            tenantId = response.data;

            $scope.purchase.TenantId = tenantId;
            purchasesService.add($scope.purchase).then(function (responseAdd) {
                if (responseAdd.status === 200) {
                    $scope.navigateBack();
                }
            });
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],33:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PurchasesController = function PurchasesController($scope, $rootScope, $state, purchasesService, toasterService, modalService) {
    _classCallCheck(this, PurchasesController);

    var pageSize = 6;
    var pageCount = 0;

    $scope.purchases = [];

    $scope.getList = function () {

        $rootScope.loading = true;

        purchasesService.getList(pageSize, pageCount).then(function (purchases) {
            if (purchases.length < pageSize) {
                $scope.noMoreData = true;
            }
            purchases.forEach(function (purchase) {
                $scope.purchases.push(purchase);
            });
            pageCount++;
            $scope.refreshSelectedItems();
            if (!$scope.purchases.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function (purchaseId) {
        purchaseId ? $state.transitionTo("purchase", { id: purchaseId }) : $state.transitionTo('purchase');
    };

    $scope.nagivateToAsset = function () {
        $state.transitionTo("asset");
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.purchases.forEach(function (purchase) {
                purchase.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.purchases.some(function (purchase) {
            return purchase.selected;
        });

        $scope.everySelected = $scope.purchases.every(function (purchase) {
            return purchase.selected;
        });
    };

    $scope.getList();
};

exports["default"] = PurchasesController;
module.exports = exports["default"];

},{}],34:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersPurchasesController = require("./controllers/purchasesController");

var _controllersPurchasesController2 = _interopRequireDefault(_controllersPurchasesController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesPurchasesService = require("./services/purchasesService");

var _servicesPurchasesService2 = _interopRequireDefault(_servicesPurchasesService);

var ModuleName = "VialSoft.purchases";

angular.module(ModuleName, []).controller("purchasesController", _controllersPurchasesController2["default"]).controller("purchasesdetailController", _controllersDetailController2["default"]).service("purchasesService", _servicesPurchasesService2["default"]);
exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/detailController":32,"./controllers/purchasesController":33,"./services/purchasesService":35}],35:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function PurchasesService($http) {
    "use strict";

    var purchases;

    return {
        getCategory: getCategory,
        getSubCategory: getSubCategory,
        getSubCategoryAll: getSubCategoryAll,
        getAssetById: getAssetById,
        getAsset: getAsset,
        getProvider: getProvider,
        getTypeReceipt: getTypeReceipt,
        getCustomer: getCustomer,
        getTenant: getTenant,
        getPurchase: getPurchase,
        getList: getList,
        add: add,
        update: update
    };

    function getCustomer() {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/customers/getAll";
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getCategory() {
        return $http({
            method: "GET",
            url: "/api/Categories"
        });
    }

    function getSubCategoryAll() {
        return $http({
            method: "GET",
            url: "/api/SubCategories"
        });
    }

    function getSubCategory(categoryId) {
        return $http({
            method: "GET",
            url: "/api/SubCategories/" + categoryId
        });
    }

    function getTypeReceipt() {
        return $http({
            method: "GET",
            url: "/api/purchases/TypeReceipts"
        });
    }

    function getProvider() {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/providers/getAll";
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
    function getAsset(assetId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets/" + assetId;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getAssetById(id) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets/GetAsset";
            return $http({
                method: "GET",
                url: url,
                params: {
                    Id: id
                },
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        var handleSuccess = function handleSuccess(response) {
            purchases = response.data;
            return purchases;
        };

        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/purchases";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getPurchase(purchaseId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/purchases/" + purchaseId;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(purchase) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/purchases/";
            return $http({
                method: "POST",
                url: url,
                data: purchase,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function update(purchase) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/purchases/";
            return $http({
                method: "PUT",
                url: url,
                data: purchase,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

exports["default"] = PurchasesService;
module.exports = exports["default"];

},{}],36:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var state = new WeakMap();

var HeaderController = function HeaderController($state, $rootScope, $http, $timeout) {
    var _this = this;

    _classCallCheck(this, HeaderController);

    var vm = this;
    state.set(this, $state);

    $timeout(function () {
        _this.title = $state.current.name;
        vm.viewName = $state.current.name;
    }, 10);

    $rootScope.$on("$stateChangeStart", function (e, toState) {
        _this.title = toState.name;
        vm.viewName = toState.name;

        if (toState.name === "customers" || toState.name === "customer") _this.title = "Clientes";

        if (toState.name === "works" || toState.name === "work") _this.title = "Obras";

        if (toState.name === "assets" || toState.name === "asset") _this.title = "Bienes";

        if (toState.name === "accessories" || toState.name === "accessory") _this.title = "Accesorios";

        if (toState.name === "tenants" || toState.name === "tenant") _this.title = "Propietarios";

        if (toState.name === "purchases" || toState.name === "purchase") _this.title = "Compras";

        if (toState.name === "transfersworks" || toState.name === "transferwork") _this.title = "Traslados";

        if (toState.name === "providers" || toState.name === "provider") _this.title = "Proveedores";

        if (toState.name === "costeassets" || toState.name === "costeasset") _this.title = "Costos";

        $rootScope.menuOpen = false;
    });

    $http({
        method: "GET",
        url: "/api/users/current/user"
    }).then(function (response) {
        vm.userName = response.data;
    });
};

HeaderController.$inject = ["$state", "$rootScope", "$http", "$timeout"];
exports["default"] = HeaderController;
module.exports = exports["default"];

},{}],37:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var HeaderBar = (function () {
    function HeaderBar() {
        _classCallCheck(this, HeaderBar);

        this.restrict = 'E';
        this.templateUrl = '/app/components/shared/directives/headerBar/headerBarTemplate.html';
        this.controller = 'headerController';
        this.controllerAs = 'vm';
    }

    _createClass(HeaderBar, [{
        key: 'link',
        value: function link(scope) {
            $(document).bind('click', function (event) {
                if (!scope.menuOpen) {
                    event.stopPropagation();
                } else {
                    scope.$apply(function () {
                        scope.menuOpen = false;
                    });
                }
            });

            $('.header-hamburguer, #sidebar-container').bind('click', function (event) {
                event.stopPropagation();
            });
        }
    }], [{
        key: 'directiveFactory',
        value: function directiveFactory() {
            HeaderBar.instance = new HeaderBar();
            return HeaderBar.instance;
        }
    }]);

    return HeaderBar;
})();

exports['default'] = HeaderBar.directiveFactory;
module.exports = exports['default'];

},{}],38:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var LeftMenu = (function () {
    function LeftMenu() {
        _classCallCheck(this, LeftMenu);

        this.restrict = 'E';
        this.templateUrl = '/app/components/shared/directives/leftMenu/leftMenuTemplate.html';
        this.controller = 'headerController';
        this.controllerAs = 'vm';
    }

    _createClass(LeftMenu, null, [{
        key: 'directiveFactory',
        value: function directiveFactory() {
            LeftMenu.instance = new LeftMenu();
            return LeftMenu.instance;
        }
    }]);

    return LeftMenu;
})();

exports['default'] = LeftMenu.directiveFactory;
module.exports = exports['default'];

},{}],39:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});
function CamelCaseFilter() {
    'use strict';
    return function (input) {
        if (!input) {
            return input;
        }

        var list = input.match(/[A-Za-z][a-z]*/g);

        if (!list) {
            return input;
        }
        var result = list.join(' ');
        result = result.substr(0, 1).toUpperCase() + result.substr(1);
        return result;
    };
}

exports['default'] = CamelCaseFilter;
module.exports = exports['default'];

},{}],40:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function ExceptionHandler($injector) {
    "use strict";

    var handledExceptions = [];

    return function (exception) {
        if (handledExceptions.indexOf(exception) === -1) {
            //appInsights.trackException(exception);
            $injector.get("toasterService").showServerError();
            handledExceptions.push(exception);
            console.warn("Unhandled Exception", exception);
        }
    };
}

exports["default"] = ExceptionHandler;
module.exports = exports["default"];

},{}],41:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function ModalAssetService($modal) {
    "use strict";

    return {
        showConfirmModal: showConfirmModal
    };

    function showConfirmModal() {

        return $modal.open({
            templateUrl: "/app/components/shared/views/confirmAssetModal.html",
            controller: ["$scope", "$modalInstance", "purchasesService", "assetsService", function ($scope, $modalInstance, purchasesService, assetsService) {

                $scope.AssetId = 0;

                var tenantId;
                $scope.editMode = false;
                $scope.ownAsset = true;

                $scope.asset = {
                    CreatedAt: new Date(),
                    StatusAsset: 1
                };

                assetsService.enumStatus().then(function (response) {
                    $scope.options = response.data;
                });

                purchasesService.getCategory().then(function (response) {
                    $scope.categories = response.data;
                    $("#categories").select2({ width: "100%" });
                });

                assetsService.getSubCategory($scope.asset.CategoryId).then(function (responsesubCategory) {
                    $scope.subcategories = responsesubCategory.data;
                    $("#subcategories").select2({ width: "100%" });
                });

                $scope.getSubCategory = function () {
                    $scope.subcategories = null;
                    assetsService.getSubCategory($scope.asset.CategoryId).then(function (response) {
                        $scope.subcategories = response.data;
                        $("#subcategories").select2({
                            width: "100%"
                        });
                    });
                };

                assetsService.getWork().then(function (response) {
                    $scope.works = response.data;
                    $("#works").select2({ width: "100%" });
                });

                assetsService.enumStatus().then(function (responseStatus) {
                    $scope.options = responseStatus.data;
                    $("#property").select2({ width: "100%" });
                });

                assetsService.getTenant().then(function (response) {
                    tenantId = response.data;
                });

                $scope.changeProperty = function () {
                    if ($scope.asset.StatusAsset === 1) {
                        $scope.ownAsset = true;
                    } else {
                        $scope.ownAsset = false;

                        $("#customers").select2({
                            width: "100%"
                        });
                    }
                };

                assetsService.getCustomer().then(function (response) {
                    $scope.customers = response.data;
                    $scope.asset.CustomerId = 1;
                });

                $scope.save = function () {

                    if ($scope.asset.Picture != null) {
                        $scope.asset.Picture = $scope.asset.Picture.split(",")[1];
                    }
                    $scope.asset.TenantId = tenantId;
                    assetsService.add($scope.asset).then(function (response) {
                        if (response.status === 200) {
                            $modalInstance.close(response.data);
                        }
                    });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss("cancel");
                };
            }],
            size: "lg"
        }).result.then(function (result) {
            return result;
        });
    }
}

exports["default"] = ModalAssetService;
module.exports = exports["default"];

},{}],42:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function ModalService($modal) {
    "use strict";

    return {
        showConfirmModal: showConfirmModal
    };

    function showConfirmModal(opts) {
        return $modal.open({

            templateUrl: "/app/components/shared/views/confirmModal.html",

            controller: ["$scope", "$modalInstance", function ($scope, $modalInstance) {

                $scope.messages = opts.messages;

                $scope.ok = function () {
                    $modalInstance.close();
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss("cancel");
                };
            }]

        }).result;
    }
}

exports["default"] = ModalService;
module.exports = exports["default"];

},{}],43:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function ToasterService(toaster) {
    "use strict";

    return {
        showServerError: showServerError
    };

    function showServerError() {
        toaster.pop("error", "Error", "Oops! Ocurrio un problema!");
    }
}

exports["default"] = ToasterService;
module.exports = exports["default"];

},{}],44:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _directivesLeftMenuLeftMenuDirective = require("./directives/leftMenu/leftMenuDirective");

var _directivesLeftMenuLeftMenuDirective2 = _interopRequireDefault(_directivesLeftMenuLeftMenuDirective);

var _directivesHeaderBarHeaderBarDirective = require("./directives/headerBar/headerBarDirective");

var _directivesHeaderBarHeaderBarDirective2 = _interopRequireDefault(_directivesHeaderBarHeaderBarDirective);

var _controllersHeaderController = require("./controllers/headerController");

var _controllersHeaderController2 = _interopRequireDefault(_controllersHeaderController);

var _servicesToasterService = require("./services/toasterService");

var _servicesToasterService2 = _interopRequireDefault(_servicesToasterService);

var _servicesModalService = require("./services/modalService");

var _servicesModalService2 = _interopRequireDefault(_servicesModalService);

var _servicesModalAssetService = require("./services/modalAssetService");

var _servicesModalAssetService2 = _interopRequireDefault(_servicesModalAssetService);

var _servicesExceptionHandler = require("./services/exceptionHandler");

var _servicesExceptionHandler2 = _interopRequireDefault(_servicesExceptionHandler);

var _filtersCamelCaseFilter = require("./filters/camelCaseFilter");

var _filtersCamelCaseFilter2 = _interopRequireDefault(_filtersCamelCaseFilter);

var ModuleName = "VialSoft.shared";

angular.module(ModuleName, ["ui.bootstrap", "toaster"]).directive("leftMenu", _directivesLeftMenuLeftMenuDirective2["default"]).directive("headerBar", _directivesHeaderBarHeaderBarDirective2["default"]).controller("headerController", _controllersHeaderController2["default"]).service("toasterService", _servicesToasterService2["default"]).service("modalService", _servicesModalService2["default"]).service("modalAssetService", _servicesModalAssetService2["default"]).factory("$exceptionHandler", _servicesExceptionHandler2["default"]).filter("camelCase", _filtersCamelCaseFilter2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/headerController":36,"./directives/headerBar/headerBarDirective":37,"./directives/leftMenu/leftMenuDirective":38,"./filters/camelCaseFilter":39,"./services/exceptionHandler":40,"./services/modalAssetService":41,"./services/modalService":42,"./services/toasterService":43}],45:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, tenantsService, toasterService, modalService) {
    _classCallCheck(this, DetailController);

    var tenantId = $stateParams.id;
    $scope.editMode = tenantId !== undefined;

    if ($scope.editMode) {
        $rootScope.loading = true;
        tenantsService.getTenant(tenantId).then(function (response) {
            $scope.tenant = response.data;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    }

    $scope.navigateBack = function () {
        $state.transitionTo('tenants');
    };

    $scope.removetenant = function () {
        modalService.showConfirmModal({
            messages: {
                title: "Eliminar Propietario",
                body: "Esta seguro que desea eliminar el propietario?",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            $rootScope.loading = true;
            tenantsService.remove(tenantId).then(function (response) {
                if (response.status === 200) {
                    $state.transitionTo('tenants');
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        });
    };

    $scope.save = function () {
        if (!$scope.editMode) {
            $scope.tenant.TenantId = tenantId;
            $rootScope.loading = true;

            tenantsService.add($scope.tenant).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        } else {
            $rootScope.loading = true;
            tenantsService.update($scope.tenant).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        }
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],46:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TenantsController = function TenantsController($scope, $rootScope, $state, tenantsService, toasterService, modalService) {
    _classCallCheck(this, TenantsController);

    var pageSize = 6;
    var pageCount = 0;

    $scope.tenants = [];

    $scope.getList = function () {
        $rootScope.loading = true;
        tenantsService.getList(pageSize, pageCount).then(function (tenants) {
            if (tenants.length < pageSize) {
                $scope.noMoreData = true;
            }
            tenants.forEach(function (tenant) {
                $scope.tenants.push(tenant);
            });
            pageCount++;
            $scope.refreshSelectedItems();

            if (!$scope.tenants.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function (tenantId) {
        tenantId ? $state.transitionTo("tenant", { id: tenantId }) : $state.transitionTo("tenant");
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.tenants.forEach(function (tenant) {
                tenant.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.tenants.some(function (tenant) {
            return tenant.selected;
        });

        $scope.everySelected = $scope.tenants.every(function (tenant) {
            return tenant.selected;
        });
    };

    $scope.remove = function (tenant) {

        modalService.showConfirmModal({
            messages: {
                title: "Eliminar Propietario",
                body: "Esta seguro que desea eliminar el propietario",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            var tenantIdList;
            if (tenant) {
                tenantIdList = [tenant.TenantId];
            } else {
                tenantIdList = $scope.tenants.map(function (tenantItem) {
                    if (tenantItem.selected) {
                        return tenantItem.TenantId;
                    }
                    return null;
                });
            }

            tenantIdList.forEach(function (tenantId) {
                tenantsService.remove(tenantId).then(function (response) {
                    if (response.status === 200) {
                        $scope.tenants.forEach(function (tenantItem) {
                            if (tenantId === tenantItem.TenantId) {
                                var index = $scope.tenants.indexOf(tenantItem);
                                $scope.tenants.splice(index, 1);
                            }
                        });
                    }
                })["catch"](function (error) {
                    toasterService.showServerError(error);
                });
            });

            $scope.refreshSelectedItems();
        });
    };

    $scope.getList();
};

exports["default"] = TenantsController;
module.exports = exports["default"];

},{}],47:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});
function TenantsService($http) {
    "use strict";

    var tenants;

    return {
        getTenant: getTenant,
        getList: getList,
        add: add,
        update: update,
        remove: remove
    };

    function getList(pageSize, pageCount) {
        var handleSuccess = function handleSuccess(response) {
            tenants = response.data;
            return tenants;
        };
        var url = '/api/tenants';
        return $http({
            method: 'GET',
            url: url,
            params: {
                pageSize: pageSize,
                pageCount: pageCount
            }
        }).then(handleSuccess);
    }

    function getTenant(tenantId) {
        var url = '/api/tenants/' + tenantId;
        return $http({
            method: "GET",
            url: url
        });
    }

    function add(tenant) {
        var url = '/api/tenants/';
        return $http({
            method: 'POST',
            url: url,
            data: tenant
        });
    }

    function update(tenant) {
        var url = "/api/tenants/";
        return $http({
            method: "PUT",
            url: url,
            data: tenant
        });
    }

    function remove(tenantId) {
        var url = '/api/tenants/' + tenantId;
        return $http({
            method: "DELETE",
            url: url
        });
    }
}

exports['default'] = TenantsService;
module.exports = exports['default'];

},{}],48:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersTenantsController = require("./controllers/tenantsController");

var _controllersTenantsController2 = _interopRequireDefault(_controllersTenantsController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesTenantsService = require("./services/tenantsService");

var _servicesTenantsService2 = _interopRequireDefault(_servicesTenantsService);

var ModuleName = "VialSoft.tenants";

angular.module(ModuleName, []).controller("tenantsController", _controllersTenantsController2["default"]).controller("tenantsdetailController", _controllersDetailController2["default"]).service("tenantsService", _servicesTenantsService2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/detailController":45,"./controllers/tenantsController":46,"./services/tenantsService":47}],49:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, transfersworksService, toasterService, modalService) {
    _classCallCheck(this, DetailController);

    $("select").select2({ width: "100%" });

    var transferworkId = $stateParams.id;
    $scope.editMode = transferworkId !== undefined;
    $scope.searchMode = false;
    $scope.showDetail = false;
    $scope.countTotal = 0;
    var tenantId;

    $scope.asset = {};

    $scope.transferworkDetail = {
        AssetId: null,
        WorkId: null
    };

    $scope.transferwork = {
        CreatedAt: new Date(),
        TenantId: 0,
        Details: []
    };

    transfersworksService.getCategory().then(function (response) {
        $scope.categories = response.data;
    });

    transfersworksService.getSubCategoryAll().then(function (response) {
        $scope.subcategories = response.data;
    });

    $scope.getSubCategory = function () {
        $scope.subcategories = null;
        transfersworksService.getSubCategory($scope.asset.CategoryId).then(function (response) {
            $scope.subcategories = response.data;
            $("#subcategories").select2({
                width: "100%"
            });
        });
    };

    transfersworksService.getProvider().then(function (response) {
        $scope.providers = response.data;
    });

    transfersworksService.getTypeReceipt().then(function (response) {
        $scope.typeReceipts = response.data;
    });

    $scope.search = function () {
        transfersworksService.getAsset($scope.asset.Id).then(function (response) {
            $scope.searchMode = true;
            $scope.asset = response.data;
        });
    };

    transfersworksService.getWork().then(function (response) {
        $scope.works = response.data;
    });

    $scope.addTransferWorkDetail = function () {
        var detail = {
            AssetId: $scope.asset.AssetId,
            WorkId: $scope.asset.WorkId
        };
        $scope.countTotal++;
        $scope.transferwork.Details.push(detail);
        $scope.showDetail = true;
    };

    if ($scope.editMode) {
        $rootScope.loading = true;
        transfersworksService.getTransferWork(transferworkId).then(function (response) {
            $scope.transferwork = response.data;
            $scope.showDetail = true;

            $scope.transferwork.Details.forEach(function () {
                $scope.countTotal++;
            });

            transfersworksService.getTypeReceipt().then(function (responseType) {
                $scope.typeReceipts = responseType.data;
                $("#typeReceipt").select2({ width: "100%" });
            });
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    }

    $scope.removeAsset = function (assetItem) {
        var index = $scope.transferwork.Details.indexOf(assetItem);
        $scope.countTotal--;
        $scope.transferwork.Details.splice(index, 1);
    };

    $scope.navigateBack = function () {
        $state.transitionTo("transfersworks");
    };

    $scope.save = function () {
        $rootScope.loading = true;
        transfersworksService.getTenant().then(function (response) {
            tenantId = response.data;

            $scope.transferwork.TenantId = tenantId;
            transfersworksService.add($scope.transferwork).then(function (responseAdd) {
                if (responseAdd.status === 200) {
                    $scope.navigateBack();
                }
            });
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],50:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TransferWorksController = function TransferWorksController($scope, $rootScope, $state, transfersworksService, toasterService) {
    _classCallCheck(this, TransferWorksController);

    var pageSize = 6;
    var pageCount = 0;

    $scope.transfersworks = [];

    $scope.getList = function () {

        $rootScope.loading = true;

        transfersworksService.getList(pageSize, pageCount).then(function (transfersworks) {
            if (transfersworks.length < pageSize) {
                $scope.noMoreData = true;
            }
            transfersworks.forEach(function (transferWork) {
                $scope.transfersworks.push(transferWork);
            });
            pageCount++;
            $scope.refreshSelectedItems();
            if (!$scope.transfersworks.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function (transferWorkId) {
        transferWorkId ? $state.transitionTo("transferwork", { id: transferWorkId }) : $state.transitionTo("transferwork");
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.transfersworks.forEach(function (transferWork) {
                transferWork.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.transfersworks.some(function (transferWork) {
            return transferWork.selected;
        });

        $scope.everySelected = $scope.transfersworks.every(function (transferWork) {
            return transferWork.selected;
        });
    };

    $scope.getList();
};

exports["default"] = TransferWorksController;
module.exports = exports["default"];

},{}],51:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function TransfersWorksService($http) {
    "use strict";

    var transfersWorks;

    return {
        enumStatus: enumStatus,
        getCategory: getCategory,
        getSubCategory: getSubCategory,
        getSubCategoryAll: getSubCategoryAll,
        getAsset: getAsset,
        getProvider: getProvider,
        getTypeReceipt: getTypeReceipt,
        getCustomer: getCustomer,
        getTenant: getTenant,
        getTransferWork: getTransferWork,
        getList: getList,
        getWork: getWork,
        add: add,
        update: update
    };

    function getWork() {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/works/getAll";
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getCustomer() {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/customers/getAll";
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function enumStatus() {
        return $http({
            method: "GET",
            url: "/api/transfersWorks/EnumStatus"
        });
    }

    function getCategory() {
        return $http({
            method: "GET",
            url: "/api/Categories"
        });
    }

    function getSubCategoryAll() {
        return $http({
            method: "GET",
            url: "/api/SubCategories"
        });
    }

    function getSubCategory(categoryId) {
        return $http({
            method: "GET",
            url: "/api/SubCategories/" + categoryId
        });
    }

    function getTypeReceipt() {
        return $http({
            method: "GET",
            url: "/api/transfersWorks/TypeReceipts"
        });
    }

    function getProvider() {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/providers/getAll";
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getAsset(id) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/assets/GetAsset";
            return $http({
                method: "GET",
                url: url,
                params: {
                    Id: id
                },
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        var handleSuccess = function handleSuccess(response) {
            transfersWorks = response.data;
            return transfersWorks;
        };

        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/transfersWorks";
            return $http({
                method: "GET",
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getTransferWork(transferworkId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/transfersWorks/" + transferworkId;
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(transferwork) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/transfersWorks/";
            return $http({
                method: "POST",
                url: url,
                data: transferwork,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function update(transferwork) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/transfersWorks/";
            return $http({
                method: "PUT",
                url: url,
                data: transferwork,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

exports["default"] = TransfersWorksService;
module.exports = exports["default"];

},{}],52:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersTransfersworksController = require("./controllers/transfersworksController");

var _controllersTransfersworksController2 = _interopRequireDefault(_controllersTransfersworksController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesTransfersworksService = require("./services/transfersworksService");

var _servicesTransfersworksService2 = _interopRequireDefault(_servicesTransfersworksService);

var ModuleName = "VialSoft.transfersworks";

angular.module(ModuleName, []).controller("transfersworksController", _controllersTransfersworksController2["default"]).controller("transfersworksdetailController", _controllersDetailController2["default"]).service("transfersworksService", _servicesTransfersworksService2["default"]);
exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/detailController":49,"./controllers/transfersworksController":50,"./services/transfersworksService":51}],53:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DetailController = function DetailController($scope, $rootScope, $stateParams, $state, worksService, toasterService, modalService) {
    _classCallCheck(this, DetailController);

    $("select").select2({ width: "100%" });

    var workId = $stateParams.id;
    $scope.editMode = workId !== undefined;
    var tenantId;

    $scope.work = {
        CreatedAt: new Date()
    };

    worksService.getCustomer().then(function (response) {
        $scope.customers = response.data;
        $scope.work.CustomerId = 1;
    });

    if ($scope.editMode) {
        $rootScope.loading = true;

        worksService.getWork(workId).then(function (response) {
            $scope.work = response.data;
            worksService.getCustomer().then(function (responseCustomer) {
                $scope.customers = responseCustomer.data;
                $("select").select2({ width: "100%" });
            });
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    } else {
        $rootScope.loading = true;
        worksService.getCustomer().then(function (responseCustomer) {
            $scope.customers = responseCustomer.data;
            $("select").select2({ width: "100%" });
        });
        worksService.getTenant().then(function (response) {
            tenantId = response.data;
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    }

    $scope.navigateBack = function () {
        $state.transitionTo("works");
    };

    $scope.removework = function () {
        modalService.showConfirmModal({
            messages: {
                title: "Eliminar Obra",
                body: "Esta seguro que desea eliminar la obra?",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            $rootScope.loading = true;
            worksService.remove(workId).then(function (response) {
                if (response.status === 200) {
                    $state.transitionTo("works");
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        });
    };

    $scope.save = function () {

        if (!$scope.editMode) {
            $scope.work.TenantId = tenantId;
            $rootScope.loading = true;

            worksService.add($scope.work).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        } else {
            $rootScope.loading = true;
            worksService.update($scope.work).then(function (response) {
                if (response.status === 200) {
                    $scope.navigateBack();
                }
            })["catch"](function (error) {
                toasterService.showServerError(error);
            })["finally"](function () {
                $rootScope.loading = false;
            });
        }
    };
};

exports["default"] = DetailController;
module.exports = exports["default"];

},{}],54:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var WorksController = function WorksController($scope, $rootScope, $state, worksService, toasterService, modalService) {
    _classCallCheck(this, WorksController);

    var pageSize = 6;
    var pageCount = 0;

    $scope.works = [];

    $scope.getList = function () {
        $rootScope.loading = true;
        worksService.getList(pageSize, pageCount).then(function (works) {
            if (works.length < pageSize) {
                $scope.noMoreData = true;
            }
            works.forEach(function (work) {
                $scope.works.push(work);
            });
            pageCount++;
            $scope.refreshSelectedItems();
            if (!$scope.works.length) {
                $scope.noData = true;
            }
        })["catch"](function (error) {
            toasterService.showServerError(error);
        })["finally"](function () {
            $rootScope.loading = false;
        });
    };

    $scope.nagivateToDetail = function (workId) {
        workId ? $state.transitionTo("work", { id: workId }) : $state.transitionTo("work");
    };

    $scope.refreshSelectedItems = function (all) {
        if (all) {
            $scope.works.forEach(function (work) {
                work.selected = $scope.everySelected;
            });
        }

        $scope.anySelected = $scope.works.some(function (work) {
            return work.selected;
        });

        $scope.everySelected = $scope.works.every(function (work) {
            return work.selected;
        });
    };

    $scope.remove = function (work) {

        modalService.showConfirmModal({
            messages: {
                title: "Eliminar la obra",
                body: "Esta seguro que desea eliminar la obra",
                ok: "Si, eliminar",
                cancel: "Cancelar"
            }
        }).then(function () {
            var workIdList;
            if (work) {
                workIdList = [work.WorkId];
            } else {
                workIdList = $scope.works.map(function (workItem) {
                    if (workItem.selected) {
                        return workItem.WorkId;
                    }
                    return null;
                });
            }

            workIdList.forEach(function (workId) {
                worksService.remove(workId).then(function (response) {
                    if (response.status === 200) {
                        $scope.works.forEach(function (workItem) {
                            if (workId === workItem.WorkId) {
                                var index = $scope.works.indexOf(workItem);
                                $scope.works.splice(index, 1);
                            }
                        });
                    }
                })["catch"](function (error) {
                    toasterService.showServerError(error);
                });
            });

            $scope.refreshSelectedItems();
        });
    };

    $scope.getList();
};

exports["default"] = WorksController;
module.exports = exports["default"];

},{}],55:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function WorksService($http) {
    "use strict";

    var works;

    return {
        getCustomer: getCustomer,
        getTenant: getTenant,
        getWork: getWork,
        getList: getList,
        add: add,
        update: update,
        remove: remove
    };

    function getCustomer() {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/customers/getAll";
            return $http({
                method: "GET",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function getTenant() {
        return $http({
            method: "GET",
            url: "/api/users/current/tenant"
        });
    }

    function getList(pageSize, pageCount) {
        var handleSuccess = function handleSuccess(response) {
            works = response.data;
            return works;
        };

        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/works";
            return $http({
                method: 'GET',
                url: url,
                params: {
                    pageSize: pageSize,
                    pageCount: pageCount
                },
                headers: {
                    TenantId: tenantId
                }
            }).then(handleSuccess);
        });
    }

    function getWork(worksId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/works/" + worksId;
            return $http({
                method: 'GET',
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function add(work) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/works/";
            return $http({
                method: "POST",
                url: url,
                data: work,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function update(work) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/works/";
            return $http({
                method: "PUT",
                url: url,
                data: work,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }

    function remove(worksId) {
        return getTenant().then(function (response) {
            var tenantId = response.data;
            var url = "/api/works/" + worksId;
            return $http({
                method: "DELETE",
                url: url,
                headers: {
                    TenantId: tenantId
                }
            });
        });
    }
}

exports["default"] = WorksService;
module.exports = exports["default"];

},{}],56:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _controllersWorksController = require("./controllers/worksController");

var _controllersWorksController2 = _interopRequireDefault(_controllersWorksController);

var _controllersDetailController = require("./controllers/detailController");

var _controllersDetailController2 = _interopRequireDefault(_controllersDetailController);

var _servicesWorksService = require("./services/worksService");

var _servicesWorksService2 = _interopRequireDefault(_servicesWorksService);

var ModuleName = "VialSoft.works";

angular.module(ModuleName, []).controller("worksController", _controllersWorksController2["default"]).controller("worksdetailController", _controllersDetailController2["default"]).service("worksService", _servicesWorksService2["default"]);

exports["default"] = ModuleName;
module.exports = exports["default"];

},{"./controllers/detailController":53,"./controllers/worksController":54,"./services/worksService":55}]},{},[1])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9hcHAuYm9vdHN0cmFwcGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvYXBwLm1vZHVsZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvYWNjZXNzb3JpZXMvYWNjZXNzb3JpZXMubW9kdWxlLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy9hY2Nlc3Nvcmllcy9jb250cm9sbGVycy9hY2Nlc3Nvcmllc0NvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL2FjY2Vzc29yaWVzL2NvbnRyb2xsZXJzL2RldGFpbENvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL2FjY2Vzc29yaWVzL3NlcnZpY2VzL2FjY2Vzc29yaWVzU2VydmljZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvYXNzZXRzL2Fzc2V0cy5tb2R1bGUuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL2Fzc2V0cy9jb250cm9sbGVycy9hc3NldHNDb250cm9sbGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy9hc3NldHMvY29udHJvbGxlcnMvZGV0YWlsQ29udHJvbGxlci5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvYXNzZXRzL3NlcnZpY2VzL2Fzc2V0c1NlcnZpY2UuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL2Nvc3RlYXNzZXQvY29udHJvbGxlcnMvY29zdGVhc3NldENvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL2Nvc3RlYXNzZXQvY29udHJvbGxlcnMvZGV0YWlsQ29udHJvbGxlci5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvY29zdGVhc3NldC9jb3N0ZWFzc2V0Lm1vZHVsZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvY29zdGVhc3NldC9zZXJ2aWNlcy9jb3N0ZWFzc2V0U2VydmljZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvY3VzdG9tZXJzL2NvbnRyb2xsZXJzL2N1c3RvbWVyc0NvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL2N1c3RvbWVycy9jb250cm9sbGVycy9kZXRhaWxDb250cm9sbGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy9jdXN0b21lcnMvY3VzdG9tZXJzLm1vZHVsZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvY3VzdG9tZXJzL2RpcmVjdGl2ZXMvZmlsZURpcmVjdGl2ZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvY3VzdG9tZXJzL3NlcnZpY2VzL2N1c3RvbWVyc1NlcnZpY2UuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL2Rhc2hib2FyZC9jb250cm9sbGVycy9kYXNoYm9hcmRDb250cm9sbGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy9kYXNoYm9hcmQvZGFzaGJvYXJkLm1vZHVsZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL2RpcmVjdGl2ZXMvTUhDaGFydERpcmVjdGl2ZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL3NlcnZpY2VzL2Rhc2hib2FyZFNlcnZpY2UuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL21haW50ZW5hbmNlY2FyZHMvY29udHJvbGxlcnMvZGV0YWlsQ29udHJvbGxlci5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvbWFpbnRlbmFuY2VjYXJkcy9jb250cm9sbGVycy9tYWludGVuYW5jZWNhcmRzQ29udHJvbGxlci5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvbWFpbnRlbmFuY2VjYXJkcy9tYWludGVuYW5jZWNhcmRzLm1vZHVsZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvbWFpbnRlbmFuY2VjYXJkcy9zZXJ2aWNlcy9tYWludGVuYW5jZWNhcmRzU2VydmljZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvcHJvdmlkZXJzL2NvbnRyb2xsZXJzL2RldGFpbENvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL3Byb3ZpZGVycy9jb250cm9sbGVycy9wcm92aWRlcnNDb250cm9sbGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy9wcm92aWRlcnMvcHJvdmlkZXJzLm1vZHVsZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvcHJvdmlkZXJzL3NlcnZpY2VzL3Byb3ZpZGVyc1NlcnZpY2UuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL3B1cmNoYXNlcy9jb250cm9sbGVycy9kZXRhaWxDb250cm9sbGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy9wdXJjaGFzZXMvY29udHJvbGxlcnMvcHVyY2hhc2VzQ29udHJvbGxlci5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvcHVyY2hhc2VzL3B1cmNoYXNlcy5tb2R1bGUuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL3B1cmNoYXNlcy9zZXJ2aWNlcy9wdXJjaGFzZXNTZXJ2aWNlLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy9zaGFyZWQvY29udHJvbGxlcnMvaGVhZGVyQ29udHJvbGxlci5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvc2hhcmVkL2RpcmVjdGl2ZXMvaGVhZGVyQmFyL2hlYWRlckJhckRpcmVjdGl2ZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvc2hhcmVkL2RpcmVjdGl2ZXMvbGVmdE1lbnUvbGVmdE1lbnVEaXJlY3RpdmUuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL3NoYXJlZC9maWx0ZXJzL2NhbWVsQ2FzZUZpbHRlci5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvc2hhcmVkL3NlcnZpY2VzL2V4Y2VwdGlvbkhhbmRsZXIuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL3NoYXJlZC9zZXJ2aWNlcy9tb2RhbEFzc2V0U2VydmljZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvc2hhcmVkL3NlcnZpY2VzL21vZGFsU2VydmljZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvc2hhcmVkL3NlcnZpY2VzL3RvYXN0ZXJTZXJ2aWNlLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy9zaGFyZWQvc2hhcmVkLm1vZHVsZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvdGVuYW50cy9jb250cm9sbGVycy9kZXRhaWxDb250cm9sbGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy90ZW5hbnRzL2NvbnRyb2xsZXJzL3RlbmFudHNDb250cm9sbGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy90ZW5hbnRzL3NlcnZpY2VzL3RlbmFudHNTZXJ2aWNlLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy90ZW5hbnRzL3RlbmFudHMubW9kdWxlLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy90cmFuc2ZlcnN3b3Jrcy9jb250cm9sbGVycy9kZXRhaWxDb250cm9sbGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy90cmFuc2ZlcnN3b3Jrcy9jb250cm9sbGVycy90cmFuc2ZlcnN3b3Jrc0NvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL3RyYW5zZmVyc3dvcmtzL3NlcnZpY2VzL3RyYW5zZmVyc3dvcmtzU2VydmljZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvdHJhbnNmZXJzd29ya3MvdHJhbnNmZXJzd29ya3MubW9kdWxlLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy93b3Jrcy9jb250cm9sbGVycy9kZXRhaWxDb250cm9sbGVyLmpzIiwiQzovVXNlcnMvZ3VzdGF2by9EZXNrdG9wL1ZpYWxTb2Z0L3NyYy9WaWFsU29mdC5XZWIvY29udGVudC9hcHAvY29tcG9uZW50cy93b3Jrcy9jb250cm9sbGVycy93b3Jrc0NvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9ndXN0YXZvL0Rlc2t0b3AvVmlhbFNvZnQvc3JjL1ZpYWxTb2Z0LldlYi9jb250ZW50L2FwcC9jb21wb25lbnRzL3dvcmtzL3NlcnZpY2VzL3dvcmtzU2VydmljZS5qcyIsIkM6L1VzZXJzL2d1c3Rhdm8vRGVza3RvcC9WaWFsU29mdC9zcmMvVmlhbFNvZnQuV2ViL2NvbnRlbnQvYXBwL2NvbXBvbmVudHMvd29ya3Mvd29ya3MubW9kdWxlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozt5QkNBMEMsY0FBYzs7OztBQUV4RCxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEVBQUUsd0JBQWdCLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7NENDQXZDLG1DQUFtQzs7OztrREFDaEMseUNBQXlDOzs7O2tEQUN6Qyx5Q0FBeUM7Ozs7MENBQzdDLGlDQUFpQzs7Ozs0Q0FDaEMsbUNBQW1DOzs7O3NEQUM5Qiw2Q0FBNkM7Ozs7OENBQ2pELHFDQUFxQzs7OztrREFDbkMseUNBQXlDOzs7OzREQUNwQyxtREFBbUQ7Ozs7a0RBQ3hELHlDQUF5Qzs7OztnRUFDbEMsdURBQXVEOzs7O29EQUM3RCwyQ0FBMkM7Ozs7QUFiM0UsSUFBSSxVQUFVLEdBQUcsVUFBVSxDQUFDOztBQWdCN0IsSUFBSSxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxXQUFXLEVBQUUsV0FBVyxtbEJBSWhCLENBQUMsQ0FBQzs7QUFFaEQsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzs7QUFFbkIsU0FBUyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixFQUFFLGdCQUFnQixFQUFFOztBQUVsRSxRQUFNLFVBQVUsR0FBRyxHQUFHLENBQUM7O0FBRXZCLG9CQUFnQixDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDOztBQUV6QyxzQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFDOztBQUV4QyxzQkFBa0IsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7O0FBRXJDLGtCQUFjLENBQ1QsS0FBSyxDQUFDLFdBQVcsRUFBRTtBQUNoQixXQUFHLEVBQUUsR0FBRztBQUNSLG1CQUFXLEVBQUUsMkNBQTJDO0FBQ3hELGtCQUFVLEVBQUMscUJBQXFCO0tBQ25DLENBQUMsQ0FDRCxLQUFLLENBQUMsV0FBVyxFQUFFO0FBQ2hCLFdBQUcsRUFBRSxZQUFZO0FBQ2pCLG1CQUFXLEVBQUUsMkNBQTJDO0FBQ3hELGtCQUFVLEVBQUMscUJBQXFCO0tBQ25DLENBQUMsQ0FDRCxLQUFLLENBQUMsVUFBVSxFQUFFO0FBQ2YsV0FBRyxFQUFFLGNBQWM7QUFDbkIsbUJBQVcsRUFBRSw2Q0FBNkM7QUFDMUQsa0JBQVUsRUFBQywyQkFBMkI7S0FDekMsQ0FBQyxDQUNELEtBQUssQ0FBQyxPQUFPLEVBQUU7QUFDWixXQUFHLEVBQUUsUUFBUTtBQUNiLG1CQUFXLEVBQUUsdUNBQXVDO0FBQ3BELGtCQUFVLEVBQUMsaUJBQWlCO0tBQy9CLENBQUMsQ0FDRCxLQUFLLENBQUMsTUFBTSxFQUFFO0FBQ1gsV0FBRyxFQUFFLFVBQVU7QUFDZixtQkFBVyxFQUFFLHlDQUF5QztBQUN0RCxrQkFBVSxFQUFDLHVCQUF1QjtLQUNyQyxDQUFDLENBQ0QsS0FBSyxDQUFDLFFBQVEsRUFBRTtBQUNiLFdBQUcsRUFBRSxTQUFTO0FBQ2QsbUJBQVcsRUFBRSx3Q0FBd0M7QUFDckQsa0JBQVUsRUFBQyxrQkFBa0I7S0FDaEMsQ0FBQyxDQUNELEtBQUssQ0FBQyxPQUFPLEVBQUU7QUFDWixXQUFHLEVBQUUsV0FBVztBQUNoQixtQkFBVyxFQUFFLDBDQUEwQztBQUN2RCxrQkFBVSxFQUFDLHdCQUF3QjtLQUN0QyxDQUFDLENBQ0QsS0FBSyxDQUFDLGFBQWEsRUFBRTtBQUNsQixXQUFHLEVBQUUsaUJBQWlCO0FBQ3RCLG1CQUFXLEVBQUUsNkNBQTZDO0FBQzFELGtCQUFVLEVBQUMsdUJBQXVCO0tBQ3JDLENBQUMsQ0FDRCxLQUFLLENBQUMsV0FBVyxFQUFFO0FBQ2hCLFdBQUcsRUFBRSxlQUFlO0FBQ3BCLG1CQUFXLEVBQUUsK0NBQStDO0FBQzVELGtCQUFVLEVBQUMsNkJBQTZCO0tBQzNDLENBQUMsQ0FDRCxLQUFLLENBQUMsU0FBUyxFQUFFO0FBQ2QsV0FBRyxFQUFFLFVBQVU7QUFDZixtQkFBVyxFQUFFLHlDQUF5QztBQUN0RCxrQkFBVSxFQUFDLG1CQUFtQjtLQUNqQyxDQUFDLENBQ0QsS0FBSyxDQUFDLFFBQVEsRUFBRTtBQUNiLFdBQUcsRUFBRSxZQUFZO0FBQ2pCLG1CQUFXLEVBQUUsMkNBQTJDO0FBQ3hELGtCQUFVLEVBQUMseUJBQXlCO0tBQ3ZDLENBQUMsQ0FDRCxLQUFLLENBQUMsV0FBVyxFQUFFO0FBQ2hCLFdBQUcsRUFBRSxZQUFZO0FBQ2pCLG1CQUFXLEVBQUUsMkNBQTJDO0FBQ3hELGtCQUFVLEVBQUMscUJBQXFCO0tBQ25DLENBQUMsQ0FDRCxLQUFLLENBQUMsVUFBVSxFQUFFO0FBQ2YsV0FBRyxFQUFFLGNBQWM7QUFDbkIsbUJBQVcsRUFBRSw2Q0FBNkM7QUFDMUQsa0JBQVUsRUFBQywyQkFBMkI7S0FDekMsQ0FBQyxDQUNELEtBQUssQ0FBQyxnQkFBZ0IsRUFBRTtBQUNyQixXQUFHLEVBQUUsaUJBQWlCO0FBQ3RCLG1CQUFXLEVBQUUsZ0RBQWdEO0FBQzdELGtCQUFVLEVBQUMsMEJBQTBCO0tBQ3hDLENBQUMsQ0FDRCxLQUFLLENBQUMsY0FBYyxFQUFFO0FBQ25CLFdBQUcsRUFBRSxrQkFBa0I7QUFDdkIsbUJBQVcsRUFBRSxrREFBa0Q7QUFDL0Qsa0JBQVUsRUFBQyxnQ0FBZ0M7S0FDOUMsQ0FBQyxDQUNELEtBQUssQ0FBQyxXQUFXLEVBQUU7QUFDaEIsV0FBRyxFQUFFLFlBQVk7QUFDakIsbUJBQVcsRUFBRSwyQ0FBMkM7QUFDeEQsa0JBQVUsRUFBQyxxQkFBcUI7S0FDbkMsQ0FBQyxDQUNELEtBQUssQ0FBQyxVQUFVLEVBQUU7QUFDZixXQUFHLEVBQUUsY0FBYztBQUNuQixtQkFBVyxFQUFFLDZDQUE2QztBQUMxRCxrQkFBVSxFQUFDLDJCQUEyQjtLQUN6QyxDQUFDLENBQ0QsS0FBSyxDQUFDLGtCQUFrQixFQUFFO0FBQ3ZCLFdBQUcsRUFBRSxzQkFBc0I7QUFDM0IsbUJBQVcsRUFBRSxrREFBa0Q7QUFDL0Qsa0JBQVUsRUFBQyw0QkFBNEI7S0FDMUMsQ0FBQyxDQUNELEtBQUssQ0FBQyxpQkFBaUIsRUFBRTtBQUN0QixXQUFHLEVBQUUscUJBQXFCO0FBQzFCLG1CQUFXLEVBQUUsb0RBQW9EO0FBQ2pFLGtCQUFVLEVBQUMsa0NBQWtDO0tBQ2hELENBQUMsQ0FDRCxLQUFLLENBQUMsYUFBYSxFQUFFO0FBQ2xCLFdBQUcsRUFBRSxjQUFjO0FBQ25CLG1CQUFXLEVBQUUsNENBQTRDO0FBQ3pELGtCQUFVLEVBQUMsc0JBQXNCO0tBQ3BDLENBQUMsQ0FDRCxLQUFLLENBQUMsWUFBWSxFQUFFO0FBQ2pCLFdBQUcsRUFBRSxnQkFBZ0I7QUFDckIsbUJBQVcsRUFBRSw4Q0FBOEM7QUFDM0Qsa0JBQVUsRUFBQyw0QkFBNEI7S0FDMUMsQ0FBQyxDQUNELEtBQUssQ0FBQyxPQUFPLEVBQUU7QUFDWixXQUFHLEVBQUUsTUFBTTtBQUNYLG1CQUFXLEVBQUUseUNBQXlDO0tBQ3pELENBQUMsQ0FBQztDQUNWOztxQkFFYyxVQUFVOzs7Ozs7Ozs7Ozs7Z0RDaEpTLHFDQUFxQzs7OzsyQ0FDMUMsZ0NBQWdDOzs7OzBDQUM5QiwrQkFBK0I7Ozs7QUFKN0QsSUFBSSxVQUFVLEdBQUcsc0JBQXNCLENBQUM7O0FBTXpDLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUMxQixVQUFVLENBQUMsdUJBQXVCLGdEQUF3QixDQUMxRCxVQUFVLENBQUMsNkJBQTZCLDJDQUFtQixDQUMzRCxPQUFPLENBQUMsb0JBQW9CLDBDQUFxQixDQUFDOztxQkFFdkMsVUFBVTs7Ozs7Ozs7Ozs7O0lDWGxCLHFCQUFxQixHQUNiLFNBRFIscUJBQXFCLENBQ1osTUFBTSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLGtCQUFrQixFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQUU7MEJBRHJHLHFCQUFxQjs7QUFHcEIsUUFBSSxPQUFPLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQzs7QUFFOUIsUUFBTSxRQUFRLEdBQUcsQ0FBQyxDQUFDO0FBQ25CLFFBQUksU0FBUyxHQUFHLENBQUMsQ0FBQzs7QUFFbEIsVUFBTSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7QUFDekIsVUFBTSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7O0FBRXhCLFVBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBTTtBQUNuQixrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsMEJBQWtCLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQ25ELElBQUksQ0FBQyxVQUFDLFdBQVcsRUFBSztBQUNuQixnQkFBSSxXQUFXLENBQUMsTUFBTSxHQUFHLFFBQVEsRUFBRTtBQUMvQixzQkFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDNUI7QUFDRCx1QkFBVyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFNBQVMsRUFBSztBQUMvQixzQkFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDdEMsQ0FBQyxDQUFDO0FBQ0gscUJBQVMsRUFBRyxDQUFDO0FBQ2Isa0JBQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDOztBQUU5QixnQkFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFO0FBQzVCLHNCQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzthQUN4QjtTQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVixDQUFDOztBQUVGLFVBQU0sQ0FBQyxnQkFBZ0IsR0FBRyxZQUFNO0FBQzVCLGNBQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEVBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0tBQzVELENBQUM7O0FBRUYsVUFBTSxDQUFDLGlCQUFpQixHQUFHLFlBQU07QUFDN0IsY0FBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsRUFBQyxFQUFFLEVBQUcsTUFBTSxDQUFDLE9BQU8sRUFBQyxDQUFDLENBQUM7S0FDdkQsQ0FBQzs7QUFFRixVQUFNLENBQUMsb0JBQW9CLEdBQUcsVUFBQyxHQUFHLEVBQUs7QUFDbkMsWUFBSSxHQUFHLEVBQUU7QUFDTCxrQkFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQyxTQUFTLEVBQUs7QUFDdEMseUJBQVMsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQzthQUM3QyxDQUFDLENBQUM7U0FDTjs7QUFFRCxjQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUMsU0FBUyxFQUFLO0FBQ3hELG1CQUFPLFNBQVMsQ0FBQyxRQUFRLENBQUM7U0FDN0IsQ0FBQyxDQUFDOztBQUVILGNBQU0sQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsVUFBQyxTQUFTLEVBQUs7QUFDM0QsbUJBQU8sU0FBUyxDQUFDLFFBQVEsQ0FBQztTQUM3QixDQUFDLENBQUM7S0FDTixDQUFDOztBQUVGLFVBQU0sQ0FBQyxNQUFNLEdBQUcsVUFBQyxTQUFTLEVBQUs7O0FBRTNCLG9CQUFZLENBQUMsZ0JBQWdCLENBQUM7QUFDMUIsb0JBQVEsRUFBRTtBQUNOLHFCQUFLLHNCQUFzQjtBQUMzQixvQkFBSSwrQ0FBK0M7QUFDbkQsa0JBQUUsRUFBRSxjQUFjO0FBQ2xCLHNCQUFNLEVBQUUsVUFBVTthQUNyQjtTQUNKLENBQUMsQ0FDRCxJQUFJLENBQUMsWUFBTTtBQUNSLGdCQUFJLGVBQWUsQ0FBQztBQUNwQixnQkFBSSxTQUFTLEVBQUU7QUFDWCwrQkFBZSxHQUFHLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQzdDLE1BQU07QUFDSCwrQkFBZSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQy9CLEdBQUcsQ0FBQyxVQUFDLGFBQWEsRUFBSztBQUNwQix3QkFBSSxhQUFhLENBQUMsUUFBUSxFQUFFO0FBQ3hCLCtCQUFPLGFBQWEsQ0FBQyxXQUFXLENBQUM7cUJBQ3BDO0FBQ0QsMkJBQU8sSUFBSSxDQUFDO2lCQUNmLENBQUMsQ0FBQzthQUNWOztBQUVELDJCQUFlLENBQUMsT0FBTyxDQUFDLFVBQUMsV0FBVyxFQUFLO0FBQ3JDLGtDQUFrQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FDakMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLHdCQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDhCQUFNLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFDLGFBQWEsRUFBSztBQUMxQyxnQ0FBSSxXQUFXLEtBQUssYUFBYSxDQUFDLFdBQVcsRUFBRTtBQUMzQyxvQ0FBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7QUFDdEQsc0NBQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQzs2QkFDdkM7eUJBQ0osQ0FBQyxDQUFDO3FCQUNOO2lCQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2Qsa0NBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3pDLENBQUMsQ0FBQzthQUNWLENBQUMsQ0FBQzs7QUFFSCxrQkFBTSxDQUFDLG9CQUFvQixFQUFFLENBQUM7U0FFakMsQ0FBQyxDQUFDO0tBQ04sQ0FBQzs7QUFFRixVQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7Q0FDcEI7O3FCQUlVLHFCQUFxQjs7Ozs7Ozs7Ozs7O0lDL0c3QixnQkFBZ0IsR0FDUixTQURSLGdCQUFnQixDQUNQLE1BQU0sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxrQkFBa0IsRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFOzBCQURyRyxnQkFBZ0I7O0FBR2YsUUFBSSxXQUFXLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQzs7QUFFbEMsUUFBSSxRQUFRLENBQUM7QUFDYixVQUFNLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzs7QUFFeEIsVUFBTSxDQUFDLFNBQVMsR0FBRztBQUNmLGVBQU8sRUFBRSxZQUFZLENBQUMsRUFBRTtLQUMzQixDQUFDOztBQUVGLFFBQUksTUFBTSxDQUFDLFFBQVEsRUFBRTtBQUNqQixrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsMEJBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUN2QyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsa0JBQU0sQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUNqQyxrQkFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLDhCQUE0QixNQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sQUFBRSxDQUFDO1NBQ2xGLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVixNQUFNO0FBQ0gsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLDBCQUFrQixDQUFDLFNBQVMsRUFBRSxDQUN6QixJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDckIsb0JBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1NBQzVCLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVjs7QUFFRCxVQUFNLENBQUMsWUFBWSxHQUFHLFlBQU07QUFDeEIsY0FBTSxDQUFDLFlBQVksQ0FBQyxhQUFhLEVBQUUsRUFBQyxFQUFFLEVBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUMsQ0FBQyxDQUFDO0tBQ3ZFLENBQUM7O0FBRUYsVUFBTSxDQUFDLGVBQWUsR0FBRyxZQUFNO0FBQzNCLG9CQUFZLENBQUMsZ0JBQWdCLENBQUM7QUFDMUIsb0JBQVEsRUFBRTtBQUNOLHFCQUFLLEVBQUUsb0JBQW9CO0FBQzNCLG9CQUFJLEVBQUUsOENBQThDO0FBQ3BELGtCQUFFLEVBQUUsY0FBYztBQUNsQixzQkFBTSxFQUFFLFVBQVU7YUFDckI7U0FDSixDQUFDLENBQ0YsSUFBSSxDQUFDLFlBQU07QUFDUixzQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsOEJBQWtCLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUNqQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsb0JBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7QUFDekIsMEJBQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7aUJBQ3RDO2FBQ0osQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCw4QkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsMEJBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQzlCLENBQUMsQ0FBQztTQUNWLENBQUMsQ0FBQztLQUNMLENBQUM7O0FBRUYsVUFBTSxDQUFDLElBQUksR0FBRyxZQUFNO0FBQ2hCLFlBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUU7QUFDMUIsa0JBQU0sQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNyRTs7QUFFRCxZQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtBQUNsQixrQkFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0FBQ3JDLHNCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7QUFFMUIsOEJBQWtCLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FDbkMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLG9CQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDBCQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7aUJBQ3pCO2FBQ0osQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCw4QkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsMEJBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQzlCLENBQUMsQ0FBQztTQUNWLE1BQU07QUFDSCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsOEJBQWtCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FDdEMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLG9CQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDBCQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7aUJBQ3pCO2FBQ0osQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCw4QkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsMEJBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQzlCLENBQUMsQ0FBQztTQUNWO0tBQ0osQ0FBQztDQUNMOztxQkFHVSxnQkFBZ0I7Ozs7Ozs7OztBQzdHOUIsU0FBUyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUU7QUFDaEMsZ0JBQVksQ0FBQzs7QUFFYixRQUFJLFdBQVcsQ0FBQzs7QUFFaEIsV0FBTztBQUNILGlCQUFTLEVBQVQsU0FBUztBQUNULG9CQUFZLEVBQVosWUFBWTtBQUNaLGVBQU8sRUFBUCxPQUFPO0FBQ1AsV0FBRyxFQUFILEdBQUc7QUFDSCxjQUFNLEVBQU4sTUFBTTtBQUNOLGNBQU0sRUFBTixNQUFNO0tBQ1QsQ0FBQzs7QUFFRixhQUFTLFNBQVMsR0FBRztBQUNqQixlQUFPLEtBQUssQ0FBQztBQUNULGtCQUFNLEVBQUUsS0FBSztBQUNiLGVBQUcsRUFBRSwyQkFBMkI7U0FDbkMsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUU7QUFDM0MsWUFBSSxhQUFhLEdBQUcsU0FBaEIsYUFBYSxDQUFJLFFBQVEsRUFBSztBQUM5Qix1QkFBVyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDNUIsbUJBQU8sV0FBVyxDQUFDO1NBQ3RCLENBQUM7O0FBRUYsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDdkMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyx5QkFBdUIsT0FBTyxBQUFFLENBQUM7QUFDeEMsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isc0JBQU0sRUFBRTtBQUNKLDRCQUFRLEVBQUUsUUFBUTtBQUNsQiw2QkFBUyxFQUFDLFNBQVM7aUJBQ3RCO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzFCLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsWUFBWSxDQUFDLFdBQVcsRUFBRTtBQUMvQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLHlCQUF1QixXQUFXLEFBQUUsQ0FBQztBQUM1QyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsR0FBRyxDQUFDLFNBQVMsRUFBRTtBQUNwQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsbUJBQW1CLENBQUM7QUFDOUIsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxNQUFNO0FBQ2QsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isb0JBQUksRUFBRSxTQUFTO0FBQ2YsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLE1BQU0sQ0FBQyxTQUFTLEVBQUU7QUFDdkIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDdkMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyxHQUFHLG1CQUFtQixDQUFDO0FBQzlCLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLG9CQUFJLEVBQUUsU0FBUztBQUNmLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxNQUFNLENBQUMsV0FBVyxFQUFFO0FBQ3pCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcseUJBQXVCLFdBQVcsQUFBRSxDQUFDO0FBQzVDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsUUFBUTtBQUNoQixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOO0NBQ0o7O3FCQUVjLGtCQUFrQjs7Ozs7Ozs7Ozs7OzJDQ3JHSixnQ0FBZ0M7Ozs7MkNBQ2hDLGdDQUFnQzs7OztxQ0FDbkMsMEJBQTBCOzs7O0FBSm5ELElBQUksVUFBVSxHQUFHLGlCQUFpQixDQUFDOztBQU1wQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FDMUIsVUFBVSxDQUFDLGtCQUFrQiwyQ0FBbUIsQ0FDaEQsVUFBVSxDQUFDLHdCQUF3QiwyQ0FBbUIsQ0FDdEQsT0FBTyxDQUFDLGVBQWUscUNBQWdCLENBQUM7O3FCQUU3QixVQUFVOzs7Ozs7Ozs7Ozs7SUNYbEIsZ0JBQWdCLEdBQ1IsU0FEUixnQkFBZ0IsQ0FDUCxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRTswQkFEbEYsZ0JBQWdCOztBQUdmLFFBQU0sUUFBUSxHQUFHLENBQUMsQ0FBQztBQUNuQixRQUFJLFNBQVMsR0FBRyxDQUFDLENBQUM7O0FBRWxCLFVBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDOztBQUVuQixVQUFNLENBQUMsT0FBTyxHQUFHLFlBQU07O0FBRW5CLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7QUFFMUIscUJBQWEsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUNyQyxJQUFJLENBQUMsVUFBQyxNQUFNLEVBQUs7QUFDZCxnQkFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLFFBQVEsRUFBRTtBQUMxQixzQkFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDNUI7QUFDRCxrQkFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQUssRUFBSztBQUN0QixzQkFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDN0IsQ0FBQyxDQUFDO0FBQ0gscUJBQVMsRUFBRyxDQUFDO0FBQ2Isa0JBQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0FBQzlCLGdCQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7QUFDdkIsc0JBQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2FBQ3hCO1NBQ0osQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCwwQkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsc0JBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQzlCLENBQUMsQ0FBQztLQUNWLENBQUM7O0FBRUYsVUFBTSxDQUFDLGdCQUFnQixHQUFHLFVBQUMsT0FBTyxFQUFLO0FBQ25DLGVBQU8sR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7S0FDMUYsQ0FBQzs7QUFFRixVQUFNLENBQUMsa0JBQWtCLEdBQUcsWUFBTTtBQUM5QixjQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0tBQ25DLENBQUE7O0FBRUQsVUFBTSxDQUFDLG9CQUFvQixHQUFHLFVBQUMsR0FBRyxFQUFLO0FBQ25DLFlBQUksR0FBRyxFQUFFO0FBQ0wsa0JBQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQzdCLHFCQUFLLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7YUFDekMsQ0FBQyxDQUFDO1NBQ047O0FBRUQsY0FBTSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUMvQyxtQkFBTyxLQUFLLENBQUMsUUFBUSxDQUFDO1NBQ3pCLENBQUMsQ0FBQzs7QUFFSCxjQUFNLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2xELG1CQUFPLEtBQUssQ0FBQyxRQUFRLENBQUM7U0FDekIsQ0FBQyxDQUFDO0tBQ04sQ0FBQzs7QUFFRixVQUFNLENBQUMsTUFBTSxHQUFHLFVBQUMsS0FBSyxFQUFLOztBQUV2QixvQkFBWSxDQUFDLGdCQUFnQixDQUFDO0FBQzFCLG9CQUFRLEVBQUU7QUFDTixxQkFBSyxvQkFBb0I7QUFDekIsb0JBQUksMENBQTBDO0FBQzlDLGtCQUFFLEVBQUUsY0FBYztBQUNsQixzQkFBTSxFQUFFLFVBQVU7YUFDckI7U0FDSixDQUFDLENBQ0QsSUFBSSxDQUFDLFlBQU07QUFDUixnQkFBSSxXQUFXLENBQUM7QUFDaEIsZ0JBQUksS0FBSyxFQUFFO0FBQ1AsMkJBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNqQyxNQUFNO0FBQ0gsMkJBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUN0QixHQUFHLENBQUMsVUFBQyxTQUFTLEVBQUs7QUFDaEIsd0JBQUksU0FBUyxDQUFDLFFBQVEsRUFBRTtBQUNwQiwrQkFBTyxTQUFTLENBQUMsT0FBTyxDQUFDO3FCQUM1QjtBQUNELDJCQUFPLElBQUksQ0FBQztpQkFDZixDQUFDLENBQUM7YUFDVjs7QUFFRCx1QkFBVyxDQUFDLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBSztBQUM3Qiw2QkFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FDeEIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLHdCQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDhCQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFNBQVMsRUFBSztBQUNqQyxnQ0FBSSxPQUFPLEtBQUssU0FBUyxDQUFDLE9BQU8sRUFBRTtBQUMvQixvQ0FBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDN0Msc0NBQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQzs2QkFDbEM7eUJBQ0osQ0FBQyxDQUFDO3FCQUNOO2lCQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2Qsa0NBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3pDLENBQUMsQ0FBQzthQUNWLENBQUMsQ0FBQzs7QUFFSCxrQkFBTSxDQUFDLG9CQUFvQixFQUFFLENBQUM7U0FFakMsQ0FBQyxDQUFDO0tBQ04sQ0FBQzs7QUFFRixVQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7Q0FDcEI7O3FCQUdVLGdCQUFnQjs7Ozs7Ozs7Ozs7O0lDNUd4QixnQkFBZ0IsR0FDUixTQURSLGdCQUFnQixDQUNQLE1BQU0sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRTswQkFEaEcsZ0JBQWdCOztBQUdmLEtBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQzs7QUFFdkMsUUFBSSxPQUFPLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQztBQUM5QixVQUFNLENBQUMsUUFBUSxHQUFHLE9BQU8sS0FBSyxTQUFTLENBQUM7QUFDeEMsVUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7O0FBRXZCLFFBQUksUUFBUSxDQUFDOztBQUViLFVBQU0sQ0FBQyxLQUFLLEdBQUc7QUFDWCxpQkFBUyxFQUFFLElBQUksSUFBSSxFQUFFO0FBQ3JCLG1CQUFXLEVBQUUsQ0FBQztLQUNqQixDQUFDOztBQUVGLGlCQUFhLENBQUMsV0FBVyxFQUFFLENBQ3RCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixjQUFNLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDakMsY0FBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO0tBQy9CLENBQUMsQ0FBQzs7QUFFUCxpQkFBYSxDQUFDLFdBQVcsRUFBRSxDQUN0QixJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsY0FBTSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0tBQ3JDLENBQUMsQ0FBQzs7QUFFUCxpQkFBYSxDQUFDLGlCQUFpQixFQUFFLENBQzVCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixjQUFNLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7S0FDeEMsQ0FBQyxDQUFDOztBQUVQLFVBQU0sQ0FBQyxjQUFjLEdBQUcsWUFBTTtBQUMxQixZQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsV0FBVyxLQUFLLENBQUMsRUFBRTtBQUNoQyxrQkFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7U0FDMUIsTUFBTTtBQUNILGtCQUFNLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzs7QUFFeEIsYUFBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUNwQixxQkFBSyxFQUFFLE1BQU07YUFDaEIsQ0FBQyxDQUFDO1NBQ047S0FDSixDQUFBOztBQUVELFVBQU0sQ0FBQyxjQUFjLEdBQUcsWUFBTTtBQUMxQixjQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztBQUM1QixxQkFBYSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUNoRCxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsa0JBQU0sQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUNyQyxhQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxPQUFPLENBQUM7QUFDeEIscUJBQUssRUFBRSxNQUFNO2FBQ2hCLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNWLENBQUE7O0FBRUQsaUJBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FDckIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGNBQU0sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztLQUNsQyxDQUFDLENBQUM7O0FBRVAsaUJBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FDbEIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGNBQU0sQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztLQUNoQyxDQUFDLENBQUM7O0FBRVAsUUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO0FBQ2pCLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQixxQkFBYSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FDMUIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGtCQUFNLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0Isa0JBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyw4QkFBNEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEFBQUUsQ0FBQztBQUN2RSx5QkFBYSxDQUFDLFdBQVcsRUFBRSxDQUN0QixJQUFJLENBQUMsVUFBQyxnQkFBZ0IsRUFBSztBQUN4QixzQkFBTSxDQUFDLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7QUFDMUMsaUJBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztBQUM1QyxpQkFBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO0FBQ3ZDLGlCQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7QUFDMUMsNkJBQWEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FDaEQsSUFBSSxDQUFDLFVBQUMsbUJBQW1CLEVBQUs7QUFDM0IsMEJBQU0sQ0FBQyxhQUFhLEdBQUcsbUJBQW1CLENBQUMsSUFBSSxDQUFDO0FBQ2hELHFCQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztpQkFDbEQsQ0FBQyxDQUFDO2FBQ1YsQ0FBQyxDQUFDO1NBRVYsQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCwwQkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsc0JBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQzlCLENBQUMsQ0FBQztLQUNWLE1BQU07O0FBRUgscUJBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FDZCxJQUFJLENBQUMsVUFBQyxnQkFBZ0IsRUFBSztBQUN4QixrQkFBTSxDQUFDLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7QUFDMUMsYUFBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO0FBQzVDLGFBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztBQUN2QyxhQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7QUFDMUMseUJBQWEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FDaEQsSUFBSSxDQUFDLFVBQUMsbUJBQW1CLEVBQUs7QUFDM0Isc0JBQU0sQ0FBQyxhQUFhLEdBQUcsbUJBQW1CLENBQUMsSUFBSSxDQUFDO0FBQ2hELGlCQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQzthQUNsRCxDQUFDLENBQUM7U0FDVixDQUFDLENBQUM7O0FBRWYscUJBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FDckIsSUFBSSxDQUFDLFVBQUMsY0FBYyxFQUFLO0FBQ3RCLGtCQUFNLENBQUMsT0FBTyxHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUM7QUFDckMsYUFBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO1NBQzdDLENBQUMsQ0FBQzs7QUFFUCxrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIscUJBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FDcEIsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3JCLG9CQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztTQUM1QixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBRVY7O0FBRUQsVUFBTSxDQUFDLFlBQVksR0FBRyxZQUFNO0FBQ3hCLGNBQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDakMsQ0FBQzs7QUFFRixVQUFNLENBQUMseUJBQXlCLEdBQUcsVUFBQyxFQUFFLEVBQUs7QUFDdkMsY0FBTSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0tBQ3ZELENBQUM7O0FBR0YsVUFBTSxDQUFDLHFCQUFxQixHQUFHLFVBQUMsRUFBRSxFQUFLO0FBQ25DLGNBQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7S0FDbEQsQ0FBQzs7QUFFRixVQUFNLENBQUMsV0FBVyxHQUFHLFlBQU07QUFDdkIsb0JBQVksQ0FBQyxnQkFBZ0IsQ0FBQztBQUN0QixvQkFBUSxFQUFFO0FBQ04scUJBQUssRUFBRSxrQkFBa0I7QUFDekIsb0JBQUksRUFBRSx5Q0FBeUM7QUFDL0Msa0JBQUUsRUFBRSxjQUFjO0FBQ2xCLHNCQUFNLEVBQUUsVUFBVTthQUNyQjtTQUNKLENBQUMsQ0FDRCxJQUFJLENBQUMsWUFBTTtBQUNSLHNCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQix5QkFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FDeEIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLG9CQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDBCQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUNqQzthQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsOEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLDBCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDVixDQUFDLENBQUM7S0FDVixDQUFDOztBQUVGLFVBQU0sQ0FBQyxJQUFJLEdBQUcsWUFBTTtBQUNoQixZQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO0FBQ3RCLGtCQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDN0Q7O0FBRUQsWUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7QUFDbEIsa0JBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztBQUNqQyxzQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7O0FBRTFCLHlCQUFhLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FDMUIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLG9CQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDBCQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7aUJBQ3pCO2FBQ0osQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCw4QkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsMEJBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQzlCLENBQUMsQ0FBQztTQUNWLE1BQU07QUFDSCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIseUJBQWEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUM3QixJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsb0JBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7QUFDekIsMEJBQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDekI7YUFDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDhCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCwwQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDOUIsQ0FBQyxDQUFDO1NBQ1Y7S0FDSixDQUFDO0NBQ0w7O3FCQUdVLGdCQUFnQjs7Ozs7Ozs7O0FDN005QixTQUFTLGFBQWEsQ0FBQyxLQUFLLEVBQUU7QUFDM0IsZ0JBQVksQ0FBQzs7QUFFYixRQUFJLE1BQU0sQ0FBQzs7QUFFWCxXQUFPO0FBQ0gsa0JBQVUsRUFBVixVQUFVO0FBQ1YsbUJBQVcsRUFBWCxXQUFXO0FBQ1gsc0JBQWMsRUFBZCxjQUFjO0FBQ2QseUJBQWlCLEVBQWpCLGlCQUFpQjtBQUNqQixtQkFBVyxFQUFYLFdBQVc7QUFDWCxpQkFBUyxFQUFULFNBQVM7QUFDVCxlQUFPLEVBQVAsT0FBTztBQUNQLGdCQUFRLEVBQVIsUUFBUTtBQUNSLGVBQU8sRUFBUCxPQUFPO0FBQ1AsV0FBRyxFQUFILEdBQUc7QUFDSCxjQUFNLEVBQU4sTUFBTTtBQUNOLGNBQU0sRUFBTixNQUFNO0tBQ1QsQ0FBQzs7QUFFRixhQUFTLFdBQVcsR0FBRTtBQUNsQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLDBCQUEwQixDQUFDO0FBQ2xDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxPQUFPLEdBQUU7QUFDZCxlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLHNCQUFzQixDQUFDO0FBQzlCLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxVQUFVLEdBQUc7QUFDbEIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLEVBQUUsd0JBQXdCO1NBQ2hDLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsV0FBVyxHQUFHO0FBQ25CLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxFQUFFLGlCQUFpQjtTQUN6QixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLGlCQUFpQixHQUFHO0FBQ3pCLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxzQkFBc0I7U0FDNUIsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxjQUFjLENBQUMsVUFBVSxFQUFFO0FBQ2hDLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRywwQkFBd0IsVUFBVSxBQUFFO1NBQzFDLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsU0FBUyxHQUFHO0FBQ2pCLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxFQUFFLDJCQUEyQjtTQUNuQyxDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLE9BQU8sQ0FBQyxRQUFRLEVBQUUsU0FBUyxFQUFFO0FBQ2xDLFlBQUksYUFBYSxHQUFHLFNBQWhCLGFBQWEsQ0FBSSxRQUFRLEVBQUs7QUFDOUIsa0JBQU0sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQ3ZCLG1CQUFPLE1BQU0sQ0FBQztTQUNqQixDQUFDOztBQUVGLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsR0FBRyxhQUFhLENBQUM7QUFDeEIsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isc0JBQU0sRUFBRTtBQUNKLDRCQUFRLEVBQUUsUUFBUTtBQUNsQiw2QkFBUyxFQUFDLFNBQVM7aUJBQ3RCO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzFCLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsUUFBUSxDQUFDLE9BQU8sRUFBRTtBQUN2QixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLG9CQUFrQixPQUFPLEFBQUUsQ0FBQztBQUNuQyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsR0FBRyxDQUFDLEtBQUssRUFBRTtBQUNoQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsY0FBYyxDQUFDO0FBQ3pCLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsTUFBTTtBQUNkLG1CQUFHLEVBQUUsR0FBRztBQUNSLG9CQUFJLEVBQUUsS0FBSztBQUNYLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxNQUFNLENBQUMsS0FBSyxFQUFFO0FBQ25CLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsR0FBRyxjQUFjLENBQUM7QUFDekIsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isb0JBQUksRUFBRSxLQUFLO0FBQ1gsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLE1BQU0sQ0FBQyxPQUFPLEVBQUU7QUFDckIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDdkMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyxvQkFBa0IsT0FBTyxBQUFFLENBQUM7QUFDbkMsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxRQUFRO0FBQ2hCLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047Q0FFSjs7cUJBRWMsYUFBYTs7Ozs7Ozs7Ozs7O0lDdEtyQixvQkFBb0IsR0FDWixTQURSLG9CQUFvQixDQUNYLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixFQUFFLGNBQWMsRUFBRTswQkFEeEUsb0JBQW9COztBQUduQixRQUFNLFFBQVEsR0FBRyxDQUFDLENBQUM7QUFDbkIsUUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDOztBQUVsQixVQUFNLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQzs7QUFFeEIsVUFBTSxDQUFDLE9BQU8sR0FBRyxZQUFNOztBQUVuQixrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7O0FBRTFCLHlCQUFpQixDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsU0FBUyxDQUFDLENBQ3pDLElBQUksQ0FBQyxVQUFDLFdBQVcsRUFBSztBQUNuQixnQkFBSSxXQUFXLENBQUMsTUFBTSxHQUFHLFFBQVEsRUFBRTtBQUMvQixzQkFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDNUI7QUFDRCx1QkFBVyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFVBQVUsRUFBSztBQUNoQyxzQkFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDdkMsQ0FBQyxDQUFDO0FBQ0gscUJBQVMsRUFBRyxDQUFDO0FBQ2Isa0JBQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0FBQzlCLGdCQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUU7QUFDNUIsc0JBQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2FBQ3hCO1NBQ0osQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCwwQkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsc0JBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQzlCLENBQUMsQ0FBQztLQUNWLENBQUM7O0FBRUYsVUFBTSxDQUFDLGdCQUFnQixHQUFHLFVBQUMsT0FBTyxFQUFLO0FBQ25DLGVBQU8sR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7S0FDcEcsQ0FBQzs7QUFFRixVQUFNLENBQUMsb0JBQW9CLEdBQUcsVUFBQyxHQUFHLEVBQUs7QUFDbkMsWUFBSSxHQUFHLEVBQUU7QUFDTCxrQkFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFVLEVBQUs7QUFDdkMsMEJBQVUsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQzthQUM5QyxDQUFDLENBQUM7U0FDTjs7QUFFRCxjQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUMsVUFBVSxFQUFLO0FBQ3pELG1CQUFPLFVBQVUsQ0FBQyxRQUFRLENBQUM7U0FDOUIsQ0FBQyxDQUFDOztBQUVILGNBQU0sQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsVUFBQyxVQUFVLEVBQUs7QUFDNUQsbUJBQU8sVUFBVSxDQUFDLFFBQVEsQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDTixDQUFDOztBQUVGLFVBQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztDQUVwQjs7cUJBR1Usb0JBQW9COzs7Ozs7Ozs7Ozs7SUMzRDVCLGdCQUFnQixHQUNSLFNBRFIsZ0JBQWdCLENBQ1AsTUFBTSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQUU7MEJBRHBHLGdCQUFnQjs7QUFHZixRQUFJLFlBQVksR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDO0FBQ25DLFVBQU0sQ0FBQyxRQUFRLEdBQUcsWUFBWSxLQUFLLFNBQVMsQ0FBQztBQUM3QyxVQUFNLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztBQUMxQixVQUFNLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztBQUNsQixVQUFNLENBQUMsVUFBVSxHQUFHO0FBQ2hCLGlCQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUU7S0FDeEIsQ0FBQzs7QUFFRixVQUFNLENBQUMsTUFBTSxHQUFHLFlBQU07QUFDbEIsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLHlCQUFpQixDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUMxQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsa0JBQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0FBQ3pCLGtCQUFNLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7U0FDaEMsQ0FBQyxTQUFNLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDaEIsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVixDQUFBOztBQUVELFVBQU0sQ0FBQyxZQUFZLEdBQUcsWUFBTTtBQUN4QixjQUFNLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0tBQ3RDLENBQUM7O0FBRUYsVUFBTSxDQUFDLElBQUksR0FBRyxZQUFNO0FBQ2hCLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQix5QkFBaUIsQ0FBQyxTQUFTLEVBQUUsQ0FDeEIsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3JCLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDOztBQUU3QixrQkFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0FBQ3RDLGtCQUFNLENBQUMsVUFBVSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztBQUNqRCw2QkFBaUIsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUNuQyxJQUFJLENBQUMsVUFBQyxXQUFXLEVBQUs7QUFDbkIsb0JBQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7QUFDNUIsMEJBQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDekI7YUFDSixDQUFDLENBQUM7U0FFVixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBRVYsQ0FBQztDQUVMOztxQkFHVSxnQkFBZ0I7Ozs7Ozs7Ozs7OzsrQ0N2REUsb0NBQW9DOzs7OzJDQUN4QyxnQ0FBZ0M7Ozs7eUNBQy9CLDhCQUE4Qjs7OztBQUozRCxJQUFJLFVBQVUsR0FBRyxxQkFBcUIsQ0FBQzs7QUFNeEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQzFCLFVBQVUsQ0FBQyxzQkFBc0IsK0NBQXVCLENBQ3hELFVBQVUsQ0FBQyw0QkFBNEIsMkNBQW1CLENBQzFELE9BQU8sQ0FBQyxtQkFBbUIseUNBQW9CLENBQUM7O3FCQUVyQyxVQUFVOzs7Ozs7Ozs7QUNYeEIsU0FBUyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUU7QUFDL0IsZ0JBQVksQ0FBQzs7QUFFYixRQUFJLFVBQVUsQ0FBQzs7QUFFZixXQUFPO0FBQ0gsZ0JBQVEsRUFBUixRQUFRO0FBQ1IsaUJBQVMsRUFBVCxTQUFTO0FBQ1QsZUFBTyxFQUFQLE9BQU87QUFDUCxvQkFBWSxFQUFaLFlBQVk7QUFDWixXQUFHLEVBQUgsR0FBRztBQUNILGNBQU0sRUFBTixNQUFNO0tBQ1QsQ0FBQzs7QUFFRixhQUFTLFFBQVEsQ0FBQyxFQUFFLEVBQUU7QUFDbEIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDbEMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyx5QkFBeUIsQ0FBQztBQUNqQyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUixzQkFBTSxFQUFFO0FBQ0osc0JBQUUsRUFBRSxFQUFFO2lCQUNUO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFNBQVMsR0FBRztBQUNqQixlQUFPLEtBQUssQ0FBQztBQUNULGtCQUFNLEVBQUUsS0FBSztBQUNiLGVBQUcsRUFBRSwyQkFBMkI7U0FDbkMsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRTtBQUNsQyxZQUFJLGFBQWEsR0FBRyxTQUFoQixhQUFhLENBQUksUUFBUSxFQUFLO0FBQzlCLHNCQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUMzQixtQkFBTyxVQUFVLENBQUM7U0FDckIsQ0FBQzs7QUFFRixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsaUJBQWlCLENBQUM7QUFDNUIsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isc0JBQU0sRUFBRTtBQUNKLDRCQUFRLEVBQUUsUUFBUTtBQUNsQiw2QkFBUyxFQUFDLFNBQVM7aUJBQ3RCO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzFCLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsWUFBWSxDQUFDLEVBQUUsRUFBRTtBQUN0QixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLHlCQUF5QixDQUFDO0FBQ2pDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHNCQUFNLEVBQUU7QUFDSixzQkFBRSxFQUFFLEVBQUU7aUJBQ1Q7QUFDRCx1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsR0FBRyxDQUFDLFVBQVUsRUFBRTtBQUNyQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsa0JBQWtCLENBQUM7QUFDN0IsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxNQUFNO0FBQ2QsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isb0JBQUksRUFBRSxVQUFVO0FBQ2hCLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxNQUFNLENBQUMsVUFBVSxFQUFFO0FBQ3hCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQztBQUM3QixtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUixvQkFBSSxFQUFFLFVBQVU7QUFDaEIsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjtDQUVKOztxQkFFYyxpQkFBaUI7Ozs7Ozs7Ozs7OztJQzlHekIsbUJBQW1CLEdBQ1gsU0FEUixtQkFBbUIsQ0FDVixNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFOzBCQURyRixtQkFBbUI7O0FBR2xCLFFBQU0sUUFBUSxHQUFHLENBQUMsQ0FBQztBQUNuQixRQUFJLFNBQVMsR0FBRyxDQUFDLENBQUM7O0FBRWxCLFVBQU0sQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDOztBQUV0QixVQUFNLENBQUMsT0FBTyxHQUFHLFlBQU07QUFDbkIsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLHdCQUFnQixDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsU0FBUyxDQUFDLENBQ3hDLElBQUksQ0FBQyxVQUFDLFNBQVMsRUFBSztBQUNqQixnQkFBSSxTQUFTLENBQUMsTUFBTSxHQUFHLFFBQVEsRUFBRTtBQUM3QixzQkFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDNUI7QUFDRCxxQkFBUyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUM1QixzQkFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDbkMsQ0FBQyxDQUFDO0FBQ0gscUJBQVMsRUFBRyxDQUFDO0FBQ2Isa0JBQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDOztBQUU5QixnQkFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO0FBQzFCLHNCQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzthQUN4QjtTQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVixDQUFDOztBQUVGLFVBQU0sQ0FBQyxnQkFBZ0IsR0FBRyxVQUFDLFVBQVUsRUFBSztBQUN0QyxrQkFBVSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQztLQUN0RyxDQUFDOztBQUVGLFVBQU0sQ0FBQyxvQkFBb0IsR0FBRyxVQUFDLEdBQUcsRUFBSztBQUNuQyxZQUFJLEdBQUcsRUFBRTtBQUNMLGtCQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNuQyx3QkFBUSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDO2FBQzVDLENBQUMsQ0FBQztTQUNOOztBQUVELGNBQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDckQsbUJBQU8sUUFBUSxDQUFDLFFBQVEsQ0FBQztTQUM1QixDQUFDLENBQUM7O0FBRUgsY0FBTSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUN4RCxtQkFBTyxRQUFRLENBQUMsUUFBUSxDQUFDO1NBQzVCLENBQUMsQ0FBQztLQUNOLENBQUM7O0FBRUYsVUFBTSxDQUFDLE1BQU0sR0FBRyxVQUFDLFFBQVEsRUFBSzs7QUFFMUIsb0JBQVksQ0FBQyxnQkFBZ0IsQ0FBQztBQUMxQixvQkFBUSxFQUFFO0FBQ04scUJBQUssb0JBQW9CO0FBQ3pCLG9CQUFJLDZDQUE2QztBQUNqRCxrQkFBRSxFQUFFLGNBQWM7QUFDbEIsc0JBQU0sRUFBRSxVQUFVO2FBQ3JCO1NBQ0osQ0FBQyxDQUNELElBQUksQ0FBQyxZQUFNO0FBQ1IsZ0JBQUksY0FBYyxDQUFDO0FBQ25CLGdCQUFJLFFBQVEsRUFBRTtBQUNWLDhCQUFjLEdBQUcsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDMUMsTUFBTTtBQUNILDhCQUFjLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FDNUIsR0FBRyxDQUFDLFVBQUMsWUFBWSxFQUFLO0FBQ25CLHdCQUFJLFlBQVksQ0FBQyxRQUFRLEVBQUU7QUFDdkIsK0JBQU8sWUFBWSxDQUFDLFVBQVUsQ0FBQztxQkFDbEM7QUFDRCwyQkFBTyxJQUFJLENBQUM7aUJBQ2YsQ0FBQyxDQUFDO2FBQ1Y7O0FBRUQsMEJBQWMsQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFVLEVBQUs7QUFDbkMsZ0NBQWdCLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUM5QixJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsd0JBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7QUFDekIsOEJBQU0sQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUMsWUFBWSxFQUFLO0FBQ3ZDLGdDQUFJLFVBQVUsS0FBSyxZQUFZLENBQUMsVUFBVSxFQUFFO0FBQ3hDLG9DQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUNuRCxzQ0FBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDOzZCQUNyQzt5QkFDSixDQUFDLENBQUM7cUJBQ047aUJBQ0osQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCxrQ0FBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDekMsQ0FBQyxDQUFDO2FBQ1YsQ0FBQyxDQUFDOztBQUVILGtCQUFNLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztTQUVqQyxDQUFDLENBQUM7S0FDTixDQUFDOztBQUVGLFVBQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztDQUNwQjs7cUJBSVUsbUJBQW1COzs7O0FDeEdqQyxZQUFZLENBQUM7Ozs7Ozs7O0lBRVIsZ0JBQWdCLEdBQ1AsU0FEVCxnQkFBZ0IsQ0FDTixNQUFNLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRTswQkFEcEcsZ0JBQWdCOztBQUdkLEtBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBQyxLQUFLLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQzs7QUFFckMsUUFBSSxVQUFVLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQztBQUNqQyxVQUFNLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSyxTQUFTLENBQUM7QUFDM0MsUUFBSSxRQUFRLENBQUM7O0FBRWIsVUFBTSxDQUFDLFFBQVEsR0FBRztBQUNkLGlCQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUU7S0FDeEIsQ0FBQzs7QUFFRixvQkFBZ0IsQ0FBQyxVQUFVLEVBQUUsQ0FDeEIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGNBQU0sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztLQUNsQyxDQUFDLENBQUM7O0FBRVAsUUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO0FBQ2pCLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQix3QkFBZ0IsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQ25DLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixrQkFBTSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQ2hDLDRCQUFnQixDQUFDLFVBQVUsRUFBRSxDQUN4QixJQUFJLENBQUMsVUFBQyxjQUFjLEVBQUs7QUFDdEIsc0JBQU0sQ0FBQyxPQUFPLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQztBQUNyQyxpQkFBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO2FBQzNDLENBQUMsQ0FBQzs7QUFFUCxrQkFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLDhCQUE0QixNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQUFBRSxDQUFDO1NBQ2hGLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVixNQUFNO0FBQ0gsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLHdCQUFnQixDQUFDLFNBQVMsRUFBRSxDQUN2QixJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDckIsb0JBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1NBQzVCLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVjs7QUFFRCxVQUFNLENBQUMsWUFBWSxHQUFHLFlBQU07QUFDeEIsY0FBTSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztLQUNwQyxDQUFDOztBQUVGLFVBQU0sQ0FBQyxjQUFjLEdBQUcsWUFBTTtBQUMxQixvQkFBWSxDQUFDLGdCQUFnQixDQUFDO0FBQzFCLG9CQUFRLEVBQUU7QUFDTixxQkFBSyxFQUFFLGtCQUFrQjtBQUN6QixvQkFBSSxFQUFFLDRDQUE0QztBQUNsRCxrQkFBRSxFQUFFLGNBQWM7QUFDbEIsc0JBQU0sRUFBRSxVQUFVO2FBQ3JCO1NBQ0osQ0FBQyxDQUNGLElBQUksQ0FBQyxZQUFNO0FBQ1Isc0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLDRCQUFnQixDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FDOUIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLG9CQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDBCQUFNLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lCQUNwQzthQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsOEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLDBCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDVixDQUFDLENBQUM7S0FDTCxDQUFDOztBQUVGLFVBQU0sQ0FBQyxJQUFJLEdBQUcsWUFBTTtBQUNoQixZQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFO0FBQ3pCLGtCQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbkU7O0FBRUQsWUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7QUFDbEIsa0JBQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztBQUNwQyxzQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7O0FBRTFCLDRCQUFnQixDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQ2hDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixvQkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QiwwQkFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUN6QjthQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsOEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLDBCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDVixNQUFNO0FBQ0gsc0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLDRCQUFnQixDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQ25DLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixvQkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QiwwQkFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUN6QjthQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsOEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLDBCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDVjtLQUNKLENBQUM7Q0FDTDs7cUJBRVUsZ0JBQWdCOzs7Ozs7Ozs7Ozs7OENDeEhDLG1DQUFtQzs7OzsyQ0FDdEMsZ0NBQWdDOzs7O3dDQUNoQyw2QkFBNkI7Ozs7dUNBQ25DLDRCQUE0Qjs7OztBQUxsRCxJQUFJLFVBQVUsR0FBRyxvQkFBb0IsQ0FBQzs7QUFPdkMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQzFCLFVBQVUsQ0FBQyxxQkFBcUIsOENBQXNCLENBQ3RELFVBQVUsQ0FBQywyQkFBMkIsMkNBQW1CLENBQ3pELE9BQU8sQ0FBQyxrQkFBa0Isd0NBQW1CLENBQzdDLFNBQVMsQ0FBQyxZQUFZLHVDQUFhLENBQUM7O3FCQUV6QixVQUFVOzs7Ozs7Ozs7Ozs7OztJQ2JsQixVQUFVO0FBQ0YsYUFEUixVQUFVLEdBQ0M7OEJBRFgsVUFBVTs7QUFFVCxZQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQztBQUNwQixZQUFJLENBQUMsS0FBSyxHQUFHO0FBQ1QsaUJBQUssRUFBRyxHQUFHO1NBQ2QsQ0FBQztLQUNMOztpQkFORSxVQUFVOztlQVFSLGNBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRTs7QUFFbEIsbUJBQU8sQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLFlBQU07QUFDdkIsb0JBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ25DLG9CQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDOztBQUU5QixzQkFBTSxDQUFDLFNBQVMsR0FBRyxZQUFZO0FBQzNCLHlCQUFLLENBQUMsTUFBTSxDQUFDLFlBQVc7QUFDcEIsNkJBQUssQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztxQkFDN0IsQ0FBQyxDQUFDO2lCQUNOLENBQUM7O0FBRUYsb0JBQUksSUFBSSxFQUFFO0FBQ04sMEJBQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzlCLE1BQU07QUFDSCx5QkFBSyxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7aUJBQ2xCO2FBQ0osQ0FBQyxDQUFDO1NBQ047OztlQUVzQiw0QkFBRztBQUN0QixzQkFBVSxDQUFDLFFBQVEsR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO0FBQ3ZDLG1CQUFPLFVBQVUsQ0FBQyxRQUFRLENBQUM7U0FDOUI7OztXQS9CRSxVQUFVOzs7cUJBa0NGLFVBQVUsQ0FBQyxnQkFBZ0I7Ozs7Ozs7OztBQ2xDekMsU0FBUyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUU7QUFDOUIsZ0JBQVksQ0FBQzs7QUFFYixRQUFJLFNBQVMsQ0FBQzs7QUFFZCxXQUFPO0FBQ0gsa0JBQVUsRUFBVixVQUFVO0FBQ1YsaUJBQVMsRUFBVCxTQUFTO0FBQ1QsbUJBQVcsRUFBWCxXQUFXO0FBQ1gsZUFBTyxFQUFQLE9BQU87QUFDUCxXQUFHLEVBQUgsR0FBRztBQUNILGNBQU0sRUFBTixNQUFNO0FBQ04sY0FBTSxFQUFOLE1BQU07S0FDVCxDQUFDOztBQUVGLGFBQVMsVUFBVSxHQUFHO0FBQ2xCLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxFQUFFLDJCQUEyQjtTQUNuQyxDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFNBQVMsR0FBRztBQUNqQixlQUFPLEtBQUssQ0FBQztBQUNULGtCQUFNLEVBQUUsS0FBSztBQUNiLGVBQUcsRUFBRSwyQkFBMkI7U0FDbkMsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRTtBQUNsQyxZQUFJLGFBQWEsR0FBRyxTQUFoQixhQUFhLENBQUksUUFBUSxFQUFLO0FBQzlCLHFCQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUMxQixtQkFBTyxTQUFTLENBQUM7U0FDcEIsQ0FBQzs7QUFFRixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsZ0JBQWdCLENBQUM7QUFDM0IsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isc0JBQU0sRUFBRTtBQUNKLDRCQUFRLEVBQUUsUUFBUTtBQUNsQiw2QkFBUyxFQUFDLFNBQVM7aUJBQ3RCO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzFCLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsV0FBVyxDQUFDLFVBQVUsRUFBRTtBQUM3QixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLHVCQUFxQixVQUFVLEFBQUUsQ0FBQztBQUN6QyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsR0FBRyxDQUFDLFFBQVEsRUFBRTtBQUNuQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsaUJBQWlCLENBQUM7QUFDNUIsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxNQUFNO0FBQ2QsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isb0JBQUksRUFBRSxRQUFRO0FBQ2QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLE1BQU0sQ0FBQyxRQUFRLEVBQUU7QUFDdEIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDdkMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyxHQUFHLGlCQUFpQixDQUFDO0FBQzVCLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLG9CQUFJLEVBQUUsUUFBUTtBQUNkLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxNQUFNLENBQUMsVUFBVSxFQUFFO0FBQ3hCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsdUJBQXFCLFVBQVUsQUFBRSxDQUFDO0FBQ3pDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsUUFBUTtBQUNoQixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOO0NBQ0o7O3FCQUVjLGdCQUFnQjs7Ozs7Ozs7Ozs7O0lDL0d4QixtQkFBbUIsR0FDWCxTQURSLG1CQUFtQixDQUNWLE1BQU0sRUFBRSxVQUFVLEVBQUUsZ0JBQWdCLEVBQUUsY0FBYyxFQUFFOzBCQUQvRCxtQkFBbUI7O0FBR2xCLFFBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7QUFDdEMsVUFBTSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztBQUNsQyxVQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQzs7QUFFMUIsY0FBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsb0JBQWdCLENBQUMsVUFBVSxFQUFFLENBQ3hCLElBQUksQ0FBQyxVQUFDLE9BQU8sRUFBSztBQUNmLGNBQU0sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0tBQzVCLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2Qsc0JBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLGtCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztLQUM5QixDQUFDLENBQUM7O0FBRVAsVUFBTSxDQUFDLHNCQUFzQixHQUFHLFlBQU07QUFDbEMsWUFBSSxNQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRTtBQUNqRCxrQkFBTSxDQUFDLG1CQUFtQixJQUFJLENBQUMsQ0FBQztTQUNuQztLQUNKLENBQUM7O0FBRUYsVUFBTSxDQUFDLHlCQUF5QixHQUFHLFlBQU07QUFDckMsY0FBTSxDQUFDLG1CQUFtQixJQUFJLENBQUMsQ0FBQztLQUNuQyxDQUFDOztBQUVGLFVBQU0sQ0FBQyxXQUFXLEdBQUcsWUFBTTtBQUN2QixZQUFJLE1BQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLG1CQUFtQixFQUFFO0FBQ2pELGtCQUFNLENBQUMsbUJBQW1CLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztTQUNuRDtLQUNKLENBQUM7O0FBRUYsUUFBSSw4QkFBOEIsR0FBRyxTQUFqQyw4QkFBOEIsQ0FBSSxRQUFRLEVBQUUsT0FBTyxFQUFLO0FBQ3hELGNBQU0sQ0FBQyx3QkFBd0IsR0FBRztBQUM5QixzQkFBVSxFQUFFLG9CQUFVLFlBQVksRUFBRTtBQUNoQyx1QkFBTyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQzthQUNyRTtBQUNELGtCQUFNLEVBQUUsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFdBQVcsQ0FBQztBQUNySSxvQkFBUSxFQUFFLENBQ047QUFDSSxxQkFBSyxFQUFFLFVBQVU7QUFDakIseUJBQVMsRUFBRyxxQkFBcUI7QUFDakMsMkJBQVcsRUFBRSxtQkFBbUI7QUFDaEMsMEJBQVUsRUFBRSxtQkFBbUI7QUFDL0IsZ0NBQWdCLEVBQUUsbUJBQW1CO0FBQ3JDLGtDQUFrQixFQUFFLG1CQUFtQjtBQUN2QyxvQ0FBb0IsRUFBRyxNQUFNO0FBQzdCLG9CQUFJLEVBQUUsT0FBTzthQUNoQixFQUNEO0FBQ0kscUJBQUssRUFBRSxRQUFRO0FBQ2YseUJBQVMsRUFBRyxzQkFBc0I7QUFDbEMsMkJBQVcsRUFBRSxvQkFBb0I7QUFDakMsMEJBQVUsRUFBRSxvQkFBb0I7QUFDaEMsZ0NBQWdCLEVBQUUsb0JBQW9CO0FBQ3RDLGtDQUFrQixFQUFFLG9CQUFvQjtBQUN4QyxvQ0FBb0IsRUFBRyxNQUFNO0FBQzdCLG9CQUFJLEVBQUUsUUFBUTthQUNqQixDQUNKO1NBQ0osQ0FBQztLQUNMLENBQUM7O0FBRUYsVUFBTSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRSxVQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUs7QUFDekQsWUFBSSxRQUFRLElBQUksUUFBUSxFQUFFO0FBQ3RCLGdCQUFJLFFBQVEsR0FBRyxJQUFJLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUM3QixvQkFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ3hCLGdCQUFJLE9BQU8sR0FBRyxJQUFJLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUM1QixtQkFBTyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDOztBQUV2Qiw0QkFBZ0IsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQ25ELElBQUksQ0FBQyxVQUFDLFdBQVcsRUFBSztBQUNuQiwyQkFBVyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLLEVBQUs7QUFDakMsNEJBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO0FBQ2hDLDJCQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztpQkFDakMsQ0FBQyxDQUFDOztBQUVILDhDQUE4QixDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQzthQUNyRCxDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDhCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pDLENBQUMsQ0FBQztTQUNWO0tBQ0osQ0FBQyxDQUFDO0NBQ047O3FCQUdVLG1CQUFtQjs7Ozs7Ozs7Ozs7OzhDQ3hGRixtQ0FBbUM7Ozs7d0NBQ3RDLDZCQUE2Qjs7OzswQ0FDdEMsK0JBQStCOzs7O0FBSmxELElBQUksVUFBVSxHQUFHLG9CQUFvQixDQUFDOztBQU12QyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FDMUIsU0FBUyxDQUFDLE9BQU8sMENBQVUsQ0FDM0IsVUFBVSxDQUFDLHFCQUFxQiw4Q0FBc0IsQ0FDdEQsT0FBTyxDQUFDLGtCQUFrQix3Q0FBbUIsQ0FBQzs7cUJBRW5DLFVBQVU7Ozs7Ozs7Ozs7Ozs7O0FDWHhCLElBQU0sTUFBTSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7O0lBRXhCLE9BQU87QUFDRSxhQURULE9BQU8sQ0FDRyxPQUFPLEVBQUU7OEJBRG5CLE9BQU87O0FBRUwsWUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUM7QUFDcEIsWUFBSSxDQUFDLEtBQUssR0FBRztBQUNULHVCQUFXLEVBQUcsR0FBRztBQUNqQixrQkFBTSxFQUFFLEdBQUc7U0FDZCxDQUFDO0FBQ0YsY0FBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDN0I7O2lCQVJDLE9BQU87O2VBVUosY0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFO0FBQ2xCLGdCQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7QUFFNUQsZ0JBQUksT0FBTyxHQUFHO0FBQ1Ysa0NBQWtCLEVBQUcsSUFBSTtBQUN6QixrQ0FBa0IsRUFBRyxpQkFBaUI7QUFDdEMsa0NBQWtCLEVBQUcsQ0FBQztBQUN0Qix3Q0FBd0IsRUFBRSxJQUFJO0FBQzlCLHNDQUFzQixFQUFFLEtBQUs7QUFDN0IsMEJBQVUsRUFBRSxvQkFBVSxZQUFZLEVBQUU7QUFDaEMsMkJBQU8sWUFBWSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDM0M7QUFDRCwyQkFBVyxFQUFHLElBQUk7QUFDbEIsa0NBQWtCLEVBQUcsR0FBRztBQUN4Qix3QkFBUSxFQUFHLEtBQUs7QUFDaEIsOEJBQWMsRUFBRyxDQUFDO0FBQ2xCLG1DQUFtQixFQUFHLENBQUM7QUFDdkIsdUNBQXVCLEVBQUcsRUFBRTtBQUM1Qiw2QkFBYSxFQUFHLElBQUk7QUFDcEIsa0NBQWtCLEVBQUcsQ0FBQztBQUN0QiwyQkFBVyxFQUFHLElBQUk7QUFDbEIsOEJBQWMsRUFBRyxtT0FBbU87QUFDcFAsZ0NBQWdCLEVBQUUsU0FBUztBQUMzQixtQ0FBbUIsRUFBRSxJQUFJO0FBQ3pCLDBCQUFVLEVBQUUsSUFBSTtBQUNoQix5QkFBUyxFQUFFLElBQUk7QUFDZiwrQkFBZSxFQUFFLGNBQWM7QUFDL0IsOEJBQWMsRUFBRyxTQUFTLGNBQWMsQ0FBQyxPQUFPLEVBQUU7O0FBRTlDLHdCQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQzs7QUFFekMsd0JBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7QUFDZCx5QkFBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxrRUFBa0UsQ0FBQyxDQUFDO0FBQ3JGLGdDQUFRLEdBQUcsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLENBQUM7cUJBQzFDOztBQUVELHdCQUFJLENBQUMsT0FBTyxFQUFFO0FBQ1YsZ0NBQVEsQ0FBQyxHQUFHLENBQUM7QUFDVCxtQ0FBTyxFQUFFLENBQUM7eUJBQ2IsQ0FBQyxDQUFDO0FBQ0gsK0JBQU87cUJBQ1Y7O0FBRUQsNEJBQVEsQ0FBQyxXQUFXLENBQUMsMEJBQTBCLENBQUMsQ0FBQztBQUNqRCx3QkFBSSxPQUFPLENBQUMsTUFBTSxFQUFFO0FBQ2hCLGdDQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztxQkFDckMsTUFBTTtBQUNILGdDQUFRLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO3FCQUNyQzs7QUFFRCx3QkFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO0FBQ2QsZ0NBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUMvQixNQUFNO0FBQ0gsNEJBQUksU0FBUywyQkFBeUIsT0FBTyxDQUFDLEtBQUssV0FBUSxDQUFDO0FBQzVELDZCQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDNUMscUNBQVMsSUFBSSxDQUNULHVCQUF1QixvREFDeUIsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLDZDQUNqRCxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUMxRCxRQUFRLENBQ1gsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7eUJBQ2Q7QUFDRCxnQ0FBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDNUI7O0FBRUQsd0JBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztBQUNaLHdCQUFJLE9BQU8sQ0FBQyxNQUFNLEVBQUU7QUFDaEIsNEJBQUksT0FBTyxDQUFDLE1BQU0sS0FBSyxPQUFPLEVBQUU7QUFDNUIsK0JBQUcsR0FBRyxPQUFPLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQzt5QkFDaEUsTUFBTTtBQUNILCtCQUFHLEdBQUcsT0FBTyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUM7eUJBQ2hFO3FCQUNKOztBQUVELHdCQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQzs7QUFFOUMsNEJBQVEsQ0FBQyxHQUFHLENBQUM7QUFDVCwrQkFBTyxFQUFFLENBQUM7QUFDViw2QkFBSyxFQUFFLE9BQU8sQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLEdBQUcsTUFBTTtBQUNwRCw0QkFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJO0FBQ3BDLDJCQUFHLEVBQUUsTUFBTSxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsSUFBSTtBQUM1QixrQ0FBVSxFQUFFLE9BQU8sQ0FBQyxVQUFVO0FBQzlCLGdDQUFRLEVBQUUsT0FBTyxDQUFDLFFBQVE7QUFDMUIsaUNBQVMsRUFBRSxPQUFPLENBQUMsU0FBUztBQUM1Qix1Q0FBZSxFQUFFLG9CQUFvQjtBQUNyQyxpQ0FBUyxFQUFFLCtCQUErQjtxQkFDN0MsQ0FBQyxDQUFDO2lCQUNOO2FBQ0osQ0FBQzs7QUFFRixnQkFBSSxHQUFHLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7O0FBRTFDLGlCQUFLLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxVQUFTLFFBQVEsRUFBRSxRQUFRLEVBQUU7O0FBRW5ELG9CQUFJLFFBQVEsSUFBSSxDQUFDLFFBQVEsRUFBRTtBQUN2Qix3QkFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRTtBQUN2QiwyQkFBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO0FBQ3ZCLDZCQUFLLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFDMUUsNEJBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQztBQUN4RCxnQ0FBUSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUM7cUJBQ3RFOztBQUVELHdCQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssS0FBSyxFQUFFO0FBQ3RCLDJCQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7QUFDdkIsNkJBQUssQ0FBQyxhQUFhLEdBQUcsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7cUJBQ3RFO2lCQUNKOztBQUVELG9CQUFJLFFBQVEsSUFBSSxRQUFRLEVBQUU7QUFDdEIsd0JBQUksS0FBSyxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUU7QUFDdkIsNkJBQUssQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSyxFQUFLO0FBQ3RELGlDQUFLLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO3lCQUNwRSxDQUFDLENBQUM7O0FBRUgsNkJBQUssQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSyxFQUFLO0FBQ3RELGlDQUFLLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO3lCQUNwRSxDQUFDLENBQUM7O0FBRUgsNkJBQUssQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQztxQkFDdEM7O0FBRUQsd0JBQUksS0FBSyxDQUFDLElBQUksS0FBSyxLQUFLLEVBQUU7QUFDdEIsNkJBQUssQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSyxFQUFLO0FBQ3RELGlDQUFLLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQzt5QkFDNUQsQ0FBQyxDQUFDO0FBQ0gsNkJBQUssQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUM7cUJBQ2hDO2lCQUNKO2FBQ0osQ0FBQyxDQUFDOztBQUVILGlCQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1NBQzNDOzs7ZUFFc0IsMEJBQUMsT0FBTyxFQUFFO0FBQzdCLG1CQUFPLENBQUMsUUFBUSxHQUFHLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQ3hDLG1CQUFPLE9BQU8sQ0FBQyxRQUFRLENBQUM7U0FDM0I7OztXQWxKQyxPQUFPOzs7QUFxSmIsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDOztxQkFFaEMsT0FBTyxDQUFDLGdCQUFnQjs7Ozs7Ozs7O0FDekp0QyxTQUFTLGdCQUFnQixDQUFDLEtBQUssRUFBRTtBQUM5QixnQkFBWSxDQUFDOztBQUViLFdBQU87QUFDSCxrQkFBVSxFQUFWLFVBQVU7QUFDVixtQkFBVyxFQUFYLFdBQVc7S0FDZCxDQUFDOztBQUVGLGFBQVMsVUFBVSxHQUFHO0FBQ2xCLFlBQUksYUFBYSxHQUFHLFNBQWhCLGFBQWEsQ0FBSSxRQUFRLEVBQUs7QUFDOUIsZ0JBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDNUIsbUJBQU8sT0FBTyxDQUFDO1NBQ2xCLENBQUM7O0FBRUYsWUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDOztBQUVsQixlQUFPLEtBQUssQ0FBQztBQUNULGtCQUFNLEVBQUUsS0FBSztBQUNiLGVBQUcsRUFBRSwyQkFBMkI7U0FDbkMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QixvQkFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDekIsZ0JBQUksR0FBRyxHQUFHLDJCQUEyQixDQUFDO0FBQ3RDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUMxQixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFdBQVcsQ0FBQyxJQUFJLEVBQUU7QUFDdkIsWUFBSSxhQUFhLEdBQUcsU0FBaEIsYUFBYSxDQUFJLFFBQVEsRUFBSztBQUM5QixnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixtQkFBTyxRQUFRLENBQUM7U0FDbkIsQ0FBQzs7QUFFRixZQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7O0FBRWxCLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxFQUFFLDJCQUEyQjtTQUNuQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZCLG9CQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUN6QixnQkFBSSxHQUFHLEdBQUcsd0JBQXdCLEdBQUcsSUFBSSxDQUFDO0FBQzFDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUMxQixDQUFDLENBQUM7S0FDTjtDQUVKOztxQkFFYyxnQkFBZ0I7Ozs7Ozs7Ozs7OztJQzFEeEIsZ0JBQWdCLEdBQ1IsU0FEUixnQkFBZ0IsQ0FDUCxNQUFNLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsdUJBQXVCLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRTswQkFEMUcsZ0JBQWdCOztBQUdmLEtBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQzs7QUFFdkMsUUFBSSxrQkFBa0IsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDO0FBQ3pDLFFBQUksUUFBUSxDQUFDO0FBQ2IsVUFBTSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7QUFDeEIsVUFBTSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7O0FBRTFCLFVBQU0sQ0FBQyxnQkFBZ0IsR0FBRztBQUN0QixnQkFBUSxFQUFFLENBQUM7QUFDWCxlQUFPLEVBQUUsa0JBQWtCO0FBQzNCLDZCQUFxQixFQUFFLEVBQUU7S0FDNUIsQ0FBQzs7QUFFRiwyQkFBdUIsQ0FBQyxTQUFTLEVBQUUsQ0FDOUIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGNBQU0sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztLQUNsQyxDQUFDLENBQUM7O0FBRVAsMkJBQXVCLENBQUMsTUFBTSxFQUFFLENBQzNCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixjQUFNLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7S0FDL0IsQ0FBQyxDQUFDOztBQUVQLFVBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBTTtBQUNuQixjQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztBQUNuQiwrQkFBdUIsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FDM0QsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGtCQUFNLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDNUIsYUFBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUNkLHFCQUFLLEVBQUUsTUFBTTthQUNoQixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDVixDQUFBOztBQUVELFVBQU0sQ0FBQyx3QkFBd0IsR0FBRyxZQUFNO0FBQ3BDLFlBQUksTUFBTSxHQUFHO0FBQ1Qsb0JBQVEsRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLFFBQVE7QUFDekMsaUJBQUssRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLEtBQUs7QUFDbkMsdUJBQVcsRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLFdBQVc7U0FDbEQsQ0FBQztBQUNGLGNBQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDM0QsY0FBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7S0FDNUIsQ0FBQTs7QUFFRCxRQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7QUFDakIsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLCtCQUF1QixDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLENBQzFELElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixrQkFBTSxDQUFDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDeEMsa0JBQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1NBRTVCLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVjs7QUFFRCxVQUFNLENBQUMsV0FBVyxHQUFHLFVBQUMsU0FBUyxFQUFLO0FBQ2hDLFlBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQy9ELGNBQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztLQUNwRCxDQUFBOztBQUVELFVBQU0sQ0FBQyxZQUFZLEdBQUcsWUFBTTtBQUN4QixjQUFNLENBQUMsWUFBWSxDQUFDLGtCQUFrQixFQUFFLEVBQUMsRUFBRSxFQUFHLGtCQUFrQixFQUFDLENBQUMsQ0FBQztLQUN0RSxDQUFDOztBQUVGLFVBQU0sQ0FBQyxJQUFJLEdBQUcsWUFBTTtBQUNoQixrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsK0JBQXVCLENBQUMsU0FBUyxFQUFFLENBQzlCLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUNyQixvQkFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7O0FBRXpCLGtCQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQzs7QUFFNUMsbUNBQXVCLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUMvQyxJQUFJLENBQUMsVUFBQyxXQUFXLEVBQUs7QUFDbkIsb0JBQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7QUFDNUIsMEJBQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDekI7YUFDSixDQUFDLENBQUM7U0FFVixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBRVYsQ0FBQztDQUVMOztxQkFHVSxnQkFBZ0I7Ozs7Ozs7Ozs7OztJQ3BHeEIsMEJBQTBCLEdBQ2xCLFNBRFIsMEJBQTBCLENBQ2pCLE1BQU0sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSx1QkFBdUIsRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFOzBCQUQxRywwQkFBMEI7O0FBR3pCLFFBQUksT0FBTyxHQUFHLFlBQVksQ0FBQyxFQUFFLENBQUM7QUFDOUIsUUFBTSxRQUFRLEdBQUcsQ0FBQyxDQUFDO0FBQ25CLFFBQUksU0FBUyxHQUFHLENBQUMsQ0FBQztBQUNsQixVQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO0FBQ2pDLFVBQU0sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0FBQ3pCLFVBQU0sQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7O0FBRTdCLFVBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBTTtBQUNuQixrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsK0JBQXVCLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQ3hELElBQUksQ0FBQyxVQUFDLGdCQUFnQixFQUFLOztBQUV4QixnQkFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO0FBQzVCLHNCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO2FBQ3JDOztBQUVELGdCQUFJLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxRQUFRLEVBQUU7QUFDcEMsc0JBQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQzVCO0FBQ0QsNEJBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUMsZUFBZSxFQUFLO0FBQzFDLHNCQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ2pELENBQUMsQ0FBQztBQUNILHFCQUFTLEVBQUcsQ0FBQztBQUNiLGtCQUFNLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztBQUM5QixnQkFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7QUFDakMsc0JBQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2FBQ3hCO1NBQ0osQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCwwQkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsc0JBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQzlCLENBQUMsQ0FBQztLQUNWLENBQUM7O0FBRUYsVUFBTSxDQUFDLGdCQUFnQixHQUFHLFlBQU07QUFDNUIsY0FBTSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLEVBQUUsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztLQUNsRSxDQUFDOztBQUVGLFVBQU0sQ0FBQyxvQkFBb0IsR0FBRyxVQUFDLEdBQUcsRUFBSztBQUNuQyxZQUFJLEdBQUcsRUFBRTtBQUNMLGtCQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUMsZUFBZSxFQUFLO0FBQ2pELCtCQUFlLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7YUFDbkQsQ0FBQyxDQUFDO1NBQ047O0FBRUQsY0FBTSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQUMsZUFBZSxFQUFLO0FBQ25FLG1CQUFPLGVBQWUsQ0FBQyxRQUFRLENBQUM7U0FDbkMsQ0FBQyxDQUFDOztBQUVILGNBQU0sQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxVQUFDLGVBQWUsRUFBSztBQUN0RSxtQkFBTyxlQUFlLENBQUMsUUFBUSxDQUFDO1NBQ25DLENBQUMsQ0FBQztLQUNOLENBQUM7O0FBRUYsVUFBTSxDQUFDLGlCQUFpQixHQUFHLFlBQU07QUFDN0IsY0FBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsRUFBQyxFQUFFLEVBQUcsTUFBTSxDQUFDLE9BQU8sRUFBQyxDQUFDLENBQUM7S0FDdkQsQ0FBQzs7QUFFRixVQUFNLENBQUMsTUFBTSxHQUFHLFVBQUMsZUFBZSxFQUFLO0FBQ2pDLG9CQUFZLENBQUMsZ0JBQWdCLENBQUM7QUFDMUIsb0JBQVEsRUFBRTtBQUNOLHFCQUFLLGlCQUFpQjtBQUN0QixvQkFBSSwwQ0FBMEM7QUFDOUMsa0JBQUUsRUFBRSxjQUFjO0FBQ2xCLHNCQUFNLEVBQUUsVUFBVTthQUNyQjtTQUNKLENBQUMsQ0FDRCxJQUFJLENBQUMsWUFBTTtBQUNSLG1DQUF1QixDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FDdEMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLG9CQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDBCQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUMsbUJBQW1CLEVBQUs7QUFDckQsNEJBQUksZUFBZSxLQUFLLG1CQUFtQixDQUFDLHVCQUF1QixFQUFFO0FBQ2pFLGdDQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUM7QUFDakUsa0NBQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO3lCQUM1QztxQkFDSixDQUFDLENBQUM7aUJBQ047YUFDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDhCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2pELENBQUMsQ0FBQzs7QUFFSCxrQkFBTSxDQUFDLG9CQUFvQixFQUFFLENBQUM7U0FFakMsQ0FBQyxDQUFDO0tBQ04sQ0FBQzs7QUFFRixVQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7Q0FDcEI7O3FCQUdVLDBCQUEwQjs7Ozs7Ozs7Ozs7O3FEQy9GRiwwQ0FBMEM7Ozs7MkNBQ3BELGdDQUFnQzs7OzsrQ0FDekIsb0NBQW9DOzs7O0FBSnZFLElBQUksVUFBVSxHQUFHLDJCQUEyQixDQUFDOztBQU05QyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FDMUIsVUFBVSxDQUFDLDRCQUE0QixxREFBNkIsQ0FDcEUsVUFBVSxDQUFDLGtDQUFrQywyQ0FBbUIsQ0FDaEUsT0FBTyxDQUFDLHlCQUF5QiwrQ0FBMEIsQ0FBQzs7cUJBRWpELFVBQVU7Ozs7Ozs7OztBQ1h4QixTQUFTLHVCQUF1QixDQUFDLEtBQUssRUFBRTtBQUNyQyxnQkFBWSxDQUFDOztBQUViLFFBQUksZ0JBQWdCLENBQUM7O0FBRXJCLFdBQU87QUFDSCxjQUFNLEVBQU4sTUFBTTtBQUNOLGVBQU8sRUFBUCxPQUFPO0FBQ1AsaUJBQVMsRUFBVCxTQUFTO0FBQ1QsaUJBQVMsRUFBVCxTQUFTO0FBQ1QsMkJBQW1CLEVBQW5CLG1CQUFtQjtBQUNuQixlQUFPLEVBQVAsT0FBTztBQUNQLFdBQUcsRUFBSCxHQUFHO0FBQ0gsY0FBTSxFQUFOLE1BQU07S0FDVCxDQUFDOztBQUVGLGFBQVMsTUFBTSxHQUFFO0FBQ2IsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLEVBQUUsV0FBVztTQUNuQixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLE9BQU8sQ0FBQyxRQUFRLEVBQUU7QUFDdkIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLGlCQUFlLFFBQVEsQUFBRTtTQUMvQixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFNBQVMsR0FBRztBQUNqQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLGlCQUFpQixDQUFDO0FBQ3pCLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBR047O0FBRUQsYUFBUyxTQUFTLEdBQUc7QUFDakIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLEVBQUUsMkJBQTJCO1NBQ25DLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsT0FBTyxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFO0FBQzNDLFlBQUksYUFBYSxHQUFHLFNBQWhCLGFBQWEsQ0FBSSxRQUFRLEVBQUs7QUFDOUIsNEJBQWdCLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUNqQyxtQkFBTyxnQkFBZ0IsQ0FBQztTQUMzQixDQUFDO0FBQ0YsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDdkMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRywwQkFBMEIsQ0FBQztBQUNsQyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUixzQkFBTSxFQUFFO0FBQ0osNEJBQVEsRUFBRSxRQUFRO0FBQ2xCLDZCQUFTLEVBQUUsU0FBUztBQUNwQiwyQkFBTyxFQUFFLE9BQU87aUJBQ25CO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzFCLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsbUJBQW1CLENBQUMsa0JBQWtCLEVBQUU7QUFDN0MsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDbEMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyw4QkFBNEIsa0JBQWtCLEFBQUUsQ0FBQztBQUN4RCxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsR0FBRyxDQUFDLGdCQUFnQixFQUFFO0FBQzNCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsR0FBRyx3QkFBd0IsQ0FBQztBQUNuQyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLE1BQU07QUFDZCxtQkFBRyxFQUFFLEdBQUc7QUFDUixvQkFBSSxFQUFFLGdCQUFnQjtBQUN0Qix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsTUFBTSxDQUFDLGdCQUFnQixFQUFFO0FBQzlCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsOEJBQTRCLGdCQUFnQixBQUFFLENBQUM7QUFDdEQsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxRQUFRO0FBQ2hCLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047Q0FFSjs7cUJBRWMsdUJBQXVCOzs7O0FDekhyQyxZQUFZLENBQUM7Ozs7Ozs7O0lBRVIsZ0JBQWdCLEdBQ1AsU0FEVCxnQkFBZ0IsQ0FDTixNQUFNLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRTswQkFEcEcsZ0JBQWdCOztBQUdkLEtBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBQyxLQUFLLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQzs7QUFFckMsUUFBSSxVQUFVLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQztBQUNqQyxVQUFNLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSyxTQUFTLENBQUM7QUFDM0MsUUFBSSxRQUFRLENBQUM7O0FBRWIsVUFBTSxDQUFDLFFBQVEsR0FBRztBQUNkLGlCQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUU7S0FDeEIsQ0FBQzs7QUFFRixvQkFBZ0IsQ0FBQyxjQUFjLEVBQUUsQ0FDNUIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGNBQU0sQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztLQUN2QyxDQUFDLENBQUM7O0FBRVAsUUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO0FBQ2pCLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQix3QkFBZ0IsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQ25DLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixrQkFBTSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDOztBQUVoQyw0QkFBZ0IsQ0FBQyxjQUFjLEVBQUUsQ0FDNUIsSUFBSSxDQUFDLFVBQUMsWUFBWSxFQUFLO0FBQ3BCLHNCQUFNLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUM7O0FBRXhDLGlCQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7YUFDaEQsQ0FBQyxDQUFDO0FBQ1AsZ0JBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO0FBQ2pDLHNCQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sOEJBQTRCLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxBQUFFLENBQUM7YUFDaEY7U0FDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBQ1YsTUFBTTtBQUNILGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQix3QkFBZ0IsQ0FBQyxTQUFTLEVBQUUsQ0FDdkIsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3JCLG9CQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztTQUM1QixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBQ1Y7O0FBRUQsVUFBTSxDQUFDLFlBQVksR0FBRyxZQUFNO0FBQ3hCLGNBQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7S0FDcEMsQ0FBQzs7QUFFRixVQUFNLENBQUMsY0FBYyxHQUFHLFlBQU07QUFDMUIsb0JBQVksQ0FBQyxnQkFBZ0IsQ0FBQztBQUMxQixvQkFBUSxFQUFFO0FBQ04scUJBQUssRUFBRSxvQkFBb0I7QUFDM0Isb0JBQUksRUFBRSw4Q0FBOEM7QUFDcEQsa0JBQUUsRUFBRSxjQUFjO0FBQ2xCLHNCQUFNLEVBQUUsVUFBVTthQUNyQjtTQUNKLENBQUMsQ0FDRixJQUFJLENBQUMsWUFBTTtBQUNSLHNCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQiw0QkFBZ0IsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQzlCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixvQkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QiwwQkFBTSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztpQkFDcEM7YUFDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDhCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCwwQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDOUIsQ0FBQyxDQUFDO1NBQ1YsQ0FBQyxDQUFDO0tBQ0wsQ0FBQzs7QUFFRixVQUFNLENBQUMsSUFBSSxHQUFHLFlBQU07O0FBRWhCLFlBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO0FBQ2pDLGtCQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbkU7O0FBRUQsWUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7QUFDbEIsa0JBQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztBQUNwQyxzQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7O0FBRTFCLDRCQUFnQixDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQ2hDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixvQkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QiwwQkFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUN6QjthQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsOEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLDBCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDVixNQUFNO0FBQ0gsc0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLDRCQUFnQixDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQ25DLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixvQkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QiwwQkFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUN6QjthQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsOEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLDBCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDVjtLQUNKLENBQUM7Q0FDTDs7cUJBRVUsZ0JBQWdCOzs7Ozs7Ozs7Ozs7SUM5SHhCLG1CQUFtQixHQUNYLFNBRFIsbUJBQW1CLENBQ1YsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRTswQkFEckYsbUJBQW1COztBQUdsQixRQUFNLFFBQVEsR0FBRyxDQUFDLENBQUM7QUFDbkIsUUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDOztBQUVsQixVQUFNLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQzs7QUFFdEIsVUFBTSxDQUFDLE9BQU8sR0FBRyxZQUFNO0FBQ25CLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQix3QkFBZ0IsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUN4QyxJQUFJLENBQUMsVUFBQyxTQUFTLEVBQUs7QUFDakIsZ0JBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxRQUFRLEVBQUU7QUFDN0Isc0JBQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQzVCO0FBQ0QscUJBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDNUIsc0JBQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ25DLENBQUMsQ0FBQztBQUNILHFCQUFTLEVBQUcsQ0FBQztBQUNiLGtCQUFNLENBQUMsb0JBQW9CLEVBQUUsQ0FBQzs7QUFFOUIsZ0JBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtBQUMxQixzQkFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7YUFDeEI7U0FDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBQ1YsQ0FBQzs7QUFFRixVQUFNLENBQUMsZ0JBQWdCLEdBQUcsVUFBQyxVQUFVLEVBQUs7QUFDdEMsa0JBQVUsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7S0FDdEcsQ0FBQzs7QUFFRixVQUFNLENBQUMsb0JBQW9CLEdBQUcsVUFBQyxHQUFHLEVBQUs7QUFDbkMsWUFBSSxHQUFHLEVBQUU7QUFDTCxrQkFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDbkMsd0JBQVEsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQzthQUM1QyxDQUFDLENBQUM7U0FDTjs7QUFFRCxjQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ3JELG1CQUFPLFFBQVEsQ0FBQyxRQUFRLENBQUM7U0FDNUIsQ0FBQyxDQUFDOztBQUVILGNBQU0sQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDeEQsbUJBQU8sUUFBUSxDQUFDLFFBQVEsQ0FBQztTQUM1QixDQUFDLENBQUM7S0FDTixDQUFDOztBQUVGLFVBQU0sQ0FBQyxNQUFNLEdBQUcsVUFBQyxRQUFRLEVBQUs7O0FBRTFCLG9CQUFZLENBQUMsZ0JBQWdCLENBQUM7QUFDMUIsb0JBQVEsRUFBRTtBQUNOLHFCQUFLLHNCQUFzQjtBQUMzQixvQkFBSSwrQ0FBK0M7QUFDbkQsa0JBQUUsRUFBRSxjQUFjO0FBQ2xCLHNCQUFNLEVBQUUsVUFBVTthQUNyQjtTQUNKLENBQUMsQ0FDRCxJQUFJLENBQUMsWUFBTTtBQUNSLGdCQUFJLGNBQWMsQ0FBQztBQUNuQixnQkFBSSxRQUFRLEVBQUU7QUFDViw4QkFBYyxHQUFHLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQzFDLE1BQU07QUFDSCw4QkFBYyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQzVCLEdBQUcsQ0FBQyxVQUFDLFlBQVksRUFBSztBQUNuQix3QkFBSSxZQUFZLENBQUMsUUFBUSxFQUFFO0FBQ3ZCLCtCQUFPLFlBQVksQ0FBQyxVQUFVLENBQUM7cUJBQ2xDO0FBQ0QsMkJBQU8sSUFBSSxDQUFDO2lCQUNmLENBQUMsQ0FBQzthQUNWOztBQUVELDBCQUFjLENBQUMsT0FBTyxDQUFDLFVBQUMsVUFBVSxFQUFLO0FBQ25DLGdDQUFnQixDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FDOUIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLHdCQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDhCQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFlBQVksRUFBSztBQUN2QyxnQ0FBSSxVQUFVLEtBQUssWUFBWSxDQUFDLFVBQVUsRUFBRTtBQUN4QyxvQ0FBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDbkQsc0NBQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQzs2QkFDckM7eUJBQ0osQ0FBQyxDQUFDO3FCQUNOO2lCQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2Qsa0NBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3pDLENBQUMsQ0FBQzthQUNWLENBQUMsQ0FBQzs7QUFFSCxrQkFBTSxDQUFDLG9CQUFvQixFQUFFLENBQUM7U0FFakMsQ0FBQyxDQUFDO0tBQ04sQ0FBQzs7QUFFRixVQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7Q0FDcEI7O3FCQUlVLG1CQUFtQjs7Ozs7Ozs7Ozs7OzhDQ3RHRixtQ0FBbUM7Ozs7MkNBQ3RDLGdDQUFnQzs7Ozt3Q0FDaEMsNkJBQTZCOzs7O0FBSnpELElBQUksVUFBVSxHQUFHLG9CQUFvQixDQUFDOztBQU12QyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FDMUIsVUFBVSxDQUFDLHFCQUFxQiw4Q0FBc0IsQ0FDdEQsVUFBVSxDQUFDLDJCQUEyQiwyQ0FBbUIsQ0FDekQsT0FBTyxDQUFDLGtCQUFrQix3Q0FBbUIsQ0FBQzs7cUJBRW5DLFVBQVU7Ozs7Ozs7OztBQ1h4QixTQUFTLGdCQUFnQixDQUFDLEtBQUssRUFBRTtBQUM5QixnQkFBWSxDQUFDOztBQUViLFFBQUksU0FBUyxDQUFDOztBQUVkLFdBQU87QUFDSCxzQkFBYyxFQUFkLGNBQWM7QUFDZCxpQkFBUyxFQUFULFNBQVM7QUFDVCxtQkFBVyxFQUFYLFdBQVc7QUFDWCxlQUFPLEVBQVAsT0FBTztBQUNQLFdBQUcsRUFBSCxHQUFHO0FBQ0gsY0FBTSxFQUFOLE1BQU07QUFDTixjQUFNLEVBQU4sTUFBTTtLQUNULENBQUM7O0FBRUYsYUFBUyxjQUFjLEdBQUc7QUFDdEIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLEVBQUUsNkJBQTZCO1NBQ3JDLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsU0FBUyxHQUFHO0FBQ2pCLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxFQUFFLDJCQUEyQjtTQUNuQyxDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLE9BQU8sQ0FBQyxRQUFRLEVBQUUsU0FBUyxFQUFFO0FBQ2xDLFlBQUksYUFBYSxHQUFHLFNBQWhCLGFBQWEsQ0FBSSxRQUFRLEVBQUs7QUFDOUIscUJBQVMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzFCLG1CQUFPLFNBQVMsQ0FBQztTQUNwQixDQUFDOztBQUVGLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQztBQUMzQixtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUixzQkFBTSxFQUFFO0FBQ0osNEJBQVEsRUFBRSxRQUFRO0FBQ2xCLDZCQUFTLEVBQUMsU0FBUztpQkFDdEI7QUFDRCx1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDMUIsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxXQUFXLENBQUMsVUFBVSxFQUFFO0FBQzdCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2xDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsdUJBQXFCLFVBQVUsQUFBRSxDQUFDO0FBQ3pDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxHQUFHLENBQUMsUUFBUSxFQUFFO0FBQ25CLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsR0FBRyxpQkFBaUIsQ0FBQztBQUM1QixtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLE1BQU07QUFDZCxtQkFBRyxFQUFFLEdBQUc7QUFDUixvQkFBSSxFQUFFLFFBQVE7QUFDZCx1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsTUFBTSxDQUFDLFFBQVEsRUFBRTtBQUN0QixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsaUJBQWlCLENBQUM7QUFDNUIsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isb0JBQUksRUFBRSxRQUFRO0FBQ2QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLE1BQU0sQ0FBQyxVQUFVLEVBQUU7QUFDeEIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDdkMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyx1QkFBcUIsVUFBVSxBQUFFLENBQUM7QUFDekMsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxRQUFRO0FBQ2hCLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047Q0FDSjs7cUJBRWMsZ0JBQWdCOzs7Ozs7Ozs7Ozs7SUMvR3hCLGdCQUFnQixHQUNSLFNBRFIsZ0JBQWdCLENBQ1AsTUFBTSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLGdCQUFnQixFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQUUsaUJBQWlCLEVBQUU7MEJBRHRILGdCQUFnQjs7QUFHZixLQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7O0FBRXZDLFFBQUksVUFBVSxHQUFHLFlBQVksQ0FBQyxFQUFFLENBQUM7QUFDakMsVUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLEtBQUssU0FBUyxDQUFDO0FBQzNDLFVBQU0sQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO0FBQzFCLFVBQU0sQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO0FBQzFCLFVBQU0sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO0FBQ3RCLFFBQUksUUFBUSxDQUFDOztBQUViLFVBQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDOztBQUVsQixVQUFNLENBQUMsY0FBYyxHQUFHO0FBQ3BCLGVBQU8sRUFBRSxJQUFJO0FBQ2IsYUFBSyxFQUFFLElBQUk7S0FDZCxDQUFDOztBQUVGLFVBQU0sQ0FBQyxRQUFRLEdBQUc7QUFDZCxpQkFBUyxFQUFFLElBQUksSUFBSSxFQUFFO0FBQ3JCLGdCQUFRLEVBQUUsQ0FBQztBQUNYLGVBQU8sRUFBRSxFQUFFO0tBQ2QsQ0FBQzs7QUFFRixvQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FDekIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGNBQU0sQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztLQUNyQyxDQUFDLENBQUM7O0FBRVAsb0JBQWdCLENBQUMsaUJBQWlCLEVBQUUsQ0FDL0IsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGNBQU0sQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztLQUN4QyxDQUFDLENBQUM7O0FBRVAsVUFBTSxDQUFDLGNBQWMsR0FBRyxZQUFNO0FBQzFCLGNBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0FBQzVCLHdCQUFnQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUNuRCxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsa0JBQU0sQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUNyQyxhQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxPQUFPLENBQUM7QUFDeEIscUJBQUssRUFBRSxNQUFNO2FBQ2hCLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNWLENBQUE7O0FBRUQsb0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQ3pCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixjQUFNLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7S0FDcEMsQ0FBQyxDQUFDOztBQUVQLFVBQU0sQ0FBQyxNQUFNLEdBQUcsWUFBTTtBQUNsQixrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsd0JBQWdCLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQ3pDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixrQkFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7QUFDekIsa0JBQU0sQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztTQUNoQyxDQUFDLFNBQU0sQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNoQiwwQkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsc0JBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQzlCLENBQUMsQ0FBQztLQUNWLENBQUE7O0FBRUQsVUFBTSxDQUFDLGlCQUFpQixHQUFHLFlBQU07QUFDN0IsWUFBSSxNQUFNLEdBQUc7QUFDVCxpQkFBSyxFQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSztBQUM1QixtQkFBTyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTztTQUNoQyxDQUFDO0FBQ0YsY0FBTSxDQUFDLFVBQVUsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztBQUMzQyxjQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDckMsY0FBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7S0FDNUIsQ0FBQTs7QUFFRCxRQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7QUFDakIsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLHdCQUFnQixDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FDbkMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGtCQUFNLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDaEMsa0JBQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDOztBQUV6QixrQkFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUMsTUFBTSxFQUFLO0FBQ3hDLHNCQUFNLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUM7YUFDckMsQ0FBQyxDQUFDO0FBQ0gsNEJBQWdCLENBQUMsV0FBVyxFQUFFLENBQ3pCLElBQUksQ0FBQyxVQUFDLGdCQUFnQixFQUFLO0FBQ3hCLHNCQUFNLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDLElBQUksQ0FBQztBQUN6QyxpQkFBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO2FBQzdDLENBQUMsQ0FBQzs7QUFFUCw0QkFBZ0IsQ0FBQyxjQUFjLEVBQUUsQ0FDNUIsSUFBSSxDQUFDLFVBQUMsWUFBWSxFQUFLO0FBQ3BCLHNCQUFNLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUM7QUFDeEMsaUJBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQzthQUNoRCxDQUFDLENBQUM7U0FFVixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBQ1Y7O0FBRUQsVUFBTSxDQUFDLFdBQVcsR0FBRyxVQUFDLFNBQVMsRUFBSztBQUNoQyxZQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDdkQsY0FBTSxDQUFDLFVBQVUsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDO0FBQ3JDLGNBQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7S0FDNUMsQ0FBQTs7QUFFRCxVQUFNLENBQUMsWUFBWSxHQUFHLFlBQU07QUFDeEIsY0FBTSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztLQUNwQyxDQUFDOztBQUVGLFVBQU0sQ0FBQyxlQUFlLEdBQUcsWUFBTTtBQUMzQix5QkFBaUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUMvQixJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7O0FBRXJCLDRCQUFnQixDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FDOUIsSUFBSSxDQUFDLFVBQUMsYUFBYSxFQUFLO0FBQ3JCLHNCQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztBQUN6QixzQkFBTSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDO2FBQ3JDLENBQUMsQ0FBQztTQUVWLENBQUMsQ0FBQztLQUNWLENBQUM7O0FBRUYsVUFBTSxDQUFDLElBQUksR0FBRyxZQUFNO0FBQ2hCLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQix3QkFBZ0IsQ0FBQyxTQUFTLEVBQUUsQ0FDdkIsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3JCLG9CQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQzs7QUFFekIsa0JBQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztBQUNwQyw0QkFBZ0IsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUNoQyxJQUFJLENBQUMsVUFBQyxXQUFXLEVBQUs7QUFDbkIsb0JBQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7QUFDNUIsMEJBQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDekI7YUFDSixDQUFDLENBQUM7U0FFVixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBRVYsQ0FBQztDQUVMOztxQkFHVSxnQkFBZ0I7Ozs7Ozs7Ozs7OztJQzVKeEIsbUJBQW1CLEdBQ1gsU0FEUixtQkFBbUIsQ0FDVixNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFOzBCQURyRixtQkFBbUI7O0FBR2xCLFFBQU0sUUFBUSxHQUFHLENBQUMsQ0FBQztBQUNuQixRQUFJLFNBQVMsR0FBRyxDQUFDLENBQUM7O0FBRWxCLFVBQU0sQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDOztBQUV0QixVQUFNLENBQUMsT0FBTyxHQUFHLFlBQU07O0FBRW5CLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7QUFFMUIsd0JBQWdCLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FDeEMsSUFBSSxDQUFDLFVBQUMsU0FBUyxFQUFLO0FBQ2pCLGdCQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsUUFBUSxFQUFFO0FBQzdCLHNCQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzthQUM1QjtBQUNELHFCQUFTLENBQUMsT0FBTyxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQzVCLHNCQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNuQyxDQUFDLENBQUM7QUFDSCxxQkFBUyxFQUFHLENBQUM7QUFDYixrQkFBTSxDQUFDLG9CQUFvQixFQUFFLENBQUM7QUFDOUIsZ0JBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtBQUMxQixzQkFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7YUFDeEI7U0FDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBQ1YsQ0FBQzs7QUFFRixVQUFNLENBQUMsZ0JBQWdCLEdBQUcsVUFBQyxVQUFVLEVBQUs7QUFDdEMsa0JBQVUsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7S0FDdEcsQ0FBQzs7QUFFRixVQUFNLENBQUMsZUFBZSxHQUFHLFlBQU07QUFDM0IsY0FBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUNoQyxDQUFDOztBQUVGLFVBQU0sQ0FBQyxvQkFBb0IsR0FBRyxVQUFDLEdBQUcsRUFBSztBQUNuQyxZQUFJLEdBQUcsRUFBRTtBQUNMLGtCQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNuQyx3QkFBUSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDO2FBQzVDLENBQUMsQ0FBQztTQUNOOztBQUVELGNBQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDckQsbUJBQU8sUUFBUSxDQUFDLFFBQVEsQ0FBQztTQUM1QixDQUFDLENBQUM7O0FBRUgsY0FBTSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUN4RCxtQkFBTyxRQUFRLENBQUMsUUFBUSxDQUFDO1NBQzVCLENBQUMsQ0FBQztLQUNOLENBQUM7O0FBRUYsVUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0NBQ3BCOztxQkFHVSxtQkFBbUI7Ozs7Ozs7Ozs7Ozs4Q0M1REYsbUNBQW1DOzs7OzJDQUN0QyxnQ0FBZ0M7Ozs7d0NBQ2hDLDZCQUE2Qjs7OztBQUp6RCxJQUFJLFVBQVUsR0FBRyxvQkFBb0IsQ0FBQzs7QUFNdkMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQzFCLFVBQVUsQ0FBQyxxQkFBcUIsOENBQXNCLENBQ3RELFVBQVUsQ0FBQywyQkFBMkIsMkNBQW1CLENBQ3pELE9BQU8sQ0FBQyxrQkFBa0Isd0NBQW1CLENBQUM7cUJBQ25DLFVBQVU7Ozs7Ozs7OztBQ1Z4QixTQUFTLGdCQUFnQixDQUFDLEtBQUssRUFBRTtBQUM5QixnQkFBWSxDQUFDOztBQUViLFFBQUksU0FBUyxDQUFDOztBQUVkLFdBQU87QUFDSCxtQkFBVyxFQUFYLFdBQVc7QUFDWCxzQkFBYyxFQUFkLGNBQWM7QUFDZCx5QkFBaUIsRUFBakIsaUJBQWlCO0FBQ2pCLG9CQUFZLEVBQVosWUFBWTtBQUNaLGdCQUFRLEVBQVIsUUFBUTtBQUNSLG1CQUFXLEVBQVgsV0FBVztBQUNYLHNCQUFjLEVBQWQsY0FBYztBQUNkLG1CQUFXLEVBQVgsV0FBVztBQUNYLGlCQUFTLEVBQVQsU0FBUztBQUNULG1CQUFXLEVBQVgsV0FBVztBQUNYLGVBQU8sRUFBUCxPQUFPO0FBQ1AsV0FBRyxFQUFILEdBQUc7QUFDSCxjQUFNLEVBQU4sTUFBTTtLQUNULENBQUM7O0FBRUYsYUFBUyxXQUFXLEdBQUU7QUFDbEIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDbEMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRywwQkFBMEIsQ0FBQztBQUNsQyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsV0FBVyxHQUFHO0FBQ25CLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxFQUFFLGlCQUFpQjtTQUN6QixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLGlCQUFpQixHQUFHO0FBQ3pCLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxzQkFBc0I7U0FDNUIsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxjQUFjLENBQUMsVUFBVSxFQUFFO0FBQ2hDLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRywwQkFBd0IsVUFBVSxBQUFFO1NBQzFDLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsY0FBYyxHQUFHO0FBQ3RCLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxFQUFFLDZCQUE2QjtTQUNyQyxDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFdBQVcsR0FBRTtBQUNsQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLDBCQUEwQixDQUFDO0FBQ2xDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047QUFDRCxhQUFTLFFBQVEsQ0FBQyxPQUFPLEVBQUU7QUFDdkIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDbEMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyxvQkFBa0IsT0FBTyxBQUFFLENBQUM7QUFDbkMsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1IsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFlBQVksQ0FBQyxFQUFFLEVBQUU7QUFDdEIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDbEMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyx5QkFBeUIsQ0FBQztBQUNqQyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUixzQkFBTSxFQUFFO0FBQ0osc0JBQUUsRUFBRSxFQUFFO2lCQUNUO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFNBQVMsR0FBRztBQUNqQixlQUFPLEtBQUssQ0FBQztBQUNULGtCQUFNLEVBQUUsS0FBSztBQUNiLGVBQUcsRUFBRSwyQkFBMkI7U0FDbkMsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRTtBQUNsQyxZQUFJLGFBQWEsR0FBRyxTQUFoQixhQUFhLENBQUksUUFBUSxFQUFLO0FBQzlCLHFCQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUMxQixtQkFBTyxTQUFTLENBQUM7U0FDcEIsQ0FBQzs7QUFFRixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsZ0JBQWdCLENBQUM7QUFDM0IsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isc0JBQU0sRUFBRTtBQUNKLDRCQUFRLEVBQUUsUUFBUTtBQUNsQiw2QkFBUyxFQUFDLFNBQVM7aUJBQ3RCO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzFCLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsV0FBVyxDQUFDLFVBQVUsRUFBRTtBQUM3QixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLHVCQUFxQixVQUFVLEFBQUUsQ0FBQztBQUN6QyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsR0FBRyxDQUFDLFFBQVEsRUFBRTtBQUNuQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsaUJBQWlCLENBQUM7QUFDNUIsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxNQUFNO0FBQ2QsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isb0JBQUksRUFBRSxRQUFRO0FBQ2QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLE1BQU0sQ0FBQyxRQUFRLEVBQUU7QUFDdEIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDdkMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyxHQUFHLGlCQUFpQixDQUFDO0FBQzVCLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLG9CQUFJLEVBQUUsUUFBUTtBQUNkLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047Q0FFSjs7cUJBRWMsZ0JBQWdCOzs7Ozs7Ozs7Ozs7QUN2TDlCLElBQU0sS0FBSyxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7O0lBRXZCLGdCQUFnQixHQUNQLFNBRFQsZ0JBQWdCLENBQ04sTUFBTSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFOzs7MEJBRC9DLGdCQUFnQjs7QUFFZCxRQUFJLEVBQUUsR0FBRyxJQUFJLENBQUM7QUFDZCxTQUFLLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQzs7QUFFeEIsWUFBUSxDQUFDLFlBQU07QUFDWCxjQUFLLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztBQUNqQyxVQUFFLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO0tBQ3JDLEVBQUUsRUFBRSxDQUFDLENBQUM7O0FBRVAsY0FBVSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFDOUIsVUFBQyxDQUFDLEVBQUUsT0FBTyxFQUFLO0FBQ1osY0FBSyxLQUFLLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztBQUMxQixVQUFFLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUM7O0FBRTNCLFlBQUksT0FBTyxDQUFDLElBQUksS0FBSyxXQUFXLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxVQUFVLEVBQzNELE1BQUssS0FBSyxHQUFHLFVBQVUsQ0FBQzs7QUFFNUIsWUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFDbkQsTUFBSyxLQUFLLEdBQUcsT0FBTyxDQUFDOztBQUV6QixZQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssUUFBUSxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssT0FBTyxFQUNyRCxNQUFLLEtBQUssR0FBRyxRQUFRLENBQUM7O0FBRTFCLFlBQUksT0FBTyxDQUFDLElBQUksS0FBSyxhQUFhLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxXQUFXLEVBQzlELE1BQUssS0FBSyxHQUFHLFlBQVksQ0FBQzs7QUFFOUIsWUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLFNBQVMsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFDdkQsTUFBSyxLQUFLLEdBQUcsY0FBYyxDQUFDOztBQUVoQyxZQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssV0FBVyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUMzRCxNQUFLLEtBQUssR0FBRyxTQUFTLENBQUM7O0FBRTNCLFlBQUksT0FBTyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLGNBQWMsRUFDcEUsTUFBSyxLQUFLLEdBQUcsV0FBVyxDQUFDOztBQUU3QixZQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssV0FBVyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUMzRCxNQUFLLEtBQUssR0FBRyxhQUFhLENBQUM7O0FBRS9CLFlBQUksT0FBTyxDQUFDLElBQUksS0FBSyxhQUFhLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxZQUFZLEVBQy9ELE1BQUssS0FBSyxHQUFHLFFBQVEsQ0FBQzs7QUFFMUIsa0JBQVUsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0tBQy9CLENBQUMsQ0FBQzs7QUFFUCxTQUFLLENBQUM7QUFDRixjQUFNLEVBQUUsS0FBSztBQUNiLFdBQUcsRUFBRSx5QkFBeUI7S0FDakMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQixVQUFFLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7S0FDL0IsQ0FBQyxDQUFDO0NBRU47O0FBR0wsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLENBQUMsUUFBUSxFQUFFLFlBQVksRUFBRSxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7cUJBQzFELGdCQUFnQjs7Ozs7Ozs7Ozs7Ozs7SUMxRHhCLFNBQVM7QUFDRCxhQURSLFNBQVMsR0FDRTs4QkFEWCxTQUFTOztBQUVSLFlBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO0FBQ3BCLFlBQUksQ0FBQyxXQUFXLEdBQUcsb0VBQW9FLENBQUM7QUFDeEYsWUFBSSxDQUFDLFVBQVUsR0FBRyxrQkFBa0IsQ0FBQztBQUNyQyxZQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztLQUM1Qjs7aUJBTkUsU0FBUzs7ZUFRUixjQUFDLEtBQUssRUFBRTtBQUNSLGFBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQUMsS0FBSyxFQUFLO0FBQ2pDLG9CQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRTtBQUNqQix5QkFBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO2lCQUMzQixNQUFNO0FBQ0gseUJBQUssQ0FBQyxNQUFNLENBQUMsWUFBTTtBQUNmLDZCQUFLLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztxQkFDMUIsQ0FBQyxDQUFDO2lCQUNOO2FBQ0osQ0FBQyxDQUFDOztBQUVILGFBQUMsQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBQyxLQUFLLEVBQUs7QUFDakUscUJBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUMzQixDQUFDLENBQUM7U0FDTjs7O2VBRXNCLDRCQUFHO0FBQ3RCLHFCQUFTLENBQUMsUUFBUSxHQUFHLElBQUksU0FBUyxFQUFFLENBQUM7QUFDckMsbUJBQU8sU0FBUyxDQUFDLFFBQVEsQ0FBQztTQUM3Qjs7O1dBM0JFLFNBQVM7OztxQkE4QkQsU0FBUyxDQUFDLGdCQUFnQjs7Ozs7Ozs7Ozs7Ozs7SUM5QmxDLFFBQVE7QUFDQSxhQURSLFFBQVEsR0FDRzs4QkFEWCxRQUFROztBQUVQLFlBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO0FBQ3BCLFlBQUksQ0FBQyxXQUFXLEdBQUcsa0VBQWtFLENBQUM7QUFDdEYsWUFBSSxDQUFDLFVBQVUsR0FBRyxrQkFBa0IsQ0FBQztBQUNyQyxZQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztLQUM1Qjs7aUJBTkUsUUFBUTs7ZUFRWSw0QkFBRztBQUN0QixvQkFBUSxDQUFDLFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO0FBQ25DLG1CQUFPLFFBQVEsQ0FBQyxRQUFRLENBQUM7U0FDNUI7OztXQVhFLFFBQVE7OztxQkFjQSxRQUFRLENBQUMsZ0JBQWdCOzs7Ozs7Ozs7QUNkdkMsU0FBUyxlQUFlLEdBQUc7QUFDeEIsZ0JBQVksQ0FBQztBQUNiLFdBQU8sVUFBUyxLQUFLLEVBQUU7QUFDbkIsWUFBSSxDQUFDLEtBQUssRUFBRTtBQUNSLG1CQUFPLEtBQUssQ0FBQztTQUNoQjs7QUFFRCxZQUFJLElBQUksR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7O0FBRTFDLFlBQUksQ0FBQyxJQUFJLEVBQUU7QUFDUCxtQkFBTyxLQUFLLENBQUM7U0FDaEI7QUFDRCxZQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQzVCLGNBQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQzlELGVBQU8sTUFBTSxDQUFDO0tBQ2pCLENBQUM7Q0FDTDs7cUJBRWMsZUFBZTs7Ozs7Ozs7O0FDbEI3QixTQUFTLGdCQUFnQixDQUFDLFNBQVMsRUFBRTtBQUNsQyxnQkFBWSxDQUFDOztBQUViLFFBQUksaUJBQWlCLEdBQUcsRUFBRSxDQUFDOztBQUUzQixXQUFPLFVBQVMsU0FBUyxFQUFFO0FBQ3ZCLFlBQUksaUJBQWlCLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFOztBQUU3QyxxQkFBUyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDO0FBQ2xELDZCQUFpQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztBQUNsQyxtQkFBTyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxTQUFTLENBQUMsQ0FBQztTQUNsRDtLQUNKLENBQUM7Q0FDTDs7cUJBRWMsZ0JBQWdCOzs7Ozs7Ozs7QUNmOUIsU0FBUyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7QUFDaEMsZ0JBQVksQ0FBQzs7QUFFYixXQUFPO0FBQ0gsd0JBQWdCLEVBQWhCLGdCQUFnQjtLQUNuQixDQUFDOztBQUdGLGFBQVMsZ0JBQWdCLEdBQUc7O0FBRXhCLGVBQU8sTUFBTSxDQUFDLElBQUksQ0FBQztBQUNmLHVCQUFXLEVBQUUscURBQXFEO0FBQ2xFLHNCQUFVLEVBQUUsQ0FBQyxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsa0JBQWtCLEVBQUUsZUFBZSxFQUN4RSxVQUFVLE1BQU0sRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLEVBQUUsYUFBYSxFQUFFOztBQUVsRSxzQkFBTSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7O0FBRXBCLG9CQUFJLFFBQVEsQ0FBQztBQUNiLHNCQUFNLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztBQUN4QixzQkFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7O0FBRXZCLHNCQUFNLENBQUMsS0FBSyxHQUFHO0FBQ1gsNkJBQVMsRUFBRSxJQUFJLElBQUksRUFBRTtBQUNyQiwrQkFBVyxFQUFFLENBQUM7aUJBQ2pCLENBQUM7O0FBRUYsNkJBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FDckIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLDBCQUFNLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7aUJBQ2xDLENBQUMsQ0FBQzs7QUFFUCxnQ0FBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FDekIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLDBCQUFNLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDbEMscUJBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztpQkFDL0MsQ0FBQyxDQUFDOztBQUVQLDZCQUFhLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQ2hELElBQUksQ0FBQyxVQUFDLG1CQUFtQixFQUFLO0FBQzNCLDBCQUFNLENBQUMsYUFBYSxHQUFHLG1CQUFtQixDQUFDLElBQUksQ0FBQztBQUNoRCxxQkFBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7aUJBQ2xELENBQUMsQ0FBQzs7QUFFUCxzQkFBTSxDQUFDLGNBQWMsR0FBRyxZQUFNO0FBQzFCLDBCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztBQUM1QixpQ0FBYSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUNoRCxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsOEJBQU0sQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUNyQyx5QkFBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsT0FBTyxDQUFDO0FBQ3hCLGlDQUFLLEVBQUUsTUFBTTt5QkFDaEIsQ0FBQyxDQUFDO3FCQUNOLENBQUMsQ0FBQztpQkFDVixDQUFBOztBQUdELDZCQUFhLENBQUMsT0FBTyxFQUFFLENBQ2xCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQiwwQkFBTSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLHFCQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7aUJBQzFDLENBQUMsQ0FBQzs7QUFFUCw2QkFBYSxDQUFDLFVBQVUsRUFBRSxDQUNyQixJQUFJLENBQUMsVUFBQyxjQUFjLEVBQUs7QUFDdEIsMEJBQU0sQ0FBQyxPQUFPLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQztBQUNyQyxxQkFBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO2lCQUM3QyxDQUFDLENBQUM7O0FBRVAsNkJBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FDcEIsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3JCLDRCQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztpQkFDNUIsQ0FBQyxDQUFDOztBQUVQLHNCQUFNLENBQUMsY0FBYyxHQUFHLFlBQU07QUFDMUIsd0JBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXLEtBQUssQ0FBQyxFQUFFO0FBQ2hDLDhCQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztxQkFDMUIsTUFBTTtBQUNILDhCQUFNLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzs7QUFFeEIseUJBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLENBQUM7QUFDcEIsaUNBQUssRUFBRSxNQUFNO3lCQUNoQixDQUFDLENBQUM7cUJBQ047aUJBQ0osQ0FBQTs7QUFFRCw2QkFBYSxDQUFDLFdBQVcsRUFBRSxDQUN0QixJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsMEJBQU0sQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUNqQywwQkFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO2lCQUMvQixDQUFDLENBQUM7O0FBRVAsc0JBQU0sQ0FBQyxJQUFJLEdBQUcsWUFBTTs7QUFFaEIsd0JBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO0FBQzlCLDhCQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQzdEO0FBQ0QsMEJBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztBQUNqQyxpQ0FBYSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQzFCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQiw0QkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QiwwQ0FBYyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7eUJBQ3ZDO3FCQUNKLENBQUMsQ0FBQztpQkFDVixDQUFBOztBQUVHLHNCQUFNLENBQUMsTUFBTSxHQUFHLFlBQVc7QUFDdkIsa0NBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3BDLENBQUM7YUFDTCxDQUFDO0FBQ04sZ0JBQUksRUFBRSxJQUFJO1NBQ2IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBUyxNQUFNLEVBQUU7QUFDNUIsbUJBQU8sTUFBTSxDQUFDO1NBQ2pCLENBQUMsQ0FBQztLQUNOO0NBQ0o7O3FCQUVjLGlCQUFpQjs7Ozs7Ozs7O0FDbkgvQixTQUFTLFlBQVksQ0FBQyxNQUFNLEVBQUU7QUFDM0IsZ0JBQVksQ0FBQzs7QUFFYixXQUFPO0FBQ0gsd0JBQWdCLEVBQWhCLGdCQUFnQjtLQUNuQixDQUFDOztBQUVGLGFBQVMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFO0FBQzVCLGVBQU8sTUFBTSxDQUFDLElBQUksQ0FBQzs7QUFFZix1QkFBVyxFQUFFLGdEQUFnRDs7QUFFN0Qsc0JBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLE1BQU0sRUFBRSxjQUFjLEVBQUU7O0FBRXZFLHNCQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7O0FBRWhDLHNCQUFNLENBQUMsRUFBRSxHQUFHLFlBQVk7QUFDcEIsa0NBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDMUIsQ0FBQzs7QUFFRixzQkFBTSxDQUFDLE1BQU0sR0FBRyxZQUFZO0FBQ3hCLGtDQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUNwQyxDQUFDO2FBQ0wsQ0FBQzs7U0FFTCxDQUFDLENBQUMsTUFBTSxDQUFDO0tBQ2I7Q0FDSjs7cUJBRWMsWUFBWTs7Ozs7Ozs7O0FDN0IxQixTQUFTLGNBQWMsQ0FBQyxPQUFPLEVBQUU7QUFDOUIsZ0JBQVksQ0FBQzs7QUFFYixXQUFPO0FBQ0gsdUJBQWUsRUFBZixlQUFlO0tBQ2xCLENBQUM7O0FBRUYsYUFBUyxlQUFlLEdBQUc7QUFDdkIsZUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLDRCQUE0QixDQUFDLENBQUM7S0FDL0Q7Q0FFSjs7cUJBRWMsY0FBYzs7Ozs7Ozs7Ozs7O21EQ1hDLHlDQUF5Qzs7OztxREFDeEMsMkNBQTJDOzs7OzJDQUM3QyxnQ0FBZ0M7Ozs7c0NBQ2xDLDJCQUEyQjs7OztvQ0FDN0IseUJBQXlCOzs7O3lDQUNwQiw4QkFBOEI7Ozs7d0NBQy9CLDZCQUE2Qjs7OztzQ0FDOUIsMkJBQTJCOzs7O0FBVHRELElBQUksVUFBVSxHQUFHLGlCQUFpQixDQUFDOztBQVlwQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDLGNBQWMsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUNuRCxTQUFTLENBQUMsVUFBVSxtREFBb0IsQ0FDeEMsU0FBUyxDQUFDLFdBQVcscURBQXFCLENBQzFDLFVBQVUsQ0FBQyxrQkFBa0IsMkNBQW1CLENBQ2hELE9BQU8sQ0FBQyxnQkFBZ0Isc0NBQWlCLENBQ3pDLE9BQU8sQ0FBQyxjQUFjLG9DQUFlLENBQ3JDLE9BQU8sQ0FBQyxtQkFBbUIseUNBQW9CLENBQy9DLE9BQU8sQ0FBQyxtQkFBbUIsd0NBQW1CLENBQzlDLE1BQU0sQ0FBQyxXQUFXLHNDQUFrQixDQUFDOztxQkFFMUIsVUFBVTs7Ozs7Ozs7Ozs7O0lDdEJsQixnQkFBZ0IsR0FDUixTQURSLGdCQUFnQixDQUNQLE1BQU0sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxjQUFjLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRTswQkFEakcsZ0JBQWdCOztBQUdmLFFBQUksUUFBUSxHQUFHLFlBQVksQ0FBQyxFQUFFLENBQUM7QUFDL0IsVUFBTSxDQUFDLFFBQVEsR0FBRyxRQUFRLEtBQUssU0FBUyxDQUFDOztBQUV6QyxRQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7QUFDakIsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLHNCQUFjLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUM3QixJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsa0JBQU0sQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztTQUNqQyxDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBQ1Y7O0FBRUQsVUFBTSxDQUFDLFlBQVksR0FBRyxZQUFNO0FBQ3hCLGNBQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7S0FDbEMsQ0FBQzs7QUFFRixVQUFNLENBQUMsWUFBWSxHQUFHLFlBQU07QUFDeEIsb0JBQVksQ0FBQyxnQkFBZ0IsQ0FBQztBQUMxQixvQkFBUSxFQUFFO0FBQ04scUJBQUssRUFBRSxzQkFBc0I7QUFDN0Isb0JBQUksRUFBRSxnREFBZ0Q7QUFDdEQsa0JBQUUsRUFBRSxjQUFjO0FBQ2xCLHNCQUFNLEVBQUUsVUFBVTthQUNyQjtTQUNKLENBQUMsQ0FDRixJQUFJLENBQUMsWUFBTTtBQUNSLHNCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQiwwQkFBYyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FDMUIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLG9CQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDBCQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUNsQzthQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsOEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLDBCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDVixDQUFDLENBQUM7S0FDTCxDQUFDOztBQUVGLFVBQU0sQ0FBQyxJQUFJLEdBQUcsWUFBTTtBQUNoQixZQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtBQUNsQixrQkFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0FBQ2xDLHNCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7QUFFMUIsMEJBQWMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUM1QixJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsb0JBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7QUFDekIsMEJBQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDekI7YUFDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDhCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCwwQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDOUIsQ0FBQyxDQUFDO1NBQ1YsTUFBTTtBQUNILHNCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQiwwQkFBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQy9CLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixvQkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QiwwQkFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUN6QjthQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsOEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLDBCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDVjtLQUNKLENBQUM7Q0FDTDs7cUJBR1UsZ0JBQWdCOzs7Ozs7Ozs7Ozs7SUN0RnhCLGlCQUFpQixHQUNULFNBRFIsaUJBQWlCLENBQ1IsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQUU7MEJBRG5GLGlCQUFpQjs7QUFHaEIsUUFBTSxRQUFRLEdBQUcsQ0FBQyxDQUFDO0FBQ25CLFFBQUksU0FBUyxHQUFHLENBQUMsQ0FBQzs7QUFFbEIsVUFBTSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7O0FBRXBCLFVBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBTTtBQUNuQixrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsc0JBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUN0QyxJQUFJLENBQUMsVUFBQyxPQUFPLEVBQUs7QUFDZixnQkFBSSxPQUFPLENBQUMsTUFBTSxHQUFHLFFBQVEsRUFBRTtBQUMzQixzQkFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDNUI7QUFDRCxtQkFBTyxDQUFDLE9BQU8sQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUN4QixzQkFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDL0IsQ0FBQyxDQUFDO0FBQ0gscUJBQVMsRUFBRyxDQUFDO0FBQ2Isa0JBQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDOztBQUU5QixnQkFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO0FBQ3hCLHNCQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzthQUN4QjtTQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVixDQUFDOztBQUVGLFVBQU0sQ0FBQyxnQkFBZ0IsR0FBRyxVQUFDLFFBQVEsRUFBSztBQUNwQyxnQkFBUSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztLQUM5RixDQUFDOztBQUVGLFVBQU0sQ0FBQyxvQkFBb0IsR0FBRyxVQUFDLEdBQUcsRUFBSztBQUNuQyxZQUFJLEdBQUcsRUFBRTtBQUNMLGtCQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUMvQixzQkFBTSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDO2FBQzFDLENBQUMsQ0FBQztTQUNOOztBQUVELGNBQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFNLEVBQUs7QUFDakQsbUJBQU8sTUFBTSxDQUFDLFFBQVEsQ0FBQztTQUMxQixDQUFDLENBQUM7O0FBRUgsY0FBTSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUNwRCxtQkFBTyxNQUFNLENBQUMsUUFBUSxDQUFDO1NBQzFCLENBQUMsQ0FBQztLQUNOLENBQUM7O0FBRUYsVUFBTSxDQUFDLE1BQU0sR0FBRyxVQUFDLE1BQU0sRUFBSzs7QUFFeEIsb0JBQVksQ0FBQyxnQkFBZ0IsQ0FBQztBQUMxQixvQkFBUSxFQUFFO0FBQ04scUJBQUssd0JBQXdCO0FBQzdCLG9CQUFJLGlEQUFpRDtBQUNyRCxrQkFBRSxFQUFFLGNBQWM7QUFDbEIsc0JBQU0sRUFBRSxVQUFVO2FBQ3JCO1NBQ0osQ0FBQyxDQUNELElBQUksQ0FBQyxZQUFNO0FBQ1IsZ0JBQUksWUFBWSxDQUFDO0FBQ2pCLGdCQUFJLE1BQU0sRUFBRTtBQUNSLDRCQUFZLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDcEMsTUFBTTtBQUNILDRCQUFZLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FDeEIsR0FBRyxDQUFDLFVBQUMsVUFBVSxFQUFLO0FBQ2pCLHdCQUFJLFVBQVUsQ0FBQyxRQUFRLEVBQUU7QUFDckIsK0JBQU8sVUFBVSxDQUFDLFFBQVEsQ0FBQztxQkFDOUI7QUFDRCwyQkFBTyxJQUFJLENBQUM7aUJBQ2YsQ0FBQyxDQUFDO2FBQ1Y7O0FBRUQsd0JBQVksQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDL0IsOEJBQWMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQzFCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQix3QkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6Qiw4QkFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFVLEVBQUs7QUFDbkMsZ0NBQUksUUFBUSxLQUFLLFVBQVUsQ0FBQyxRQUFRLEVBQUU7QUFDbEMsb0NBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQy9DLHNDQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7NkJBQ25DO3lCQUNKLENBQUMsQ0FBQztxQkFDTjtpQkFDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLGtDQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN6QyxDQUFDLENBQUM7YUFDVixDQUFDLENBQUM7O0FBRUgsa0JBQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1NBRWpDLENBQUMsQ0FBQztLQUNOLENBQUM7O0FBRUYsVUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0NBQ3BCOztxQkFJVSxpQkFBaUI7Ozs7Ozs7OztBQ3hHL0IsU0FBUyxjQUFjLENBQUMsS0FBSyxFQUFFO0FBQzVCLGdCQUFZLENBQUM7O0FBRWIsUUFBSSxPQUFPLENBQUM7O0FBRVosV0FBTztBQUNILGlCQUFTLEVBQVQsU0FBUztBQUNULGVBQU8sRUFBUCxPQUFPO0FBQ1AsV0FBRyxFQUFILEdBQUc7QUFDSCxjQUFNLEVBQU4sTUFBTTtBQUNOLGNBQU0sRUFBTixNQUFNO0tBQ1QsQ0FBQzs7QUFFRixhQUFTLE9BQU8sQ0FBQyxRQUFRLEVBQUUsU0FBUyxFQUFFO0FBQ2xDLFlBQUksYUFBYSxHQUFHLFNBQWhCLGFBQWEsQ0FBSSxRQUFRLEVBQUs7QUFDOUIsbUJBQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQ3hCLG1CQUFPLE9BQU8sQ0FBQztTQUNsQixDQUFDO0FBQ0YsWUFBSSxHQUFHLEdBQUcsY0FBYyxDQUFDO0FBQ3pCLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxLQUFLO0FBQ2IsZUFBRyxFQUFFLEdBQUc7QUFDUixrQkFBTSxFQUFFO0FBQ0osd0JBQVEsRUFBRSxRQUFRO0FBQ2xCLHlCQUFTLEVBQUMsU0FBUzthQUN0QjtTQUNKLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7S0FDMUI7O0FBRUQsYUFBUyxTQUFTLENBQUMsUUFBUSxFQUFFO0FBQ3pCLFlBQUksR0FBRyxxQkFBbUIsUUFBUSxBQUFFLENBQUM7QUFDckMsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLEVBQUUsR0FBRztTQUNYLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsR0FBRyxDQUFDLE1BQU0sRUFBRTtBQUNqQixZQUFJLEdBQUcsR0FBRyxlQUFlLENBQUM7QUFDMUIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLE1BQU07QUFDZCxlQUFHLEVBQUUsR0FBRztBQUNSLGdCQUFJLEVBQUUsTUFBTTtTQUNmLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsTUFBTSxDQUFDLE1BQU0sRUFBRTtBQUNwQixZQUFJLEdBQUcsR0FBRyxlQUFlLENBQUM7QUFDMUIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLEVBQUUsR0FBRztBQUNSLGdCQUFJLEVBQUUsTUFBTTtTQUNmLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsTUFBTSxDQUFDLFFBQVEsRUFBRTtBQUN0QixZQUFJLEdBQUcscUJBQW1CLFFBQVEsQUFBRSxDQUFDO0FBQ3JDLGVBQU8sS0FBSyxDQUFDO0FBQ1Qsa0JBQU0sRUFBRSxRQUFRO0FBQ2hCLGVBQUcsRUFBRSxHQUFHO1NBQ1gsQ0FBQyxDQUFDO0tBQ047Q0FDSjs7cUJBRWMsY0FBYzs7Ozs7Ozs7Ozs7OzRDQzlEQyxpQ0FBaUM7Ozs7MkNBQ2xDLGdDQUFnQzs7OztzQ0FDbEMsMkJBQTJCOzs7O0FBSnJELElBQUksVUFBVSxHQUFHLGtCQUFrQixDQUFDOztBQU1yQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FDMUIsVUFBVSxDQUFDLG1CQUFtQiw0Q0FBb0IsQ0FDbEQsVUFBVSxDQUFDLHlCQUF5QiwyQ0FBbUIsQ0FDdkQsT0FBTyxDQUFDLGdCQUFnQixzQ0FBaUIsQ0FBQzs7cUJBRS9CLFVBQVU7Ozs7Ozs7Ozs7OztJQ1hsQixnQkFBZ0IsR0FDUixTQURSLGdCQUFnQixDQUNQLE1BQU0sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxxQkFBcUIsRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFOzBCQUR4RyxnQkFBZ0I7O0FBR2YsS0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDOztBQUV2QyxRQUFJLGNBQWMsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDO0FBQ3JDLFVBQU0sQ0FBQyxRQUFRLEdBQUcsY0FBYyxLQUFLLFNBQVMsQ0FBQztBQUMvQyxVQUFNLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztBQUMxQixVQUFNLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztBQUMxQixVQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztBQUN0QixRQUFJLFFBQVEsQ0FBQzs7QUFFYixVQUFNLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQzs7QUFFbEIsVUFBTSxDQUFDLGtCQUFrQixHQUFHO0FBQ3hCLGVBQU8sRUFBRSxJQUFJO0FBQ2IsY0FBTSxFQUFFLElBQUk7S0FDZixDQUFDOztBQUVGLFVBQU0sQ0FBQyxZQUFZLEdBQUc7QUFDbEIsaUJBQVMsRUFBRSxJQUFJLElBQUksRUFBRTtBQUNyQixnQkFBUSxFQUFFLENBQUM7QUFDWCxlQUFPLEVBQUUsRUFBRTtLQUNkLENBQUM7O0FBRUYseUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQzlCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixjQUFNLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7S0FDckMsQ0FBQyxDQUFDOztBQUVQLHlCQUFxQixDQUFDLGlCQUFpQixFQUFFLENBQ3BDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixjQUFNLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7S0FDeEMsQ0FBQyxDQUFDOztBQUVQLFVBQU0sQ0FBQyxjQUFjLEdBQUcsWUFBTTtBQUMxQixjQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztBQUM1Qiw2QkFBcUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FDeEQsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGtCQUFNLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDckMsYUFBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsT0FBTyxDQUFDO0FBQ3hCLHFCQUFLLEVBQUUsTUFBTTthQUNoQixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDVixDQUFBOztBQUVELHlCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUM5QixJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsY0FBTSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0tBQ3BDLENBQUMsQ0FBQzs7QUFFUCx5QkFBcUIsQ0FBQyxjQUFjLEVBQUUsQ0FDakMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGNBQU0sQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztLQUN2QyxDQUFDLENBQUM7O0FBRVAsVUFBTSxDQUFDLE1BQU0sR0FBRyxZQUFNO0FBQ2xCLDZCQUFxQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUMxQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsa0JBQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0FBQ3pCLGtCQUFNLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7U0FDaEMsQ0FBQyxDQUFDO0tBQ1YsQ0FBQTs7QUFFRCx5QkFBcUIsQ0FBQyxPQUFPLEVBQUUsQ0FDMUIsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLGNBQU0sQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztLQUNoQyxDQUFDLENBQUM7O0FBRVAsVUFBTSxDQUFDLHFCQUFxQixHQUFHLFlBQU07QUFDakMsWUFBSSxNQUFNLEdBQUc7QUFDVCxtQkFBTyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTztBQUM3QixrQkFBTSxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTTtTQUM5QixDQUFDO0FBQ0YsY0FBTSxDQUFDLFVBQVUsRUFBRyxDQUFDO0FBQ3JCLGNBQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUN6QyxjQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztLQUM1QixDQUFBOztBQUVELFFBQUksTUFBTSxDQUFDLFFBQVEsRUFBRTtBQUNqQixrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsNkJBQXFCLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxDQUNoRCxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsa0JBQU0sQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUNwQyxrQkFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7O0FBRXpCLGtCQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsWUFBTTtBQUN0QyxzQkFBTSxDQUFDLFVBQVUsRUFBRyxDQUFDO2FBQ3hCLENBQUMsQ0FBQzs7QUFFSCxpQ0FBcUIsQ0FBQyxjQUFjLEVBQUUsQ0FDakMsSUFBSSxDQUFDLFVBQUMsWUFBWSxFQUFLO0FBQ3BCLHNCQUFNLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUM7QUFDeEMsaUJBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQzthQUNoRCxDQUFDLENBQUM7U0FFVixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBQ1Y7O0FBRUQsVUFBTSxDQUFDLFdBQVcsR0FBRyxVQUFDLFNBQVMsRUFBSztBQUNoQyxZQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDM0QsY0FBTSxDQUFDLFVBQVUsRUFBRyxDQUFDO0FBQ3JCLGNBQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7S0FDaEQsQ0FBQTs7QUFFRCxVQUFNLENBQUMsWUFBWSxHQUFHLFlBQU07QUFDeEIsY0FBTSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0tBQ3pDLENBQUM7O0FBRUYsVUFBTSxDQUFDLElBQUksR0FBRyxZQUFNO0FBQ2hCLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQiw2QkFBcUIsQ0FBQyxTQUFTLEVBQUUsQ0FDNUIsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3JCLG9CQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQzs7QUFFekIsa0JBQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztBQUN4QyxpQ0FBcUIsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUN6QyxJQUFJLENBQUMsVUFBQyxXQUFXLEVBQUs7QUFDbkIsb0JBQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7QUFDNUIsMEJBQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDekI7YUFDSixDQUFDLENBQUM7U0FFVixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBRVYsQ0FBQztDQUVMOztxQkFHVSxnQkFBZ0I7Ozs7Ozs7Ozs7OztJQzlJeEIsdUJBQXVCLEdBQ2YsU0FEUix1QkFBdUIsQ0FDZCxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxxQkFBcUIsRUFBRSxjQUFjLEVBQUU7MEJBRDVFLHVCQUF1Qjs7QUFHdEIsUUFBTSxRQUFRLEdBQUcsQ0FBQyxDQUFDO0FBQ25CLFFBQUksU0FBUyxHQUFHLENBQUMsQ0FBQzs7QUFFbEIsVUFBTSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7O0FBRTNCLFVBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBTTs7QUFFbkIsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDOztBQUUxQiw2QkFBcUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUM3QyxJQUFJLENBQUMsVUFBQyxjQUFjLEVBQUs7QUFDdEIsZ0JBQUksY0FBYyxDQUFDLE1BQU0sR0FBRyxRQUFRLEVBQUU7QUFDbEMsc0JBQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQzVCO0FBQ0QsMEJBQWMsQ0FBQyxPQUFPLENBQUMsVUFBQyxZQUFZLEVBQUs7QUFDckMsc0JBQU0sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQzVDLENBQUMsQ0FBQztBQUNILHFCQUFTLEVBQUcsQ0FBQztBQUNiLGtCQUFNLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztBQUM5QixnQkFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFO0FBQy9CLHNCQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzthQUN4QjtTQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVixDQUFDOztBQUVGLFVBQU0sQ0FBQyxnQkFBZ0IsR0FBRyxVQUFDLGNBQWMsRUFBSztBQUMxQyxzQkFBYyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsY0FBYyxFQUFFLEVBQUUsRUFBRSxFQUFFLGNBQWMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztLQUN0SCxDQUFDOztBQUVGLFVBQU0sQ0FBQyxvQkFBb0IsR0FBRyxVQUFDLEdBQUcsRUFBSztBQUNuQyxZQUFJLEdBQUcsRUFBRTtBQUNMLGtCQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFlBQVksRUFBSztBQUM1Qyw0QkFBWSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDO2FBQ2hELENBQUMsQ0FBQztTQUNOOztBQUVELGNBQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBQyxZQUFZLEVBQUs7QUFDOUQsbUJBQU8sWUFBWSxDQUFDLFFBQVEsQ0FBQztTQUNoQyxDQUFDLENBQUM7O0FBRUgsY0FBTSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxVQUFDLFlBQVksRUFBSztBQUNqRSxtQkFBTyxZQUFZLENBQUMsUUFBUSxDQUFDO1NBQ2hDLENBQUMsQ0FBQztLQUNOLENBQUM7O0FBRUYsVUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0NBQ3BCOztxQkFHVSx1QkFBdUI7Ozs7Ozs7OztBQzFEckMsU0FBUyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUU7QUFDbkMsZ0JBQVksQ0FBQzs7QUFFYixRQUFJLGNBQWMsQ0FBQzs7QUFFbkIsV0FBTztBQUNILGtCQUFVLEVBQVYsVUFBVTtBQUNWLG1CQUFXLEVBQVgsV0FBVztBQUNYLHNCQUFjLEVBQWQsY0FBYztBQUNkLHlCQUFpQixFQUFqQixpQkFBaUI7QUFDakIsZ0JBQVEsRUFBUixRQUFRO0FBQ1IsbUJBQVcsRUFBWCxXQUFXO0FBQ1gsc0JBQWMsRUFBZCxjQUFjO0FBQ2QsbUJBQVcsRUFBWCxXQUFXO0FBQ1gsaUJBQVMsRUFBVCxTQUFTO0FBQ1QsdUJBQWUsRUFBZixlQUFlO0FBQ2YsZUFBTyxFQUFQLE9BQU87QUFDUCxlQUFPLEVBQVAsT0FBTztBQUNQLFdBQUcsRUFBSCxHQUFHO0FBQ0gsY0FBTSxFQUFOLE1BQU07S0FDVCxDQUFDOztBQUVGLGFBQVMsT0FBTyxHQUFFO0FBQ2QsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDbEMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyxzQkFBc0IsQ0FBQztBQUM5QixtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsV0FBVyxHQUFFO0FBQ2xCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2xDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsMEJBQTBCLENBQUM7QUFDbEMsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1IsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFVBQVUsR0FBRztBQUNsQixlQUFPLEtBQUssQ0FBQztBQUNULGtCQUFNLEVBQUUsS0FBSztBQUNiLGVBQUcsRUFBRSxnQ0FBZ0M7U0FDeEMsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxXQUFXLEdBQUc7QUFDbkIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLEVBQUUsaUJBQWlCO1NBQ3pCLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsaUJBQWlCLEdBQUc7QUFDekIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLHNCQUFzQjtTQUM1QixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLGNBQWMsQ0FBQyxVQUFVLEVBQUU7QUFDaEMsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLDBCQUF3QixVQUFVLEFBQUU7U0FDMUMsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxjQUFjLEdBQUc7QUFDdEIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLEVBQUUsa0NBQWtDO1NBQzFDLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsV0FBVyxHQUFFO0FBQ2xCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2xDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsMEJBQTBCLENBQUM7QUFDbEMsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1IsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFFBQVEsQ0FBQyxFQUFFLEVBQUU7QUFDbEIsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDbEMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyx5QkFBeUIsQ0FBQztBQUNqQyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUixzQkFBTSxFQUFFO0FBQ0osc0JBQUUsRUFBRSxFQUFFO2lCQUNUO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjs7QUFFRCxhQUFTLFNBQVMsR0FBRztBQUNqQixlQUFPLEtBQUssQ0FBQztBQUNULGtCQUFNLEVBQUUsS0FBSztBQUNiLGVBQUcsRUFBRSwyQkFBMkI7U0FDbkMsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRTtBQUNsQyxZQUFJLGFBQWEsR0FBRyxTQUFoQixhQUFhLENBQUksUUFBUSxFQUFLO0FBQzlCLDBCQUFjLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUMvQixtQkFBTyxjQUFjLENBQUM7U0FDekIsQ0FBQzs7QUFFRixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcscUJBQXFCLENBQUM7QUFDaEMsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxLQUFLO0FBQ2IsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isc0JBQU0sRUFBRTtBQUNKLDRCQUFRLEVBQUUsUUFBUTtBQUNsQiw2QkFBUyxFQUFDLFNBQVM7aUJBQ3RCO0FBQ0QsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzFCLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsZUFBZSxDQUFDLGNBQWMsRUFBRTtBQUNyQyxlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLDRCQUEwQixjQUFjLEFBQUUsQ0FBQztBQUNsRCxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsR0FBRyxDQUFDLFlBQVksRUFBRTtBQUN2QixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsc0JBQXNCLENBQUM7QUFDakMsbUJBQU8sS0FBSyxDQUFDO0FBQ1Qsc0JBQU0sRUFBRSxNQUFNO0FBQ2QsbUJBQUcsRUFBRSxHQUFHO0FBQ1Isb0JBQUksRUFBRSxZQUFZO0FBQ2xCLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxNQUFNLENBQUMsWUFBWSxFQUFFO0FBQzFCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsR0FBRyxzQkFBc0IsQ0FBQztBQUNqQyxtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUixvQkFBSSxFQUFFLFlBQVk7QUFDbEIsdUJBQU8sRUFBRTtBQUNMLDRCQUFRLEVBQUUsUUFBUTtpQkFDckI7YUFDSixDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTjtDQUVKOztxQkFFYyxxQkFBcUI7Ozs7Ozs7Ozs7OzttREM5TEMsd0NBQXdDOzs7OzJDQUNoRCxnQ0FBZ0M7Ozs7NkNBQzNCLGtDQUFrQzs7OztBQUpuRSxJQUFJLFVBQVUsR0FBRyx5QkFBeUIsQ0FBQzs7QUFNNUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQzFCLFVBQVUsQ0FBQywwQkFBMEIsbURBQTJCLENBQ2hFLFVBQVUsQ0FBQyxnQ0FBZ0MsMkNBQW1CLENBQzlELE9BQU8sQ0FBQyx1QkFBdUIsNkNBQXdCLENBQUM7cUJBQzdDLFVBQVU7Ozs7Ozs7Ozs7OztJQ1ZsQixnQkFBZ0IsR0FDUixTQURSLGdCQUFnQixDQUNQLE1BQU0sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRTswQkFEL0YsZ0JBQWdCOztBQUdmLEtBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQzs7QUFFdkMsUUFBSSxNQUFNLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQztBQUM3QixVQUFNLENBQUMsUUFBUSxHQUFHLE1BQU0sS0FBSyxTQUFTLENBQUM7QUFDdkMsUUFBSSxRQUFRLENBQUM7O0FBRWIsVUFBTSxDQUFDLElBQUksR0FBRztBQUNWLGlCQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUU7S0FDeEIsQ0FBQzs7QUFFRixnQkFBWSxDQUFDLFdBQVcsRUFBRSxDQUNyQixJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDaEIsY0FBTSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQ2pDLGNBQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztLQUM5QixDQUFDLENBQUM7O0FBRVAsUUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO0FBQ2pCLGtCQUFVLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7QUFFMUIsb0JBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQ3ZCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixrQkFBTSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzVCLHdCQUFZLENBQUMsV0FBVyxFQUFFLENBQ3JCLElBQUksQ0FBQyxVQUFDLGdCQUFnQixFQUFLO0FBQ3hCLHNCQUFNLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDLElBQUksQ0FBQztBQUN6QyxpQkFBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO2FBQzFDLENBQUMsQ0FBQztTQUNWLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FHVixNQUFNO0FBQ0gsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLG9CQUFZLENBQUMsV0FBVyxFQUFFLENBQ3JCLElBQUksQ0FBQyxVQUFDLGdCQUFnQixFQUFLO0FBQ3hCLGtCQUFNLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDLElBQUksQ0FBQztBQUN6QyxhQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7U0FDMUMsQ0FBQyxDQUFDO0FBQ1Asb0JBQVksQ0FBQyxTQUFTLEVBQUUsQ0FDbkIsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3JCLG9CQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztTQUM1QixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDBCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCxzQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0tBRVY7O0FBRUQsVUFBTSxDQUFDLFlBQVksR0FBRyxZQUFNO0FBQ3hCLGNBQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7S0FDaEMsQ0FBQzs7QUFFRixVQUFNLENBQUMsVUFBVSxHQUFHLFlBQU07QUFDdEIsb0JBQVksQ0FBQyxnQkFBZ0IsQ0FBQztBQUMxQixvQkFBUSxFQUFFO0FBQ04scUJBQUssRUFBRSxlQUFlO0FBQ3RCLG9CQUFJLEVBQUUseUNBQXlDO0FBQy9DLGtCQUFFLEVBQUUsY0FBYztBQUNsQixzQkFBTSxFQUFFLFVBQVU7YUFDckI7U0FDSixDQUFDLENBQ0YsSUFBSSxDQUFDLFlBQU07QUFDUixzQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDMUIsd0JBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQ3RCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixvQkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QiwwQkFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDaEM7YUFDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLDhCQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pDLENBQUMsV0FDTSxDQUFDLFlBQU07QUFDWCwwQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDOUIsQ0FBQyxDQUFDO1NBQ1YsQ0FBQyxDQUFDO0tBQ0wsQ0FBQzs7QUFFRixVQUFNLENBQUMsSUFBSSxHQUFHLFlBQU07O0FBRWhCLFlBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFO0FBQ2xCLGtCQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7QUFDaEMsc0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDOztBQUUxQix3QkFBWSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQ3hCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQixvQkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QiwwQkFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUN6QjthQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsOEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLDBCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDVixNQUFNO0FBQ0gsc0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLHdCQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FDM0IsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2hCLG9CQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO0FBQ3pCLDBCQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7aUJBQ3pCO2FBQ0osQ0FBQyxTQUNJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDZCw4QkFBYyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN6QyxDQUFDLFdBQ00sQ0FBQyxZQUFNO0FBQ1gsMEJBQVUsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQzlCLENBQUMsQ0FBQztTQUNWO0tBQ0osQ0FBQztDQUNMOztxQkFHVSxnQkFBZ0I7Ozs7Ozs7Ozs7OztJQzlIeEIsZUFBZSxHQUNQLFNBRFIsZUFBZSxDQUNOLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFOzBCQURqRixlQUFlOztBQUdkLFFBQU0sUUFBUSxHQUFHLENBQUMsQ0FBQztBQUNuQixRQUFJLFNBQVMsR0FBRyxDQUFDLENBQUM7O0FBRWxCLFVBQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDOztBQUVsQixVQUFNLENBQUMsT0FBTyxHQUFHLFlBQU07QUFDbkIsa0JBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQzFCLG9CQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FDcEMsSUFBSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2IsZ0JBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxRQUFRLEVBQUU7QUFDekIsc0JBQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQzVCO0FBQ0QsaUJBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUs7QUFDcEIsc0JBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzNCLENBQUMsQ0FBQztBQUNILHFCQUFTLEVBQUcsQ0FBQztBQUNiLGtCQUFNLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztBQUM5QixnQkFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFO0FBQ3RCLHNCQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzthQUN4QjtTQUNKLENBQUMsU0FDSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2QsMEJBQWMsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekMsQ0FBQyxXQUNNLENBQUMsWUFBTTtBQUNYLHNCQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUM5QixDQUFDLENBQUM7S0FDVixDQUFDOztBQUVGLFVBQU0sQ0FBQyxnQkFBZ0IsR0FBRyxVQUFDLE1BQU0sRUFBSztBQUNsQyxjQUFNLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQ3RGLENBQUM7O0FBRUYsVUFBTSxDQUFDLG9CQUFvQixHQUFHLFVBQUMsR0FBRyxFQUFLO0FBQ25DLFlBQUksR0FBRyxFQUFFO0FBQ0wsa0JBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQzNCLG9CQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7YUFDeEMsQ0FBQyxDQUFDO1NBQ047O0FBRUQsY0FBTSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUM3QyxtQkFBTyxJQUFJLENBQUMsUUFBUSxDQUFDO1NBQ3hCLENBQUMsQ0FBQzs7QUFFSCxjQUFNLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ2hELG1CQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7U0FDeEIsQ0FBQyxDQUFDO0tBQ04sQ0FBQzs7QUFFRixVQUFNLENBQUMsTUFBTSxHQUFHLFVBQUMsSUFBSSxFQUFLOztBQUV0QixvQkFBWSxDQUFDLGdCQUFnQixDQUFDO0FBQzFCLG9CQUFRLEVBQUU7QUFDTixxQkFBSyxvQkFBb0I7QUFDekIsb0JBQUksMENBQTBDO0FBQzlDLGtCQUFFLEVBQUUsY0FBYztBQUNsQixzQkFBTSxFQUFFLFVBQVU7YUFDckI7U0FDSixDQUFDLENBQ0QsSUFBSSxDQUFDLFlBQU07QUFDUixnQkFBSSxVQUFVLENBQUM7QUFDZixnQkFBSSxJQUFJLEVBQUU7QUFDTiwwQkFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzlCLE1BQU07QUFDSCwwQkFBVSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQ3BCLEdBQUcsQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNmLHdCQUFJLFFBQVEsQ0FBQyxRQUFRLEVBQUU7QUFDbkIsK0JBQU8sUUFBUSxDQUFDLE1BQU0sQ0FBQztxQkFDMUI7QUFDRCwyQkFBTyxJQUFJLENBQUM7aUJBQ2YsQ0FBQyxDQUFDO2FBQ1Y7O0FBRUQsc0JBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQyxNQUFNLEVBQUs7QUFDM0IsNEJBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQ3RCLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNoQix3QkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6Qiw4QkFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDL0IsZ0NBQUksTUFBTSxLQUFLLFFBQVEsQ0FBQyxNQUFNLEVBQUU7QUFDNUIsb0NBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzNDLHNDQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7NkJBQ2pDO3lCQUNKLENBQUMsQ0FBQztxQkFDTjtpQkFDSixDQUFDLFNBQ0ksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNkLGtDQUFjLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN6QyxDQUFDLENBQUM7YUFDVixDQUFDLENBQUM7O0FBRUgsa0JBQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1NBRWpDLENBQUMsQ0FBQztLQUNOLENBQUM7O0FBRUYsVUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0NBQ3BCOztxQkFJVSxlQUFlOzs7Ozs7Ozs7QUN2RzdCLFNBQVMsWUFBWSxDQUFDLEtBQUssRUFBRTtBQUMxQixnQkFBWSxDQUFDOztBQUViLFFBQUksS0FBSyxDQUFDOztBQUVWLFdBQU87QUFDSCxtQkFBVyxFQUFYLFdBQVc7QUFDWCxpQkFBUyxFQUFULFNBQVM7QUFDVCxlQUFPLEVBQVAsT0FBTztBQUNQLGVBQU8sRUFBUCxPQUFPO0FBQ1AsV0FBRyxFQUFILEdBQUc7QUFDSCxjQUFNLEVBQU4sTUFBTTtBQUNOLGNBQU0sRUFBTixNQUFNO0tBQ1QsQ0FBQzs7QUFFRixhQUFTLFdBQVcsR0FBRTtBQUNsQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNsQyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLDBCQUEwQixDQUFDO0FBQ2xDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxTQUFTLEdBQUc7QUFDakIsZUFBTyxLQUFLLENBQUM7QUFDVCxrQkFBTSxFQUFFLEtBQUs7QUFDYixlQUFHLEVBQUUsMkJBQTJCO1NBQ25DLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsT0FBTyxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUU7QUFDbEMsWUFBSSxhQUFhLEdBQUcsU0FBaEIsYUFBYSxDQUFJLFFBQVEsRUFBSztBQUM5QixpQkFBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDdEIsbUJBQU8sS0FBSyxDQUFDO1NBQ2hCLENBQUM7O0FBRUYsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDdkMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyxHQUFHLFlBQVksQ0FBQztBQUN2QixtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLEtBQUs7QUFDYixtQkFBRyxFQUFFLEdBQUc7QUFDUixzQkFBTSxFQUFFO0FBQ0osNEJBQVEsRUFBRSxRQUFRO0FBQ2xCLDZCQUFTLEVBQUMsU0FBUztpQkFDdEI7QUFDRCx1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDMUIsQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxPQUFPLENBQUMsT0FBTyxFQUFFO0FBQ3RCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ2xDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsbUJBQWlCLE9BQU8sQUFBRSxDQUFDO0FBQ2xDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxHQUFHLENBQUMsSUFBSSxFQUFFO0FBQ2YsZUFBTyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRLEVBQUU7QUFDdkMsZ0JBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7QUFDN0IsZ0JBQUksR0FBRyxHQUFHLGFBQWEsQ0FBQztBQUN4QixtQkFBTyxLQUFLLENBQUM7QUFDVCxzQkFBTSxFQUFFLE1BQU07QUFDZCxtQkFBRyxFQUFFLEdBQUc7QUFDUixvQkFBSSxFQUFFLElBQUk7QUFDVix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOOztBQUVELGFBQVMsTUFBTSxDQUFDLElBQUksRUFBRTtBQUNsQixlQUFPLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVEsRUFBRTtBQUN2QyxnQkFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztBQUM3QixnQkFBSSxHQUFHLEdBQUcsYUFBYSxDQUFDO0FBQ3hCLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsS0FBSztBQUNiLG1CQUFHLEVBQUUsR0FBRztBQUNSLG9CQUFJLEVBQUUsSUFBSTtBQUNWLHVCQUFPLEVBQUU7QUFDTCw0QkFBUSxFQUFFLFFBQVE7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ047O0FBRUQsYUFBUyxNQUFNLENBQUMsT0FBTyxFQUFFO0FBQ3JCLGVBQU8sU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsUUFBUSxFQUFFO0FBQ3ZDLGdCQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0FBQzdCLGdCQUFJLEdBQUcsbUJBQWlCLE9BQU8sQUFBRSxDQUFDO0FBQ2xDLG1CQUFPLEtBQUssQ0FBQztBQUNULHNCQUFNLEVBQUUsUUFBUTtBQUNoQixtQkFBRyxFQUFFLEdBQUc7QUFDUix1QkFBTyxFQUFFO0FBQ0wsNEJBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztLQUNOO0NBQ0o7O3FCQUVjLFlBQVk7Ozs7Ozs7Ozs7OzswQ0NwSEMsK0JBQStCOzs7OzJDQUM5QixnQ0FBZ0M7Ozs7b0NBQ3BDLHlCQUF5Qjs7OztBQUpqRCxJQUFJLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQzs7QUFNbkMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQzFCLFVBQVUsQ0FBQyxpQkFBaUIsMENBQWtCLENBQzlDLFVBQVUsQ0FBQyx1QkFBdUIsMkNBQW1CLENBQ3JELE9BQU8sQ0FBQyxjQUFjLG9DQUFlLENBQUM7O3FCQUUzQixVQUFVIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIu+7v2ltcG9ydCB7IGRlZmF1bHQgYXMgVmlhbFNvZnRNb2R1bGV9IGZyb20gXCIuL2FwcC5tb2R1bGVcIjtcblxuYW5ndWxhci5ib290c3RyYXAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhcHBcIiksIFtWaWFsU29mdE1vZHVsZV0pOyIsIu+7v3ZhciBNb2R1bGVOYW1lID0gXCJWaWFsU29mdFwiO1xyXG5cclxuaW1wb3J0IFNoYXJlZE1vZHVsZU5hbWUgZnJvbSBcIi4vY29tcG9uZW50cy9zaGFyZWQvc2hhcmVkLm1vZHVsZVwiO1xyXG5pbXBvcnQgRGFzaGJvYXJkTW9kdWxlTmFtZSBmcm9tIFwiLi9jb21wb25lbnRzL2Rhc2hib2FyZC9kYXNoYm9hcmQubW9kdWxlXCI7XHJcbmltcG9ydCBDdXN0b21lcnNNb2R1bGVOYW1lIGZyb20gXCIuL2NvbXBvbmVudHMvY3VzdG9tZXJzL2N1c3RvbWVycy5tb2R1bGVcIjtcclxuaW1wb3J0IFdvcmtzTW9kdWxlTmFtZSBmcm9tIFwiLi9jb21wb25lbnRzL3dvcmtzL3dvcmtzLm1vZHVsZVwiO1xyXG5pbXBvcnQgQXNzZXRzTW9kdWxlTmFtZSBmcm9tIFwiLi9jb21wb25lbnRzL2Fzc2V0cy9hc3NldHMubW9kdWxlXCI7XHJcbmltcG9ydCBBY2Nlc3Nvcmllc01vZHVsZU5hbWUgZnJvbSBcIi4vY29tcG9uZW50cy9hY2Nlc3Nvcmllcy9hY2Nlc3Nvcmllcy5tb2R1bGVcIjtcclxuaW1wb3J0IFRlbmFudHNNb2R1bGVOYW1lIGZyb20gXCIuL2NvbXBvbmVudHMvdGVuYW50cy90ZW5hbnRzLm1vZHVsZVwiO1xyXG5pbXBvcnQgUHVyY2hhc2VzTW9kdWxlTmFtZSBmcm9tIFwiLi9jb21wb25lbnRzL3B1cmNoYXNlcy9wdXJjaGFzZXMubW9kdWxlXCI7XHJcbmltcG9ydCBUcmFuc2ZlcnNXb3Jrc01vZHVsZU5hbWUgZnJvbSBcIi4vY29tcG9uZW50cy90cmFuc2ZlcnN3b3Jrcy90cmFuc2ZlcnN3b3Jrcy5tb2R1bGVcIjtcclxuaW1wb3J0IFByb3ZpZGVyc01vZHVsZU5hbWUgZnJvbSBcIi4vY29tcG9uZW50cy9wcm92aWRlcnMvcHJvdmlkZXJzLm1vZHVsZVwiO1xyXG5pbXBvcnQgTWFpbnRlbmFuY2VjYXJkc01vZHVsZU5hbWUgZnJvbSBcIi4vY29tcG9uZW50cy9tYWludGVuYW5jZWNhcmRzL21haW50ZW5hbmNlY2FyZHMubW9kdWxlXCI7XHJcbmltcG9ydCBDb3N0ZUFzc2V0TW9kdWxlTmFtZSBmcm9tIFwiLi9jb21wb25lbnRzL2Nvc3RlYXNzZXQvY29zdGVhc3NldC5tb2R1bGVcIjtcclxuXHJcblxyXG52YXIgYXBwID0gYW5ndWxhci5tb2R1bGUoTW9kdWxlTmFtZSwgW1widWkucm91dGVyXCIsIFwibmdBbmltYXRlXCIsIFNoYXJlZE1vZHVsZU5hbWUsIERhc2hib2FyZE1vZHVsZU5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBDdXN0b21lcnNNb2R1bGVOYW1lLCBXb3Jrc01vZHVsZU5hbWUsIEFzc2V0c01vZHVsZU5hbWUsIEFjY2Vzc29yaWVzTW9kdWxlTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgIFRlbmFudHNNb2R1bGVOYW1lLCBQdXJjaGFzZXNNb2R1bGVOYW1lLCBUcmFuc2ZlcnNXb3Jrc01vZHVsZU5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBQcm92aWRlcnNNb2R1bGVOYW1lLCBNYWludGVuYW5jZWNhcmRzTW9kdWxlTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgIENvc3RlQXNzZXRNb2R1bGVOYW1lXSk7XHJcblxyXG5hcHAuY29uZmlnKENvbmZpZyk7XHJcblxyXG5mdW5jdGlvbiBDb25maWcoJHN0YXRlUHJvdmlkZXIsICR1cmxSb3V0ZXJQcm92aWRlciwgJGNvbXBpbGVQcm92aWRlcikge1xyXG5cclxuICAgIGNvbnN0IGRlZmF1bHRVcmwgPSBcIi9cIjtcclxuXHJcbiAgICAkY29tcGlsZVByb3ZpZGVyLmRlYnVnSW5mb0VuYWJsZWQoZmFsc2UpO1xyXG5cclxuICAgICR1cmxSb3V0ZXJQcm92aWRlci53aGVuKFwiXCIsIGRlZmF1bHRVcmwpO1xyXG5cclxuICAgICR1cmxSb3V0ZXJQcm92aWRlci5vdGhlcndpc2UoJy80MDQnKTtcclxuXHJcbiAgICAkc3RhdGVQcm92aWRlclxyXG4gICAgICAgIC5zdGF0ZShcImRhc2hib2FyZFwiLCB7XHJcbiAgICAgICAgICAgIHVybDogXCIvXCIsXHJcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBcIi9hcHAvY29tcG9uZW50cy9kYXNoYm9hcmQvdmlld3MvbWFpbi5odG1sXCIsXHJcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6XCJkYXNoYm9hcmRDb250cm9sbGVyXCJcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5zdGF0ZShcImN1c3RvbWVyc1wiLCB7XHJcbiAgICAgICAgICAgIHVybDogXCIvY3VzdG9tZXJzXCIsXHJcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBcIi9hcHAvY29tcG9uZW50cy9jdXN0b21lcnMvdmlld3MvbWFpbi5odG1sXCIsXHJcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6XCJjdXN0b21lcnNDb250cm9sbGVyXCJcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5zdGF0ZShcImN1c3RvbWVyXCIsIHtcclxuICAgICAgICAgICAgdXJsOiBcIi9jdXN0b21lcj9pZFwiLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCIvYXBwL2NvbXBvbmVudHMvY3VzdG9tZXJzL3ZpZXdzL2RldGFpbC5odG1sXCIsXHJcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6XCJjdXN0b21lcnNkZXRhaWxDb250cm9sbGVyXCJcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5zdGF0ZShcIndvcmtzXCIsIHtcclxuICAgICAgICAgICAgdXJsOiBcIi93b3Jrc1wiLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCIvYXBwL2NvbXBvbmVudHMvd29ya3Mvdmlld3MvbWFpbi5odG1sXCIsXHJcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6XCJ3b3Jrc0NvbnRyb2xsZXJcIlxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnN0YXRlKFwid29ya1wiLCB7XHJcbiAgICAgICAgICAgIHVybDogXCIvd29yaz9pZFwiLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCIvYXBwL2NvbXBvbmVudHMvd29ya3Mvdmlld3MvZGV0YWlsLmh0bWxcIixcclxuICAgICAgICAgICAgY29udHJvbGxlcjpcIndvcmtzZGV0YWlsQ29udHJvbGxlclwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoXCJhc3NldHNcIiwge1xyXG4gICAgICAgICAgICB1cmw6IFwiL2Fzc2V0c1wiLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCIvYXBwL2NvbXBvbmVudHMvYXNzZXRzL3ZpZXdzL21haW4uaHRtbFwiLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOlwiYXNzZXRzQ29udHJvbGxlclwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoXCJhc3NldFwiLCB7XHJcbiAgICAgICAgICAgIHVybDogXCIvYXNzZXQ/aWRcIixcclxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwiL2FwcC9jb21wb25lbnRzL2Fzc2V0cy92aWV3cy9kZXRhaWwuaHRtbFwiLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOlwiYXNzZXRzZGV0YWlsQ29udHJvbGxlclwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoXCJhY2Nlc3Nvcmllc1wiLCB7XHJcbiAgICAgICAgICAgIHVybDogXCIvYWNjZXNzb3JpZXM/aWRcIixcclxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwiL2FwcC9jb21wb25lbnRzL2FjY2Vzc29yaWVzL3ZpZXdzL21haW4uaHRtbFwiLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOlwiYWNjZXNzb3JpZXNDb250cm9sbGVyXCJcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5zdGF0ZShcImFjY2Vzc29yeVwiLCB7XHJcbiAgICAgICAgICAgIHVybDogXCIvYWNjZXNzb3J5P2lkXCIsXHJcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBcIi9hcHAvY29tcG9uZW50cy9hY2Nlc3Nvcmllcy92aWV3cy9kZXRhaWwuaHRtbFwiLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOlwiYWNjZXNzb3JpZXNkZXRhaWxDb250cm9sbGVyXCJcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5zdGF0ZShcInRlbmFudHNcIiwge1xyXG4gICAgICAgICAgICB1cmw6IFwiL3RlbmFudHNcIixcclxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwiL2FwcC9jb21wb25lbnRzL3RlbmFudHMvdmlld3MvbWFpbi5odG1sXCIsXHJcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6XCJ0ZW5hbnRzQ29udHJvbGxlclwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoXCJ0ZW5hbnRcIiwge1xyXG4gICAgICAgICAgICB1cmw6IFwiL3RlbmFudD9pZFwiLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCIvYXBwL2NvbXBvbmVudHMvdGVuYW50cy92aWV3cy9kZXRhaWwuaHRtbFwiLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOlwidGVuYW50c2RldGFpbENvbnRyb2xsZXJcIlxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnN0YXRlKFwicHVyY2hhc2VzXCIsIHtcclxuICAgICAgICAgICAgdXJsOiBcIi9wdXJjaGFzZXNcIixcclxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwiL2FwcC9jb21wb25lbnRzL3B1cmNoYXNlcy92aWV3cy9tYWluLmh0bWxcIixcclxuICAgICAgICAgICAgY29udHJvbGxlcjpcInB1cmNoYXNlc0NvbnRyb2xsZXJcIlxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnN0YXRlKFwicHVyY2hhc2VcIiwge1xyXG4gICAgICAgICAgICB1cmw6IFwiL3B1cmNoYXNlP2lkXCIsXHJcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBcIi9hcHAvY29tcG9uZW50cy9wdXJjaGFzZXMvdmlld3MvZGV0YWlsLmh0bWxcIixcclxuICAgICAgICAgICAgY29udHJvbGxlcjpcInB1cmNoYXNlc2RldGFpbENvbnRyb2xsZXJcIlxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnN0YXRlKFwidHJhbnNmZXJzd29ya3NcIiwge1xyXG4gICAgICAgICAgICB1cmw6IFwiL3RyYW5zZmVyc3dvcmtzXCIsXHJcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBcIi9hcHAvY29tcG9uZW50cy90cmFuc2ZlcnN3b3Jrcy92aWV3cy9tYWluLmh0bWxcIixcclxuICAgICAgICAgICAgY29udHJvbGxlcjpcInRyYW5zZmVyc3dvcmtzQ29udHJvbGxlclwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoXCJ0cmFuc2ZlcndvcmtcIiwge1xyXG4gICAgICAgICAgICB1cmw6IFwiL3RyYW5zZmVyd29yaz9pZFwiLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCIvYXBwL2NvbXBvbmVudHMvdHJhbnNmZXJzd29ya3Mvdmlld3MvZGV0YWlsLmh0bWxcIixcclxuICAgICAgICAgICAgY29udHJvbGxlcjpcInRyYW5zZmVyc3dvcmtzZGV0YWlsQ29udHJvbGxlclwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoXCJwcm92aWRlcnNcIiwge1xyXG4gICAgICAgICAgICB1cmw6IFwiL3Byb3ZpZGVyc1wiLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCIvYXBwL2NvbXBvbmVudHMvcHJvdmlkZXJzL3ZpZXdzL21haW4uaHRtbFwiLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOlwicHJvdmlkZXJzQ29udHJvbGxlclwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoXCJwcm92aWRlclwiLCB7XHJcbiAgICAgICAgICAgIHVybDogXCIvcHJvdmlkZXI/aWRcIixcclxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwiL2FwcC9jb21wb25lbnRzL3Byb3ZpZGVycy92aWV3cy9kZXRhaWwuaHRtbFwiLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOlwicHJvdmlkZXJzZGV0YWlsQ29udHJvbGxlclwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoXCJtYWludGVuYW5jZWNhcmRzXCIsIHtcclxuICAgICAgICAgICAgdXJsOiBcIi9tYWludGVuYW5jZWNhcmRzP2lkXCIsXHJcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBcIi9hcHAvY29tcG9uZW50cy9tYWludGVuYW5jZWNhcmRzL3ZpZXdzL21haW4uaHRtbFwiLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOlwibWFpbnRlbmFuY2VjYXJkc0NvbnRyb2xsZXJcIlxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnN0YXRlKFwibWFpbnRlbmFuY2VjYXJkXCIsIHtcclxuICAgICAgICAgICAgdXJsOiBcIi9tYWludGVuYW5jZWNhcmQ/aWRcIixcclxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwiL2FwcC9jb21wb25lbnRzL21haW50ZW5hbmNlY2FyZHMvdmlld3MvZGV0YWlsLmh0bWxcIixcclxuICAgICAgICAgICAgY29udHJvbGxlcjpcIm1haW50ZW5hbmNlY2FyZHNkZXRhaWxDb250cm9sbGVyXCJcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5zdGF0ZShcImNvc3RlYXNzZXRzXCIsIHtcclxuICAgICAgICAgICAgdXJsOiBcIi9jb3N0ZWFzc2V0c1wiLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCIvYXBwL2NvbXBvbmVudHMvY29zdGVhc3NldC92aWV3cy9tYWluLmh0bWxcIixcclxuICAgICAgICAgICAgY29udHJvbGxlcjpcImNvc3RlYXNzZXRDb250cm9sbGVyXCJcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5zdGF0ZShcImNvc3RlYXNzZXRcIiwge1xyXG4gICAgICAgICAgICB1cmw6IFwiL2Nvc3RlYXNzZXQ/aWRcIixcclxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwiL2FwcC9jb21wb25lbnRzL2Nvc3RlYXNzZXQvdmlld3MvZGV0YWlsLmh0bWxcIixcclxuICAgICAgICAgICAgY29udHJvbGxlcjpcImNvc3RlYXNzZXRkZXRhaWxDb250cm9sbGVyXCJcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5zdGF0ZShcImVycm9yXCIsIHtcclxuICAgICAgICAgICAgdXJsOiBcIi80MDRcIixcclxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwiL2FwcC9jb21wb25lbnRzL3NoYXJlZC92aWV3cy9lcnJvci5odG1sXCJcclxuICAgICAgICB9KTtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgTW9kdWxlTmFtZTsiLCLvu792YXIgTW9kdWxlTmFtZSA9IFwiVmlhbFNvZnQuYWNjZXNzb3JpZXNcIjtcblxuaW1wb3J0IEFjY2Vzc29yaWVzQ29udHJvbGxlciBmcm9tIFwiLi9jb250cm9sbGVycy9hY2Nlc3Nvcmllc0NvbnRyb2xsZXJcIjtcbmltcG9ydCBEZXRhaWxDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL2RldGFpbENvbnRyb2xsZXJcIjtcbmltcG9ydCBBY2Nlc3Nvcmllc1NlcnZpY2UgZnJvbSBcIi4vc2VydmljZXMvYWNjZXNzb3JpZXNTZXJ2aWNlXCI7XG5cbmFuZ3VsYXIubW9kdWxlKE1vZHVsZU5hbWUsIFtdKS5cbiAgICBjb250cm9sbGVyKFwiYWNjZXNzb3JpZXNDb250cm9sbGVyXCIsIEFjY2Vzc29yaWVzQ29udHJvbGxlcikuXG4gICAgY29udHJvbGxlcihcImFjY2Vzc29yaWVzZGV0YWlsQ29udHJvbGxlclwiLCBEZXRhaWxDb250cm9sbGVyKS5cbiAgICBzZXJ2aWNlKFwiYWNjZXNzb3JpZXNTZXJ2aWNlXCIsIEFjY2Vzc29yaWVzU2VydmljZSk7XG5cbmV4cG9ydCBkZWZhdWx0IE1vZHVsZU5hbWU7Iiwi77u/Y2xhc3MgQWNjZXNzb3JpZXNDb250cm9sbGVyIHtcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICRyb290U2NvcGUsICRzdGF0ZVBhcmFtcywgJHN0YXRlLCBhY2Nlc3Nvcmllc1NlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlLCBtb2RhbFNlcnZpY2UpIHtcblxuICAgICAgICB2YXIgYXNzZXRJZCA9ICRzdGF0ZVBhcmFtcy5pZDtcblxuICAgICAgICBjb25zdCBwYWdlU2l6ZSA9IDY7XG4gICAgICAgIHZhciBwYWdlQ291bnQgPSAwO1xuXG4gICAgICAgICRzY29wZS5Bc3NldElkID0gYXNzZXRJZDtcbiAgICAgICAgJHNjb3BlLmFjY2Vzc29yaWVzID0gW107XG5cbiAgICAgICAgJHNjb3BlLmdldExpc3QgPSAoKSA9PiB7XG4gICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgICAgICAgYWNjZXNzb3JpZXNTZXJ2aWNlLmdldExpc3QocGFnZVNpemUsIHBhZ2VDb3VudCwgYXNzZXRJZClcbiAgICAgICAgICAgICAgICAudGhlbigoYWNjZXNzb3JpZXMpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGFjY2Vzc29yaWVzLmxlbmd0aCA8IHBhZ2VTaXplKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubm9Nb3JlRGF0YSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgYWNjZXNzb3JpZXMuZm9yRWFjaCgoYWNjZXNzb3J5KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWNjZXNzb3JpZXMucHVzaChhY2Nlc3NvcnkpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcGFnZUNvdW50ICsrO1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUucmVmcmVzaFNlbGVjdGVkSXRlbXMoKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoISRzY29wZS5hY2Nlc3Nvcmllcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5ub0RhdGEgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5uYWdpdmF0ZVRvRGV0YWlsID0gKCkgPT4ge1xuICAgICAgICAgICAgJHN0YXRlLnRyYW5zaXRpb25UbyhcImFjY2Vzc29yeVwiLCB7IGlkOiAkc2NvcGUuQXNzZXRJZCB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrQXNzZXQgPSAoKSA9PiB7XG4gICAgICAgICAgICAkc3RhdGUudHJhbnNpdGlvblRvKFwiYXNzZXRcIiwge2lkIDogJHNjb3BlLkFzc2V0SWR9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVmcmVzaFNlbGVjdGVkSXRlbXMgPSAoYWxsKSA9PiB7XG4gICAgICAgICAgICBpZiAoYWxsKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmFjY2Vzc29yaWVzLmZvckVhY2goKGFjY2Vzc29yeSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBhY2Nlc3Nvcnkuc2VsZWN0ZWQgPSAkc2NvcGUuZXZlcnlTZWxlY3RlZDtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLmFueVNlbGVjdGVkID0gJHNjb3BlLmFjY2Vzc29yaWVzLnNvbWUoKGFjY2Vzc29yeSkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBhY2Nlc3Nvcnkuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQgPSAkc2NvcGUuYWNjZXNzb3JpZXMuZXZlcnkoKGFjY2Vzc29yeSkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBhY2Nlc3Nvcnkuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVtb3ZlID0gKGFjY2Vzc29yeSkgPT4ge1xuXG4gICAgICAgICAgICBtb2RhbFNlcnZpY2Uuc2hvd0NvbmZpcm1Nb2RhbCh7XG4gICAgICAgICAgICAgICAgbWVzc2FnZXM6IHtcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IGBFbGltaW5hciBhY2Nlc29yaW9gLFxuICAgICAgICAgICAgICAgICAgICBib2R5OiBgRXN0YSBzZWd1cm8gcXVlIGRlc2VhIGVsaW1pbmFyIGVsIGFjY2Vzb3Jpb2AsXG4gICAgICAgICAgICAgICAgICAgIG9rOiBcIlNpLCBlbGltaW5hclwiLFxuICAgICAgICAgICAgICAgICAgICBjYW5jZWw6IFwiQ2FuY2VsYXJcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgdmFyIGFjY2Vzc29yeUlkTGlzdDtcbiAgICAgICAgICAgICAgICBpZiAoYWNjZXNzb3J5KSB7XG4gICAgICAgICAgICAgICAgICAgIGFjY2Vzc29yeUlkTGlzdCA9IFthY2Nlc3NvcnkuQWNjZXNzb3J5SWRdO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGFjY2Vzc29yeUlkTGlzdCA9ICRzY29wZS5hY2Nlc3Nvcmllc1xuICAgICAgICAgICAgICAgICAgICAgICAgLm1hcCgoYWNjZXNzb3J5SXRlbSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhY2Nlc3NvcnlJdGVtLnNlbGVjdGVkKSB7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFjY2Vzc29yeUl0ZW0uQWNjZXNzb3J5SWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGFjY2Vzc29yeUlkTGlzdC5mb3JFYWNoKChhY2Nlc3NvcnlJZCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBhY2Nlc3Nvcmllc1NlcnZpY2UucmVtb3ZlKGFjY2Vzc29yeUlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hY2Nlc3Nvcmllcy5mb3JFYWNoKChhY2Nlc3NvcnlJdGVtKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYWNjZXNzb3J5SWQgPT09IGFjY2Vzc29yeUl0ZW0uQWNjZXNzb3J5SWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgaW5kZXggPSAkc2NvcGUuYWNjZXNzb3JpZXMuaW5kZXhPZihhY2Nlc3NvcnlJdGVtKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWNjZXNzb3JpZXMuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAkc2NvcGUucmVmcmVzaFNlbGVjdGVkSXRlbXMoKTtcblxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmdldExpc3QoKTtcbiAgICB9XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgQWNjZXNzb3JpZXNDb250cm9sbGVyOyIsIu+7v2NsYXNzIERldGFpbENvbnRyb2xsZXIge1xyXG4gICAgY29uc3RydWN0b3IoJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGVQYXJhbXMsICRzdGF0ZSwgYWNjZXNzb3JpZXNTZXJ2aWNlLCB0b2FzdGVyU2VydmljZSwgbW9kYWxTZXJ2aWNlKSB7XHJcblxyXG4gICAgICAgIHZhciBhY2Nlc3NvcnlJZCA9ICRzdGF0ZVBhcmFtcy5pZDtcclxuICAgICAgICAvLyRzY29wZS5lZGl0TW9kZSA9IGFjY2Vzc29yeUlkICE9PSB1bmRlZmluZWQ7XHJcbiAgICAgICAgdmFyIHRlbmFudElkO1xyXG4gICAgICAgICRzY29wZS5lZGl0TW9kZSA9IGZhbHNlO1xyXG5cclxuICAgICAgICAkc2NvcGUuYWNjZXNzb3J5ID0ge1xyXG4gICAgICAgICAgICBBc3NldElkOiAkc3RhdGVQYXJhbXMuaWRcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAoJHNjb3BlLmVkaXRNb2RlKSB7XHJcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgICAgIGFjY2Vzc29yaWVzU2VydmljZS5nZXRBY2Nlc3NvcnkoYWNjZXNzb3J5SWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWNjZXNzb3J5ID0gcmVzcG9uc2UuZGF0YTtcclxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWNjZXNzb3J5LlBpY3R1cmUgPSBgZGF0YTppbWFnZS9wbmc7YmFzZTY0LCR7JHNjb3BlLmFjY2Vzc29yeS5QaWN0dXJlfWA7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgYWNjZXNzb3JpZXNTZXJ2aWNlLmdldFRlbmFudCgpXHJcbiAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICRzY29wZS5uYXZpZ2F0ZUJhY2sgPSAoKSA9PiB7XG4gICAgICAgICAgICAkc3RhdGUudHJhbnNpdGlvblRvKFwiYWNjZXNzb3JpZXNcIiwge2lkIDogJHNjb3BlLmFjY2Vzc29yeS5Bc3NldElkfSk7XG4gICAgICAgIH07XHJcblxyXG4gICAgICAgICRzY29wZS5yZW1vdmVhY2Nlc3NvcnkgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIG1vZGFsU2VydmljZS5zaG93Q29uZmlybU1vZGFsKHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwiRWxpbWluYXIgQWNjZXNvcmlvXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9keTogXCJFc3RhIHNlZ3VybyBxdWUgZGVzZWEgZWxpbWluYXIgZWwgYWNjZXNvcmlvP1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIG9rOiBcIlNpLCBlbGltaW5hclwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhbmNlbDogXCJDYW5jZWxhclwiXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICBhY2Nlc3Nvcmllc1NlcnZpY2UucmVtb3ZlKGFjY2Vzc29yeUlkKVxyXG4gICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJhY2Nlc3Nvcmllc1wiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgJHNjb3BlLnNhdmUgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICgkc2NvcGUuYWNjZXNzb3J5LlBpY3R1cmUpIHtcclxuICAgICAgICAgICAgICAgICRzY29wZS5hY2Nlc3NvcnkuUGljdHVyZSA9ICRzY29wZS5hY2Nlc3NvcnkuUGljdHVyZS5zcGxpdCgnLCcpWzFdO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoISRzY29wZS5lZGl0TW9kZSkge1xyXG4gICAgICAgICAgICAgICAgJHNjb3BlLmFjY2Vzc29yeS5UZW5hbnRJZCA9IHRlbmFudElkO1xyXG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICBhY2Nlc3Nvcmllc1NlcnZpY2UuYWRkKCRzY29wZS5hY2Nlc3NvcnkpXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjaygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGFjY2Vzc29yaWVzU2VydmljZS51cGRhdGUoJHNjb3BlLmFjY2Vzc29yeSlcclxuICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBEZXRhaWxDb250cm9sbGVyOyIsIu+7v2Z1bmN0aW9uIEFjY2Vzc29yaWVzU2VydmljZSgkaHR0cCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgdmFyIGFjY2Vzc29yaWVzO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgZ2V0VGVuYW50LFxuICAgICAgICBnZXRBY2Nlc3NvcnksXG4gICAgICAgIGdldExpc3QsXG4gICAgICAgIGFkZCxcbiAgICAgICAgdXBkYXRlLFxuICAgICAgICByZW1vdmVcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gZ2V0VGVuYW50KCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBcIi9hcGkvdXNlcnMvY3VycmVudC90ZW5hbnRcIlxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRMaXN0KHBhZ2VTaXplLCBwYWdlQ291bnQsIGFzc2V0SWQpIHtcbiAgICAgICAgbGV0IGhhbmRsZVN1Y2Nlc3MgPSAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGFjY2Vzc29yaWVzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHJldHVybiBhY2Nlc3NvcmllcztcbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBgL2FwaS9hY2Nlc3Nvcmllcy8ke2Fzc2V0SWR9YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIHBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICBwYWdlU2l6ZTogcGFnZVNpemUsXG4gICAgICAgICAgICAgICAgICAgIHBhZ2VDb3VudDpwYWdlQ291bnRcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSkudGhlbihoYW5kbGVTdWNjZXNzKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0QWNjZXNzb3J5KGFjY2Vzc29yeUlkKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIGxldCB1cmwgPSBgL2FwaS9hY2Nlc3Nvcmllcy8ke2FjY2Vzc29yeUlkfWA7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBhZGQoYWNjZXNzb3J5KSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaS9hY2Nlc3Nvcmllcy9cIjtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBkYXRhOiBhY2Nlc3NvcnksXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlKGFjY2Vzc29yeSkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvYWNjZXNzb3JpZXMvXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQVVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBkYXRhOiBhY2Nlc3NvcnksXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVtb3ZlKGFjY2Vzc29yeUlkKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL2FjY2Vzc29yaWVzLyR7YWNjZXNzb3J5SWR9YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkRFTEVURVwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQWNjZXNzb3JpZXNTZXJ2aWNlOyIsIu+7v3ZhciBNb2R1bGVOYW1lID0gXCJWaWFsU29mdC5hc3NldHNcIjtcblxuaW1wb3J0IEFzc2V0c0NvbnRyb2xsZXIgZnJvbSBcIi4vY29udHJvbGxlcnMvYXNzZXRzQ29udHJvbGxlclwiO1xuaW1wb3J0IERldGFpbENvbnRyb2xsZXIgZnJvbSBcIi4vY29udHJvbGxlcnMvZGV0YWlsQ29udHJvbGxlclwiO1xuaW1wb3J0IEFzc2V0c1NlcnZpY2UgZnJvbSBcIi4vc2VydmljZXMvYXNzZXRzU2VydmljZVwiO1xuXG5hbmd1bGFyLm1vZHVsZShNb2R1bGVOYW1lLCBbXSkuXG4gICAgY29udHJvbGxlcihcImFzc2V0c0NvbnRyb2xsZXJcIiwgQXNzZXRzQ29udHJvbGxlcikuXG4gICAgY29udHJvbGxlcihcImFzc2V0c2RldGFpbENvbnRyb2xsZXJcIiwgRGV0YWlsQ29udHJvbGxlcikuXG4gICAgc2VydmljZShcImFzc2V0c1NlcnZpY2VcIiwgQXNzZXRzU2VydmljZSk7XG5cbmV4cG9ydCBkZWZhdWx0IE1vZHVsZU5hbWU7Iiwi77u/Y2xhc3MgQXNzZXRzQ29udHJvbGxlciB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGUsIGFzc2V0c1NlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlLCBtb2RhbFNlcnZpY2UpIHtcblxuICAgICAgICBjb25zdCBwYWdlU2l6ZSA9IDY7XG4gICAgICAgIHZhciBwYWdlQ291bnQgPSAwO1xuXG4gICAgICAgICRzY29wZS5hc3NldHMgPSBbXTtcblxuICAgICAgICAkc2NvcGUuZ2V0TGlzdCA9ICgpID0+IHtcblxuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgYXNzZXRzU2VydmljZS5nZXRMaXN0KHBhZ2VTaXplLCBwYWdlQ291bnQpXG4gICAgICAgICAgICAgICAgLnRoZW4oKGFzc2V0cykgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAoYXNzZXRzLmxlbmd0aCA8IHBhZ2VTaXplKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubm9Nb3JlRGF0YSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgYXNzZXRzLmZvckVhY2goKGFzc2V0KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYXNzZXRzLnB1c2goYXNzZXQpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcGFnZUNvdW50ICsrOyAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEkc2NvcGUuYXNzZXRzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5vRGF0YSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLm5hZ2l2YXRlVG9EZXRhaWwgPSAoYXNzZXRJZCkgPT4ge1xuICAgICAgICAgICAgYXNzZXRJZCA/ICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJhc3NldFwiLCB7IGlkOiBhc3NldElkIH0pIDogJHN0YXRlLnRyYW5zaXRpb25UbyhcImFzc2V0XCIpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5uYWdpdmF0ZVRvUHVyY2hhc2UgPSAoKSA9PiB7XG4gICAgICAgICAgICAkc3RhdGUudHJhbnNpdGlvblRvKFwicHVyY2hhc2VcIik7XG4gICAgICAgIH1cblxuICAgICAgICAkc2NvcGUucmVmcmVzaFNlbGVjdGVkSXRlbXMgPSAoYWxsKSA9PiB7XG4gICAgICAgICAgICBpZiAoYWxsKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmFzc2V0cy5mb3JFYWNoKChhc3NldCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBhc3NldC5zZWxlY3RlZCA9ICRzY29wZS5ldmVyeVNlbGVjdGVkO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuYW55U2VsZWN0ZWQgPSAkc2NvcGUuYXNzZXRzLnNvbWUoKGFzc2V0KSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGFzc2V0LnNlbGVjdGVkO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICRzY29wZS5ldmVyeVNlbGVjdGVkID0gJHNjb3BlLmFzc2V0cy5ldmVyeSgoYXNzZXQpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gYXNzZXQuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVtb3ZlID0gKGFzc2V0KSA9PiB7XG5cbiAgICAgICAgICAgIG1vZGFsU2VydmljZS5zaG93Q29uZmlybU1vZGFsKHtcbiAgICAgICAgICAgICAgICBtZXNzYWdlczoge1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogYEVsaW1pbmFyIGVsIGJpZW5gLFxuICAgICAgICAgICAgICAgICAgICBib2R5OiBgRXN0YSBzZWd1cm8gcXVlIGRlc2VhIGVsaW1pbmFyIGVsIGJpZW5gLFxuICAgICAgICAgICAgICAgICAgICBvazogXCJTaSwgZWxpbWluYXJcIixcbiAgICAgICAgICAgICAgICAgICAgY2FuY2VsOiBcIkNhbmNlbGFyXCJcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIHZhciBhc3NldElkTGlzdDtcbiAgICAgICAgICAgICAgICBpZiAoYXNzZXQpIHtcbiAgICAgICAgICAgICAgICAgICAgYXNzZXRJZExpc3QgPSBbYXNzZXQuQXNzZXRJZF07XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgYXNzZXRJZExpc3QgPSAkc2NvcGUuYXNzZXRzXG4gICAgICAgICAgICAgICAgICAgICAgICAubWFwKChhc3NldEl0ZW0pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXNzZXRJdGVtLnNlbGVjdGVkKSB7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFzc2V0SXRlbS5Bc3NldElkO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBhc3NldElkTGlzdC5mb3JFYWNoKChhc3NldElkKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UucmVtb3ZlKGFzc2V0SWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFzc2V0cy5mb3JFYWNoKChhc3NldEl0ZW0pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhc3NldElkID09PSBhc3NldEl0ZW0uQXNzZXRJZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBpbmRleCA9ICRzY29wZS5hc3NldHMuaW5kZXhPZihhc3NldEl0ZW0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hc3NldHMuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAkc2NvcGUucmVmcmVzaFNlbGVjdGVkSXRlbXMoKTtcblxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmdldExpc3QoKTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEFzc2V0c0NvbnRyb2xsZXI7Iiwi77u/Y2xhc3MgRGV0YWlsQ29udHJvbGxlciB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGVQYXJhbXMsICRzdGF0ZSwgYXNzZXRzU2VydmljZSwgdG9hc3RlclNlcnZpY2UsIG1vZGFsU2VydmljZSkge1xuXG4gICAgICAgICQoXCJzZWxlY3RcIikuc2VsZWN0Mih7IHdpZHRoOiBcIjEwMCVcIiB9KTtcblxuICAgICAgICB2YXIgYXNzZXRJZCA9ICRzdGF0ZVBhcmFtcy5pZDtcbiAgICAgICAgJHNjb3BlLmVkaXRNb2RlID0gYXNzZXRJZCAhPT0gdW5kZWZpbmVkO1xuICAgICAgICAkc2NvcGUub3duQXNzZXQgPSB0cnVlO1xuXG4gICAgICAgIHZhciB0ZW5hbnRJZDtcblxuICAgICAgICAkc2NvcGUuYXNzZXQgPSB7XG4gICAgICAgICAgICBDcmVhdGVkQXQ6IG5ldyBEYXRlKCksXG4gICAgICAgICAgICBTdGF0dXNBc3NldDogMVxuICAgICAgICB9O1xuXG4gICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0Q3VzdG9tZXIoKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmN1c3RvbWVycyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmFzc2V0LkN1c3RvbWVySWQgPSAxO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgYXNzZXRzU2VydmljZS5nZXRDYXRlZ29yeSgpXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuY2F0ZWdvcmllcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICBhc3NldHNTZXJ2aWNlLmdldFN1YkNhdGVnb3J5QWxsKClcbiAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICRzY29wZS5zdWJjYXRlZ29yaWVzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICRzY29wZS5jaGFuZ2VQcm9wZXJ0eSA9ICgpID0+IHtcbiAgICAgICAgICAgIGlmICgkc2NvcGUuYXNzZXQuU3RhdHVzQXNzZXQgPT09IDEpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUub3duQXNzZXQgPSB0cnVlO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUub3duQXNzZXQgPSBmYWxzZTtcblxuICAgICAgICAgICAgICAgICQoXCIjY3VzdG9tZXJzXCIpLnNlbGVjdDIoe1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogXCIxMDAlXCJcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgICRzY29wZS5nZXRTdWJDYXRlZ29yeSA9ICgpID0+IHtcbiAgICAgICAgICAgICRzY29wZS5zdWJjYXRlZ29yaWVzID0gbnVsbDtcbiAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0U3ViQ2F0ZWdvcnkoJHNjb3BlLmFzc2V0LkNhdGVnb3J5SWQpXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5zdWJjYXRlZ29yaWVzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgJChcIiNzdWJjYXRlZ29yaWVzXCIpLnNlbGVjdDIoe1xuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IFwiMTAwJVwiXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgYXNzZXRzU2VydmljZS5lbnVtU3RhdHVzKClcbiAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICRzY29wZS5vcHRpb25zID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0V29yaygpXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAkc2NvcGUud29ya3MgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKCRzY29wZS5lZGl0TW9kZSkge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0QXNzZXQoYXNzZXRJZClcbiAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFzc2V0ID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFzc2V0LlBpY3R1cmUgPSBgZGF0YTppbWFnZS9wbmc7YmFzZTY0LCR7JHNjb3BlLmFzc2V0LlBpY3R1cmV9YDtcbiAgICAgICAgICAgICAgICAgICAgYXNzZXRzU2VydmljZS5nZXRDYXRlZ29yeSgpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2VDYXRlZ29yeSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5jYXRlZ29yaWVzID0gcmVzcG9uc2VDYXRlZ29yeS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjY2F0ZWdvcmllc1wiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjd29ya3NcIikuc2VsZWN0Mih7IHdpZHRoOiBcIjEwMCVcIiB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI3Byb3BlcnR5XCIpLnNlbGVjdDIoeyB3aWR0aDogXCIxMDAlXCIgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzZXRzU2VydmljZS5nZXRTdWJDYXRlZ29yeSgkc2NvcGUuYXNzZXQuQ2F0ZWdvcnlJZClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlc3ViQ2F0ZWdvcnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5zdWJjYXRlZ29yaWVzID0gcmVzcG9uc2VzdWJDYXRlZ29yeS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNzdWJjYXRlZ29yaWVzXCIpLnNlbGVjdDIoeyB3aWR0aDogXCIxMDAlXCIgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0Q2F0ZWdvcnkoKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlQ2F0ZWdvcnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuY2F0ZWdvcmllcyA9IHJlc3BvbnNlQ2F0ZWdvcnkuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2NhdGVnb3JpZXNcIikuc2VsZWN0Mih7IHdpZHRoOiBcIjEwMCVcIiB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI3dvcmtzXCIpLnNlbGVjdDIoeyB3aWR0aDogXCIxMDAlXCIgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNwcm9wZXJ0eVwiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0U3ViQ2F0ZWdvcnkoJHNjb3BlLmFzc2V0LkNhdGVnb3J5SWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZXN1YkNhdGVnb3J5KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc3ViY2F0ZWdvcmllcyA9IHJlc3BvbnNlc3ViQ2F0ZWdvcnkuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjc3ViY2F0ZWdvcmllc1wiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBhc3NldHNTZXJ2aWNlLmVudW1TdGF0dXMoKVxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZVN0YXR1cykgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3B0aW9ucyA9IHJlc3BvbnNlU3RhdHVzLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICQoXCIjcHJvcGVydHlcIikuc2VsZWN0Mih7IHdpZHRoOiBcIjEwMCVcIiB9KTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0VGVuYW50KClcbiAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIH1cblxuICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrID0gKCkgPT4ge1xuICAgICAgICAgICAgJHN0YXRlLnRyYW5zaXRpb25UbyhcImFzc2V0c1wiKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUubmFnaXZhdGVUb01haW50ZW5hbmNlQ2FyZCA9IChpZCkgPT4ge1xuICAgICAgICAgICAgJHN0YXRlLnRyYW5zaXRpb25UbyhcIm1haW50ZW5hbmNlY2FyZHNcIiwgeyBpZDogaWQgfSk7XG4gICAgICAgIH07XG4gICAgICAgIFxuXG4gICAgICAgICRzY29wZS5uYWdpdmF0ZVRvQWNjZXNzb3JpZXMgPSAoaWQpID0+IHtcbiAgICAgICAgICAgICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJhY2Nlc3Nvcmllc1wiLCB7IGlkOiBpZCB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVtb3ZlQXNzZXQgPSAoKSA9PiB7XG4gICAgICAgICAgICBtb2RhbFNlcnZpY2Uuc2hvd0NvbmZpcm1Nb2RhbCh7XG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJFbGltaW5hciBlbCBCaWVuXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBib2R5OiBcIkVzdGEgc2VndXJvIHF1ZSBkZXNlYSBlbGltaW5hciBlbCBCaWVuP1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgb2s6IFwiU2ksIGVsaW1pbmFyXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBjYW5jZWw6IFwiQ2FuY2VsYXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UucmVtb3ZlKGFzc2V0SWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHN0YXRlLnRyYW5zaXRpb25UbyhcImFzc2V0c1wiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnNhdmUgPSAoKSA9PiB7XG4gICAgICAgICAgICBpZiAoJHNjb3BlLmFzc2V0LlBpY3R1cmUpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuYXNzZXQuUGljdHVyZSA9ICRzY29wZS5hc3NldC5QaWN0dXJlLnNwbGl0KFwiLFwiKVsxXTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCEkc2NvcGUuZWRpdE1vZGUpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuYXNzZXQuVGVuYW50SWQgPSB0ZW5hbnRJZDtcbiAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSB0cnVlO1xuXG4gICAgICAgICAgICAgICAgYXNzZXRzU2VydmljZS5hZGQoJHNjb3BlLmFzc2V0KVxuICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5uYXZpZ2F0ZUJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UudXBkYXRlKCRzY29wZS5hc3NldClcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IERldGFpbENvbnRyb2xsZXI7Iiwi77u/ZnVuY3Rpb24gQXNzZXRzU2VydmljZSgkaHR0cCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgdmFyIGFzc2V0cztcblxuICAgIHJldHVybiB7XG4gICAgICAgIGVudW1TdGF0dXMsXG4gICAgICAgIGdldENhdGVnb3J5LFxuICAgICAgICBnZXRTdWJDYXRlZ29yeSxcbiAgICAgICAgZ2V0U3ViQ2F0ZWdvcnlBbGwsXG4gICAgICAgIGdldEN1c3RvbWVyLFxuICAgICAgICBnZXRUZW5hbnQsXG4gICAgICAgIGdldFdvcmssXG4gICAgICAgIGdldEFzc2V0LFxuICAgICAgICBnZXRMaXN0LFxuICAgICAgICBhZGQsXG4gICAgICAgIHVwZGF0ZSxcbiAgICAgICAgcmVtb3ZlXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIGdldEN1c3RvbWVyKCl7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIGxldCB1cmwgPSBgL2FwaS9jdXN0b21lcnMvZ2V0QWxsYDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFdvcmsoKXtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL3dvcmtzL2dldEFsbGA7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBlbnVtU3RhdHVzKCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBcIi9hcGkvYXNzZXRzL0VudW1TdGF0dXNcIlxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRDYXRlZ29yeSgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogXCIvYXBpL0NhdGVnb3JpZXNcIlxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRTdWJDYXRlZ29yeUFsbCgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogYC9hcGkvU3ViQ2F0ZWdvcmllc2BcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0U3ViQ2F0ZWdvcnkoY2F0ZWdvcnlJZCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBgL2FwaS9TdWJDYXRlZ29yaWVzLyR7Y2F0ZWdvcnlJZH1gXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFRlbmFudCgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogXCIvYXBpL3VzZXJzL2N1cnJlbnQvdGVuYW50XCJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0TGlzdChwYWdlU2l6ZSwgcGFnZUNvdW50KSB7XG4gICAgICAgIGxldCBoYW5kbGVTdWNjZXNzID0gKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBhc3NldHMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgcmV0dXJuIGFzc2V0cztcbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvYXNzZXRzXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgcGFnZVNpemU6IHBhZ2VTaXplLFxuICAgICAgICAgICAgICAgICAgICBwYWdlQ291bnQ6cGFnZUNvdW50XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnRoZW4oaGFuZGxlU3VjY2Vzcyk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldEFzc2V0KGFzc2V0SWQpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL2Fzc2V0cy8ke2Fzc2V0SWR9YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBhZGQoYXNzZXQpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvYXBpL2Fzc2V0cy9cIjtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBkYXRhOiBhc3NldCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZCAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHVwZGF0ZShhc3NldCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvYXNzZXRzL1wiO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUFVUXCIsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgZGF0YTogYXNzZXQsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVtb3ZlKGFzc2V0SWQpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvYXNzZXRzLyR7YXNzZXRJZH1gO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiREVMRVRFXCIsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIFxufVxuXG5leHBvcnQgZGVmYXVsdCBBc3NldHNTZXJ2aWNlOyIsIu+7v2NsYXNzIENvc3RlQXNzZXRDb250cm9sbGVyIHtcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICRyb290U2NvcGUsICRzdGF0ZSwgY29zdGVhc3NldFNlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlKSB7XG5cbiAgICAgICAgY29uc3QgcGFnZVNpemUgPSA2O1xuICAgICAgICB2YXIgcGFnZUNvdW50ID0gMDtcblxuICAgICAgICAkc2NvcGUuY29zdGVhc3NldHMgPSBbXTtcblxuICAgICAgICAkc2NvcGUuZ2V0TGlzdCA9ICgpID0+IHtcblxuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY29zdGVhc3NldFNlcnZpY2UuZ2V0TGlzdChwYWdlU2l6ZSwgcGFnZUNvdW50KVxuICAgICAgICAgICAgICAgIC50aGVuKChjb3N0ZWFzc2V0cykgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAoY29zdGVhc3NldHMubGVuZ3RoIDwgcGFnZVNpemUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5ub01vcmVEYXRhID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjb3N0ZWFzc2V0cy5mb3JFYWNoKChjb3N0ZUFzc2V0KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuY29zdGVhc3NldHMucHVzaChjb3N0ZUFzc2V0KTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHBhZ2VDb3VudCArKzsgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5yZWZyZXNoU2VsZWN0ZWRJdGVtcygpOyAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIGlmICghJHNjb3BlLmNvc3RlYXNzZXRzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5vRGF0YSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLm5hZ2l2YXRlVG9EZXRhaWwgPSAoY29zdGVJZCkgPT4ge1xuICAgICAgICAgICAgY29zdGVJZCA/ICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJjb3N0ZWFzc2V0XCIsIHsgaWQ6IGNvc3RlSWQgfSkgOiAkc3RhdGUudHJhbnNpdGlvblRvKFwiY29zdGVhc3NldFwiKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVmcmVzaFNlbGVjdGVkSXRlbXMgPSAoYWxsKSA9PiB7XG4gICAgICAgICAgICBpZiAoYWxsKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmNvc3RlYXNzZXRzLmZvckVhY2goKGNvc3RlQXNzZXQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29zdGVBc3NldC5zZWxlY3RlZCA9ICRzY29wZS5ldmVyeVNlbGVjdGVkO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuYW55U2VsZWN0ZWQgPSAkc2NvcGUuY29zdGVhc3NldHMuc29tZSgoY29zdGVBc3NldCkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBjb3N0ZUFzc2V0LnNlbGVjdGVkO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICRzY29wZS5ldmVyeVNlbGVjdGVkID0gJHNjb3BlLmNvc3RlYXNzZXRzLmV2ZXJ5KChjb3N0ZUFzc2V0KSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGNvc3RlQXNzZXQuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuZ2V0TGlzdCgpO1xuXG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBDb3N0ZUFzc2V0Q29udHJvbGxlcjsiLCLvu79jbGFzcyBEZXRhaWxDb250cm9sbGVyIHtcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICRyb290U2NvcGUsICRzdGF0ZVBhcmFtcywgJHN0YXRlLCBjb3N0ZWFzc2V0U2VydmljZSwgdG9hc3RlclNlcnZpY2UsIG1vZGFsU2VydmljZSkge1xuXG4gICAgICAgIHZhciBjb3N0ZWFzc2V0SWQgPSAkc3RhdGVQYXJhbXMuaWQ7XG4gICAgICAgICRzY29wZS5lZGl0TW9kZSA9IGNvc3RlYXNzZXRJZCAhPT0gdW5kZWZpbmVkO1xuICAgICAgICAkc2NvcGUuc2VhcmNoTW9kZSA9IGZhbHNlO1xuICAgICAgICAkc2NvcGUuYXNzZXQgPSB7fTtcbiAgICAgICAgJHNjb3BlLmNvc3RlYXNzZXQgPSB7XG4gICAgICAgICAgICBDcmVhdGVkQXQ6IG5ldyBEYXRlKClcbiAgICAgICAgfTsgICAgXG5cbiAgICAgICAgJHNjb3BlLnNlYXJjaCA9ICgpID0+IHtcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICBjb3N0ZWFzc2V0U2VydmljZS5nZXRBc3NldEJ5SWQoJHNjb3BlLmFzc2V0LklkKVxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2VhcmNoTW9kZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hc3NldCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgfSkuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjayA9ICgpID0+IHtcbiAgICAgICAgICAgICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJjb3N0ZWFzc2V0c1wiKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuc2F2ZSA9ICgpID0+IHtcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICBjb3N0ZWFzc2V0U2VydmljZS5nZXRUZW5hbnQoKVxuICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG5cbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmNvc3RlYXNzZXQuVGVuYW50SWQgPSB0ZW5hbnRJZDtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmNvc3RlYXNzZXQuQXNzZXRJZCA9ICRzY29wZS5hc3NldC5Bc3NldElkO1xuICAgICAgICAgICAgICAgICAgICBjb3N0ZWFzc2V0U2VydmljZS5hZGQoJHNjb3BlLmNvc3RlYXNzZXQpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2VBZGQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2VBZGQuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjaygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIH07XG5cbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IERldGFpbENvbnRyb2xsZXI7Iiwi77u/dmFyIE1vZHVsZU5hbWUgPSBcIlZpYWxTb2Z0LmNvc3RlYXNzZXRcIjtcblxuaW1wb3J0IENvc3RlQXNzZXRDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL2Nvc3RlYXNzZXRDb250cm9sbGVyXCI7XG5pbXBvcnQgRGV0YWlsQ29udHJvbGxlciBmcm9tIFwiLi9jb250cm9sbGVycy9kZXRhaWxDb250cm9sbGVyXCI7XG5pbXBvcnQgQ29zdGVBc3NldFNlcnZpY2UgZnJvbSBcIi4vc2VydmljZXMvY29zdGVhc3NldFNlcnZpY2VcIjtcblxuYW5ndWxhci5tb2R1bGUoTW9kdWxlTmFtZSwgW10pLlxuICAgIGNvbnRyb2xsZXIoXCJjb3N0ZWFzc2V0Q29udHJvbGxlclwiLCBDb3N0ZUFzc2V0Q29udHJvbGxlcikuXG4gICAgY29udHJvbGxlcihcImNvc3RlYXNzZXRkZXRhaWxDb250cm9sbGVyXCIsIERldGFpbENvbnRyb2xsZXIpLlxuICAgIHNlcnZpY2UoXCJjb3N0ZWFzc2V0U2VydmljZVwiLCBDb3N0ZUFzc2V0U2VydmljZSk7XG5cbmV4cG9ydCBkZWZhdWx0IE1vZHVsZU5hbWU7Iiwi77u/ZnVuY3Rpb24gQ29zdGVBc3NldFNlcnZpY2UoJGh0dHApIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuICAgIHZhciBjb3N0ZUFzc2V0O1xuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgZ2V0QXNzZXQsXG4gICAgICAgIGdldFRlbmFudCxcbiAgICAgICAgZ2V0TGlzdCxcbiAgICAgICAgZ2V0QXNzZXRCeUlkLFxuICAgICAgICBhZGQsXG4gICAgICAgIHVwZGF0ZVxuICAgIH07XG5cbiAgICBmdW5jdGlvbiBnZXRBc3NldChpZCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvYXNzZXRzL0dldEFzc2V0YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIHBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICBJZDogaWRcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFRlbmFudCgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogXCIvYXBpL3VzZXJzL2N1cnJlbnQvdGVuYW50XCJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0TGlzdChwYWdlU2l6ZSwgcGFnZUNvdW50KSB7XG4gICAgICAgIGxldCBoYW5kbGVTdWNjZXNzID0gKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBjb3N0ZUFzc2V0ID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHJldHVybiBjb3N0ZUFzc2V0O1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaS9jb3N0ZUFzc2V0XCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgcGFnZVNpemU6IHBhZ2VTaXplLFxuICAgICAgICAgICAgICAgICAgICBwYWdlQ291bnQ6cGFnZUNvdW50XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnRoZW4oaGFuZGxlU3VjY2Vzcyk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldEFzc2V0QnlJZChpZCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvYXNzZXRzL0dldEFzc2V0YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIHBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICBJZDogaWRcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFkZChjb3N0ZWFzc2V0KSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaS9jb3N0ZUFzc2V0L1wiO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGRhdGE6IGNvc3RlYXNzZXQsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWQgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB1cGRhdGUoY29zdGVhc3NldCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvY29zdGVBc3NldC9cIjtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBVVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGRhdGE6IGNvc3RlYXNzZXQsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIFxufVxuXG5leHBvcnQgZGVmYXVsdCBDb3N0ZUFzc2V0U2VydmljZTsiLCLvu79jbGFzcyBDdXN0b21lcnNDb250cm9sbGVyIHtcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICRyb290U2NvcGUsICRzdGF0ZSwgY3VzdG9tZXJzU2VydmljZSwgdG9hc3RlclNlcnZpY2UsIG1vZGFsU2VydmljZSkge1xuXG4gICAgICAgIGNvbnN0IHBhZ2VTaXplID0gNjtcbiAgICAgICAgdmFyIHBhZ2VDb3VudCA9IDA7XG5cbiAgICAgICAgJHNjb3BlLmN1c3RvbWVycyA9IFtdO1xuXG4gICAgICAgICRzY29wZS5nZXRMaXN0ID0gKCkgPT4ge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIGN1c3RvbWVyc1NlcnZpY2UuZ2V0TGlzdChwYWdlU2l6ZSwgcGFnZUNvdW50KVxuICAgICAgICAgICAgICAgIC50aGVuKChjdXN0b21lcnMpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGN1c3RvbWVycy5sZW5ndGggPCBwYWdlU2l6ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5vTW9yZURhdGEgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGN1c3RvbWVycy5mb3JFYWNoKChjdXN0b21lcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmN1c3RvbWVycy5wdXNoKGN1c3RvbWVyKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHBhZ2VDb3VudCArKztcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKCEkc2NvcGUuY3VzdG9tZXJzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5vRGF0YSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLm5hZ2l2YXRlVG9EZXRhaWwgPSAoY3VzdG9tZXJJZCkgPT4ge1xuICAgICAgICAgICAgY3VzdG9tZXJJZCA/ICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJjdXN0b21lclwiLCB7IGlkOiBjdXN0b21lcklkIH0pIDogJHN0YXRlLnRyYW5zaXRpb25UbyhcImN1c3RvbWVyXCIpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZWZyZXNoU2VsZWN0ZWRJdGVtcyA9IChhbGwpID0+IHtcbiAgICAgICAgICAgIGlmIChhbGwpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuY3VzdG9tZXJzLmZvckVhY2goKGN1c3RvbWVyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGN1c3RvbWVyLnNlbGVjdGVkID0gJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQ7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5hbnlTZWxlY3RlZCA9ICRzY29wZS5jdXN0b21lcnMuc29tZSgoY3VzdG9tZXIpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gY3VzdG9tZXIuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQgPSAkc2NvcGUuY3VzdG9tZXJzLmV2ZXJ5KChjdXN0b21lcikgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBjdXN0b21lci5zZWxlY3RlZDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZW1vdmUgPSAoY3VzdG9tZXIpID0+IHtcblxuICAgICAgICAgICAgbW9kYWxTZXJ2aWNlLnNob3dDb25maXJtTW9kYWwoe1xuICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiB7XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBgRWxpbWluYXIgY2xpZW50ZWAsXG4gICAgICAgICAgICAgICAgICAgIGJvZHk6IGBFc3RhIHNlZ3VybyBxdWUgZGVzZWEgZWxpbWluYXIgZWwgY2xpZW50ZWAsXG4gICAgICAgICAgICAgICAgICAgIG9rOiBcIlNpLCBlbGltaW5hclwiLFxuICAgICAgICAgICAgICAgICAgICBjYW5jZWw6IFwiQ2FuY2VsYXJcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgdmFyIGN1c3RvbWVySWRMaXN0O1xuICAgICAgICAgICAgICAgIGlmIChjdXN0b21lcikge1xuICAgICAgICAgICAgICAgICAgICBjdXN0b21lcklkTGlzdCA9IFtjdXN0b21lci5DdXN0b21lcklkXTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjdXN0b21lcklkTGlzdCA9ICRzY29wZS5jdXN0b21lcnNcbiAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAoKGN1c3RvbWVySXRlbSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjdXN0b21lckl0ZW0uc2VsZWN0ZWQpIHsgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gY3VzdG9tZXJJdGVtLkN1c3RvbWVySWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGN1c3RvbWVySWRMaXN0LmZvckVhY2goKGN1c3RvbWVySWQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY3VzdG9tZXJzU2VydmljZS5yZW1vdmUoY3VzdG9tZXJJZClcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuY3VzdG9tZXJzLmZvckVhY2goKGN1c3RvbWVySXRlbSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGN1c3RvbWVySWQgPT09IGN1c3RvbWVySXRlbS5DdXN0b21lcklkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGluZGV4ID0gJHNjb3BlLmN1c3RvbWVycy5pbmRleE9mKGN1c3RvbWVySXRlbSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmN1c3RvbWVycy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICRzY29wZS5yZWZyZXNoU2VsZWN0ZWRJdGVtcygpO1xuXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuZ2V0TGlzdCgpO1xuICAgIH1cblxufVxuXG5leHBvcnQgZGVmYXVsdCBDdXN0b21lcnNDb250cm9sbGVyOyIsIu+7v1widXNlIHN0cmljdFwiO1xyXG5cclxuY2xhc3MgRGV0YWlsQ29udHJvbGxlciB7XHJcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICRyb290U2NvcGUsICRzdGF0ZVBhcmFtcywgJHN0YXRlLCBjdXN0b21lcnNTZXJ2aWNlLCB0b2FzdGVyU2VydmljZSwgbW9kYWxTZXJ2aWNlKSB7XHJcblxyXG4gICAgICAgICQoXCJzZWxlY3RcIikuc2VsZWN0Mih7d2lkdGg6IFwiMTAwJVwifSk7XHJcblxyXG4gICAgICAgIHZhciBjdXN0b21lcklkID0gJHN0YXRlUGFyYW1zLmlkO1xyXG4gICAgICAgICRzY29wZS5lZGl0TW9kZSA9IGN1c3RvbWVySWQgIT09IHVuZGVmaW5lZDtcclxuICAgICAgICB2YXIgdGVuYW50SWQ7XHJcblxyXG4gICAgICAgICRzY29wZS5jdXN0b21lciA9IHtcclxuICAgICAgICAgICAgQ3JlYXRlZEF0OiBuZXcgRGF0ZSgpXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY3VzdG9tZXJzU2VydmljZS5lbnVtRW50aXR5KClcclxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAkc2NvcGUub3B0aW9ucyA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAoJHNjb3BlLmVkaXRNb2RlKSB7XHJcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgICAgIGN1c3RvbWVyc1NlcnZpY2UuZ2V0Q3VzdG9tZXIoY3VzdG9tZXJJZClcclxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5jdXN0b21lciA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgY3VzdG9tZXJzU2VydmljZS5lbnVtRW50aXR5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlRW50aXR5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3B0aW9ucyA9IHJlc3BvbnNlRW50aXR5LmRhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2VudGl0eVwiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5jdXN0b21lci5QaWN0dXJlID0gYGRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCwkeyRzY29wZS5jdXN0b21lci5QaWN0dXJlfWA7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgY3VzdG9tZXJzU2VydmljZS5nZXRUZW5hbnQoKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrID0gKCkgPT4ge1xyXG4gICAgICAgICAgICAkc3RhdGUudHJhbnNpdGlvblRvKFwiY3VzdG9tZXJzXCIpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgICRzY29wZS5yZW1vdmVDdXN0b21lciA9ICgpID0+IHtcclxuICAgICAgICAgICAgbW9kYWxTZXJ2aWNlLnNob3dDb25maXJtTW9kYWwoe1xyXG4gICAgICAgICAgICAgICAgbWVzc2FnZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJFbGltaW5hciBDbGllbnRlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9keTogXCJFc3RhIHNlZ3VybyBxdWUgZGVzZWEgZWxpbWluYXIgZWwgY2xpZW50ZT9cIixcclxuICAgICAgICAgICAgICAgICAgICBvazogXCJTaSwgZWxpbWluYXJcIixcclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWw6IFwiQ2FuY2VsYXJcIlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgY3VzdG9tZXJzU2VydmljZS5yZW1vdmUoY3VzdG9tZXJJZClcclxuICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAkc3RhdGUudHJhbnNpdGlvblRvKFwiY3VzdG9tZXJzXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICB9KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAkc2NvcGUuc2F2ZSA9ICgpID0+IHtcclxuICAgICAgICAgICAgaWYgKCRzY29wZS5jdXN0b21lci5QaWN0dXJlKSB7XHJcbiAgICAgICAgICAgICAgICAkc2NvcGUuY3VzdG9tZXIuUGljdHVyZSA9ICRzY29wZS5jdXN0b21lci5QaWN0dXJlLnNwbGl0KFwiLFwiKVsxXTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKCEkc2NvcGUuZWRpdE1vZGUpIHtcclxuICAgICAgICAgICAgICAgICRzY29wZS5jdXN0b21lci5UZW5hbnRJZCA9IHRlbmFudElkO1xyXG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICBjdXN0b21lcnNTZXJ2aWNlLmFkZCgkc2NvcGUuY3VzdG9tZXIpXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjaygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGN1c3RvbWVyc1NlcnZpY2UudXBkYXRlKCRzY29wZS5jdXN0b21lcilcclxuICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IERldGFpbENvbnRyb2xsZXI7Iiwi77u/dmFyIE1vZHVsZU5hbWUgPSBcIlZpYWxTb2Z0LmN1c3RvbWVyc1wiO1xyXG5cclxuaW1wb3J0IEN1c3RvbWVyc0NvbnRyb2xsZXIgZnJvbSBcIi4vY29udHJvbGxlcnMvY3VzdG9tZXJzQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgRGV0YWlsQ29udHJvbGxlciBmcm9tIFwiLi9jb250cm9sbGVycy9kZXRhaWxDb250cm9sbGVyXCI7XHJcbmltcG9ydCBDdXN0b21lcnNTZXJ2aWNlIGZyb20gXCIuL3NlcnZpY2VzL2N1c3RvbWVyc1NlcnZpY2VcIjtcclxuaW1wb3J0IEZpbGVCYXNlNjQgZnJvbSBcIi4vZGlyZWN0aXZlcy9maWxlRGlyZWN0aXZlXCI7XHJcblxyXG5hbmd1bGFyLm1vZHVsZShNb2R1bGVOYW1lLCBbXSkuXHJcbiAgICBjb250cm9sbGVyKFwiY3VzdG9tZXJzQ29udHJvbGxlclwiLCBDdXN0b21lcnNDb250cm9sbGVyKS5cclxuICAgIGNvbnRyb2xsZXIoXCJjdXN0b21lcnNkZXRhaWxDb250cm9sbGVyXCIsIERldGFpbENvbnRyb2xsZXIpLlxyXG4gICAgc2VydmljZShcImN1c3RvbWVyc1NlcnZpY2VcIiwgQ3VzdG9tZXJzU2VydmljZSkuXHJcbiAgICBkaXJlY3RpdmUoXCJmaWxlQmFzZTY0XCIsIEZpbGVCYXNlNjQpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgTW9kdWxlTmFtZTsiLCLvu79jbGFzcyBGaWxlQmFzZTY0IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5yZXN0cmljdCA9ICdBJztcbiAgICAgICAgdGhpcy5zY29wZSA9IHtcbiAgICAgICAgICAgICdiNjQnIDogJz0nXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgbGluayAoc2NvcGUsIGVsZW1lbnQpIHtcblxuICAgICAgICBlbGVtZW50Lm9uKCdjaGFuZ2UnLCAoKSA9PiB7XG4gICAgICAgICAgICB2YXIgZmlsZSA9IGVsZW1lbnQuZ2V0KDApLmZpbGVzWzBdO1xuICAgICAgICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG5cbiAgICAgICAgICAgIHJlYWRlci5vbmxvYWRlbmQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgc2NvcGUuJGFwcGx5KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBzY29wZS5iNjQgPSByZWFkZXIucmVzdWx0O1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgaWYgKGZpbGUpIHtcbiAgICAgICAgICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgc2NvcGUuYjY0ID0gJyc7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHN0YXRpYyBkaXJlY3RpdmVGYWN0b3J5KCkge1xuICAgICAgICBGaWxlQmFzZTY0Lmluc3RhbmNlID0gbmV3IEZpbGVCYXNlNjQoKTtcbiAgICAgICAgcmV0dXJuIEZpbGVCYXNlNjQuaW5zdGFuY2U7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBGaWxlQmFzZTY0LmRpcmVjdGl2ZUZhY3Rvcnk7Iiwi77u/ZnVuY3Rpb24gQ3VzdG9tZXJzU2VydmljZSgkaHR0cCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgdmFyIGN1c3RvbWVycztcblxuICAgIHJldHVybiB7XG4gICAgICAgIGVudW1FbnRpdHksXG4gICAgICAgIGdldFRlbmFudCxcbiAgICAgICAgZ2V0Q3VzdG9tZXIsXG4gICAgICAgIGdldExpc3QsXG4gICAgICAgIGFkZCxcbiAgICAgICAgdXBkYXRlLFxuICAgICAgICByZW1vdmVcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gZW51bUVudGl0eSgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogXCIvYXBpL2N1c3RvbWVycy9FbnVtRW50aXR5XCJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0VGVuYW50KCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBcIi9hcGkvdXNlcnMvY3VycmVudC90ZW5hbnRcIlxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRMaXN0KHBhZ2VTaXplLCBwYWdlQ291bnQpIHtcbiAgICAgICAgbGV0IGhhbmRsZVN1Y2Nlc3MgPSAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGN1c3RvbWVycyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICByZXR1cm4gY3VzdG9tZXJzO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaS9jdXN0b21lcnNcIjtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIHBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICBwYWdlU2l6ZTogcGFnZVNpemUsXG4gICAgICAgICAgICAgICAgICAgIHBhZ2VDb3VudDpwYWdlQ291bnRcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSkudGhlbihoYW5kbGVTdWNjZXNzKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0Q3VzdG9tZXIoY3VzdG9tZXJJZCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvY3VzdG9tZXJzLyR7Y3VzdG9tZXJJZH1gO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gYWRkKGN1c3RvbWVyKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaS9jdXN0b21lcnMvXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgZGF0YTogY3VzdG9tZXIsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWQgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB1cGRhdGUoY3VzdG9tZXIpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvYXBpL2N1c3RvbWVycy9cIjtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBVVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGRhdGE6IGN1c3RvbWVyLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJlbW92ZShjdXN0b21lcklkKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL2N1c3RvbWVycy8ke2N1c3RvbWVySWR9YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkRFTEVURVwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQ3VzdG9tZXJzU2VydmljZTsiLCLvu79jbGFzcyBEYXNoYm9hcmRDb250cm9sbGVyIHtcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICRyb290U2NvcGUsIGRhc2hib2FyZFNlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlKSB7XG5cbiAgICAgICAgY29uc3QgeWVhciA9IG5ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKTtcbiAgICAgICAgJHNjb3BlLmluY29tZXNFeHBlbnNlc1llYXIgPSB5ZWFyO1xuICAgICAgICAkc2NvcGUuY3VycmVudFllYXIgPSB5ZWFyO1xuXG4gICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XG4gICAgICAgIGRhc2hib2FyZFNlcnZpY2UuZ2V0U3VtbWFyeSgpXG4gICAgICAgICAgICAudGhlbigoc3VtbWFyeSkgPT4ge1xuICAgICAgICAgICAgICAgICRzY29wZS5zdW1tYXJ5ID0gc3VtbWFyeTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAkc2NvcGUuYWRkWWVhckluY29tZXNFeHBlbnNlcyA9ICgpID0+IHtcbiAgICAgICAgICAgIGlmICgkc2NvcGUuY3VycmVudFllYXIgPiAkc2NvcGUuaW5jb21lc0V4cGVuc2VzWWVhcikge1xuICAgICAgICAgICAgICAgICRzY29wZS5pbmNvbWVzRXhwZW5zZXNZZWFyICs9IDE7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnJlZHVjZVllYXJJbmNvbWVzRXhwZW5zZXMgPSAoKSA9PiB7XG4gICAgICAgICAgICAkc2NvcGUuaW5jb21lc0V4cGVuc2VzWWVhciAtPSAxO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5jb3JyZWN0WWVhciA9ICgpID0+IHtcbiAgICAgICAgICAgIGlmICgkc2NvcGUuY3VycmVudFllYXIgPCAkc2NvcGUuaW5jb21lc0V4cGVuc2VzWWVhcikge1xuICAgICAgICAgICAgICAgICRzY29wZS5pbmNvbWVzRXhwZW5zZXNZZWFyID0gJHNjb3BlLmN1cnJlbnRZZWFyO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIHZhciBjcmVhdGVDaGFydERhdGFJbmNvbWVzRXhwZW5zZXMgPSAoZXhwZW5zZXMsIGluY29tZXMpID0+IHtcbiAgICAgICAgICAgICRzY29wZS5jaGFydERhdGFJbmNvbWVzRXhwZW5zZXMgPSB7XG4gICAgICAgICAgICAgICAgc2NhbGVMYWJlbDogZnVuY3Rpb24gKHZhbHVlUGF5bG9hZCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlUGF5bG9hZC52YWx1ZSkudG9GaXhlZC5yZXBsYWNlKCcuJywgJywnKSArICckJztcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGxhYmVsczogWydFbmVybycsICdGZWJyZXJvJywgJ01hcnpvJywgJ0FicmlsJywgJ01heW8nLCAnSnVuaW8nLCAnSnVsaW8nLCAnQWdvc3RvJywgJ1NlcHRpZW1icmUnLCAnT2N0dWJyZScsICdOb3ZpZW1icmUnLCAnRGVjaWVtYnJlJ10sXG4gICAgICAgICAgICAgICAgZGF0YXNldHM6IFtcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6ICdJTkdSRVNPUycsXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxsQ29sb3I6ICAncmdiYSgwLDIxNiwyMDQsMC4yKScsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHJva2VDb2xvcjogJ3JnYmEoMCwyMTYsMjA0LDEpJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvaW50Q29sb3I6ICdyZ2JhKDAsMjE2LDIwNCwxKScsXG4gICAgICAgICAgICAgICAgICAgICAgICBwb2ludFN0cm9rZUNvbG9yOiAncmdiYSgwLDIxNiwyMDQsMSknLFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9pbnRIaWdobGlnaHRGaWxsOiAncmdiYSgwLDIxNiwyMDQsMSknLFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9pbnRIaWdobGlnaHRTdHJva2U6ICAnI2ZmZicsXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBpbmNvbWVzXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiAnR0FTVE9TJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGxDb2xvcjogICdyZ2JhKDI1NSwyMywxMTIsMC4yKScsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHJva2VDb2xvcjogJ3JnYmEoMjU1LDIzLDExMiwxKScsXG4gICAgICAgICAgICAgICAgICAgICAgICBwb2ludENvbG9yOiAncmdiYSgyNTUsMjMsMTEyLDEpJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvaW50U3Ryb2tlQ29sb3I6ICdyZ2JhKDI1NSwyMywxMTIsMSknLFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9pbnRIaWdobGlnaHRGaWxsOiAncmdiYSgyNTUsMjMsMTEyLDEpJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvaW50SGlnaGxpZ2h0U3Ryb2tlOiAgJyNmZmYnLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogZXhwZW5zZXNcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIH07XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLiR3YXRjaCgnaW5jb21lc0V4cGVuc2VzWWVhcicsIChuZXdWYWx1ZSwgb2xkVmFsdWUpID0+IHtcbiAgICAgICAgICAgIGlmIChuZXdWYWx1ZSB8fCBvbGRWYWx1ZSkge1xuICAgICAgICAgICAgICAgIHZhciBleHBlbnNlcyA9IG5ldyBBcnJheSgxMik7XG4gICAgICAgICAgICAgICAgZXhwZW5zZXMuZmlsbCgwLCAwLCAxMyk7XG4gICAgICAgICAgICAgICAgdmFyIGluY29tZXMgPSBuZXcgQXJyYXkoMTIpO1xuICAgICAgICAgICAgICAgIGluY29tZXMuZmlsbCgwLCAwLCAxMyk7XG5cbiAgICAgICAgICAgICAgICBkYXNoYm9hcmRTZXJ2aWNlLmdldEV4cGVuc2VzKCRzY29wZS5pbmNvbWVzRXhwZW5zZXNZZWFyKVxuICAgICAgICAgICAgICAgICAgICAudGhlbigoYWxsRXhwZW5zZXMpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsbEV4cGVuc2VzLmZvckVhY2goKGVsZW0sIGluZGV4KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwZW5zZXNbaW5kZXhdID0gZWxlbS5FeHBlbnNlcztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmNvbWVzW2luZGV4XSA9IGVsZW0uSW5jb21lcztcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBjcmVhdGVDaGFydERhdGFJbmNvbWVzRXhwZW5zZXMoZXhwZW5zZXMsIGluY29tZXMpO1xuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBEYXNoYm9hcmRDb250cm9sbGVyOyIsIu+7v3ZhciBNb2R1bGVOYW1lID0gXCJWaWFsU29mdC5kYXNoYm9hcmRcIjtcblxuaW1wb3J0IERhc2hib2FyZENvbnRyb2xsZXIgZnJvbSBcIi4vY29udHJvbGxlcnMvZGFzaGJvYXJkQ29udHJvbGxlclwiO1xuaW1wb3J0IERhc2hib2FyZFNlcnZpY2UgZnJvbSBcIi4vc2VydmljZXMvZGFzaGJvYXJkU2VydmljZVwiO1xuaW1wb3J0IE1oQ2hhcnQgZnJvbSBcIi4vZGlyZWN0aXZlcy9NSENoYXJ0RGlyZWN0aXZlXCI7XG5cbmFuZ3VsYXIubW9kdWxlKE1vZHVsZU5hbWUsIFtdKS5cbiAgICBkaXJlY3RpdmUoXCJjaGFydFwiLCBNaENoYXJ0KS5cbiAgICBjb250cm9sbGVyKFwiZGFzaGJvYXJkQ29udHJvbGxlclwiLCBEYXNoYm9hcmRDb250cm9sbGVyKS5cbiAgICBzZXJ2aWNlKFwiZGFzaGJvYXJkU2VydmljZVwiLCBEYXNoYm9hcmRTZXJ2aWNlKTtcblxuZXhwb3J0IGRlZmF1bHQgTW9kdWxlTmFtZTsiLCLvu79jb25zdCBGSUxURVIgPSBuZXcgV2Vha01hcCgpO1xyXG5cclxuY2xhc3MgTWhDaGFydCB7XHJcbiAgICBjb25zdHJ1Y3RvcigkZmlsdGVyKSB7XHJcbiAgICAgICAgdGhpcy5yZXN0cmljdCA9ICdBJztcclxuICAgICAgICB0aGlzLnNjb3BlID0ge1xyXG4gICAgICAgICAgICAnY2hhcnRkYXRhJyA6ICc9JyxcclxuICAgICAgICAgICAgJ2tpbmQnOiAnQCdcclxuICAgICAgICB9O1xyXG4gICAgICAgIEZJTFRFUi5zZXQodGhpcywgJGZpbHRlcik7XHJcbiAgICB9XHJcblxyXG4gICAgbGluayAoc2NvcGUsIGVsZW1lbnQpIHtcclxuICAgICAgICBjb25zdCBudW1iZXJGaWx0ZXIgPSBGSUxURVIuZ2V0KE1oQ2hhcnQuaW5zdGFuY2UpKCdudW1iZXInKTtcclxuXHJcbiAgICAgICAgdmFyIG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIHNjYWxlU2hvd0dyaWRMaW5lcyA6IHRydWUsXHJcbiAgICAgICAgICAgIHNjYWxlR3JpZExpbmVDb2xvciA6ICdyZ2JhKDAsMCwwLC4wNSknLFxyXG4gICAgICAgICAgICBzY2FsZUdyaWRMaW5lV2lkdGggOiAxLFxyXG4gICAgICAgICAgICBzY2FsZVNob3dIb3Jpem9udGFsTGluZXM6IHRydWUsXHJcbiAgICAgICAgICAgIHNjYWxlU2hvd1ZlcnRpY2FsTGluZXM6IGZhbHNlLFxyXG4gICAgICAgICAgICBzY2FsZUxhYmVsOiBmdW5jdGlvbiAodmFsdWVQYXlsb2FkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVtYmVyRmlsdGVyKHZhbHVlUGF5bG9hZC52YWx1ZSk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGJlemllckN1cnZlIDogdHJ1ZSxcclxuICAgICAgICAgICAgYmV6aWVyQ3VydmVUZW5zaW9uIDogMC40LFxyXG4gICAgICAgICAgICBwb2ludERvdCA6IGZhbHNlLFxyXG4gICAgICAgICAgICBwb2ludERvdFJhZGl1cyA6IDMsXHJcbiAgICAgICAgICAgIHBvaW50RG90U3Ryb2tlV2lkdGggOiAxLFxyXG4gICAgICAgICAgICBwb2ludEhpdERldGVjdGlvblJhZGl1cyA6IDIwLFxyXG4gICAgICAgICAgICBkYXRhc2V0U3Ryb2tlIDogdHJ1ZSxcclxuICAgICAgICAgICAgZGF0YXNldFN0cm9rZVdpZHRoIDogMixcclxuICAgICAgICAgICAgZGF0YXNldEZpbGwgOiB0cnVlLFxyXG4gICAgICAgICAgICBsZWdlbmRUZW1wbGF0ZSA6ICc8dWwgY2xhc3M9XCI8JT1uYW1lLnRvTG93ZXJDYXNlKCklPi1sZWdlbmRcIj48JSBmb3IgKHZhciBpPTA7IGk8ZGF0YXNldHMubGVuZ3RoOyBpKyspeyU+PGxpPjxzcGFuIHN0eWxlPVwiYmFja2dyb3VuZC1jb2xvcjo8JT1kYXRhc2V0c1tpXS5zdHJva2VDb2xvciU+XCI+PC9zcGFuPjwlaWYoZGF0YXNldHNbaV0ubGFiZWwpeyU+PCU9ZGF0YXNldHNbaV0ubGFiZWwlPjwlfSU+PC9saT48JX0lPjwvdWw+JyxcclxuICAgICAgICAgICAgdG9vbHRpcEZvbnRDb2xvcjogJyM3YzdjODEnLFxyXG4gICAgICAgICAgICBtYWludGFpbkFzcGVjdFJhdGlvOiB0cnVlLFxyXG4gICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxyXG4gICAgICAgICAgICBhbmltYXRpb246IHRydWUsXHJcbiAgICAgICAgICAgIGFuaW1hdGlvbkVhc2luZzogJ2Vhc2VPdXRRdWludCcsXHJcbiAgICAgICAgICAgIGN1c3RvbVRvb2x0aXBzOiAgZnVuY3Rpb24gY3VzdG9tVG9vbHRpcHModG9vbHRpcCkge1xyXG5cclxuICAgICAgICAgICAgICAgIHZhciAkdG9vbHRpcCA9ICQoJyNjaGFydC1jdXN0b210b29sdGlwJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCEkdG9vbHRpcFswXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoJ2JvZHknKS5hcHBlbmQoJzxkaXYgaWQ9XCJjaGFydC1jdXN0b210b29sdGlwXCIgY2xhc3M9XCJjaGFydC1jdXN0b210b29sdGlwXCI+PC9kaXY+Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgJHRvb2x0aXAgPSAkKCcjY2hhcnRqcy1jdXN0b210b29sdGlwJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCF0b29sdGlwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHRvb2x0aXAuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAkdG9vbHRpcC5yZW1vdmVDbGFzcygnYWJvdmUgYmVsb3cgbm8tdHJhbnNmb3JtJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAodG9vbHRpcC55QWxpZ24pIHtcclxuICAgICAgICAgICAgICAgICAgICAkdG9vbHRpcC5hZGRDbGFzcyh0b29sdGlwLnlBbGlnbik7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICR0b29sdGlwLmFkZENsYXNzKCduby10cmFuc2Zvcm0nKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodG9vbHRpcC50ZXh0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHRvb2x0aXAuaHRtbCh0b29sdGlwLnRleHQpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgaW5uZXJIdG1sID0gYDxkaXYgY2xhc3M9XCJ0aXRsZVwiPiR7dG9vbHRpcC50aXRsZX08L2Rpdj5gO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdG9vbHRpcC5sYWJlbHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5uZXJIdG1sICs9IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwic2VjdGlvblwiPicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBgICAgPHNwYW4gY2xhc3M9XCJrZXlcIiBzdHlsZT1cImJhY2tncm91bmQtY29sb3I6JHt0b29sdGlwLmxlZ2VuZENvbG9yc1tpXS5maWxsfVwiPjwvc3Bhbj5gLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYCAgIDxzcGFuIGNsYXNzPVwidmFsdWVcIj4kJHtudW1iZXJGaWx0ZXIodG9vbHRpcC5sYWJlbHNbaV0pfTwvc3Bhbj5gLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJzwvZGl2PidcclxuICAgICAgICAgICAgICAgICAgICAgICAgXS5qb2luKCcnKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgJHRvb2x0aXAuaHRtbChpbm5lckh0bWwpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHZhciB0b3AgPSAwO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRvb2x0aXAueUFsaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRvb2x0aXAueUFsaWduID09PSAnYWJvdmUnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcCA9IHRvb2x0aXAueSAtIHRvb2x0aXAuY2FyZXRIZWlnaHQgLSB0b29sdGlwLmNhcmV0UGFkZGluZztcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b3AgPSB0b29sdGlwLnkgKyB0b29sdGlwLmNhcmV0SGVpZ2h0ICsgdG9vbHRpcC5jYXJldFBhZGRpbmc7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHZhciBvZmZzZXQgPSAkKHRvb2x0aXAuY2hhcnQuY2FudmFzKS5vZmZzZXQoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAkdG9vbHRpcC5jc3Moe1xyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IHRvb2x0aXAud2lkdGggPyB0b29sdGlwLndpZHRoICsgJ3B4JyA6ICdhdXRvJyxcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0OiBvZmZzZXQubGVmdCArIHRvb2x0aXAueCArICdweCcsXHJcbiAgICAgICAgICAgICAgICAgICAgdG9wOiBvZmZzZXQudG9wICsgdG9wICsgJ3B4JyxcclxuICAgICAgICAgICAgICAgICAgICBmb250RmFtaWx5OiB0b29sdGlwLmZvbnRGYW1pbHksXHJcbiAgICAgICAgICAgICAgICAgICAgZm9udFNpemU6IHRvb2x0aXAuZm9udFNpemUsXHJcbiAgICAgICAgICAgICAgICAgICAgZm9udFN0eWxlOiB0b29sdGlwLmZvbnRTdHlsZSxcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6ICdyZ2IoMjU1LCAyNTUsIDI1NSknLFxyXG4gICAgICAgICAgICAgICAgICAgIGJveFNoYWRvdzogJzAgMnB4IDZweCAwIHJnYmEoMCwgMCwgMCwgLjgpJ1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB2YXIgY3R4ID0gZWxlbWVudC5nZXQoMCkuZ2V0Q29udGV4dCgnMmQnKTtcclxuXHJcbiAgICAgICAgc2NvcGUuJHdhdGNoKCdjaGFydGRhdGEnLCBmdW5jdGlvbihuZXdWYWx1ZSwgb2xkVmFsdWUpIHtcclxuXHJcbiAgICAgICAgICAgIGlmIChuZXdWYWx1ZSAmJiAhb2xkVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChzY29wZS5raW5kID09PSAnbGluZScpIHtcclxuICAgICAgICAgICAgICAgICAgICBjdHguY2FudmFzLmhlaWdodCA9IDgwO1xyXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmluY29tZUV4cGVuc2VzQ2hhcnQgPSBuZXcgQ2hhcnQoY3R4KS5MaW5lKHNjb3BlLmNoYXJ0ZGF0YSwgb3B0aW9ucyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGxlZ2VuZCA9IHNjb3BlLmluY29tZUV4cGVuc2VzQ2hhcnQuZ2VuZXJhdGVMZWdlbmQoKTtcclxuICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbGVnZW5kSW5jb21lRXhwZW5zZXMnKS5pbm5lckhUTUwgPSBsZWdlbmQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHNjb3BlLmtpbmQgPT09ICdiYXInKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY3R4LmNhbnZhcy5oZWlnaHQgPSA4MDtcclxuICAgICAgICAgICAgICAgICAgICBzY29wZS5wYXRpZW50c0NoYXJ0ID0gbmV3IENoYXJ0KGN0eCkuQmFyKHNjb3BlLmNoYXJ0ZGF0YSwgb3B0aW9ucyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChuZXdWYWx1ZSAmJiBvbGRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHNjb3BlLmtpbmQgPT09ICdsaW5lJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmNoYXJ0ZGF0YS5kYXRhc2V0c1swXS5kYXRhLmZvckVhY2goKGVsZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmluY29tZUV4cGVuc2VzQ2hhcnQuZGF0YXNldHNbMF0ucG9pbnRzW2luZGV4XS52YWx1ZSA9IGVsZW07XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmNoYXJ0ZGF0YS5kYXRhc2V0c1sxXS5kYXRhLmZvckVhY2goKGVsZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmluY29tZUV4cGVuc2VzQ2hhcnQuZGF0YXNldHNbMV0ucG9pbnRzW2luZGV4XS52YWx1ZSA9IGVsZW07XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmluY29tZUV4cGVuc2VzQ2hhcnQudXBkYXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHNjb3BlLmtpbmQgPT09ICdiYXInKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuY2hhcnRkYXRhLmRhdGFzZXRzWzBdLmRhdGEuZm9yRWFjaCgoZWxlbSwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucGF0aWVudHNDaGFydC5kYXRhc2V0c1swXS5iYXJzW2luZGV4XS52YWx1ZSA9IGVsZW07XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUucGF0aWVudHNDaGFydC51cGRhdGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBDaGFydC5kZWZhdWx0cy5nbG9iYWwucmVzcG9uc2l2ZSA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGRpcmVjdGl2ZUZhY3RvcnkoJGZpbHRlcikge1xyXG4gICAgICAgIE1oQ2hhcnQuaW5zdGFuY2UgPSBuZXcgTWhDaGFydCgkZmlsdGVyKTtcclxuICAgICAgICByZXR1cm4gTWhDaGFydC5pbnN0YW5jZTtcclxuICAgIH1cclxufVxyXG5cclxuTWhDaGFydC5kaXJlY3RpdmVGYWN0b3J5LiRpbmplY3QgPSBbJyRmaWx0ZXInXTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1oQ2hhcnQuZGlyZWN0aXZlRmFjdG9yeTsiLCLvu79mdW5jdGlvbiBEYXNoYm9hcmRTZXJ2aWNlKCRodHRwKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBnZXRTdW1tYXJ5LFxuICAgICAgICBnZXRFeHBlbnNlc1xuICAgIH07XG5cbiAgICBmdW5jdGlvbiBnZXRTdW1tYXJ5KCkge1xuICAgICAgICBsZXQgaGFuZGxlU3VjY2VzcyA9IChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdmFyIHN1bW1hcnkgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgcmV0dXJuIHN1bW1hcnk7XG4gICAgICAgIH07XG5cbiAgICAgICAgdmFyIHRlbmFudElkID0gJyc7XG5cbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgICAgICAgICB1cmw6ICcvYXBpL3VzZXJzL2N1cnJlbnQvdGVuYW50J1xuICAgICAgICB9KS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB2YXIgdXJsID0gJy9hcGkvcmVwb3J0cy9hc3NldHN1bW1hcnknO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSkudGhlbihoYW5kbGVTdWNjZXNzKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0RXhwZW5zZXMoeWVhcikge1xuICAgICAgICBsZXQgaGFuZGxlU3VjY2VzcyA9IChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdmFyIGV4cGVuc2VzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHJldHVybiBleHBlbnNlcztcbiAgICAgICAgfTtcblxuICAgICAgICB2YXIgdGVuYW50SWQgPSAnJztcblxuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgICAgICAgIHVybDogJy9hcGkvdXNlcnMvY3VycmVudC90ZW5hbnQnXG4gICAgICAgIH0pLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSAnL2FwaS9yZXBvcnRzL2V4cGVuc2VzLycgKyB5ZWFyO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSkudGhlbihoYW5kbGVTdWNjZXNzKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IERhc2hib2FyZFNlcnZpY2U7Iiwi77u/Y2xhc3MgRGV0YWlsQ29udHJvbGxlciB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGVQYXJhbXMsICRzdGF0ZSwgbWFpbnRlbmFuY2VjYXJkc1NlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlLCBtb2RhbFNlcnZpY2UpIHtcblxuICAgICAgICAkKFwic2VsZWN0XCIpLnNlbGVjdDIoeyB3aWR0aDogXCIxMDAlXCIgfSk7XG5cbiAgICAgICAgdmFyIG1haW50ZW5hbmNlY2FyZHNJZCA9ICRzdGF0ZVBhcmFtcy5pZDtcbiAgICAgICAgdmFyIHRlbmFudElkO1xuICAgICAgICAkc2NvcGUuZWRpdE1vZGUgPSBmYWxzZTtcbiAgICAgICAgJHNjb3BlLnNob3dEZXRhaWwgPSBmYWxzZTtcblxuICAgICAgICAkc2NvcGUubWFpbnRlbmFuY2VjYXJkcyA9IHtcbiAgICAgICAgICAgIFRlbmFudElkOiAwLFxuICAgICAgICAgICAgQXNzZXRJZDogbWFpbnRlbmFuY2VjYXJkc0lkLFxuICAgICAgICAgICAgTWFpbnRlbmFuY2VDYXJkRGV0YWlsOiBbXVxuICAgICAgICB9O1xuXG4gICAgICAgIG1haW50ZW5hbmNlY2FyZHNTZXJ2aWNlLmdldFBlcmlvZCgpXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAkc2NvcGUucGVyaW9kcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICBtYWludGVuYW5jZWNhcmRzU2VydmljZS5nZXRKb2IoKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmpvYnMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgJHNjb3BlLmdldEpvYnMgPSAoKSA9PiB7XG4gICAgICAgICAgICAkc2NvcGUuam9icyA9IG51bGw7XG4gICAgICAgICAgICBtYWludGVuYW5jZWNhcmRzU2VydmljZS5nZXRKb2JzKCRzY29wZS5tYWludGVuYW5jZWNhcmQuUGVyaW9kSWQpXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5qb2JzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgJChcIiNqb2JcIikuc2VsZWN0Mih7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogXCIxMDAlXCJcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICAkc2NvcGUuYWRkTWFpbnRlbmFuY2VDYXJkRGV0YWlsID0gKCkgPT4ge1xuICAgICAgICAgICAgdmFyIGRldGFpbCA9IHtcbiAgICAgICAgICAgICAgICBQZXJpb2RJZCA6JHNjb3BlLm1haW50ZW5hbmNlY2FyZC5QZXJpb2RJZCxcbiAgICAgICAgICAgICAgICBKb2JJZDogJHNjb3BlLm1haW50ZW5hbmNlY2FyZC5Kb2JJZCxcbiAgICAgICAgICAgICAgICBSZXBsYWNlbWVudDogJHNjb3BlLm1haW50ZW5hbmNlY2FyZC5SZXBsYWNlbWVudCAgICAgICAgICAgICBcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICAkc2NvcGUubWFpbnRlbmFuY2VjYXJkcy5NYWludGVuYW5jZUNhcmREZXRhaWwucHVzaChkZXRhaWwpO1xuICAgICAgICAgICAgJHNjb3BlLnNob3dEZXRhaWwgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCRzY29wZS5lZGl0TW9kZSkge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIG1haW50ZW5hbmNlY2FyZHNTZXJ2aWNlLmdldG1haW50ZW5hbmNlY2FyZHMobWFpbnRlbmFuY2VjYXJkc0lkKVxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubWFpbnRlbmFuY2VjYXJkcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5zaG93RGV0YWlsID0gdHJ1ZTtcblxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgICRzY29wZS5yZW1vdmVBc3NldCA9IChhc3NldEl0ZW0pID0+IHtcbiAgICAgICAgICAgIGxldCBpbmRleCA9ICRzY29wZS5tYWludGVuYW5jZWNhcmRzLkRldGFpbHMuaW5kZXhPZihhc3NldEl0ZW0pO1xuICAgICAgICAgICAgJHNjb3BlLm1haW50ZW5hbmNlY2FyZHMuRGV0YWlscy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjayA9ICgpID0+IHtcbiAgICAgICAgICAgICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJtYWludGVuYW5jZWNhcmRzXCIsIHtpZCA6IG1haW50ZW5hbmNlY2FyZHNJZH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5zYXZlID0gKCkgPT4ge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIG1haW50ZW5hbmNlY2FyZHNTZXJ2aWNlLmdldFRlbmFudCgpXG4gICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5tYWludGVuYW5jZWNhcmRzLlRlbmFudElkID0gdGVuYW50SWQ7XG5cbiAgICAgICAgICAgICAgICAgICAgbWFpbnRlbmFuY2VjYXJkc1NlcnZpY2UuYWRkKCRzY29wZS5tYWludGVuYW5jZWNhcmRzKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlQWRkKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlQWRkLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5uYXZpZ2F0ZUJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICB9O1xuXG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBEZXRhaWxDb250cm9sbGVyOyIsIu+7v2NsYXNzIE1haW50ZW5hbmNlQ2FyZHNDb250cm9sbGVyIHtcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICRyb290U2NvcGUsICRzdGF0ZVBhcmFtcywgJHN0YXRlLCBtYWludGVuYW5jZWNhcmRzU2VydmljZSwgdG9hc3RlclNlcnZpY2UsIG1vZGFsU2VydmljZSkge1xuXG4gICAgICAgIHZhciBhc3NldElkID0gJHN0YXRlUGFyYW1zLmlkO1xuICAgICAgICBjb25zdCBwYWdlU2l6ZSA9IDY7XG4gICAgICAgIHZhciBwYWdlQ291bnQgPSAwO1xuICAgICAgICAkc2NvcGUubmV3TWFpbnRlbmFuY2VDYXJkID0gdHJ1ZTtcbiAgICAgICAgJHNjb3BlLkFzc2V0SWQgPSBhc3NldElkO1xuICAgICAgICAkc2NvcGUubWFpbnRlbmFuY2VjYXJkcyA9IFtdO1xuXG4gICAgICAgICRzY29wZS5nZXRMaXN0ID0gKCkgPT4ge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIG1haW50ZW5hbmNlY2FyZHNTZXJ2aWNlLmdldExpc3QocGFnZVNpemUsIHBhZ2VDb3VudCwgYXNzZXRJZClcbiAgICAgICAgICAgICAgICAudGhlbigobWFpbnRlbmFuY2VDYXJkcykgPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChtYWludGVuYW5jZUNhcmRzLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5ld01haW50ZW5hbmNlQ2FyZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKG1haW50ZW5hbmNlQ2FyZHMubGVuZ3RoIDwgcGFnZVNpemUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5ub01vcmVEYXRhID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBtYWludGVuYW5jZUNhcmRzLmZvckVhY2goKG1haW50ZW5hbmNlQ2FyZCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm1haW50ZW5hbmNlY2FyZHMucHVzaChtYWludGVuYW5jZUNhcmQpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcGFnZUNvdW50ICsrOyAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEkc2NvcGUubWFpbnRlbmFuY2VjYXJkcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5ub0RhdGEgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5uYWdpdmF0ZVRvRGV0YWlsID0gKCkgPT4ge1xuICAgICAgICAgICAgJHN0YXRlLnRyYW5zaXRpb25UbyhcIm1haW50ZW5hbmNlY2FyZFwiLCB7IGlkOiAkc2NvcGUuQXNzZXRJZCB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVmcmVzaFNlbGVjdGVkSXRlbXMgPSAoYWxsKSA9PiB7XG4gICAgICAgICAgICBpZiAoYWxsKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm1haW50ZW5hbmNlY2FyZHMuZm9yRWFjaCgobWFpbnRlbmFuY2VDYXJkKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIG1haW50ZW5hbmNlQ2FyZC5zZWxlY3RlZCA9ICRzY29wZS5ldmVyeVNlbGVjdGVkO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuYW55U2VsZWN0ZWQgPSAkc2NvcGUubWFpbnRlbmFuY2VjYXJkcy5zb21lKChtYWludGVuYW5jZUNhcmQpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbWFpbnRlbmFuY2VDYXJkLnNlbGVjdGVkO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICRzY29wZS5ldmVyeVNlbGVjdGVkID0gJHNjb3BlLm1haW50ZW5hbmNlY2FyZHMuZXZlcnkoKG1haW50ZW5hbmNlQ2FyZCkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBtYWludGVuYW5jZUNhcmQuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrQXNzZXQgPSAoKSA9PiB7XG4gICAgICAgICAgICAkc3RhdGUudHJhbnNpdGlvblRvKFwiYXNzZXRcIiwge2lkIDogJHNjb3BlLkFzc2V0SWR9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVtb3ZlID0gKG1haW50ZW5hbmNlQ2FyZCkgPT4ge1xuICAgICAgICAgICAgbW9kYWxTZXJ2aWNlLnNob3dDb25maXJtTW9kYWwoe1xuICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiB7XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBgRWxpbWluYXIgaXRlbWAsXG4gICAgICAgICAgICAgICAgICAgIGJvZHk6IGBFc3RhIHNlZ3VybyBxdWUgZGVzZWEgZWxpbWluYXIgZWwgaXRlbWAsXG4gICAgICAgICAgICAgICAgICAgIG9rOiBcIlNpLCBlbGltaW5hclwiLFxuICAgICAgICAgICAgICAgICAgICBjYW5jZWw6IFwiQ2FuY2VsYXJcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbigoKSA9PiB7ICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIG1haW50ZW5hbmNlY2FyZHNTZXJ2aWNlLnJlbW92ZShtYWludGVuYW5jZUNhcmQpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm1haW50ZW5hbmNlY2FyZHMuZm9yRWFjaCgobWFpbnRlbmFuY2VDYXJkSXRlbSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1haW50ZW5hbmNlQ2FyZCA9PT0gbWFpbnRlbmFuY2VDYXJkSXRlbS5NYWludGVuYW5jZUNhcmREZXRhaWxJZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBpbmRleCA9ICRzY29wZS5tYWludGVuYW5jZWNhcmRzLmluZGV4T2YobWFpbnRlbmFuY2VDYXJkSXRlbSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm1haW50ZW5hbmNlY2FyZHMuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7XG5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5nZXRMaXN0KCk7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBNYWludGVuYW5jZUNhcmRzQ29udHJvbGxlcjsiLCLvu792YXIgTW9kdWxlTmFtZSA9IFwiVmlhbFNvZnQubWFpbnRlbmFuY2VjYXJkc1wiO1xyXG5cclxuaW1wb3J0IE1haW50ZW5hbmNlY2FyZHNDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL21haW50ZW5hbmNlY2FyZHNDb250cm9sbGVyXCI7XHJcbmltcG9ydCBEZXRhaWxDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL2RldGFpbENvbnRyb2xsZXJcIjtcclxuaW1wb3J0IE1haW50ZW5hbmNlY2FyZHNTZXJ2aWNlIGZyb20gXCIuL3NlcnZpY2VzL21haW50ZW5hbmNlY2FyZHNTZXJ2aWNlXCI7XHJcblxyXG5hbmd1bGFyLm1vZHVsZShNb2R1bGVOYW1lLCBbXSkuXHJcbiAgICBjb250cm9sbGVyKFwibWFpbnRlbmFuY2VjYXJkc0NvbnRyb2xsZXJcIiwgTWFpbnRlbmFuY2VjYXJkc0NvbnRyb2xsZXIpLlxyXG4gICAgY29udHJvbGxlcihcIm1haW50ZW5hbmNlY2FyZHNkZXRhaWxDb250cm9sbGVyXCIsIERldGFpbENvbnRyb2xsZXIpLlxyXG4gICAgc2VydmljZShcIm1haW50ZW5hbmNlY2FyZHNTZXJ2aWNlXCIsIE1haW50ZW5hbmNlY2FyZHNTZXJ2aWNlKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1vZHVsZU5hbWU7Iiwi77u/ZnVuY3Rpb24gTWFpbnRlbmFuY2VDYXJkc1NlcnZpY2UoJGh0dHApIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuICAgIHZhciBtYWludGVuYW5jZWNhcmRzO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgZ2V0Sm9iLFxuICAgICAgICBnZXRKb2JzLFxuICAgICAgICBnZXRQZXJpb2QsXG4gICAgICAgIGdldFRlbmFudCxcbiAgICAgICAgZ2V0TWFpbnRlbmFuY2VDYXJkcyxcbiAgICAgICAgZ2V0TGlzdCxcbiAgICAgICAgYWRkLFxuICAgICAgICByZW1vdmVcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gZ2V0Sm9iKCl7XG4gICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICB1cmw6IFwiL2FwaS9Kb2JzXCJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0Sm9icyhwZXJpb2RJZCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBgL2FwaS9Kb2JzLyR7cGVyaW9kSWR9YFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRQZXJpb2QoKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIGxldCB1cmwgPSBgL2FwaS9QZXJpb2RzYDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG5cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRUZW5hbnQoKSB7XG4gICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICB1cmw6IFwiL2FwaS91c2Vycy9jdXJyZW50L3RlbmFudFwiXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldExpc3QocGFnZVNpemUsIHBhZ2VDb3VudCwgYXNzZXRJZCkge1xuICAgICAgICBsZXQgaGFuZGxlU3VjY2VzcyA9IChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgbWFpbnRlbmFuY2VjYXJkcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICByZXR1cm4gbWFpbnRlbmFuY2VjYXJkcztcbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB2YXIgdXJsID0gYC9hcGkvbWFpbnRlbmFuY2VjYXJkc2A7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgcGFnZVNpemU6IHBhZ2VTaXplLFxuICAgICAgICAgICAgICAgICAgICBwYWdlQ291bnQ6IHBhZ2VDb3VudCxcbiAgICAgICAgICAgICAgICAgICAgYXNzZXRJZDogYXNzZXRJZFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS50aGVuKGhhbmRsZVN1Y2Nlc3MpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRNYWludGVuYW5jZUNhcmRzKG1haW50ZW5hbmNlY2FyZHNJZCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvbWFpbnRlbmFuY2VjYXJkcy8ke21haW50ZW5hbmNlY2FyZHNJZH1gO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gYWRkKG1haW50ZW5hbmNlY2FyZHMpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvYXBpL21haW50ZW5hbmNlY2FyZHMvXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgZGF0YTogbWFpbnRlbmFuY2VjYXJkcyxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZCAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJlbW92ZShtYW50ZW5hbmNlQ2FyZElkKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL21haW50ZW5hbmNlY2FyZHMvJHttYW50ZW5hbmNlQ2FyZElkfWA7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJERUxFVEVcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgXG59XG5cbmV4cG9ydCBkZWZhdWx0IE1haW50ZW5hbmNlQ2FyZHNTZXJ2aWNlOyIsIu+7v1widXNlIHN0cmljdFwiO1xyXG5cclxuY2xhc3MgRGV0YWlsQ29udHJvbGxlciB7XHJcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICRyb290U2NvcGUsICRzdGF0ZVBhcmFtcywgJHN0YXRlLCBwcm92aWRlcnNTZXJ2aWNlLCB0b2FzdGVyU2VydmljZSwgbW9kYWxTZXJ2aWNlKSB7XHJcblxyXG4gICAgICAgICQoXCJzZWxlY3RcIikuc2VsZWN0Mih7d2lkdGg6IFwiMTAwJVwifSk7XHJcblxyXG4gICAgICAgIHZhciBwcm92aWRlcklkID0gJHN0YXRlUGFyYW1zLmlkO1xyXG4gICAgICAgICRzY29wZS5lZGl0TW9kZSA9IHByb3ZpZGVySWQgIT09IHVuZGVmaW5lZDtcclxuICAgICAgICB2YXIgdGVuYW50SWQ7XHJcblxyXG4gICAgICAgICRzY29wZS5wcm92aWRlciA9IHtcclxuICAgICAgICAgICAgQ3JlYXRlZEF0OiBuZXcgRGF0ZSgpXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcHJvdmlkZXJzU2VydmljZS5nZXRUeXBlUmVjZWlwdCgpXHJcbiAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgJHNjb3BlLnR5cGVSZWNlaXB0cyA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAoJHNjb3BlLmVkaXRNb2RlKSB7XHJcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgICAgIHByb3ZpZGVyc1NlcnZpY2UuZ2V0UHJvdmlkZXIocHJvdmlkZXJJZClcclxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5wcm92aWRlciA9IHJlc3BvbnNlLmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGVyc1NlcnZpY2UuZ2V0VHlwZVJlY2VpcHQoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2VUeXBlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUudHlwZVJlY2VpcHRzID0gcmVzcG9uc2VUeXBlLmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiN0eXBlUmVjZWlwdFwiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoJHNjb3BlLnByb3ZpZGVyLlBpY3R1cmUgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUucHJvdmlkZXIuUGljdHVyZSA9IGBkYXRhOmltYWdlL3BuZztiYXNlNjQsJHskc2NvcGUucHJvdmlkZXIuUGljdHVyZX1gO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICBwcm92aWRlcnNTZXJ2aWNlLmdldFRlbmFudCgpXHJcbiAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICRzY29wZS5uYXZpZ2F0ZUJhY2sgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJwcm92aWRlcnNcIik7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgJHNjb3BlLnJlbW92ZVByb3ZpZGVyID0gKCkgPT4ge1xyXG4gICAgICAgICAgICBtb2RhbFNlcnZpY2Uuc2hvd0NvbmZpcm1Nb2RhbCh7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlczoge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIkVsaW1pbmFyIFByb3ZlZWRvclwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGJvZHk6IFwiRXN0YSBzZWd1cm8gcXVlIGRlc2VhIGVsaW1pbmFyIGVsIHByb3ZlZWRvcj9cIixcclxuICAgICAgICAgICAgICAgICAgICBvazogXCJTaSwgZWxpbWluYXJcIixcclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWw6IFwiQ2FuY2VsYXJcIlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgcHJvdmlkZXJzU2VydmljZS5yZW1vdmUocHJvdmlkZXJJZClcclxuICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAkc3RhdGUudHJhbnNpdGlvblRvKFwicHJvdmlkZXJzXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICB9KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAkc2NvcGUuc2F2ZSA9ICgpID0+IHtcclxuXHJcbiAgICAgICAgICAgIGlmICgkc2NvcGUucHJvdmlkZXIuUGljdHVyZSAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAkc2NvcGUucHJvdmlkZXIuUGljdHVyZSA9ICRzY29wZS5wcm92aWRlci5QaWN0dXJlLnNwbGl0KFwiLFwiKVsxXTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKCEkc2NvcGUuZWRpdE1vZGUpIHtcclxuICAgICAgICAgICAgICAgICRzY29wZS5wcm92aWRlci5UZW5hbnRJZCA9IHRlbmFudElkO1xyXG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICBwcm92aWRlcnNTZXJ2aWNlLmFkZCgkc2NvcGUucHJvdmlkZXIpXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjaygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHByb3ZpZGVyc1NlcnZpY2UudXBkYXRlKCRzY29wZS5wcm92aWRlcilcclxuICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IERldGFpbENvbnRyb2xsZXI7Iiwi77u/Y2xhc3MgUHJvdmlkZXJzQ29udHJvbGxlciB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGUsIHByb3ZpZGVyc1NlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlLCBtb2RhbFNlcnZpY2UpIHtcblxuICAgICAgICBjb25zdCBwYWdlU2l6ZSA9IDY7XG4gICAgICAgIHZhciBwYWdlQ291bnQgPSAwO1xuXG4gICAgICAgICRzY29wZS5wcm92aWRlcnMgPSBbXTtcblxuICAgICAgICAkc2NvcGUuZ2V0TGlzdCA9ICgpID0+IHtcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICBwcm92aWRlcnNTZXJ2aWNlLmdldExpc3QocGFnZVNpemUsIHBhZ2VDb3VudClcbiAgICAgICAgICAgICAgICAudGhlbigocHJvdmlkZXJzKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwcm92aWRlcnMubGVuZ3RoIDwgcGFnZVNpemUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5ub01vcmVEYXRhID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBwcm92aWRlcnMuZm9yRWFjaCgocHJvdmlkZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5wcm92aWRlcnMucHVzaChwcm92aWRlcik7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBwYWdlQ291bnQgKys7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5yZWZyZXNoU2VsZWN0ZWRJdGVtcygpO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmICghJHNjb3BlLnByb3ZpZGVycy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5ub0RhdGEgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5uYWdpdmF0ZVRvRGV0YWlsID0gKHByb3ZpZGVySWQpID0+IHtcbiAgICAgICAgICAgIHByb3ZpZGVySWQgPyAkc3RhdGUudHJhbnNpdGlvblRvKFwicHJvdmlkZXJcIiwgeyBpZDogcHJvdmlkZXJJZCB9KSA6ICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJwcm92aWRlclwiKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVmcmVzaFNlbGVjdGVkSXRlbXMgPSAoYWxsKSA9PiB7XG4gICAgICAgICAgICBpZiAoYWxsKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnByb3ZpZGVycy5mb3JFYWNoKChwcm92aWRlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBwcm92aWRlci5zZWxlY3RlZCA9ICRzY29wZS5ldmVyeVNlbGVjdGVkO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuYW55U2VsZWN0ZWQgPSAkc2NvcGUucHJvdmlkZXJzLnNvbWUoKHByb3ZpZGVyKSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHByb3ZpZGVyLnNlbGVjdGVkO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICRzY29wZS5ldmVyeVNlbGVjdGVkID0gJHNjb3BlLnByb3ZpZGVycy5ldmVyeSgocHJvdmlkZXIpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcHJvdmlkZXIuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVtb3ZlID0gKHByb3ZpZGVyKSA9PiB7XG5cbiAgICAgICAgICAgIG1vZGFsU2VydmljZS5zaG93Q29uZmlybU1vZGFsKHtcbiAgICAgICAgICAgICAgICBtZXNzYWdlczoge1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogYEVsaW1pbmFyIHByb3ZlZWRvcmAsXG4gICAgICAgICAgICAgICAgICAgIGJvZHk6IGBFc3RhIHNlZ3VybyBxdWUgZGVzZWEgZWxpbWluYXIgYWwgcHJvdmVlZG9yYCxcbiAgICAgICAgICAgICAgICAgICAgb2s6IFwiU2ksIGVsaW1pbmFyXCIsXG4gICAgICAgICAgICAgICAgICAgIGNhbmNlbDogXCJDYW5jZWxhclwiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICB2YXIgcHJvdmlkZXJJZExpc3Q7XG4gICAgICAgICAgICAgICAgaWYgKHByb3ZpZGVyKSB7XG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGVySWRMaXN0ID0gW3Byb3ZpZGVyLlByb3ZpZGVySWRdO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGVySWRMaXN0ID0gJHNjb3BlLnByb3ZpZGVyc1xuICAgICAgICAgICAgICAgICAgICAgICAgLm1hcCgocHJvdmlkZXJJdGVtKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHByb3ZpZGVySXRlbS5zZWxlY3RlZCkgeyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBwcm92aWRlckl0ZW0uUHJvdmlkZXJJZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgcHJvdmlkZXJJZExpc3QuZm9yRWFjaCgocHJvdmlkZXJJZCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBwcm92aWRlcnNTZXJ2aWNlLnJlbW92ZShwcm92aWRlcklkKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5wcm92aWRlcnMuZm9yRWFjaCgocHJvdmlkZXJJdGVtKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocHJvdmlkZXJJZCA9PT0gcHJvdmlkZXJJdGVtLlByb3ZpZGVySWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgaW5kZXggPSAkc2NvcGUucHJvdmlkZXJzLmluZGV4T2YocHJvdmlkZXJJdGVtKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUucHJvdmlkZXJzLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7XG5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5nZXRMaXN0KCk7XG4gICAgfVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IFByb3ZpZGVyc0NvbnRyb2xsZXI7Iiwi77u/dmFyIE1vZHVsZU5hbWUgPSBcIlZpYWxTb2Z0LnByb3ZpZGVyc1wiO1xyXG5cclxuaW1wb3J0IFByb3ZpZGVyc0NvbnRyb2xsZXIgZnJvbSBcIi4vY29udHJvbGxlcnMvcHJvdmlkZXJzQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgRGV0YWlsQ29udHJvbGxlciBmcm9tIFwiLi9jb250cm9sbGVycy9kZXRhaWxDb250cm9sbGVyXCI7XHJcbmltcG9ydCBQcm92aWRlcnNTZXJ2aWNlIGZyb20gXCIuL3NlcnZpY2VzL3Byb3ZpZGVyc1NlcnZpY2VcIjtcclxuXHJcbmFuZ3VsYXIubW9kdWxlKE1vZHVsZU5hbWUsIFtdKS5cclxuICAgIGNvbnRyb2xsZXIoXCJwcm92aWRlcnNDb250cm9sbGVyXCIsIFByb3ZpZGVyc0NvbnRyb2xsZXIpLlxyXG4gICAgY29udHJvbGxlcihcInByb3ZpZGVyc2RldGFpbENvbnRyb2xsZXJcIiwgRGV0YWlsQ29udHJvbGxlcikuXHJcbiAgICBzZXJ2aWNlKFwicHJvdmlkZXJzU2VydmljZVwiLCBQcm92aWRlcnNTZXJ2aWNlKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1vZHVsZU5hbWU7Iiwi77u/ZnVuY3Rpb24gUHJvdmlkZXJzU2VydmljZSgkaHR0cCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgdmFyIHByb3ZpZGVycztcblxuICAgIHJldHVybiB7XG4gICAgICAgIGdldFR5cGVSZWNlaXB0LFxuICAgICAgICBnZXRUZW5hbnQsXG4gICAgICAgIGdldFByb3ZpZGVyLFxuICAgICAgICBnZXRMaXN0LFxuICAgICAgICBhZGQsXG4gICAgICAgIHVwZGF0ZSxcbiAgICAgICAgcmVtb3ZlXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIGdldFR5cGVSZWNlaXB0KCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBcIi9hcGkvcHVyY2hhc2VzL1R5cGVSZWNlaXB0c1wiXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFRlbmFudCgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogXCIvYXBpL3VzZXJzL2N1cnJlbnQvdGVuYW50XCJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0TGlzdChwYWdlU2l6ZSwgcGFnZUNvdW50KSB7XG4gICAgICAgIGxldCBoYW5kbGVTdWNjZXNzID0gKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBwcm92aWRlcnMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgcmV0dXJuIHByb3ZpZGVycztcbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvcHJvdmlkZXJzXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgcGFnZVNpemU6IHBhZ2VTaXplLFxuICAgICAgICAgICAgICAgICAgICBwYWdlQ291bnQ6cGFnZUNvdW50XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnRoZW4oaGFuZGxlU3VjY2Vzcyk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFByb3ZpZGVyKHByb3ZpZGVySWQpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL3Byb3ZpZGVycy8ke3Byb3ZpZGVySWR9YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFkZChwcm92aWRlcikge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvcHJvdmlkZXJzL1wiO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGRhdGE6IHByb3ZpZGVyLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlKHByb3ZpZGVyKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaS9wcm92aWRlcnMvXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQVVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBkYXRhOiBwcm92aWRlcixcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiByZW1vdmUocHJvdmlkZXJJZCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIGxldCB1cmwgPSBgL2FwaS9wcm92aWRlcnMvJHtwcm92aWRlcklkfWA7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJERUxFVEVcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFByb3ZpZGVyc1NlcnZpY2U7Iiwi77u/Y2xhc3MgRGV0YWlsQ29udHJvbGxlciB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGVQYXJhbXMsICRzdGF0ZSwgcHVyY2hhc2VzU2VydmljZSwgdG9hc3RlclNlcnZpY2UsIG1vZGFsU2VydmljZSwgbW9kYWxBc3NldFNlcnZpY2UpIHtcblxuICAgICAgICAkKFwic2VsZWN0XCIpLnNlbGVjdDIoeyB3aWR0aDogXCIxMDAlXCIgfSk7XG5cbiAgICAgICAgdmFyIHB1cmNoYXNlSWQgPSAkc3RhdGVQYXJhbXMuaWQ7XG4gICAgICAgICRzY29wZS5lZGl0TW9kZSA9IHB1cmNoYXNlSWQgIT09IHVuZGVmaW5lZDtcbiAgICAgICAgJHNjb3BlLnNlYXJjaE1vZGUgPSBmYWxzZTtcbiAgICAgICAgJHNjb3BlLnNob3dEZXRhaWwgPSBmYWxzZTtcbiAgICAgICAgJHNjb3BlLnByaWNlVG90YWwgPSAwO1xuICAgICAgICB2YXIgdGVuYW50SWQ7XG5cbiAgICAgICAgJHNjb3BlLmFzc2V0ID0ge307XG5cbiAgICAgICAgJHNjb3BlLnB1cmNoYXNlRGV0YWlsID0ge1xuICAgICAgICAgICAgQXNzZXRJZDogbnVsbCxcbiAgICAgICAgICAgIFByaWNlOiBudWxsXG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnB1cmNoYXNlID0ge1xuICAgICAgICAgICAgQ3JlYXRlZEF0OiBuZXcgRGF0ZSgpLFxuICAgICAgICAgICAgVGVuYW50SWQ6IDAsXG4gICAgICAgICAgICBEZXRhaWxzOiBbXVxuICAgICAgICB9O1xuXG4gICAgICAgIHB1cmNoYXNlc1NlcnZpY2UuZ2V0Q2F0ZWdvcnkoKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmNhdGVnb3JpZXMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgcHVyY2hhc2VzU2VydmljZS5nZXRTdWJDYXRlZ29yeUFsbCgpXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuc3ViY2F0ZWdvcmllcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAkc2NvcGUuZ2V0U3ViQ2F0ZWdvcnkgPSAoKSA9PiB7XG4gICAgICAgICAgICAkc2NvcGUuc3ViY2F0ZWdvcmllcyA9IG51bGw7XG4gICAgICAgICAgICBwdXJjaGFzZXNTZXJ2aWNlLmdldFN1YkNhdGVnb3J5KCRzY29wZS5hc3NldC5DYXRlZ29yeUlkKVxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc3ViY2F0ZWdvcmllcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICQoXCIjc3ViY2F0ZWdvcmllc1wiKS5zZWxlY3QyKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIlxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHB1cmNoYXNlc1NlcnZpY2UuZ2V0UHJvdmlkZXIoKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnByb3ZpZGVycyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAkc2NvcGUuc2VhcmNoID0gKCkgPT4ge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIHB1cmNoYXNlc1NlcnZpY2UuZ2V0QXNzZXRCeUlkKCRzY29wZS5hc3NldC5JZClcbiAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnNlYXJjaE1vZGUgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYXNzZXQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgIH0pLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgICRzY29wZS5hZGRQdXJjaGFjZURldGFpbCA9ICgpID0+IHtcbiAgICAgICAgICAgIHZhciBkZXRhaWwgPSB7XG4gICAgICAgICAgICAgICAgUHJpY2U6ICRzY29wZS5wdXJjaGFzZS5QcmljZSxcbiAgICAgICAgICAgICAgICBBc3NldElkOiAkc2NvcGUuYXNzZXQuQXNzZXRJZFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICRzY29wZS5wcmljZVRvdGFsICs9ICRzY29wZS5wdXJjaGFzZS5QcmljZTtcbiAgICAgICAgICAgICRzY29wZS5wdXJjaGFzZS5EZXRhaWxzLnB1c2goZGV0YWlsKTtcbiAgICAgICAgICAgICRzY29wZS5zaG93RGV0YWlsID0gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICgkc2NvcGUuZWRpdE1vZGUpIHtcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICBwdXJjaGFzZXNTZXJ2aWNlLmdldFB1cmNoYXNlKHB1cmNoYXNlSWQpXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5wdXJjaGFzZSA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5zaG93RGV0YWlsID0gdHJ1ZTtcblxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUucHVyY2hhc2UuRGV0YWlscy5mb3JFYWNoKChkZXRhaWwpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5wcmljZVRvdGFsICs9IGRldGFpbC5QcmljZTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHB1cmNoYXNlc1NlcnZpY2UuZ2V0UHJvdmlkZXIoKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlUHJvdmlkZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUucHJvdmlkZXJzID0gcmVzcG9uc2VQcm92aWRlci5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjcHJvdmlkZXJcIikuc2VsZWN0Mih7IHdpZHRoOiBcIjEwMCVcIiB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIHB1cmNoYXNlc1NlcnZpY2UuZ2V0VHlwZVJlY2VpcHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlVHlwZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS50eXBlUmVjZWlwdHMgPSByZXNwb25zZVR5cGUuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI3R5cGVSZWNlaXB0XCIpLnNlbGVjdDIoeyB3aWR0aDogXCIxMDAlXCIgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgICRzY29wZS5yZW1vdmVBc3NldCA9IChhc3NldEl0ZW0pID0+IHtcbiAgICAgICAgICAgIGxldCBpbmRleCA9ICRzY29wZS5wdXJjaGFzZS5EZXRhaWxzLmluZGV4T2YoYXNzZXRJdGVtKTtcbiAgICAgICAgICAgICRzY29wZS5wcmljZVRvdGFsIC09IGFzc2V0SXRlbS5QcmljZTtcbiAgICAgICAgICAgICRzY29wZS5wdXJjaGFzZS5EZXRhaWxzLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgIH1cblxuICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrID0gKCkgPT4ge1xuICAgICAgICAgICAgJHN0YXRlLnRyYW5zaXRpb25UbyhcInB1cmNoYXNlc1wiKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUubmFnaXZhdGVUb0Fzc2V0ID0gKCkgPT4ge1xuICAgICAgICAgICAgbW9kYWxBc3NldFNlcnZpY2Uuc2hvd0NvbmZpcm1Nb2RhbCgpXG4gICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcblxuICAgICAgICAgICAgICAgICAgICBwdXJjaGFzZXNTZXJ2aWNlLmdldEFzc2V0KHJlc3BvbnNlKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlQXNzZXQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2VhcmNoTW9kZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFzc2V0ID0gcmVzcG9uc2VBc3NldC5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuc2F2ZSA9ICgpID0+IHtcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICBwdXJjaGFzZXNTZXJ2aWNlLmdldFRlbmFudCgpXG4gICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5wdXJjaGFzZS5UZW5hbnRJZCA9IHRlbmFudElkO1xuICAgICAgICAgICAgICAgICAgICBwdXJjaGFzZXNTZXJ2aWNlLmFkZCgkc2NvcGUucHVyY2hhc2UpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2VBZGQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2VBZGQuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjaygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIH07XG5cbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IERldGFpbENvbnRyb2xsZXI7Iiwi77u/Y2xhc3MgUHVyY2hhc2VzQ29udHJvbGxlciB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGUsIHB1cmNoYXNlc1NlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlLCBtb2RhbFNlcnZpY2UpIHtcblxuICAgICAgICBjb25zdCBwYWdlU2l6ZSA9IDY7XG4gICAgICAgIHZhciBwYWdlQ291bnQgPSAwO1xuXG4gICAgICAgICRzY29wZS5wdXJjaGFzZXMgPSBbXTtcblxuICAgICAgICAkc2NvcGUuZ2V0TGlzdCA9ICgpID0+IHtcblxuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgcHVyY2hhc2VzU2VydmljZS5nZXRMaXN0KHBhZ2VTaXplLCBwYWdlQ291bnQpXG4gICAgICAgICAgICAgICAgLnRoZW4oKHB1cmNoYXNlcykgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAocHVyY2hhc2VzLmxlbmd0aCA8IHBhZ2VTaXplKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubm9Nb3JlRGF0YSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcHVyY2hhc2VzLmZvckVhY2goKHB1cmNoYXNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUucHVyY2hhc2VzLnB1c2gocHVyY2hhc2UpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcGFnZUNvdW50ICsrOyAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEkc2NvcGUucHVyY2hhc2VzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5vRGF0YSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLm5hZ2l2YXRlVG9EZXRhaWwgPSAocHVyY2hhc2VJZCkgPT4ge1xuICAgICAgICAgICAgcHVyY2hhc2VJZCA/ICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJwdXJjaGFzZVwiLCB7IGlkOiBwdXJjaGFzZUlkIH0pIDogJHN0YXRlLnRyYW5zaXRpb25UbygncHVyY2hhc2UnKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUubmFnaXZhdGVUb0Fzc2V0ID0gKCkgPT4ge1xuICAgICAgICAgICAgJHN0YXRlLnRyYW5zaXRpb25UbyhcImFzc2V0XCIpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZWZyZXNoU2VsZWN0ZWRJdGVtcyA9IChhbGwpID0+IHtcbiAgICAgICAgICAgIGlmIChhbGwpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUucHVyY2hhc2VzLmZvckVhY2goKHB1cmNoYXNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHB1cmNoYXNlLnNlbGVjdGVkID0gJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQ7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5hbnlTZWxlY3RlZCA9ICRzY29wZS5wdXJjaGFzZXMuc29tZSgocHVyY2hhc2UpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcHVyY2hhc2Uuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQgPSAkc2NvcGUucHVyY2hhc2VzLmV2ZXJ5KChwdXJjaGFzZSkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBwdXJjaGFzZS5zZWxlY3RlZDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5nZXRMaXN0KCk7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBQdXJjaGFzZXNDb250cm9sbGVyOyIsIu+7v3ZhciBNb2R1bGVOYW1lID0gXCJWaWFsU29mdC5wdXJjaGFzZXNcIjtcclxuXHJcbmltcG9ydCBQdXJjaGFzZXNDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL3B1cmNoYXNlc0NvbnRyb2xsZXJcIjtcclxuaW1wb3J0IERldGFpbENvbnRyb2xsZXIgZnJvbSBcIi4vY29udHJvbGxlcnMvZGV0YWlsQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgUHVyY2hhc2VzU2VydmljZSBmcm9tIFwiLi9zZXJ2aWNlcy9wdXJjaGFzZXNTZXJ2aWNlXCI7XHJcblxyXG5hbmd1bGFyLm1vZHVsZShNb2R1bGVOYW1lLCBbXSkuXHJcbiAgICBjb250cm9sbGVyKFwicHVyY2hhc2VzQ29udHJvbGxlclwiLCBQdXJjaGFzZXNDb250cm9sbGVyKS5cclxuICAgIGNvbnRyb2xsZXIoXCJwdXJjaGFzZXNkZXRhaWxDb250cm9sbGVyXCIsIERldGFpbENvbnRyb2xsZXIpLlxyXG4gICAgc2VydmljZShcInB1cmNoYXNlc1NlcnZpY2VcIiwgUHVyY2hhc2VzU2VydmljZSk7XHJcbmV4cG9ydCBkZWZhdWx0IE1vZHVsZU5hbWU7Iiwi77u/ZnVuY3Rpb24gUHVyY2hhc2VzU2VydmljZSgkaHR0cCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgdmFyIHB1cmNoYXNlcztcblxuICAgIHJldHVybiB7XG4gICAgICAgIGdldENhdGVnb3J5LFxuICAgICAgICBnZXRTdWJDYXRlZ29yeSxcbiAgICAgICAgZ2V0U3ViQ2F0ZWdvcnlBbGwsXG4gICAgICAgIGdldEFzc2V0QnlJZCxcbiAgICAgICAgZ2V0QXNzZXQsXG4gICAgICAgIGdldFByb3ZpZGVyLFxuICAgICAgICBnZXRUeXBlUmVjZWlwdCxcbiAgICAgICAgZ2V0Q3VzdG9tZXIsXG4gICAgICAgIGdldFRlbmFudCxcbiAgICAgICAgZ2V0UHVyY2hhc2UsXG4gICAgICAgIGdldExpc3QsXG4gICAgICAgIGFkZCxcbiAgICAgICAgdXBkYXRlXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIGdldEN1c3RvbWVyKCl7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIGxldCB1cmwgPSBgL2FwaS9jdXN0b21lcnMvZ2V0QWxsYDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldENhdGVnb3J5KCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBcIi9hcGkvQ2F0ZWdvcmllc1wiXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFN1YkNhdGVnb3J5QWxsKCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBgL2FwaS9TdWJDYXRlZ29yaWVzYFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRTdWJDYXRlZ29yeShjYXRlZ29yeUlkKSB7XG4gICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICB1cmw6IGAvYXBpL1N1YkNhdGVnb3JpZXMvJHtjYXRlZ29yeUlkfWBcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0VHlwZVJlY2VpcHQoKSB7XG4gICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICB1cmw6IFwiL2FwaS9wdXJjaGFzZXMvVHlwZVJlY2VpcHRzXCJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0UHJvdmlkZXIoKXtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL3Byb3ZpZGVycy9nZXRBbGxgO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIGZ1bmN0aW9uIGdldEFzc2V0KGFzc2V0SWQpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL2Fzc2V0cy8ke2Fzc2V0SWR9YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldEFzc2V0QnlJZChpZCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvYXNzZXRzL0dldEFzc2V0YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIHBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICBJZDogaWRcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFRlbmFudCgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogXCIvYXBpL3VzZXJzL2N1cnJlbnQvdGVuYW50XCJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0TGlzdChwYWdlU2l6ZSwgcGFnZUNvdW50KSB7XG4gICAgICAgIGxldCBoYW5kbGVTdWNjZXNzID0gKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBwdXJjaGFzZXMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgcmV0dXJuIHB1cmNoYXNlcztcbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvcHVyY2hhc2VzXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgcGFnZVNpemU6IHBhZ2VTaXplLFxuICAgICAgICAgICAgICAgICAgICBwYWdlQ291bnQ6cGFnZUNvdW50XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnRoZW4oaGFuZGxlU3VjY2Vzcyk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFB1cmNoYXNlKHB1cmNoYXNlSWQpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL3B1cmNoYXNlcy8ke3B1cmNoYXNlSWR9YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFkZChwdXJjaGFzZSkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvcHVyY2hhc2VzL1wiO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGRhdGE6IHB1cmNoYXNlLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlKHB1cmNoYXNlKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaS9wdXJjaGFzZXMvXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQVVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBkYXRhOiBwdXJjaGFzZSxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgXG59XG5cbmV4cG9ydCBkZWZhdWx0IFB1cmNoYXNlc1NlcnZpY2U7Iiwi77u/Y29uc3Qgc3RhdGUgPSBuZXcgV2Vha01hcCgpO1xuXG5jbGFzcyBIZWFkZXJDb250cm9sbGVyIHtcbiAgICBjb25zdHJ1Y3Rvcigkc3RhdGUsICRyb290U2NvcGUsICRodHRwLCAkdGltZW91dCkge1xuICAgICAgICB2YXIgdm0gPSB0aGlzO1xuICAgICAgICBzdGF0ZS5zZXQodGhpcywgJHN0YXRlKTtcblxuICAgICAgICAkdGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnRpdGxlID0gJHN0YXRlLmN1cnJlbnQubmFtZTtcbiAgICAgICAgICAgIHZtLnZpZXdOYW1lID0gJHN0YXRlLmN1cnJlbnQubmFtZTtcbiAgICAgICAgfSwgMTApO1xuXG4gICAgICAgICRyb290U2NvcGUuJG9uKFwiJHN0YXRlQ2hhbmdlU3RhcnRcIixcbiAgICAgICAgICAgIChlLCB0b1N0YXRlKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy50aXRsZSA9IHRvU3RhdGUubmFtZTtcbiAgICAgICAgICAgICAgICB2bS52aWV3TmFtZSA9IHRvU3RhdGUubmFtZTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZiAodG9TdGF0ZS5uYW1lID09PSBcImN1c3RvbWVyc1wiIHx8IHRvU3RhdGUubmFtZSA9PT0gXCJjdXN0b21lclwiKVxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRpdGxlID0gXCJDbGllbnRlc1wiO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRvU3RhdGUubmFtZSA9PT0gXCJ3b3Jrc1wiIHx8IHRvU3RhdGUubmFtZSA9PT0gXCJ3b3JrXCIpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGl0bGUgPSBcIk9icmFzXCI7XG5cbiAgICAgICAgICAgICAgICBpZiAodG9TdGF0ZS5uYW1lID09PSBcImFzc2V0c1wiIHx8IHRvU3RhdGUubmFtZSA9PT0gXCJhc3NldFwiKVxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRpdGxlID0gXCJCaWVuZXNcIjtcblxuICAgICAgICAgICAgICAgIGlmICh0b1N0YXRlLm5hbWUgPT09IFwiYWNjZXNzb3JpZXNcIiB8fCB0b1N0YXRlLm5hbWUgPT09IFwiYWNjZXNzb3J5XCIpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGl0bGUgPSBcIkFjY2Vzb3Jpb3NcIjtcblxuICAgICAgICAgICAgICAgIGlmICh0b1N0YXRlLm5hbWUgPT09IFwidGVuYW50c1wiIHx8IHRvU3RhdGUubmFtZSA9PT0gXCJ0ZW5hbnRcIilcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50aXRsZSA9IFwiUHJvcGlldGFyaW9zXCI7XG5cbiAgICAgICAgICAgICAgICBpZiAodG9TdGF0ZS5uYW1lID09PSBcInB1cmNoYXNlc1wiIHx8IHRvU3RhdGUubmFtZSA9PT0gXCJwdXJjaGFzZVwiKVxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRpdGxlID0gXCJDb21wcmFzXCI7XG5cbiAgICAgICAgICAgICAgICBpZiAodG9TdGF0ZS5uYW1lID09PSBcInRyYW5zZmVyc3dvcmtzXCIgfHwgdG9TdGF0ZS5uYW1lID09PSBcInRyYW5zZmVyd29ya1wiKVxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRpdGxlID0gXCJUcmFzbGFkb3NcIjtcblxuICAgICAgICAgICAgICAgIGlmICh0b1N0YXRlLm5hbWUgPT09IFwicHJvdmlkZXJzXCIgfHwgdG9TdGF0ZS5uYW1lID09PSBcInByb3ZpZGVyXCIpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGl0bGUgPSBcIlByb3ZlZWRvcmVzXCI7XG5cbiAgICAgICAgICAgICAgICBpZiAodG9TdGF0ZS5uYW1lID09PSBcImNvc3RlYXNzZXRzXCIgfHwgdG9TdGF0ZS5uYW1lID09PSBcImNvc3RlYXNzZXRcIilcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50aXRsZSA9IFwiQ29zdG9zXCI7XG5cbiAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLm1lbnVPcGVuID0gZmFsc2U7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAkaHR0cCh7XG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICB1cmw6IFwiL2FwaS91c2Vycy9jdXJyZW50L3VzZXJcIlxuICAgICAgICB9KS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdm0udXNlck5hbWUgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICB9KTtcblxuICAgIH1cbn1cblxuSGVhZGVyQ29udHJvbGxlci4kaW5qZWN0ID0gW1wiJHN0YXRlXCIsIFwiJHJvb3RTY29wZVwiLCBcIiRodHRwXCIsIFwiJHRpbWVvdXRcIl07XG5leHBvcnQgZGVmYXVsdCBIZWFkZXJDb250cm9sbGVyOyIsIu+7v2NsYXNzIEhlYWRlckJhciB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMucmVzdHJpY3QgPSAnRSc7XG4gICAgICAgIHRoaXMudGVtcGxhdGVVcmwgPSAnL2FwcC9jb21wb25lbnRzL3NoYXJlZC9kaXJlY3RpdmVzL2hlYWRlckJhci9oZWFkZXJCYXJUZW1wbGF0ZS5odG1sJztcbiAgICAgICAgdGhpcy5jb250cm9sbGVyID0gJ2hlYWRlckNvbnRyb2xsZXInO1xuICAgICAgICB0aGlzLmNvbnRyb2xsZXJBcyA9ICd2bSc7XG4gICAgfVxuXG4gICAgbGluayhzY29wZSkge1xuICAgICAgICAkKGRvY3VtZW50KS5iaW5kKCdjbGljaycsIChldmVudCkgPT4ge1xuICAgICAgICAgICAgaWYgKCFzY29wZS5tZW51T3Blbikge1xuICAgICAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBzY29wZS4kYXBwbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBzY29wZS5tZW51T3BlbiA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAkKCcuaGVhZGVyLWhhbWJ1cmd1ZXIsICNzaWRlYmFyLWNvbnRhaW5lcicpLmJpbmQoJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3RhdGljIGRpcmVjdGl2ZUZhY3RvcnkoKSB7XG4gICAgICAgIEhlYWRlckJhci5pbnN0YW5jZSA9IG5ldyBIZWFkZXJCYXIoKTtcbiAgICAgICAgcmV0dXJuIEhlYWRlckJhci5pbnN0YW5jZTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEhlYWRlckJhci5kaXJlY3RpdmVGYWN0b3J5OyIsIu+7v2NsYXNzIExlZnRNZW51IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5yZXN0cmljdCA9ICdFJztcbiAgICAgICAgdGhpcy50ZW1wbGF0ZVVybCA9ICcvYXBwL2NvbXBvbmVudHMvc2hhcmVkL2RpcmVjdGl2ZXMvbGVmdE1lbnUvbGVmdE1lbnVUZW1wbGF0ZS5odG1sJztcbiAgICAgICAgdGhpcy5jb250cm9sbGVyID0gJ2hlYWRlckNvbnRyb2xsZXInO1xuICAgICAgICB0aGlzLmNvbnRyb2xsZXJBcyA9ICd2bSc7XG4gICAgfVxuXG4gICAgc3RhdGljIGRpcmVjdGl2ZUZhY3RvcnkoKSB7XG4gICAgICAgIExlZnRNZW51Lmluc3RhbmNlID0gbmV3IExlZnRNZW51KCk7XG4gICAgICAgIHJldHVybiBMZWZ0TWVudS5pbnN0YW5jZTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IExlZnRNZW51LmRpcmVjdGl2ZUZhY3Rvcnk7Iiwi77u/ZnVuY3Rpb24gQ2FtZWxDYXNlRmlsdGVyKCkge1xuICAgICd1c2Ugc3RyaWN0JztcbiAgICByZXR1cm4gZnVuY3Rpb24oaW5wdXQpIHtcbiAgICAgICAgaWYgKCFpbnB1dCkge1xuICAgICAgICAgICAgcmV0dXJuIGlucHV0O1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGxpc3QgPSBpbnB1dC5tYXRjaCgvW0EtWmEtel1bYS16XSovZyk7XG5cbiAgICAgICAgaWYgKCFsaXN0KSB7XG4gICAgICAgICAgICByZXR1cm4gaW5wdXQ7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IHJlc3VsdCA9IGxpc3Quam9pbignICcpO1xuICAgICAgICByZXN1bHQgPSByZXN1bHQuc3Vic3RyKDAsIDEpLnRvVXBwZXJDYXNlKCkgKyByZXN1bHQuc3Vic3RyKDEpO1xuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IENhbWVsQ2FzZUZpbHRlcjsiLCLvu79mdW5jdGlvbiBFeGNlcHRpb25IYW5kbGVyKCRpbmplY3Rvcikge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgdmFyIGhhbmRsZWRFeGNlcHRpb25zID0gW107XG5cbiAgICByZXR1cm4gZnVuY3Rpb24oZXhjZXB0aW9uKSB7XG4gICAgICAgIGlmIChoYW5kbGVkRXhjZXB0aW9ucy5pbmRleE9mKGV4Y2VwdGlvbikgPT09IC0xKSB7XG4gICAgICAgICAgICAvL2FwcEluc2lnaHRzLnRyYWNrRXhjZXB0aW9uKGV4Y2VwdGlvbik7XG4gICAgICAgICAgICAkaW5qZWN0b3IuZ2V0KFwidG9hc3RlclNlcnZpY2VcIikuc2hvd1NlcnZlckVycm9yKCk7XG4gICAgICAgICAgICBoYW5kbGVkRXhjZXB0aW9ucy5wdXNoKGV4Y2VwdGlvbik7XG4gICAgICAgICAgICBjb25zb2xlLndhcm4oXCJVbmhhbmRsZWQgRXhjZXB0aW9uXCIsIGV4Y2VwdGlvbik7XG4gICAgICAgIH1cbiAgICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBFeGNlcHRpb25IYW5kbGVyOyIsIu+7v2Z1bmN0aW9uIE1vZGFsQXNzZXRTZXJ2aWNlKCRtb2RhbCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgc2hvd0NvbmZpcm1Nb2RhbFxuICAgIH07XG4gICBcblxuICAgIGZ1bmN0aW9uIHNob3dDb25maXJtTW9kYWwoKSB7XG5cbiAgICAgICAgcmV0dXJuICRtb2RhbC5vcGVuKHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBcIi9hcHAvY29tcG9uZW50cy9zaGFyZWQvdmlld3MvY29uZmlybUFzc2V0TW9kYWwuaHRtbFwiLFxuICAgICAgICAgICAgY29udHJvbGxlcjogW1wiJHNjb3BlXCIsIFwiJG1vZGFsSW5zdGFuY2VcIiwgXCJwdXJjaGFzZXNTZXJ2aWNlXCIsIFwiYXNzZXRzU2VydmljZVwiLFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uICgkc2NvcGUsICRtb2RhbEluc3RhbmNlLCBwdXJjaGFzZXNTZXJ2aWNlLCBhc3NldHNTZXJ2aWNlKSB7XG5cbiAgICAgICAgICAgICAgICAgJHNjb3BlLkFzc2V0SWQgPSAwO1xuXG4gICAgICAgICAgICAgICAgdmFyIHRlbmFudElkO1xuICAgICAgICAgICAgICAgICRzY29wZS5lZGl0TW9kZSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICRzY29wZS5vd25Bc3NldCA9IHRydWU7XG5cbiAgICAgICAgICAgICAgICAkc2NvcGUuYXNzZXQgPSB7XG4gICAgICAgICAgICAgICAgICAgIENyZWF0ZWRBdDogbmV3IERhdGUoKSxcbiAgICAgICAgICAgICAgICAgICAgU3RhdHVzQXNzZXQ6IDFcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgYXNzZXRzU2VydmljZS5lbnVtU3RhdHVzKClcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3B0aW9ucyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgcHVyY2hhc2VzU2VydmljZS5nZXRDYXRlZ29yeSgpXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmNhdGVnb3JpZXMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjYXRlZ29yaWVzXCIpLnNlbGVjdDIoeyB3aWR0aDogXCIxMDAlXCIgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgYXNzZXRzU2VydmljZS5nZXRTdWJDYXRlZ29yeSgkc2NvcGUuYXNzZXQuQ2F0ZWdvcnlJZClcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlc3ViQ2F0ZWdvcnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5zdWJjYXRlZ29yaWVzID0gcmVzcG9uc2VzdWJDYXRlZ29yeS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNzdWJjYXRlZ29yaWVzXCIpLnNlbGVjdDIoeyB3aWR0aDogXCIxMDAlXCIgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgJHNjb3BlLmdldFN1YkNhdGVnb3J5ID0gKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc3ViY2F0ZWdvcmllcyA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0U3ViQ2F0ZWdvcnkoJHNjb3BlLmFzc2V0LkNhdGVnb3J5SWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc3ViY2F0ZWdvcmllcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNzdWJjYXRlZ29yaWVzXCIpLnNlbGVjdDIoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogXCIxMDAlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICAgICAgYXNzZXRzU2VydmljZS5nZXRXb3JrKClcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUud29ya3MgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiN3b3Jrc1wiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UuZW51bVN0YXR1cygpXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZVN0YXR1cykgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm9wdGlvbnMgPSByZXNwb25zZVN0YXR1cy5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNwcm9wZXJ0eVwiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0VGVuYW50KClcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAkc2NvcGUuY2hhbmdlUHJvcGVydHkgPSAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICgkc2NvcGUuYXNzZXQuU3RhdHVzQXNzZXQgPT09IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5vd25Bc3NldCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3duQXNzZXQgPSBmYWxzZTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjdXN0b21lcnNcIikuc2VsZWN0Mih7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IFwiMTAwJVwiXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGFzc2V0c1NlcnZpY2UuZ2V0Q3VzdG9tZXIoKVxuICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5jdXN0b21lcnMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFzc2V0LkN1c3RvbWVySWQgPSAxO1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICRzY29wZS5zYXZlID0gKCkgPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgIGlmICgkc2NvcGUuYXNzZXQuUGljdHVyZSAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYXNzZXQuUGljdHVyZSA9ICRzY29wZS5hc3NldC5QaWN0dXJlLnNwbGl0KFwiLFwiKVsxXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYXNzZXQuVGVuYW50SWQgPSB0ZW5hbnRJZDtcbiAgICAgICAgICAgICAgICAgICAgYXNzZXRzU2VydmljZS5hZGQoJHNjb3BlLmFzc2V0KVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRtb2RhbEluc3RhbmNlLmNsb3NlKHJlc3BvbnNlLmRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuY2FuY2VsID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkbW9kYWxJbnN0YW5jZS5kaXNtaXNzKFwiY2FuY2VsXCIpO1xuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH1dLFxuICAgICAgICAgICAgc2l6ZTogXCJsZ1wiXG4gICAgICAgIH0pLnJlc3VsdC50aGVuKGZ1bmN0aW9uKHJlc3VsdCkge1xuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBNb2RhbEFzc2V0U2VydmljZTsiLCLvu79mdW5jdGlvbiBNb2RhbFNlcnZpY2UoJG1vZGFsKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBzaG93Q29uZmlybU1vZGFsXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIHNob3dDb25maXJtTW9kYWwob3B0cykge1xuICAgICAgICByZXR1cm4gJG1vZGFsLm9wZW4oe1xuXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCIvYXBwL2NvbXBvbmVudHMvc2hhcmVkL3ZpZXdzL2NvbmZpcm1Nb2RhbC5odG1sXCIsXG5cbiAgICAgICAgICAgIGNvbnRyb2xsZXI6IFtcIiRzY29wZVwiLCBcIiRtb2RhbEluc3RhbmNlXCIsIGZ1bmN0aW9uICgkc2NvcGUsICRtb2RhbEluc3RhbmNlKSB7XG5cbiAgICAgICAgICAgICAgICAkc2NvcGUubWVzc2FnZXMgPSBvcHRzLm1lc3NhZ2VzO1xuXG4gICAgICAgICAgICAgICAgJHNjb3BlLm9rID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAkbW9kYWxJbnN0YW5jZS5jbG9zZSgpO1xuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICAgICAkc2NvcGUuY2FuY2VsID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAkbW9kYWxJbnN0YW5jZS5kaXNtaXNzKFwiY2FuY2VsXCIpO1xuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XVxuXG4gICAgICAgIH0pLnJlc3VsdDtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IE1vZGFsU2VydmljZTsiLCLvu79mdW5jdGlvbiBUb2FzdGVyU2VydmljZSh0b2FzdGVyKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBzaG93U2VydmVyRXJyb3JcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gc2hvd1NlcnZlckVycm9yKCkge1xuICAgICAgICB0b2FzdGVyLnBvcChcImVycm9yXCIsIFwiRXJyb3JcIiwgXCJPb3BzISBPY3VycmlvIHVuIHByb2JsZW1hIVwiKTtcbiAgICB9XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgVG9hc3RlclNlcnZpY2U7Iiwi77u/dmFyIE1vZHVsZU5hbWUgPSBcIlZpYWxTb2Z0LnNoYXJlZFwiO1xuXG5pbXBvcnQgTGVmdE1lbnVEaXJlY3RpdmUgZnJvbSBcIi4vZGlyZWN0aXZlcy9sZWZ0TWVudS9sZWZ0TWVudURpcmVjdGl2ZVwiO1xuaW1wb3J0IEhlYWRlckJhckRpcmVjdGl2ZSBmcm9tIFwiLi9kaXJlY3RpdmVzL2hlYWRlckJhci9oZWFkZXJCYXJEaXJlY3RpdmVcIjtcbmltcG9ydCBIZWFkZXJDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL2hlYWRlckNvbnRyb2xsZXJcIjtcbmltcG9ydCBUb2FzdGVyU2VydmljZSBmcm9tIFwiLi9zZXJ2aWNlcy90b2FzdGVyU2VydmljZVwiO1xuaW1wb3J0IE1vZGFsU2VydmljZSBmcm9tIFwiLi9zZXJ2aWNlcy9tb2RhbFNlcnZpY2VcIjtcbmltcG9ydCBNb2RhbEFzc2V0U2VydmljZSBmcm9tIFwiLi9zZXJ2aWNlcy9tb2RhbEFzc2V0U2VydmljZVwiO1xuaW1wb3J0IEV4Y2VwdGlvbkhhbmRsZXIgZnJvbSBcIi4vc2VydmljZXMvZXhjZXB0aW9uSGFuZGxlclwiO1xuaW1wb3J0IENhbWVsQ2FzZUZpbHRlciBmcm9tIFwiLi9maWx0ZXJzL2NhbWVsQ2FzZUZpbHRlclwiO1xuXG5cbmFuZ3VsYXIubW9kdWxlKE1vZHVsZU5hbWUsIFtcInVpLmJvb3RzdHJhcFwiLCBcInRvYXN0ZXJcIl0pLlxuICAgIGRpcmVjdGl2ZShcImxlZnRNZW51XCIsIExlZnRNZW51RGlyZWN0aXZlKS5cbiAgICBkaXJlY3RpdmUoXCJoZWFkZXJCYXJcIiwgSGVhZGVyQmFyRGlyZWN0aXZlKS5cbiAgICBjb250cm9sbGVyKFwiaGVhZGVyQ29udHJvbGxlclwiLCBIZWFkZXJDb250cm9sbGVyKS5cbiAgICBzZXJ2aWNlKFwidG9hc3RlclNlcnZpY2VcIiwgVG9hc3RlclNlcnZpY2UpLlxuICAgIHNlcnZpY2UoXCJtb2RhbFNlcnZpY2VcIiwgTW9kYWxTZXJ2aWNlKS5cbiAgICBzZXJ2aWNlKFwibW9kYWxBc3NldFNlcnZpY2VcIiwgTW9kYWxBc3NldFNlcnZpY2UpLlxuICAgIGZhY3RvcnkoXCIkZXhjZXB0aW9uSGFuZGxlclwiLCBFeGNlcHRpb25IYW5kbGVyKS5cbiAgICBmaWx0ZXIoXCJjYW1lbENhc2VcIiwgQ2FtZWxDYXNlRmlsdGVyKTtcblxuZXhwb3J0IGRlZmF1bHQgTW9kdWxlTmFtZTsiLCLvu79jbGFzcyBEZXRhaWxDb250cm9sbGVyIHtcclxuICAgIGNvbnN0cnVjdG9yKCRzY29wZSwgJHJvb3RTY29wZSwgJHN0YXRlUGFyYW1zLCAkc3RhdGUsIHRlbmFudHNTZXJ2aWNlLCB0b2FzdGVyU2VydmljZSwgbW9kYWxTZXJ2aWNlKSB7XHJcblxyXG4gICAgICAgIHZhciB0ZW5hbnRJZCA9ICRzdGF0ZVBhcmFtcy5pZDtcclxuICAgICAgICAkc2NvcGUuZWRpdE1vZGUgPSB0ZW5hbnRJZCAhPT0gdW5kZWZpbmVkO1xyXG5cclxuICAgICAgICBpZiAoJHNjb3BlLmVkaXRNb2RlKSB7XHJcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgICAgIHRlbmFudHNTZXJ2aWNlLmdldFRlbmFudCh0ZW5hbnRJZClcclxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS50ZW5hbnQgPSByZXNwb25zZS5kYXRhO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjayA9ICgpID0+IHtcclxuICAgICAgICAgICAgJHN0YXRlLnRyYW5zaXRpb25UbygndGVuYW50cycpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgICRzY29wZS5yZW1vdmV0ZW5hbnQgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIG1vZGFsU2VydmljZS5zaG93Q29uZmlybU1vZGFsKHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwiRWxpbWluYXIgUHJvcGlldGFyaW9cIixcclxuICAgICAgICAgICAgICAgICAgICBib2R5OiBcIkVzdGEgc2VndXJvIHF1ZSBkZXNlYSBlbGltaW5hciBlbCBwcm9waWV0YXJpbz9cIixcclxuICAgICAgICAgICAgICAgICAgICBvazogXCJTaSwgZWxpbWluYXJcIixcclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWw6IFwiQ2FuY2VsYXJcIlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgdGVuYW50c1NlcnZpY2UucmVtb3ZlKHRlbmFudElkKVxyXG4gICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICRzdGF0ZS50cmFuc2l0aW9uVG8oJ3RlbmFudHMnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgJHNjb3BlLnNhdmUgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghJHNjb3BlLmVkaXRNb2RlKSB7XHJcbiAgICAgICAgICAgICAgICAkc2NvcGUudGVuYW50LlRlbmFudElkID0gdGVuYW50SWQ7XHJcbiAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgICAgIHRlbmFudHNTZXJ2aWNlLmFkZCgkc2NvcGUudGVuYW50KVxyXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5uYXZpZ2F0ZUJhY2soKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0ZW5hbnRzU2VydmljZS51cGRhdGUoJHNjb3BlLnRlbmFudClcclxuICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBEZXRhaWxDb250cm9sbGVyOyIsIu+7v2NsYXNzIFRlbmFudHNDb250cm9sbGVyIHtcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICRyb290U2NvcGUsICRzdGF0ZSwgdGVuYW50c1NlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlLCBtb2RhbFNlcnZpY2UpIHtcblxuICAgICAgICBjb25zdCBwYWdlU2l6ZSA9IDY7XG4gICAgICAgIHZhciBwYWdlQ291bnQgPSAwO1xuXG4gICAgICAgICRzY29wZS50ZW5hbnRzID0gW107XG5cbiAgICAgICAgJHNjb3BlLmdldExpc3QgPSAoKSA9PiB7XG4gICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgICAgICAgdGVuYW50c1NlcnZpY2UuZ2V0TGlzdChwYWdlU2l6ZSwgcGFnZUNvdW50KVxuICAgICAgICAgICAgICAgIC50aGVuKCh0ZW5hbnRzKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0ZW5hbnRzLmxlbmd0aCA8IHBhZ2VTaXplKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubm9Nb3JlRGF0YSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdGVuYW50cy5mb3JFYWNoKCh0ZW5hbnQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS50ZW5hbnRzLnB1c2godGVuYW50KTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHBhZ2VDb3VudCArKztcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKCEkc2NvcGUudGVuYW50cy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5ub0RhdGEgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5uYWdpdmF0ZVRvRGV0YWlsID0gKHRlbmFudElkKSA9PiB7XG4gICAgICAgICAgICB0ZW5hbnRJZCA/ICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJ0ZW5hbnRcIiwgeyBpZDogdGVuYW50SWQgfSkgOiAkc3RhdGUudHJhbnNpdGlvblRvKFwidGVuYW50XCIpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZWZyZXNoU2VsZWN0ZWRJdGVtcyA9IChhbGwpID0+IHtcbiAgICAgICAgICAgIGlmIChhbGwpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUudGVuYW50cy5mb3JFYWNoKCh0ZW5hbnQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGVuYW50LnNlbGVjdGVkID0gJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQ7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5hbnlTZWxlY3RlZCA9ICRzY29wZS50ZW5hbnRzLnNvbWUoKHRlbmFudCkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiB0ZW5hbnQuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQgPSAkc2NvcGUudGVuYW50cy5ldmVyeSgodGVuYW50KSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRlbmFudC5zZWxlY3RlZDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZW1vdmUgPSAodGVuYW50KSA9PiB7XG5cbiAgICAgICAgICAgIG1vZGFsU2VydmljZS5zaG93Q29uZmlybU1vZGFsKHtcbiAgICAgICAgICAgICAgICBtZXNzYWdlczoge1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogYEVsaW1pbmFyIFByb3BpZXRhcmlvYCxcbiAgICAgICAgICAgICAgICAgICAgYm9keTogYEVzdGEgc2VndXJvIHF1ZSBkZXNlYSBlbGltaW5hciBlbCBwcm9waWV0YXJpb2AsXG4gICAgICAgICAgICAgICAgICAgIG9rOiBcIlNpLCBlbGltaW5hclwiLFxuICAgICAgICAgICAgICAgICAgICBjYW5jZWw6IFwiQ2FuY2VsYXJcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgdmFyIHRlbmFudElkTGlzdDtcbiAgICAgICAgICAgICAgICBpZiAodGVuYW50KSB7XG4gICAgICAgICAgICAgICAgICAgIHRlbmFudElkTGlzdCA9IFt0ZW5hbnQuVGVuYW50SWRdO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRlbmFudElkTGlzdCA9ICRzY29wZS50ZW5hbnRzXG4gICAgICAgICAgICAgICAgICAgICAgICAubWFwKCh0ZW5hbnRJdGVtKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRlbmFudEl0ZW0uc2VsZWN0ZWQpIHsgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGVuYW50SXRlbS5UZW5hbnRJZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGVuYW50SWRMaXN0LmZvckVhY2goKHRlbmFudElkKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRlbmFudHNTZXJ2aWNlLnJlbW92ZSh0ZW5hbnRJZClcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUudGVuYW50cy5mb3JFYWNoKCh0ZW5hbnRJdGVtKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGVuYW50SWQgPT09IHRlbmFudEl0ZW0uVGVuYW50SWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgaW5kZXggPSAkc2NvcGUudGVuYW50cy5pbmRleE9mKHRlbmFudEl0ZW0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS50ZW5hbnRzLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7XG5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5nZXRMaXN0KCk7XG4gICAgfVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IFRlbmFudHNDb250cm9sbGVyOyIsIu+7v2Z1bmN0aW9uIFRlbmFudHNTZXJ2aWNlKCRodHRwKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICB2YXIgdGVuYW50cztcblxuICAgIHJldHVybiB7XG4gICAgICAgIGdldFRlbmFudCxcbiAgICAgICAgZ2V0TGlzdCxcbiAgICAgICAgYWRkLFxuICAgICAgICB1cGRhdGUsXG4gICAgICAgIHJlbW92ZVxuICAgIH07XG5cbiAgICBmdW5jdGlvbiBnZXRMaXN0KHBhZ2VTaXplLCBwYWdlQ291bnQpIHtcbiAgICAgICAgbGV0IGhhbmRsZVN1Y2Nlc3MgPSAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHRlbmFudHMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgcmV0dXJuIHRlbmFudHM7XG4gICAgICAgIH07XG4gICAgICAgIHZhciB1cmwgPSAnL2FwaS90ZW5hbnRzJztcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgIHBhcmFtczoge1xuICAgICAgICAgICAgICAgIHBhZ2VTaXplOiBwYWdlU2l6ZSxcbiAgICAgICAgICAgICAgICBwYWdlQ291bnQ6cGFnZUNvdW50XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pLnRoZW4oaGFuZGxlU3VjY2Vzcyk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0VGVuYW50KHRlbmFudElkKSB7XG4gICAgICAgIGxldCB1cmwgPSBgL2FwaS90ZW5hbnRzLyR7dGVuYW50SWR9YDtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogdXJsXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFkZCh0ZW5hbnQpIHtcbiAgICAgICAgdmFyIHVybCA9ICcvYXBpL3RlbmFudHMvJztcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICBkYXRhOiB0ZW5hbnRcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlKHRlbmFudCkge1xuICAgICAgICB2YXIgdXJsID0gXCIvYXBpL3RlbmFudHMvXCI7XG4gICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICBtZXRob2Q6IFwiUFVUXCIsXG4gICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgIGRhdGE6IHRlbmFudFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiByZW1vdmUodGVuYW50SWQpIHtcbiAgICAgICAgbGV0IHVybCA9IGAvYXBpL3RlbmFudHMvJHt0ZW5hbnRJZH1gO1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkRFTEVURVwiLFxuICAgICAgICAgICAgdXJsOiB1cmxcbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBUZW5hbnRzU2VydmljZTsiLCLvu792YXIgTW9kdWxlTmFtZSA9IFwiVmlhbFNvZnQudGVuYW50c1wiO1xyXG5cclxuaW1wb3J0IFRlbmFudHNDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL3RlbmFudHNDb250cm9sbGVyXCI7XHJcbmltcG9ydCBEZXRhaWxDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL2RldGFpbENvbnRyb2xsZXJcIjtcclxuaW1wb3J0IFRlbmFudHNTZXJ2aWNlIGZyb20gXCIuL3NlcnZpY2VzL3RlbmFudHNTZXJ2aWNlXCI7XHJcblxyXG5hbmd1bGFyLm1vZHVsZShNb2R1bGVOYW1lLCBbXSkuXHJcbiAgICBjb250cm9sbGVyKFwidGVuYW50c0NvbnRyb2xsZXJcIiwgVGVuYW50c0NvbnRyb2xsZXIpLlxyXG4gICAgY29udHJvbGxlcihcInRlbmFudHNkZXRhaWxDb250cm9sbGVyXCIsIERldGFpbENvbnRyb2xsZXIpLlxyXG4gICAgc2VydmljZShcInRlbmFudHNTZXJ2aWNlXCIsIFRlbmFudHNTZXJ2aWNlKTsgICBcclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1vZHVsZU5hbWU7Iiwi77u/Y2xhc3MgRGV0YWlsQ29udHJvbGxlciB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGVQYXJhbXMsICRzdGF0ZSwgdHJhbnNmZXJzd29ya3NTZXJ2aWNlLCB0b2FzdGVyU2VydmljZSwgbW9kYWxTZXJ2aWNlKSB7XG5cbiAgICAgICAgJChcInNlbGVjdFwiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xuXG4gICAgICAgIHZhciB0cmFuc2ZlcndvcmtJZCA9ICRzdGF0ZVBhcmFtcy5pZDtcbiAgICAgICAgJHNjb3BlLmVkaXRNb2RlID0gdHJhbnNmZXJ3b3JrSWQgIT09IHVuZGVmaW5lZDtcbiAgICAgICAgJHNjb3BlLnNlYXJjaE1vZGUgPSBmYWxzZTtcbiAgICAgICAgJHNjb3BlLnNob3dEZXRhaWwgPSBmYWxzZTtcbiAgICAgICAgJHNjb3BlLmNvdW50VG90YWwgPSAwO1xuICAgICAgICB2YXIgdGVuYW50SWQ7XG5cbiAgICAgICAgJHNjb3BlLmFzc2V0ID0ge307XG5cbiAgICAgICAgJHNjb3BlLnRyYW5zZmVyd29ya0RldGFpbCA9IHtcbiAgICAgICAgICAgIEFzc2V0SWQ6IG51bGwsXG4gICAgICAgICAgICBXb3JrSWQ6IG51bGxcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUudHJhbnNmZXJ3b3JrID0ge1xuICAgICAgICAgICAgQ3JlYXRlZEF0OiBuZXcgRGF0ZSgpLFxuICAgICAgICAgICAgVGVuYW50SWQ6IDAsXG4gICAgICAgICAgICBEZXRhaWxzOiBbXVxuICAgICAgICB9O1xuXG4gICAgICAgIHRyYW5zZmVyc3dvcmtzU2VydmljZS5nZXRDYXRlZ29yeSgpXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuY2F0ZWdvcmllcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICB0cmFuc2ZlcnN3b3Jrc1NlcnZpY2UuZ2V0U3ViQ2F0ZWdvcnlBbGwoKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnN1YmNhdGVnb3JpZXMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgJHNjb3BlLmdldFN1YkNhdGVnb3J5ID0gKCkgPT4ge1xuICAgICAgICAgICAgJHNjb3BlLnN1YmNhdGVnb3JpZXMgPSBudWxsO1xuICAgICAgICAgICAgdHJhbnNmZXJzd29ya3NTZXJ2aWNlLmdldFN1YkNhdGVnb3J5KCRzY29wZS5hc3NldC5DYXRlZ29yeUlkKVxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc3ViY2F0ZWdvcmllcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICQoXCIjc3ViY2F0ZWdvcmllc1wiKS5zZWxlY3QyKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIlxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyYW5zZmVyc3dvcmtzU2VydmljZS5nZXRQcm92aWRlcigpXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAkc2NvcGUucHJvdmlkZXJzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIHRyYW5zZmVyc3dvcmtzU2VydmljZS5nZXRUeXBlUmVjZWlwdCgpXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAkc2NvcGUudHlwZVJlY2VpcHRzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICRzY29wZS5zZWFyY2ggPSAoKSA9PiB7XG4gICAgICAgICAgICB0cmFuc2ZlcnN3b3Jrc1NlcnZpY2UuZ2V0QXNzZXQoJHNjb3BlLmFzc2V0LklkKVxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2VhcmNoTW9kZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hc3NldCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cmFuc2ZlcnN3b3Jrc1NlcnZpY2UuZ2V0V29yaygpXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAkc2NvcGUud29ya3MgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgJHNjb3BlLmFkZFRyYW5zZmVyV29ya0RldGFpbCA9ICgpID0+IHtcbiAgICAgICAgICAgIHZhciBkZXRhaWwgPSB7XG4gICAgICAgICAgICAgICAgQXNzZXRJZDogJHNjb3BlLmFzc2V0LkFzc2V0SWQsXG4gICAgICAgICAgICAgICAgV29ya0lkOiAkc2NvcGUuYXNzZXQuV29ya0lkXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgJHNjb3BlLmNvdW50VG90YWwgKys7XG4gICAgICAgICAgICAkc2NvcGUudHJhbnNmZXJ3b3JrLkRldGFpbHMucHVzaChkZXRhaWwpO1xuICAgICAgICAgICAgJHNjb3BlLnNob3dEZXRhaWwgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCRzY29wZS5lZGl0TW9kZSkge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIHRyYW5zZmVyc3dvcmtzU2VydmljZS5nZXRUcmFuc2ZlcldvcmsodHJhbnNmZXJ3b3JrSWQpXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS50cmFuc2ZlcndvcmsgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2hvd0RldGFpbCA9IHRydWU7XG5cbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnRyYW5zZmVyd29yay5EZXRhaWxzLmZvckVhY2goKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmNvdW50VG90YWwgKys7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zZmVyc3dvcmtzU2VydmljZS5nZXRUeXBlUmVjZWlwdCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2VUeXBlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnR5cGVSZWNlaXB0cyA9IHJlc3BvbnNlVHlwZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjdHlwZVJlY2VpcHRcIikuc2VsZWN0Mih7IHdpZHRoOiBcIjEwMCVcIiB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgIFxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgICRzY29wZS5yZW1vdmVBc3NldCA9IChhc3NldEl0ZW0pID0+IHtcbiAgICAgICAgICAgIGxldCBpbmRleCA9ICRzY29wZS50cmFuc2ZlcndvcmsuRGV0YWlscy5pbmRleE9mKGFzc2V0SXRlbSk7XG4gICAgICAgICAgICAkc2NvcGUuY291bnRUb3RhbCAtLTtcbiAgICAgICAgICAgICRzY29wZS50cmFuc2ZlcndvcmsuRGV0YWlscy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjayA9ICgpID0+IHtcbiAgICAgICAgICAgICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJ0cmFuc2ZlcnN3b3Jrc1wiKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuc2F2ZSA9ICgpID0+IHtcbiAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICB0cmFuc2ZlcnN3b3Jrc1NlcnZpY2UuZ2V0VGVuYW50KClcbiAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG5cbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnRyYW5zZmVyd29yay5UZW5hbnRJZCA9IHRlbmFudElkO1xuICAgICAgICAgICAgICAgICAgICB0cmFuc2ZlcnN3b3Jrc1NlcnZpY2UuYWRkKCRzY29wZS50cmFuc2ZlcndvcmspXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2VBZGQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2VBZGQuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5hdmlnYXRlQmFjaygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgXG4gICAgICAgIH07XG5cbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IERldGFpbENvbnRyb2xsZXI7Iiwi77u/Y2xhc3MgVHJhbnNmZXJXb3Jrc0NvbnRyb2xsZXIge1xuICAgIGNvbnN0cnVjdG9yKCRzY29wZSwgJHJvb3RTY29wZSwgJHN0YXRlLCB0cmFuc2ZlcnN3b3Jrc1NlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlKSB7XG5cbiAgICAgICAgY29uc3QgcGFnZVNpemUgPSA2O1xuICAgICAgICB2YXIgcGFnZUNvdW50ID0gMDtcblxuICAgICAgICAkc2NvcGUudHJhbnNmZXJzd29ya3MgPSBbXTtcblxuICAgICAgICAkc2NvcGUuZ2V0TGlzdCA9ICgpID0+IHtcblxuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdHJhbnNmZXJzd29ya3NTZXJ2aWNlLmdldExpc3QocGFnZVNpemUsIHBhZ2VDb3VudClcbiAgICAgICAgICAgICAgICAudGhlbigodHJhbnNmZXJzd29ya3MpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRyYW5zZmVyc3dvcmtzLmxlbmd0aCA8IHBhZ2VTaXplKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubm9Nb3JlRGF0YSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdHJhbnNmZXJzd29ya3MuZm9yRWFjaCgodHJhbnNmZXJXb3JrKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUudHJhbnNmZXJzd29ya3MucHVzaCh0cmFuc2ZlcldvcmspO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcGFnZUNvdW50ICsrOyAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEkc2NvcGUudHJhbnNmZXJzd29ya3MubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubm9EYXRhID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUubmFnaXZhdGVUb0RldGFpbCA9ICh0cmFuc2ZlcldvcmtJZCkgPT4ge1xuICAgICAgICAgICAgdHJhbnNmZXJXb3JrSWQgPyAkc3RhdGUudHJhbnNpdGlvblRvKFwidHJhbnNmZXJ3b3JrXCIsIHsgaWQ6IHRyYW5zZmVyV29ya0lkIH0pIDogJHN0YXRlLnRyYW5zaXRpb25UbyhcInRyYW5zZmVyd29ya1wiKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVmcmVzaFNlbGVjdGVkSXRlbXMgPSAoYWxsKSA9PiB7XG4gICAgICAgICAgICBpZiAoYWxsKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnRyYW5zZmVyc3dvcmtzLmZvckVhY2goKHRyYW5zZmVyV29yaykgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0cmFuc2Zlcldvcmsuc2VsZWN0ZWQgPSAkc2NvcGUuZXZlcnlTZWxlY3RlZDtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLmFueVNlbGVjdGVkID0gJHNjb3BlLnRyYW5zZmVyc3dvcmtzLnNvbWUoKHRyYW5zZmVyV29yaykgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cmFuc2Zlcldvcmsuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQgPSAkc2NvcGUudHJhbnNmZXJzd29ya3MuZXZlcnkoKHRyYW5zZmVyV29yaykgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cmFuc2Zlcldvcmsuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuZ2V0TGlzdCgpO1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgVHJhbnNmZXJXb3Jrc0NvbnRyb2xsZXI7Iiwi77u/ZnVuY3Rpb24gVHJhbnNmZXJzV29ya3NTZXJ2aWNlKCRodHRwKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICB2YXIgdHJhbnNmZXJzV29ya3M7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBlbnVtU3RhdHVzLFxuICAgICAgICBnZXRDYXRlZ29yeSxcbiAgICAgICAgZ2V0U3ViQ2F0ZWdvcnksXG4gICAgICAgIGdldFN1YkNhdGVnb3J5QWxsLFxuICAgICAgICBnZXRBc3NldCxcbiAgICAgICAgZ2V0UHJvdmlkZXIsXG4gICAgICAgIGdldFR5cGVSZWNlaXB0LFxuICAgICAgICBnZXRDdXN0b21lcixcbiAgICAgICAgZ2V0VGVuYW50LFxuICAgICAgICBnZXRUcmFuc2ZlcldvcmssXG4gICAgICAgIGdldExpc3QsXG4gICAgICAgIGdldFdvcmssXG4gICAgICAgIGFkZCxcbiAgICAgICAgdXBkYXRlXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIGdldFdvcmsoKXtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL3dvcmtzL2dldEFsbGA7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRDdXN0b21lcigpe1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvY3VzdG9tZXJzL2dldEFsbGA7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBlbnVtU3RhdHVzKCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBcIi9hcGkvdHJhbnNmZXJzV29ya3MvRW51bVN0YXR1c1wiXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldENhdGVnb3J5KCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBcIi9hcGkvQ2F0ZWdvcmllc1wiXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFN1YkNhdGVnb3J5QWxsKCkge1xuICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgdXJsOiBgL2FwaS9TdWJDYXRlZ29yaWVzYFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRTdWJDYXRlZ29yeShjYXRlZ29yeUlkKSB7XG4gICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICB1cmw6IGAvYXBpL1N1YkNhdGVnb3JpZXMvJHtjYXRlZ29yeUlkfWBcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0VHlwZVJlY2VpcHQoKSB7XG4gICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICB1cmw6IFwiL2FwaS90cmFuc2ZlcnNXb3Jrcy9UeXBlUmVjZWlwdHNcIlxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRQcm92aWRlcigpe1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvcHJvdmlkZXJzL2dldEFsbGA7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRBc3NldChpZCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvYXNzZXRzL0dldEFzc2V0YDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIHBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICBJZDogaWRcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFRlbmFudCgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogXCIvYXBpL3VzZXJzL2N1cnJlbnQvdGVuYW50XCJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0TGlzdChwYWdlU2l6ZSwgcGFnZUNvdW50KSB7XG4gICAgICAgIGxldCBoYW5kbGVTdWNjZXNzID0gKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB0cmFuc2ZlcnNXb3JrcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICByZXR1cm4gdHJhbnNmZXJzV29ya3M7XG4gICAgICAgIH07XG5cbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvYXBpL3RyYW5zZmVyc1dvcmtzXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgcGFnZVNpemU6IHBhZ2VTaXplLFxuICAgICAgICAgICAgICAgICAgICBwYWdlQ291bnQ6cGFnZUNvdW50XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnRoZW4oaGFuZGxlU3VjY2Vzcyk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFRyYW5zZmVyV29yayh0cmFuc2ZlcndvcmtJZCkge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvdHJhbnNmZXJzV29ya3MvJHt0cmFuc2ZlcndvcmtJZH1gO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gYWRkKHRyYW5zZmVyd29yaykge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvdHJhbnNmZXJzV29ya3MvXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgZGF0YTogdHJhbnNmZXJ3b3JrLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlKHRyYW5zZmVyd29yaykge1xuICAgICAgICByZXR1cm4gZ2V0VGVuYW50KCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9hcGkvdHJhbnNmZXJzV29ya3MvXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQVVRcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBkYXRhOiB0cmFuc2ZlcndvcmssXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIFxufVxuXG5leHBvcnQgZGVmYXVsdCBUcmFuc2ZlcnNXb3Jrc1NlcnZpY2U7Iiwi77u/dmFyIE1vZHVsZU5hbWUgPSBcIlZpYWxTb2Z0LnRyYW5zZmVyc3dvcmtzXCI7XHJcblxyXG5pbXBvcnQgVHJhbnNmZXJzV29ya3NDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL3RyYW5zZmVyc3dvcmtzQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgRGV0YWlsQ29udHJvbGxlciBmcm9tIFwiLi9jb250cm9sbGVycy9kZXRhaWxDb250cm9sbGVyXCI7XHJcbmltcG9ydCBUcmFuc2ZlcnNXb3Jrc1NlcnZpY2UgZnJvbSBcIi4vc2VydmljZXMvdHJhbnNmZXJzd29ya3NTZXJ2aWNlXCI7XHJcblxyXG5hbmd1bGFyLm1vZHVsZShNb2R1bGVOYW1lLCBbXSkuXHJcbiAgICBjb250cm9sbGVyKFwidHJhbnNmZXJzd29ya3NDb250cm9sbGVyXCIsIFRyYW5zZmVyc1dvcmtzQ29udHJvbGxlcikuXHJcbiAgICBjb250cm9sbGVyKFwidHJhbnNmZXJzd29ya3NkZXRhaWxDb250cm9sbGVyXCIsIERldGFpbENvbnRyb2xsZXIpLlxyXG4gICAgc2VydmljZShcInRyYW5zZmVyc3dvcmtzU2VydmljZVwiLCBUcmFuc2ZlcnNXb3Jrc1NlcnZpY2UpO1xyXG5leHBvcnQgZGVmYXVsdCBNb2R1bGVOYW1lOyIsIu+7v2NsYXNzIERldGFpbENvbnRyb2xsZXIge1xuICAgIGNvbnN0cnVjdG9yKCRzY29wZSwgJHJvb3RTY29wZSwgJHN0YXRlUGFyYW1zLCAkc3RhdGUsIHdvcmtzU2VydmljZSwgdG9hc3RlclNlcnZpY2UsIG1vZGFsU2VydmljZSkge1xuICAgICAgICBcbiAgICAgICAgJChcInNlbGVjdFwiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xuXG4gICAgICAgIHZhciB3b3JrSWQgPSAkc3RhdGVQYXJhbXMuaWQ7XG4gICAgICAgICRzY29wZS5lZGl0TW9kZSA9IHdvcmtJZCAhPT0gdW5kZWZpbmVkO1xuICAgICAgICB2YXIgdGVuYW50SWQ7XG5cbiAgICAgICAgJHNjb3BlLndvcmsgPSB7XG4gICAgICAgICAgICBDcmVhdGVkQXQ6IG5ldyBEYXRlKClcbiAgICAgICAgfTtcblxuICAgICAgICB3b3Jrc1NlcnZpY2UuZ2V0Q3VzdG9tZXIoKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmN1c3RvbWVycyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgJHNjb3BlLndvcmsuQ3VzdG9tZXJJZCA9IDE7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICBpZiAoJHNjb3BlLmVkaXRNb2RlKSB7XG4gICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSB0cnVlO1xuXG4gICAgICAgICAgICB3b3Jrc1NlcnZpY2UuZ2V0V29yayh3b3JrSWQpXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS53b3JrID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgd29ya3NTZXJ2aWNlLmdldEN1c3RvbWVyKClcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZUN1c3RvbWVyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmN1c3RvbWVycyA9IHJlc3BvbnNlQ3VzdG9tZXIuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwic2VsZWN0XCIpLnNlbGVjdDIoeyB3aWR0aDogXCIxMDAlXCIgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIFxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIHdvcmtzU2VydmljZS5nZXRDdXN0b21lcigpXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlQ3VzdG9tZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmN1c3RvbWVycyA9IHJlc3BvbnNlQ3VzdG9tZXIuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgJChcInNlbGVjdFwiKS5zZWxlY3QyKHsgd2lkdGg6IFwiMTAwJVwiIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgd29ya3NTZXJ2aWNlLmdldFRlbmFudCgpXG4gICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgXG4gICAgICAgIH1cblxuICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrID0gKCkgPT4ge1xuICAgICAgICAgICAgJHN0YXRlLnRyYW5zaXRpb25UbyhcIndvcmtzXCIpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZW1vdmV3b3JrID0gKCkgPT4ge1xuICAgICAgICAgICAgbW9kYWxTZXJ2aWNlLnNob3dDb25maXJtTW9kYWwoe1xuICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiB7XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIkVsaW1pbmFyIE9icmFcIixcbiAgICAgICAgICAgICAgICAgICAgYm9keTogXCJFc3RhIHNlZ3VybyBxdWUgZGVzZWEgZWxpbWluYXIgbGEgb2JyYT9cIixcbiAgICAgICAgICAgICAgICAgICAgb2s6IFwiU2ksIGVsaW1pbmFyXCIsXG4gICAgICAgICAgICAgICAgICAgIGNhbmNlbDogXCJDYW5jZWxhclwiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgICAgIHdvcmtzU2VydmljZS5yZW1vdmUod29ya0lkKVxuICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJ3b3Jrc1wiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnNhdmUgPSAoKSA9PiB7XG5cbiAgICAgICAgICAgIGlmICghJHNjb3BlLmVkaXRNb2RlKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLndvcmsuVGVuYW50SWQgPSB0ZW5hbnRJZDtcbiAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSB0cnVlO1xuXG4gICAgICAgICAgICAgICAgd29ya3NTZXJ2aWNlLmFkZCgkc2NvcGUud29yaylcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmF2aWdhdGVCYWNrKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC5maW5hbGx5KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB3b3Jrc1NlcnZpY2UudXBkYXRlKCRzY29wZS53b3JrKVxuICAgICAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5uYXZpZ2F0ZUJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RlclNlcnZpY2Uuc2hvd1NlcnZlckVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgRGV0YWlsQ29udHJvbGxlcjsiLCLvu79jbGFzcyBXb3Jrc0NvbnRyb2xsZXIge1xuICAgIGNvbnN0cnVjdG9yKCRzY29wZSwgJHJvb3RTY29wZSwgJHN0YXRlLCB3b3Jrc1NlcnZpY2UsIHRvYXN0ZXJTZXJ2aWNlLCBtb2RhbFNlcnZpY2UpIHtcblxuICAgICAgICBjb25zdCBwYWdlU2l6ZSA9IDY7XG4gICAgICAgIHZhciBwYWdlQ291bnQgPSAwO1xuXG4gICAgICAgICRzY29wZS53b3JrcyA9IFtdO1xuXG4gICAgICAgICRzY29wZS5nZXRMaXN0ID0gKCkgPT4ge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIHdvcmtzU2VydmljZS5nZXRMaXN0KHBhZ2VTaXplLCBwYWdlQ291bnQpXG4gICAgICAgICAgICAgICAgLnRoZW4oKHdvcmtzKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh3b3Jrcy5sZW5ndGggPCBwYWdlU2l6ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5vTW9yZURhdGEgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHdvcmtzLmZvckVhY2goKHdvcmspID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS53b3Jrcy5wdXNoKHdvcmspO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcGFnZUNvdW50ICsrOyAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZnJlc2hTZWxlY3RlZEl0ZW1zKCk7ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEkc2NvcGUud29ya3MubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubm9EYXRhID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0b2FzdGVyU2VydmljZS5zaG93U2VydmVyRXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUubmFnaXZhdGVUb0RldGFpbCA9ICh3b3JrSWQpID0+IHtcbiAgICAgICAgICAgIHdvcmtJZCA/ICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJ3b3JrXCIsIHsgaWQ6IHdvcmtJZCB9KSA6ICRzdGF0ZS50cmFuc2l0aW9uVG8oXCJ3b3JrXCIpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZWZyZXNoU2VsZWN0ZWRJdGVtcyA9IChhbGwpID0+IHtcbiAgICAgICAgICAgIGlmIChhbGwpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUud29ya3MuZm9yRWFjaCgod29yaykgPT4ge1xuICAgICAgICAgICAgICAgICAgICB3b3JrLnNlbGVjdGVkID0gJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQ7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5hbnlTZWxlY3RlZCA9ICRzY29wZS53b3Jrcy5zb21lKCh3b3JrKSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHdvcmsuc2VsZWN0ZWQ7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHNjb3BlLmV2ZXJ5U2VsZWN0ZWQgPSAkc2NvcGUud29ya3MuZXZlcnkoKHdvcmspID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gd29yay5zZWxlY3RlZDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZW1vdmUgPSAod29yaykgPT4ge1xuXG4gICAgICAgICAgICBtb2RhbFNlcnZpY2Uuc2hvd0NvbmZpcm1Nb2RhbCh7XG4gICAgICAgICAgICAgICAgbWVzc2FnZXM6IHtcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IGBFbGltaW5hciBsYSBvYnJhYCxcbiAgICAgICAgICAgICAgICAgICAgYm9keTogYEVzdGEgc2VndXJvIHF1ZSBkZXNlYSBlbGltaW5hciBsYSBvYnJhYCxcbiAgICAgICAgICAgICAgICAgICAgb2s6IFwiU2ksIGVsaW1pbmFyXCIsXG4gICAgICAgICAgICAgICAgICAgIGNhbmNlbDogXCJDYW5jZWxhclwiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICB2YXIgd29ya0lkTGlzdDtcbiAgICAgICAgICAgICAgICBpZiAod29yaykge1xuICAgICAgICAgICAgICAgICAgICB3b3JrSWRMaXN0ID0gW3dvcmsuV29ya0lkXTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB3b3JrSWRMaXN0ID0gJHNjb3BlLndvcmtzXG4gICAgICAgICAgICAgICAgICAgICAgICAubWFwKCh3b3JrSXRlbSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh3b3JrSXRlbS5zZWxlY3RlZCkgeyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB3b3JrSXRlbS5Xb3JrSWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHdvcmtJZExpc3QuZm9yRWFjaCgod29ya0lkKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHdvcmtzU2VydmljZS5yZW1vdmUod29ya0lkKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS53b3Jrcy5mb3JFYWNoKCh3b3JrSXRlbSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHdvcmtJZCA9PT0gd29ya0l0ZW0uV29ya0lkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGluZGV4ID0gJHNjb3BlLndvcmtzLmluZGV4T2Yod29ya0l0ZW0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS53b3Jrcy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXJTZXJ2aWNlLnNob3dTZXJ2ZXJFcnJvcihlcnJvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICRzY29wZS5yZWZyZXNoU2VsZWN0ZWRJdGVtcygpO1xuXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuZ2V0TGlzdCgpO1xuICAgIH1cblxufVxuXG5leHBvcnQgZGVmYXVsdCBXb3Jrc0NvbnRyb2xsZXI7Iiwi77u/ZnVuY3Rpb24gV29ya3NTZXJ2aWNlKCRodHRwKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICB2YXIgd29ya3M7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBnZXRDdXN0b21lcixcbiAgICAgICAgZ2V0VGVuYW50LFxuICAgICAgICBnZXRXb3JrLFxuICAgICAgICBnZXRMaXN0LFxuICAgICAgICBhZGQsXG4gICAgICAgIHVwZGF0ZSxcbiAgICAgICAgcmVtb3ZlXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIGdldEN1c3RvbWVyKCl7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgdmFyIHRlbmFudElkID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIGxldCB1cmwgPSBgL2FwaS9jdXN0b21lcnMvZ2V0QWxsYDtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFRlbmFudCgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgICAgIHVybDogXCIvYXBpL3VzZXJzL2N1cnJlbnQvdGVuYW50XCJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0TGlzdChwYWdlU2l6ZSwgcGFnZUNvdW50KSB7XG4gICAgICAgIGxldCBoYW5kbGVTdWNjZXNzID0gKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB3b3JrcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICByZXR1cm4gd29ya3M7XG4gICAgICAgIH07XG5cbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvYXBpL3dvcmtzXCI7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgIHBhZ2VTaXplOiBwYWdlU2l6ZSxcbiAgICAgICAgICAgICAgICAgICAgcGFnZUNvdW50OnBhZ2VDb3VudFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS50aGVuKGhhbmRsZVN1Y2Nlc3MpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRXb3JrKHdvcmtzSWQpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IHVybCA9IGAvYXBpL3dvcmtzLyR7d29ya3NJZH1gO1xuICAgICAgICAgICAgcmV0dXJuICRodHRwKHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFkZCh3b3JrKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaS93b3Jrcy9cIjtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBkYXRhOiB3b3JrLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgVGVuYW50SWQ6IHRlbmFudElkICAgXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHVwZGF0ZSh3b3JrKSB7XG4gICAgICAgIHJldHVybiBnZXRUZW5hbnQoKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICB2YXIgdGVuYW50SWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaS93b3Jrcy9cIjtcbiAgICAgICAgICAgIHJldHVybiAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBVVFwiLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGRhdGE6IHdvcmssXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBUZW5hbnRJZDogdGVuYW50SWRcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVtb3ZlKHdvcmtzSWQpIHtcbiAgICAgICAgcmV0dXJuIGdldFRlbmFudCgpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciB0ZW5hbnRJZCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hcGkvd29ya3MvJHt3b3Jrc0lkfWA7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAoe1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJERUxFVEVcIixcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFRlbmFudElkOiB0ZW5hbnRJZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFdvcmtzU2VydmljZTsiLCLvu792YXIgTW9kdWxlTmFtZSA9IFwiVmlhbFNvZnQud29ya3NcIjtcblxuaW1wb3J0IFdvcmtzQ29udHJvbGxlciBmcm9tIFwiLi9jb250cm9sbGVycy93b3Jrc0NvbnRyb2xsZXJcIjtcbmltcG9ydCBEZXRhaWxDb250cm9sbGVyIGZyb20gXCIuL2NvbnRyb2xsZXJzL2RldGFpbENvbnRyb2xsZXJcIjtcbmltcG9ydCBXb3Jrc1NlcnZpY2UgZnJvbSBcIi4vc2VydmljZXMvd29ya3NTZXJ2aWNlXCI7XG5cbmFuZ3VsYXIubW9kdWxlKE1vZHVsZU5hbWUsIFtdKS5cbiAgICBjb250cm9sbGVyKFwid29ya3NDb250cm9sbGVyXCIsIFdvcmtzQ29udHJvbGxlcikuXG4gICAgY29udHJvbGxlcihcIndvcmtzZGV0YWlsQ29udHJvbGxlclwiLCBEZXRhaWxDb250cm9sbGVyKS5cbiAgICBzZXJ2aWNlKFwid29ya3NTZXJ2aWNlXCIsIFdvcmtzU2VydmljZSk7ICAgXG5cbmV4cG9ydCBkZWZhdWx0IE1vZHVsZU5hbWU7Il19
