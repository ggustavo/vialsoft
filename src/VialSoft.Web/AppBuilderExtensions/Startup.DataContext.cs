using VialSoft.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Entity;
using System;

namespace VialSoft.Web.AppBuilderExtensions
{
    public static class DataContextExtensions
    {
        public static IServiceCollection ConfigureDataContext(this IServiceCollection services, IConfiguration configuration, bool useInMemoryStore)
        {
            services.AddEntityFramework()
                    .AddStore(useInMemoryStore)
                    .AddDbContext<VialSoftContext>(options =>
                    {
                        if (useInMemoryStore)
                        {
                            options.UseInMemoryDatabase();
                        }
                        else
                        {
                            options.UseSqlServer(configuration["Data:DefaultConnection:Connectionstring"]);
                        }
                    });

            return services;
        }
    }
}