﻿using Microsoft.Extensions.DependencyInjection;
using VialSoft.Data.Infraestructure;
using VialSoft.Data.Repositories;

namespace VialSoft.Web.AppBuilderExtensions
{
    public static class DependenciesExtensions
    {
        public static IServiceCollection ConfigureDependencies(this IServiceCollection services)
        {
            services.AddScoped<CustomersRepository>();
            services.AddScoped<WorksRepository>();
            services.AddScoped<AssetsRepository>();
            services.AddScoped<CategoriesRepository>();
            services.AddScoped<SubCategoriesRepository>();
            services.AddScoped<ReportsRepository>();
            services.AddScoped<AccessoriesRepository>();
            services.AddScoped<TenantsRepository>();
            services.AddScoped<PurchasesRepository>();
            services.AddScoped<ProvidersRepository>();
            services.AddScoped<TransfersWorksRepository>();
            services.AddScoped<JobsRepository>();
            services.AddScoped<PeriodsRepository>();
            services.AddScoped<MaintenanceCardRepository>();
            services.AddScoped<CosteAssetRepository>();
            services.AddScoped<VialSoftDataInitializer>();

            return services;
        }
    }
}