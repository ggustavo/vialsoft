﻿namespace VialSoft.BusinessLogic
{
    public static class CosteAsset
    {
        public static void CalculateCoste(Model.CosteAsset costeAsset, Model.Asset asset)
        {
            const double dolarvalue = 15.45;
            const double fuelPrice = 21.55;
            
            var ra = costeAsset.PriceAsset * dolarvalue * costeAsset.Amortization / costeAsset.UsefulLife;
            var ri = costeAsset.PriceAsset * dolarvalue * costeAsset.FinancialCost / 2 * costeAsset.AnualHorus;
            var rrr = costeAsset.PriceAsset * dolarvalue / costeAsset.UsefulLife * costeAsset.Repair;

            var rd = costeAsset.FactorHp * asset.Power;
            var rgc = fuelPrice * rd;

            var rgl = asset.Lubricant * rgc;

            costeAsset.Coste = ra + ri + rrr + rgc + rgl;


        }
    }
}
