﻿using Microsoft.AspNet.Http;
using Microsoft.Extensions.Primitives;
using System.Linq;

namespace VialSoft.API.AppExtensions
{
    public static class HttpRequestExtensions
    {
        public static int GetTenant(this HttpRequest request)
        {
            var tenantId = 0;
            StringValues headerValues;
            if (request.Headers.TryGetValue("TenantId", out headerValues))
            {
                int.TryParse(headerValues.First(), out tenantId);
            }

            return tenantId;
        }
    }
}
