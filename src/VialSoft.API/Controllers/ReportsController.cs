﻿using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;
using System.Threading.Tasks;
using VialSoft.API.AppExtensions;
using System.Collections.Generic;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class ReportsController : Controller
    {
        private readonly ReportsRepository _reportsRepository;

        public ReportsController(ReportsRepository reportsRepository)
        {
            _reportsRepository = reportsRepository;
        }

        [HttpGet("assetsummary")]
        public async Task<AssetSummary> GetAssetSummaryAsync()
        {
            return await _reportsRepository.GetAssetsSummaryAsync(Request.GetTenant());
        }

        [HttpGet("expenses/{year}")]
        public async Task<IEnumerable<ExpensesSummary>> GetExpensesSummaryAsync(int year)
        {
            return await _reportsRepository.GetExpensesSummaryAsync(Request.GetTenant(), year);
        }
    }
}
