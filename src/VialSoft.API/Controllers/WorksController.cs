﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;
using VialSoft.API.AppExtensions;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class WorksController : Controller
    {
        private readonly WorksRepository _worksRepository;

        public WorksController(WorksRepository worksRepository)
        {
            _worksRepository = worksRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Work>> Get(int pageSize, int pageCount)
        {
            return await _worksRepository.GetAsync(Request.GetTenant(), pageSize, pageCount);
        }

        [HttpGet("getAll")]
        public Task<IEnumerable<Work>> Get()
        {
            return _worksRepository.GetAsync(Request.GetTenant());
        }

        [HttpGet("{id}")]
        public async Task<Work> Get(int id)
        {
            return await _worksRepository.GetAsync(Request.GetTenant(), id);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            await _worksRepository.DeleteAsync(Request.GetTenant(), id);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]Work work)
        {
            return await _worksRepository.AddAsync(work);
        }

        [HttpPut]
        public async Task UpdateAsync([FromBody]Work work)
        {
            await _worksRepository.UpdateAsync(work);
        }

    }
}
