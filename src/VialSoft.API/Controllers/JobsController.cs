﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class JobsController : Controller
    {
        private readonly JobsRepository _jobsRepository;

        public JobsController(JobsRepository JobsRepository)
        {
            _jobsRepository = JobsRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Job>> Get()
        {
            return await _jobsRepository.GetAsync();
        }

        [HttpGet("{id}")]
        public async Task<IEnumerable<Job>> Get(int id)
        {
            return await _jobsRepository.GetAsync(id);
        }

    }
}
