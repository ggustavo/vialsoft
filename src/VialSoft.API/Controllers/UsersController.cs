﻿using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using VialSoft.Data;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Data.Entity;

namespace VialSoft.API.Controllers
{

    [Route("api/[controller]")]
    [Authorize()]
    public class UsersController : Controller
    {
        private readonly VialSoftContext _context;

        public UsersController(VialSoftContext context)
        {
            _context = context;
        }

        [HttpGet("current/avatar")]
        public async Task<IActionResult> GetCurrentUserAvatarAsync()
        {
            var picture = await _context.Users
                .Where(u => u.UserName == User.Identity.Name)
                .Select(u => u.Picture)
                .FirstOrDefaultAsync();

            if (picture != null)
            {
                return new FileContentResult(picture, "image/png");
            }

            return null;
        }

        [HttpGet("current/user")]
        public async Task<string> GetCurrentUserAsync()
        {
            return await _context.Users
                .Where(u => u.UserName == User.Identity.Name)
                .Select(u => u.UserName)
                .FirstOrDefaultAsync();
        }

        [HttpGet("current/tenant")]
        public async Task<int> GetCurrentTenantAsync()
        {
            return await _context.Users
                .Where(u => u.UserName == User.Identity.Name)
                .Select(u => u.TenantId)
                .FirstOrDefaultAsync();

        }
    }
}
