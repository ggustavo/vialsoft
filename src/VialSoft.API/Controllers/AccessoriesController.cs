﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;
using VialSoft.API.AppExtensions;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class AccessoriesController : Controller
    {
        private readonly AccessoriesRepository _accessoriesRepository;

        public AccessoriesController(AccessoriesRepository accessoriesRepository)
        {
            _accessoriesRepository = accessoriesRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Accessory>> Get(int pageSize, int pageCount)
        {
            return await _accessoriesRepository.GetAsync(Request.GetTenant(), pageSize, pageCount);
        }

        [HttpGet("{id}")]
        public async Task<IEnumerable<Accessory>> Get(int id)
        {
            return await _accessoriesRepository.GetAsync(Request.GetTenant(), id);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            await _accessoriesRepository.DeleteAsync(Request.GetTenant(), id);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]Accessory accessory)
        {
            return await _accessoriesRepository.AddAsync(accessory);
        }

        [HttpPut]
        public async Task UpdateAsync([FromBody]Accessory accessory)
        {
            await _accessoriesRepository.UpdateAsync(accessory);
        }

    }
}
