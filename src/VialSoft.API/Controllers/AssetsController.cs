﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;
using VialSoft.API.AppExtensions;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class AssetsController : Controller
    {
        private readonly AssetsRepository _assetsRepository;

        public AssetsController(AssetsRepository assetsRepository)
        {
            _assetsRepository = assetsRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Asset>> Get(int pageSize, int pageCount)
        {
            return await _assetsRepository.GetAsync(Request.GetTenant(), pageSize, pageCount);
        }

        [HttpGet("{id}")]
        public async Task<Asset> Get(int id)
        {
            return await _assetsRepository.GetAsync(Request.GetTenant(), id);
        }

        [HttpGet("GetAsset")]
        public async Task<Asset> Get(string id)
        {
            return await _assetsRepository.GetAsync(Request.GetTenant(), id);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            await _assetsRepository.DeleteAsync(Request.GetTenant(), id);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]Asset asset)
        {
            return await _assetsRepository.AddAsync(asset);
        }

        [HttpPut]
        public async Task UpdateAsync([FromBody]Asset asset)
        {
            await _assetsRepository.UpdateAsync(asset);
        }

        [HttpGet("EnumStatus")]
        public string EnumStatus()
        {
            return _assetsRepository.EnumStatusAsset();
        }

    }
}
