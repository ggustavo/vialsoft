﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class TenantsController : Controller
    {
        private readonly TenantsRepository _tenantsRepository;

        public TenantsController(TenantsRepository tenantsRepository)
        {
            _tenantsRepository = tenantsRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Tenant>> Get(int pageSize, int pageCount)
        {
            return await _tenantsRepository.GetAsync(pageSize, pageCount);
        }

        [HttpGet("name/{name}")]
        public async Task<IEnumerable<Tenant>> GetByName(string name, int count)
        {
            return await _tenantsRepository.GetAsync(name, count);
        }

        [HttpGet("{id}")]
        public async Task<Tenant> Get(int id)
        {
            return await _tenantsRepository.GetAsync(id);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            await _tenantsRepository.DeleteAsync(id);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]Tenant tenant)
        {
            return await _tenantsRepository.AddAsync(tenant);
        }

        [HttpPut]
        public async Task UpdateAsync([FromBody]Tenant tenant)
        {
            await _tenantsRepository.UpdateAsync(tenant);
        }

    }
}
