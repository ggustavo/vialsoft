﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;
using VialSoft.API.AppExtensions;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class ProvidersController : Controller
    {
        private readonly ProvidersRepository _providersRepository;

        public ProvidersController(ProvidersRepository providersRepository)
        {
            _providersRepository = providersRepository;
        }

        [HttpGet("getAll")]
        public Task<IEnumerable<Provider>> Get()
        {
            return _providersRepository.GetAsync(Request.GetTenant());
        }

        [HttpGet]
        public async Task<IEnumerable<Provider>> Get(int pageSize, int pageCount)
        {
            return await _providersRepository.GetAsync(Request.GetTenant(), pageSize, pageCount);
        }

        [HttpGet("name/{name}")]
        public async Task<IEnumerable<Provider>> GetByName(string name, int count)
        {
            return await _providersRepository.GetAsync(Request.GetTenant(), name, count);
        }

        [HttpGet("{id}")]
        public async Task<Provider> Get(int id)
        {
            return await _providersRepository.GetAsync(Request.GetTenant(), id);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            await _providersRepository.DeleteAsync(Request.GetTenant(), id);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]Provider provider)
        {
            return await _providersRepository.AddAsync(provider);
        }

        [HttpPut]
        public async Task UpdateAsync([FromBody]Provider provider)
        {
            await _providersRepository.UpdateAsync(provider);
        }

        [HttpGet("TypeReceipts")]
        public string TypeReceipts()
        {
            return _providersRepository.TypeReceipts();
        }

    }
}
