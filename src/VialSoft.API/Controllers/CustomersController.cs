﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;
using VialSoft.API.AppExtensions;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
        private readonly CustomersRepository _customersRepository;

        public CustomersController(CustomersRepository customersRepository)
        {
            _customersRepository = customersRepository;
        }

        [HttpGet("getAll")]
        public Task<IEnumerable<Customer>> Get()
        {
            return _customersRepository.GetAsync(Request.GetTenant());
        }

        [HttpGet]
        public async Task<IEnumerable<Customer>> Get(int pageSize, int pageCount)
        {
            return await _customersRepository.GetAsync(Request.GetTenant(), pageSize, pageCount);
        }

        [HttpGet("name/{name}")]
        public async Task<IEnumerable<Customer>> GetByName(string name, int count)
        {
            return await _customersRepository.GetAsync(Request.GetTenant(), name, count);
        }

        [HttpGet("{id}")]
        public async Task<Customer> Get(int id)
        {
            return await _customersRepository.GetAsync(Request.GetTenant(), id);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            await _customersRepository.DeleteAsync(Request.GetTenant(), id);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]Customer customer)
        {
            return await _customersRepository.AddAsync(customer);
        }

        [HttpPut]
        public async Task UpdateAsync([FromBody]Customer customer)
        {
            await _customersRepository.UpdateAsync(customer);
        }


        [HttpGet("EnumEntity")]
        public string EnumEntity()
        {
            return _customersRepository.EnumEntity();
        }

    }
}
