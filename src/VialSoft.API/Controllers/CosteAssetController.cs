﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.API.AppExtensions;
using VialSoft.Data.Repositories;
using VialSoft.Model;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class CosteAssetController : Controller
    {
        private readonly CosteAssetRepository _costeassetRepository;
        private readonly AssetsRepository _assetsRepository;

        public CosteAssetController(CosteAssetRepository costeassetRepository, AssetsRepository assetRepository)
        {
            _costeassetRepository = costeassetRepository;
            _assetsRepository = assetRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<CosteAsset>> Get(int pageSize, int pageCount)
        {
            return await _costeassetRepository.GetAsync(Request.GetTenant(), pageSize, pageCount);
        }

        [HttpGet("{id}")]
        public async Task<CosteAsset> Get(int id)
        {
            return await _costeassetRepository.GetAsync(id);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]CosteAsset costeAsset)
        {
            var asset = _assetsRepository.GetAsync(Request.GetTenant(), costeAsset.AssetId).Result;
            BusinessLogic.CosteAsset.CalculateCoste(costeAsset, asset);
            return await _costeassetRepository.AddAsync(costeAsset);
        }

        [HttpPut]
        public async Task UpdateAsync([FromBody]CosteAsset costeAsset)
        {
            await _costeassetRepository.UpdateAsync(costeAsset);
        }

    }
}
