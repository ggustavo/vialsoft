﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;
using VialSoft.API.AppExtensions;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class TransfersWorksController : Controller
    {
        private readonly TransfersWorksRepository _transfersWorksRepository;

        public TransfersWorksController(TransfersWorksRepository transfersWorksRepositoryRepository)
        {
            _transfersWorksRepository = transfersWorksRepositoryRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<TransferWork>> Get(int pageSize, int pageCount)
        {
            return await _transfersWorksRepository.GetAsync(Request.GetTenant(), pageSize, pageCount);
        }

        [HttpGet("{id}")]
        public async Task<TransferWork> Get(int id)
        {
            return await _transfersWorksRepository.GetAsync(Request.GetTenant(), id);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]TransferWork transferWork)
        {
            return await _transfersWorksRepository.AddAsync(transferWork);
        }

        [HttpPut]
        public async Task UpdateAsync([FromBody]TransferWork transferWork)
        {
            await _transfersWorksRepository.UpdateAsync(transferWork);
        }

        [HttpGet("TypeReceipts")]
        public string TypeReceipts()
        {
            return _transfersWorksRepository.TypeReceipts();
        }

    }
}
