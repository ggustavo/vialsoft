﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;
using VialSoft.API.AppExtensions;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class PurchasesController : Controller
    {
        private readonly PurchasesRepository _purchasesRepository;

        public PurchasesController(PurchasesRepository purchasesRepository)
        {
            _purchasesRepository = purchasesRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Purchase>> Get(int pageSize, int pageCount)
        {
            return await _purchasesRepository.GetAsync(Request.GetTenant(), pageSize, pageCount);
        }

        [HttpGet("{id}")]
        public async Task<Purchase> Get(int id)
        {
            return await _purchasesRepository.GetAsync(Request.GetTenant(), id);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            await _purchasesRepository.DeleteAsync(Request.GetTenant(), id);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]Purchase asset)
        {
            return await _purchasesRepository.AddAsync(asset);
        }

        [HttpPut]
        public async Task UpdateAsync([FromBody]Purchase asset)
        {
            await _purchasesRepository.UpdateAsync(asset);
        }

        [HttpGet("TypeReceipts")]
        public string TypeReceipts()
        {
            return _purchasesRepository.TypeReceipts();
        }

    }
}
