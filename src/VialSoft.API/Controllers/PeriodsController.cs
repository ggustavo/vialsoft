﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class PeriodsController : Controller
    {
        private readonly PeriodsRepository _periodsRepository;

        public PeriodsController(PeriodsRepository PeriodsRepository)
        {
            _periodsRepository = PeriodsRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Period>> Get()
        {
            return await _periodsRepository.GetAsync();
        }

    }
}
