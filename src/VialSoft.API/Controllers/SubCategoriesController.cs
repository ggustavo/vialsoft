﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class SubCategoriesController : Controller
    {
        private readonly SubCategoriesRepository _subCategoriesRepository;

        public SubCategoriesController(SubCategoriesRepository subCategoriesRepository)
        {
            _subCategoriesRepository = subCategoriesRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<SubCategory>> Get()
        {
            return await _subCategoriesRepository.GetAsync();
        }

        [HttpGet("{id}")]
        public async Task<IEnumerable<SubCategory>> Get(int id)
        {
            return await _subCategoriesRepository.GetAsync(id);
        }

    }
}
