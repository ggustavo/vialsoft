﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VialSoft.Data.Repositories;
using VialSoft.Model;
using VialSoft.API.AppExtensions;

namespace VialSoft.API.Controllers
{
    [Route("api/[controller]")]
    public class MaintenanceCardsController : Controller
    {
        private readonly MaintenanceCardRepository _maintenanceCardRepository;

        public MaintenanceCardsController(MaintenanceCardRepository accessoriesRepository)
        {
            _maintenanceCardRepository = accessoriesRepository;
        }

        public async Task<IEnumerable<MaintenanceCardDetail>> Get(int pageSize, int pageCount, int assetId)
        {
            return await _maintenanceCardRepository.GetAsync(Request.GetTenant(), pageSize, pageCount, assetId);
        }

        [HttpPost]
        public async Task<int> AddAsync([FromBody]MaintenanceCard maintenanceCard)
        {
            return await _maintenanceCardRepository.AddAsync(maintenanceCard);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id)
        {
            await _maintenanceCardRepository.DeleteAsync(Request.GetTenant(), id);
        }

    }
}
