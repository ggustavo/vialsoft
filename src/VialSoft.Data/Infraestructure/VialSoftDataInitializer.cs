﻿using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VialSoft.Model;
using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.PlatformAbstractions;

namespace VialSoft.Data.Infraestructure
{
    public class VialSoftDataInitializer
    {
        private static readonly Random Randomize = new Random();


        public static async Task InitializeDatabaseAsync(IServiceProvider serviceProvider)
        {
            using (var db = serviceProvider.GetService<VialSoftContext>())
            {
                var databaseCreated = await db.Database.EnsureCreatedAsync();
                if (databaseCreated)
                {
                    await CreateSampleData(serviceProvider, db);
                }
            }
        }

        private static async Task CreateSampleData(IServiceProvider serviceProvider, VialSoftContext context)
        {
            var tenantId = CreateSampleData(context);

            CreateWorks(context, tenantId);
            CreateCategories(context);
            CreateSubCategories(context);
            CreateAssets(context, tenantId);
            CreateAssetSummary(context, tenantId);
            CreateExpensesSummaries(context, tenantId);
            CreateAccessories(context);
            CreateProviders(context, tenantId);
            CreatePurchaces(context, tenantId);
            CreateTransfersWorks(context, tenantId);
            CreatePeriods(context, tenantId);
            CreateJobs(context);
            CreateMaintenanceCard(context, tenantId);
            CreateCosteAsset(context, tenantId);

            await CreateDefaultUser(serviceProvider, tenantId);
        }

        private static async Task CreateDefaultUser(IServiceProvider serviceProvider, int tenantId)
        {
            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
            var appEnv = serviceProvider.GetService<IApplicationEnvironment>();

            var builder = new ConfigurationBuilder()
                    .SetBasePath(appEnv.ApplicationBasePath)
                    .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            var user = await userManager.FindByNameAsync(configuration["DefaultUsername"]);
            if (user == null)
            {
                user = new ApplicationUser {
                    UserName = configuration["DefaultUsername"],
                    TenantId = tenantId,
                    Picture = GetDefaultUser()
                };
                await userManager.CreateAsync(user, configuration["DefaultPassword"]);
                await userManager.AddClaimAsync(user, new Claim("ManageStore", "Allowed"));
            }
        }

        private static int CreateSampleData(VialSoftContext context)
        {
            var tenantId = CreateTenants(context);

            CreateCustomers(context, tenantId);

            return tenantId;
        }

        private static int CreateTenants(VialSoftContext context)
        {
            var defaultTenant = new Tenant
            {
                Name = "Rovial SA",
                Address = "Alem 2302",
                City = "Rosario",
                Cuit = "30-57339108-6",
                WaitTimeAvg = Randomize.Next(1, 10)
            };
            context.Tenants.Add(defaultTenant);
            context.SaveChanges();

            return defaultTenant.TenantId;
        }

        private static void CreateCustomers(VialSoftContext context, int tenantId)
        {
            var customers = new List<Customer>();

            var customer = new Customer
            {
                Name = "ROVIAL",
                Address = "La paz 409",
                Deleted = false,
                City = "Rosario",
                Country = "Argentina",
                State = "Santa Fe",
                Picture = GetCustomerPicture(1),
                Entity = 0,
                CreatedAt = DateTime.Now,
                TenantId = tenantId
            };
            customers.Add(customer);

            customer = new Customer
            {
                Name = "MILICIC",
                Address = "Av. Pte. Perón - 8110",
                Deleted = false,
                City = "Rosario",
                Country = "Argentina",
                State = "Santa Fe",
                Picture = GetCustomerPicture(2),
                Entity = 0,
                CreatedAt = DateTime.Now,
                TenantId = tenantId
            };
            customers.Add(customer);

            customer = new Customer
            {
                Name = "EPRECO",
                Address = "Arijón 584.",
                Deleted = false,
                City = "Rosario",
                Country = "Argentina",
                State = "Santa Fe",
                Picture = GetCustomerPicture(3),
                Entity = 0,
                TenantId = tenantId,
                CreatedAt = DateTime.Now
            };
            customers.Add(customer);

            customer = new Customer
            {
                Name = "MSR",
                Address = "Rioja 1993",
                Deleted = false,
                City = "Rosario",
                Country = "Argentina",
                State = "Santa Fe",
                Picture = GetCustomerPicture(4),
                Entity = 1,
                TenantId = tenantId
            };
            customers.Add(customer);

            customer = new Customer
            {
                Name = "PRECON",
                Address = "San Lorenzo 1580",
                Deleted = false,
                City = "Rosario",
                Country = "Argentina",
                State = "Santa Fe",
                Picture = GetCustomerPicture(5),
                Entity = 0,
                TenantId = tenantId,
                CreatedAt = DateTime.Now
            };
            customers.Add(customer);

            customer = new Customer
            {
                Name = "SUPERVILLE",
                Address = "Sarmiento 601",
                Deleted = false,
                City = "Rosario",
                Country = "Argentina",
                State = "Santa Fe",
                Picture = GetCustomerPicture(6),
                Entity = 0,
                TenantId = tenantId,
                CreatedAt = DateTime.Now
            };
            customers.Add(customer);

            context.Customers.AddRange(customers);
            context.SaveChanges();
        }

        private static void CreateWorks(VialSoftContext context, int tenantId)
        {
            var works = new List<Work>();

            var work = new Work()
            {
                Description = "Obra ROSARIO 1",
                CustomerId = 1,
                CreatedAt = DateTime.Now,
                Id = "Cod obra 1",
                TenantId = tenantId,
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                Status = true
            };
            works.Add(work);

            work = new Work()
            {
                Description = "Obra ROSARIO 2",
                CustomerId = 1,
                CreatedAt = DateTime.Now,
                Id = "Cod obra 2",
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                TenantId = tenantId,
                Status = true
            };
            works.Add(work);

            work = new Work()
            {
                Description = "Obra ROSARIO 3",
                CreatedAt = DateTime.Now,
                CustomerId = 1,
                Id = "Cod obra 3",
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                TenantId = tenantId,
                Status = true
            };
            works.Add(work);

            work = new Work()
            {
                Description = "Obra ROSARIO 4",
                CustomerId = 1,
                CreatedAt = DateTime.Now,
                Id = "Cod obra 5",
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                TenantId = tenantId,
                Status = true
            };
            works.Add(work);

            work = new Work()
            {
                Description = "Obra ROSARIO 5",
                CustomerId = 1,
                CreatedAt = DateTime.Now,
                Id = "Cod obra 5",
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                TenantId = tenantId,
                Status = true
            };
            works.Add(work);

            work = new Work()
            {
                Description = "Obra ROSARIO 6",
                CustomerId = 1,
                CreatedAt = DateTime.Now,
                Id = "Cod obra 6",
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                TenantId = tenantId,
                Status = true
            };
            works.Add(work);

            work = new Work()
            {
                Description = "Obra ROSARIO 7",
                CustomerId = 1,
                CreatedAt = DateTime.Now,
                Id = "Cod obra 7",
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                TenantId = tenantId,
                Status = true
            };
            works.Add(work);

            work = new Work()
            {
                Description = "Obra ROSARIO 8",
                CustomerId = 1,
                CreatedAt = DateTime.Now,
                Id = "Cod obra 8",
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                TenantId = tenantId,
                Status = true
            };
            works.Add(work);

            work = new Work()
            {
                Description = "Obra ROSARIO 9",
                CustomerId = 1,
                CreatedAt = DateTime.Now,
                Id = "Cod obra 9",
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                TenantId = tenantId,
                Status = true
            };
            works.Add(work);

            work = new Work()
            {
                Description = "Obra ROSARIO 10",
                CustomerId = 1,
                CreatedAt = DateTime.Now,
                Id = "Cod obra 10",
                City = "Rosario",
                ZipCode = 2000,
                Country = "Argentina",
                TenantId = tenantId,
                Status = true
            };
            works.Add(work);

            context.Works.AddRange(works);
            context.SaveChanges();
        }

        private static void CreateCategories(VialSoftContext context)
        {
            var categories = new List<Category>();

            var category = new Category()
            {
                Name = "BIENES INMUEBLES"
            };

            categories.Add(category);

            category = new Category()
            {
                Name = "INSTALACIONES MOVILES"
            };

            categories.Add(category);

            category = new Category()
            {
                Name = "VEHICULOS"
            };

            categories.Add(category);

            category = new Category()
            {
                Name = "EQUIPOS Y MAQUINAS"
            };

            categories.Add(category);

            category = new Category()
            {
                Name = "EQUIPOS NO MOTORIZADOS"
            };

            categories.Add(category);


            category = new Category()
            {
                Name = "EQUIPOS Y HERRAMIENTAS DE TALLER"
            };

            categories.Add(category);

            category = new Category()
            {
                Name = "MAQ, MUEBL y UTILES de OFICINA"
            };

            categories.Add(category);

            category = new Category()
            {
                Name = "EQ, M y U de LABORAT/TOPOGRAF"
            };

            categories.Add(category);

            category = new Category()
            {
                Name = "OTROS BIENES DE USO / HERRAMIENTAS DE MANO"
            };

            categories.Add(category);
            context.Categories.AddRange(categories);
            context.SaveChanges();
        }

        private static void CreateSubCategories(VialSoftContext context)
        {
            var subCategories = new List<SubCategory>();

            var subCategory = new SubCategory()
            {
                CategoryId = 2,
                Name = "BASCULA PESAJE CAMIONES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 2,
                Name = "CASILLAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 2,
                Name = "CONTAINERS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 2,
                Name = "GALPONES Y TINGLADOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 2,
                Name = "TANQUES AGUA"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 2,
                Name = "TANQUES COMBUSTIBLE"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 2,
                Name = "TANQUES MAT. ASFALTICOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 2,
                Name = "TANQUES GAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 3,
                Name = "AUTOMOVILES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 3,
                Name = "CAMIONETAS Y UTILITARIOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 3,
                Name = "CAMIONES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 3,
                Name = "ACOPLADOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 3,
                Name = "SEMIRREMOLQUES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "TOPADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "MOTOPALAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "MOTONIVELADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "CARGADORAS FRONTAL"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "RETROCARGADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "MINICARGADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "EXCAVADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "TRACTORES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "COMPACTADORES AUTOPROPULSADOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "COMPACTADORES VIBRANTES DE TIRO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "RODILLOS NEUMÁTICOS AUTOPROPULSADOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "APLANADORAS TANDEM"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "RODILLOS COMBINADOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "FRESADORAS DE PAVIMIENTOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "RECICLADORAS / ESTABILIZADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "BARREDORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = ""
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "GRUPOS ELECTROGENOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "TRANSFORMADORES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "REGADORES DE ASFALTO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "FUSORES DE ASFALTO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "PLANTAS ASFÁLTICAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "CALDERAS DE PLANTA"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "TERMINADORAS ASFALTICAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "PLANTAS MEZCLADORAS DE SUELOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "DISTRIBUIDORAS DE SUELOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "DISTRIBUIDORAS DE ARIDOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "CAMIONES FUERA DE RUTA"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "GRUAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "AUTOELEVADORES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "MANIPULADORES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "PLANTAS HORMIGONERAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "MOTOHORMIGONERAS y HORMIGONERAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "TERMINADORAS HORMIGÓN"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "REGLAS VIBRATORIAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "VIBRADORES DE INMERSIÓN"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "ALLANADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "OTROS EQUIPOS HORMIGÓN"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "TRITURADORAS ARIDOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "CLASIFICADORAS ARIDOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "CINTAS TRANSPORTADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "ARENADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "BOMBAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "COMPRESORES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "HIDROLAVADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "COMPACTADORES MANUALES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "ASERRADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "EXTRACTORES PROBETAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 4,
                Name = "OTROS EQUIPOS MOTORIZADOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 5,
                Name = "COMPACTADORES DE TIRO ESTÁTICOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 5,
                Name = "RASTRAS DE DISCOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 5,
                Name = "PALAS DE TIRO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 5,
                Name = "IMPLEMENTOS MINICARGADORA"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 5,
                Name = "MARTILLOS HIDRAULICOS/NEUMATICOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 5,
                Name = "DESMALEZADORAS DE TIRO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 5,
                Name = "MOLDES HORMIGÓN"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 5,
                Name = "PLATAFORMAS MANTENIMIENTO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = "TALADROS Y AGUJEREADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = "AMOLADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = "APAREJOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = ""
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = "BANCOS DE TRABAJO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = "MATAFUEGOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = "MORSAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = "CARGADORES BATERIA"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = "SOLDADORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 6,
                Name = "EQUIPOS DE MEDICION ELECTRICA"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 7,
                Name = "ESCRITORIOS - TABLEROS - MESAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 7,
                Name = "MUEBLES / ARMARIOS / REPISAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 7,
                Name = "SILLAS, SILLONES, BANQUETAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 7,
                Name = "COMPUTADORAS / IMPRESORAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 7,
                Name = "CAMAS / Jgos Dormitorios / Placares"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 7,
                Name = "LIBRERÍA Y UTILES ESCRITORIO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 7,
                Name = "COMUNICACIONES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 7,
                Name = "ELECTRODOMESTICOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 7,
                Name = "EQUIPAMIENTOS VARIOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "ENVASES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "TAMICES"
            };

            subCategories.Add(subCategory);


            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "SECADO, APARATOS E INSTALACIONES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "INSTRUMENTAL PARA MEDIR PESOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "APARATOS P/ENSAYOS DE SUELOS Y MATERIALES ESTABILIZADOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "ANALISIS MECANICO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "DENSIDAD APARENTE DE MATERIALES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "COMPACTACION DE SUELOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "DETERMINACION DE DENSIDADES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "VALOR SOPORTE"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "CONTROL MEZCLAS BITUMINOSAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "AMOBLAMIENTO E INSTALACIONES FIJAS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "EQUIPOS GPS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "ESTACIONES TOTALES"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "TEODOLITOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "NIVELES OPTICOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "INSTRUMENTAL TOPOGRAFICO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "INSTRUMENTAL LABORATORIO"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 8,
                Name = "ELEMENTOS DE SEGURIDAD Y SEÑALIZACION"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 9,
                Name = "HERRAMIENTAS DE USO GENERAL"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 9,
                Name = "INSTRUMENTAL DE USO GENERAL"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 9,
                Name = "BULONES / TORNILLOS / REPUESTOS / FERRETERIA"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 9,
                Name = "LUBRICANTES y FLUIDOS"
            };

            subCategories.Add(subCategory);

            subCategory = new SubCategory()
            {
                CategoryId = 9,
                Name = "REPUESTOS VARIOS"
            };

            subCategories.Add(subCategory);

            context.SubCategories.AddRange(subCategories);
            context.SaveChanges();
        }

        private static void CreateAssets(VialSoftContext context, int tenantId)
        {
            var assets = new List<Asset>();

            var asset = new Asset()
            {
                CategoryId = 2,
                SubCategoryId = 1,
                Description = "Description",
                Detail = "Detail",
                TradeMark = "Marca 1",
                Model = "Model 1",
                Domain = "LLL503",
                Year = 2016,
                MotorNumber = "8820028844021",
                NumberSerie = "8310030302003",
                MotorTradeMark = "Motor Marca",
                Capacity  = "Capacity 50",
                Power = 128,
                Lubricant = 20,
                Status = true,
                TenantId = tenantId,
                CreatedAt = DateTime.Now,
                StatusAsset = 2,
                Picture = GetAssetPicture(1),
                CustomerId = 2,
                Id = "01",
                WorkId = 1
            };

            assets.Add(asset);

            asset = new Asset()
            {
                CategoryId = 2,
                SubCategoryId = 1,
                Description = "Description",
                Detail = "Detail",
                TradeMark = "Marca 2",
                Model = "Model 2",
                Domain = "LLL103",
                Year = 2014,
                MotorNumber = "1820028844021",
                NumberSerie = "1310030302003",
                MotorTradeMark = "Motor Marca 1",
                Capacity = "Capacity 10",
                Power = 128,
                Lubricant = 20,
                Status = true,
                TenantId = tenantId,
                CreatedAt = DateTime.Now,
                StatusAsset = 1,
                Picture = GetAssetPicture(2),
                CustomerId = 1,
                Id = "02",
                WorkId = 1
            };

            assets.Add(asset);

            

            context.Assets.AddRange(assets);
            context.SaveChanges();
        }

        private static void CreateAssetSummary(VialSoftContext context, int tenantId)
        {
            var assetSummary = new AssetSummary
            {
                AnualProfit = Randomize.Next(50000, 60000),
                AnualProfitVariation = Randomize.Next(1, 5),
                MonthProfit = Randomize.Next(4000, 6000),
                MonthProfitVariation = Randomize.Next(1, 5),
                NewAssets = Randomize.Next(100, 500),
                NewAssetsVariation = Randomize.Next(1, 10),
                Date = DateTime.UtcNow,
                TenantId = tenantId
            };

            context.AssetSummaries.Add(assetSummary);
            context.SaveChanges();
        }

        private static void CreateExpensesSummaries(VialSoftContext context, int tenantId)
        {
            var expensesSummaries = new List<ExpensesSummary>();

            for (int i = 0; i < 24; i++)
            {
                var expensesSummary = new ExpensesSummary
                {
                    Year = DateTime.UtcNow.AddMonths(-i).Year,
                    Month = DateTime.UtcNow.AddMonths(-i).Month,
                    Incomes = Randomize.Next(120000, 150000),
                    Expenses = Randomize.Next(70000, 80000),
                    TenantId = tenantId
                };
                expensesSummaries.Add(expensesSummary);
            }

            context.ExpensesSummaries.AddRange(expensesSummaries);
            context.SaveChanges();
        }

        private static void CreateAccessories(VialSoftContext context)
        {
            var accessories = new List<Accessory>();

            var accessory = new Accessory()
            {
                Description = "Description 1",
                AssetId = 1,
                Picture = GetAssetPicture(1)
            };

            accessories.Add(accessory);

            accessory = new Accessory()
            {
                Description = "Description 2",
                AssetId = 1,
                Picture = GetAssetPicture(2)
            };

            accessories.Add(accessory);

            context.Accessories.AddRange(accessories);
            context.SaveChanges();
        }

        private static void CreateProviders(VialSoftContext context, int tenantId)
        {
            var providers = new List<Provider>();

            var provider = new Provider()
            {
                CreatedAt = DateTime.Now,
                Name =  "Proveedor 1",
                City = "Rosario",
                State = "Santa Fe",
                TypeReceiptId = 1,
                TypeReceipt = TypeReceipt.FacturaA,
                Cuit = "2034999222",     
                TenantId =  tenantId
            };

            providers.Add(provider);

            provider = new Provider()
            {
                CreatedAt = DateTime.Now,
                Name = "Proveedor 2",
                City = "Rosario",
                State = "Santa Fe",
                TypeReceiptId = 1,
                TypeReceipt = TypeReceipt.FacturaA,
                Cuit = "2034999222",
                TenantId = tenantId
            };

            providers.Add(provider);

            context.Providers.AddRange(providers);
            context.SaveChanges();
        }

        private static void CreatePurchaces(VialSoftContext context, int tenantId)
        {
            var purchaces = new List<Purchase>();

            var purchace = new Purchase()
            {
                CreatedAt = DateTime.Now,
                NumberReceipt = 1,
                TenantId = tenantId,
                ProviderId = 1
            };

            purchaces.Add(purchace);

            context.Purchases.AddRange(purchaces);
            context.SaveChanges();

            //Detail
            var purchaceIds = context.Purchases.Select(_ => _.PurchaseId).ToList();

            var purchacesDetail = new List<PurchaseDetail>();

            foreach (var purchaceId in purchaceIds)
            {
                var purchaceDetail = new PurchaseDetail()
                {
                    PurchaseId = purchaceId,
                    AssetId = 1,
                    Price = 20000 
                };
                purchacesDetail.Add(purchaceDetail);

                purchaceDetail = new PurchaseDetail()
                {
                    PurchaseId = purchaceId,
                    AssetId = 2,
                    Price = 50000
                };
                purchacesDetail.Add(purchaceDetail);
            }

            context.PurchaseDetails.AddRange(purchacesDetail);
            context.SaveChanges();
        }

        private static void CreateTransfersWorks(VialSoftContext context, int tenantId)
        {
            var transfersworks = new List<TransferWork>();

            var transferwork = new TransferWork()
            {
                CreatedAt = DateTime.Now,
                NumberReceipt = 1,
                TenantId = tenantId,
                TypeReceipt = TypeReceipt.NotaCreditoA,
                TypeReceiptId = 1
            };

            transfersworks.Add(transferwork);

            context.TransfersWorks.AddRange(transfersworks);
            context.SaveChanges();

            //Detail
            var transferWorksIds = context.TransfersWorks.Select(_ => _.TransferWorkId).ToList();

            var transfersWorksDetail = new List<TransferWorkDetail>();

            foreach (var transferWorksId in transferWorksIds)
            {
                var transferWorkDetail = new TransferWorkDetail()
                {
                    TransferWorkId = transferWorksId,
                    WorkId = 2,
                    AssetId = 1
                };
                transfersWorksDetail.Add(transferWorkDetail);

                transferWorkDetail = new TransferWorkDetail()
                {
                    TransferWorkId = transferWorksId,
                    WorkId = 2,
                    AssetId = 2
                };
                transfersWorksDetail.Add(transferWorkDetail);
            }

            context.TransfersWorksDetails.AddRange(transfersWorksDetail);
            context.SaveChanges();
        }

        private static void CreatePeriods(VialSoftContext context, int tenantId)
        {
            var periods = new List<Period>();

            var period = new Period()
            {
                Name = "Diario o cada 10 Hs",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Semanal o Cada 50 Hs",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Cada 100 Hs",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Cada 250 Hs",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Cada 1000 Hs",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Cada 2000 Hs",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Cada 3000 Hs",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Diario o ante puesta en marcha",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Cada 2500 Km",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Cada 5000 Km",
                TenantId = tenantId
            };

            periods.Add(period);

            period = new Period()
            {
                Name = "Cada 10000 Km",
                TenantId = tenantId
            };

            periods.Add(period);

            context.Periods.AddRange(periods);
            context.SaveChanges();
        }

        private static void CreateJobs(VialSoftContext context)
        {
            var jobs = new List<Job>();

            var job = new Job()
            { 
                Name = "Controlar Nivel Aceite Motor",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar Nivel Refrigerante",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar Nivel Aceite Hidraulico",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar Nivel Aceite Transmision",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar Nivel Aceite Convertidor",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Limpiar filtro de aire",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Drenar tanque de combustible",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Engrase",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar instrumento tablero",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar instrumento tablero",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar aceite compresor",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar tension de correas",
                PeriodId = 1
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar baterias (Untar con grasa los bornes)",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar presion de neumatico",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Engrasar cojinetes de oilacion del eje",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar cojinetes de articulacion y del cilindro de inclinacion de ruedas",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar rotula de la barra de tiro",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar cojinetes del pibote de direccion",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar cojinetes del cilindro del desgarrador (Escarificador)",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar rotula del eslabon de levante del escarificador",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar cojinetes de barra de inclinacion de ruedas",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los casquillos del pasador pivote del eje delantero",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los cojinetes de los tirantes",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los casquillos del pivote de direccion del eje delantero",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los cojinetes de los cilindros de inclinacion de las ruedas",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar cojinetes de articulacion y del cilindro de inclinacion de ruedas",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los casquillos del pasador pivote de inclinacion de las ruedas",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar el cojinete interior del cilindro de la direccion",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los motores de traccion integral (Opcional)",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar el gancho de bola de la barra de tiro",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar el cilindro de giro del circulo y los cojinetes",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar el cojinete del eje de accionamiento de la valvula de giro del circulo",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los pasadores de bola de los cilindros de elevacion de la hoja",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los cojinetes del cilindro de desplazamiento del circulo",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar el cojinete del cilindro de desplazamiento de la hoja",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los cojinetes deñ cilindro de inclinacion de la vertedera",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Lubricar los cojinetes del cilindro de la articulacion",
                PeriodId = 2
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Verificar elementos de desgaste",
                PeriodId = 3
            };

            jobs.Add(job);

            job = new Job()
            {
                Name = "Controlar cuchillas, puntas y dientes de escarificador",
                PeriodId = 3
            };

            jobs.Add(job);

            context.Jobs.AddRange(jobs);
            context.SaveChanges();
            
        }

        private static void CreateCosteAsset(VialSoftContext context, int tenantId)
        {
            var costeAssets = new List<CosteAsset>();

            var coste = new CosteAsset
            {
                AssetId = 1,
                Amortization = 2,
                AnualHorus = 10000,
                FactorHp = 2,
                FinancialCost =  20,
                Performance = 20,
                Repair = 10,
                UsefulLife =  1000,
                PriceAsset = 10000,
                Coste = 20000,
                TenantId = tenantId,
                CreatedAt = DateTime.Now
            };

            costeAssets.Add(coste);
            context.CosteAsset.AddRange(costeAssets);

            context.SaveChanges();

        }

        private static void CreateMaintenanceCard(VialSoftContext context, int tenantId)
        {
            var mantenanceCards = new List<MaintenanceCard>();         

            var mantenanceCard = new MaintenanceCard()
            {
               AssetId = 1,
               TenantId = tenantId,           
            };
            mantenanceCards.Add(mantenanceCard);
            context.MaintenanceCards.AddRange(mantenanceCards);
            context.SaveChanges();

            var maintenaceCardIds = context.MaintenanceCards.Select(_ => _.MaintenanceCardId).ToList();

            foreach (var item in maintenaceCardIds)
            {
                //Detail
                var maintenanceCardDetails = new List<MaintenanceCardDetail>();

                var detail = new MaintenanceCardDetail()
                {
                    PeriodId = 1,
                    JobId = 1,
                    Replacement = "SHELL",
                    MaintenanceCardId = item
                };
                maintenanceCardDetails.Add(detail);

                detail = new MaintenanceCardDetail()
                {
                    PeriodId = 1,
                    JobId = 2,
                    Replacement = "SHELL 2",
                    MaintenanceCardId = item
                };
                maintenanceCardDetails.Add(detail);

                context.MaintenanceCardDetails.AddRange(maintenanceCardDetails);
            }         
            context.SaveChanges();
        }

        private static byte[] GetDefaultUser()
        {
            // Default logo
            return Convert.FromBase64String(UsersFakeImages.Users.First());
        }

        private static byte[] GetCustomerPicture(int index)
        {
            return Convert.FromBase64String(CustomersFakeImages.MsCustomers[index - 1]);
        }
        
        private static byte[] GetAssetPicture(int index)
        {
            return Convert.FromBase64String(AssetsFakeImages.MsAssets[index - 1]);
        }

    }
}
