﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;
using VialSoft.Model;

namespace VialSoft.Data
{

    public class VialSoftContext : IdentityDbContext<ApplicationUser>
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Customer>().Property(_ => _.CustomerId).UseSqlServerIdentityColumn();

            builder.Entity<Work>().Property(_ => _.WorkId).UseSqlServerIdentityColumn();

            builder.Entity<Work>().HasOne(_ => _.Tenant).WithOne().OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Work>().HasOne(_ => _.Customer).WithOne().OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Category>().Property(_ => _.CategoryId).UseSqlServerIdentityColumn();
            builder.Entity<SubCategory>().Property(_ => _.SubCategoryId).UseSqlServerIdentityColumn();
            builder.Entity<SubCategory>().HasOne(_ => _.Category).WithOne().OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Asset>().HasOne(_ => _.Customer).WithOne().OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Asset>().Property(_ => _.AssetId).UseSqlServerIdentityColumn();
            builder.Entity<Accessory>().Property(_ => _.AccessoryId).UseSqlServerIdentityColumn();

            builder.Entity<Purchase>().HasOne(_ => _.Tenant).WithOne().OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Purchase>().HasOne(_ => _.Provider).WithOne().OnDelete(DeleteBehavior.Restrict);

            builder.Entity<PurchaseDetail>().Property(_ => _.PurchaseDetailId).UseSqlServerIdentityColumn();
            builder.Entity<Purchase>().Property(_ => _.PurchaseId).UseSqlServerIdentityColumn();

            builder.Entity<TransferWork>().HasOne(_ => _.Tenant).WithOne().OnDelete(DeleteBehavior.Restrict);
            builder.Entity<TransferWorkDetail>().HasOne(_ => _.Work).WithOne().OnDelete(DeleteBehavior.Restrict);

            builder.Entity<TransferWorkDetail>().Property(_ => _.TransferWorkDetailId).UseSqlServerIdentityColumn();
            builder.Entity<TransferWork>().Property(_ => _.TransferWorkId).UseSqlServerIdentityColumn();

            builder.Entity<Period>().HasOne(_ => _.Tenant).WithOne().OnDelete(DeleteBehavior.Restrict);

            builder.Entity<MaintenanceCard>().Property(_ => _.MaintenanceCardId).UseSqlServerIdentityColumn();
            builder.Entity<MaintenanceCard>().HasOne(_ => _.Tenant).WithOne().OnDelete(DeleteBehavior.Restrict);

            builder.Entity<MaintenanceCardDetail>().HasOne(_ => _.Job).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.Entity<MaintenanceCardDetail>().HasOne(_ => _.Period).WithMany().OnDelete(DeleteBehavior.Restrict);

            builder.Entity<MaintenanceCardDetail>().Property(_ => _.MaintenanceCardDetailId).UseSqlServerIdentityColumn();

            builder.Entity<CosteAsset>().Property(_ => _.CosteId).UseSqlServerIdentityColumn();
            builder.Entity<CosteAsset>().HasOne(_ => _.Tenant).WithOne().OnDelete(DeleteBehavior.Restrict);
            builder.Entity<CosteAsset>().HasOne(_ => _.Asset).WithOne().OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(builder);
        }

        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Work> Works { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<AssetSummary> AssetSummaries { get; set; }
        public DbSet<ExpensesSummary> ExpensesSummaries { get; set; }
        public DbSet<Accessory> Accessories { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<PurchaseDetail> PurchaseDetails { get; set; }
        public DbSet<TransferWork> TransfersWorks { get; set; }
        public DbSet<TransferWorkDetail> TransfersWorksDetails { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<MaintenanceCard> MaintenanceCards { get; set; }
        public DbSet<MaintenanceCardDetail> MaintenanceCardDetails { get; set; }
        public DbSet<CosteAsset> CosteAsset { get; set; }

    }
}
