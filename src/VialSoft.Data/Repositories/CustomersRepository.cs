﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class CustomersRepository
    {
        private readonly VialSoftContext _context;

        public CustomersRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<Customer> GetAsync(int tenantId, int customerId)
        {
            var customer = await _context.Customers
                .Where(p => p.CustomerId == customerId && p.TenantId == tenantId && !p.Deleted)
                .SingleOrDefaultAsync();

            return Build(customer);
        }

        public async Task<IEnumerable<Customer>> GetAsync(int tenantId)
        {
            var customers = await _context.Customers
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .OrderBy(p => p.CustomerId)
                .ToListAsync();

            return customers.Select(_ => new Customer()
                {CustomerId = _.CustomerId, Name = _.Name}).ToList();
        }
        public async Task<IEnumerable<Customer>> GetAsync(int tenantId, int pageSize, int pageCount)
        {
            var customers = await _context.Customers
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .OrderBy(p => p.CustomerId)
                .Skip(pageSize * pageCount)
                .Take(pageSize)
                .ToListAsync();

            return customers.Select(p => Build(p)).ToList();
        }

        public async Task<IEnumerable<Customer>> GetAsync(int tenantId, string name, int count)
        {
            var customers = await _context.Customers
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .Where(p => p.Name.StartsWith(name))
                .OrderBy(p => p.Name)
                .Take(count)
                .ToListAsync();

            return customers.Select(p => Build(p)).ToList();
        }

        public async Task DeleteAsync(int tenantId, int customerId)
        {
            var customer = await _context.Customers
                .SingleOrDefaultAsync(p => p.CustomerId == customerId && p.TenantId == tenantId && !p.Deleted);

            if (customer != null)
            {
                customer.Deleted = true;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<int> AddAsync(Customer customer)
        {
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();
            return customer.CustomerId;
        }

        public async Task UpdateAsync(Customer customer)
        {
            _context.Customers.Update(customer);
            await _context.SaveChangesAsync();
        }

        public string EnumEntity()
        {
            var type = typeof(Entity);
            var enumArray = Enum.GetNames(type)
                        .Select(name => new
                        {
                            id = (int)Enum.Parse(type, name),
                            text = name
                        }).ToArray();

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(enumArray,
                Newtonsoft.Json.Formatting.None,
                new Newtonsoft.Json.JsonSerializerSettings()
                {
                    Converters = new List<Newtonsoft.Json.JsonConverter>
                    {
                        new Newtonsoft.Json.Converters.StringEnumConverter()
                    }
                });

            return data;
        }

        internal Customer Build(Customer customer)
        {
            return new Customer()
            {
                CustomerId = customer.CustomerId,
                Name = customer.Name,
                Address = customer.Address,
                City = customer.City,
                Country = customer.Country,
                State = customer.State,
                Entity = customer.Entity,
                EntityName = RetrieveEntityName(customer.Entity),
                Picture = customer.Picture,
                CreatedAt = customer.CreatedAt,
                TenantId = customer.TenantId
            };
        }

        internal Entity RetrieveEntityName(int entity)
        {
            return entity == 1 ? Entity.Publica : Entity.Privada;
        }
    }
}
