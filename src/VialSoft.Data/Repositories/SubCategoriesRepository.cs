﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class SubCategoriesRepository
    {
        private readonly VialSoftContext _context;

        public SubCategoriesRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<IEnumerable<SubCategory>> GetAsync(int categoryId)
        {
            var subCategories = await _context.SubCategories
                .Where(p => p.CategoryId == categoryId)
                .ToListAsync();
            return subCategories.Select(Build).ToList();
        }

        public async Task<IEnumerable<SubCategory>> GetAsync()
        {
            var subCategories = await _context.SubCategories
                .OrderBy(p => p.SubCategoryId)
                .ToListAsync();

            return subCategories.Select(Build).ToList();
        }

        internal SubCategory Build(SubCategory subcategory)
        {
            return new SubCategory()
            {
                id = subcategory.SubCategoryId,
                text = subcategory.Name
            };
        }
    }
}
