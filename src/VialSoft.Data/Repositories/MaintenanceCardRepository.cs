﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class MaintenanceCardRepository
    {
        private readonly VialSoftContext _context;

        public MaintenanceCardRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<IEnumerable<MaintenanceCardDetail>> GetAsync(int tenantId, int pageSize, int pageCount, int assetId)
        {
            var maintenanceCardDetail = await _context.MaintenanceCards
                .Where(_ => _.AssetId == assetId)
                .Select(_ => _.MaintenanceCardId)
                .FirstOrDefaultAsync();

            var maintenanceCardsDetails = await _context.MaintenanceCardDetails
                            .Include(_ => _.Period)
                            .Include(_ => _.Job)
                            .Where(_ => _.MaintenanceCardId == maintenanceCardDetail && !_.Deleted)
                            .OrderBy(_ => _.PeriodId)
                            .Skip(pageSize * pageCount)
                            .Take(pageSize)
                            .ToListAsync();

            return maintenanceCardsDetails.Select(Build).ToList();
        }

        public async Task<int> AddAsync(MaintenanceCard maintenanceCard)
        {
            var details = maintenanceCard.MaintenanceCardDetail;
            _context.MaintenanceCards.Add(maintenanceCard);

            foreach (var detail in details)
            {
                _context.MaintenanceCardDetails.Add(detail);
            }
            await _context.SaveChangesAsync();
            return maintenanceCard.MaintenanceCardId;
        }

        public async Task DeleteAsync(int tenantId, int maintenanceCardDetailId)
        {
            var detail = await _context.MaintenanceCardDetails
                .SingleOrDefaultAsync(_ => _.MaintenanceCardDetailId  == maintenanceCardDetailId
                                      && !_.Deleted);

            if (detail != null)
            {
                detail.Deleted = true;
                await _context.SaveChangesAsync();
            }
        }

        internal MaintenanceCardDetail Build(MaintenanceCardDetail maintenanceCard)
        {
            return new MaintenanceCardDetail()
            {
                MaintenanceCardDetailId = maintenanceCard.MaintenanceCardDetailId,
                MaintenanceCardId = maintenanceCard.MaintenanceCardId,
                JobId = maintenanceCard.JobId,
                Job = maintenanceCard.Job,
                PeriodId = maintenanceCard.PeriodId,
                Period = maintenanceCard.Period,
                Replacement = maintenanceCard.Replacement
            };
        }


    }
}
