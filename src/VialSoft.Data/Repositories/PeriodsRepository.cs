﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class PeriodsRepository
    {
        private readonly VialSoftContext _context;

        public PeriodsRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<Period> GetAsync(int tenantId, int periodId)
        {
            var Periods = await _context.Periods
                .Where(p => p.PeriodId == periodId)
                .SingleOrDefaultAsync();

            return Build(Periods);
        }

        public async Task<IEnumerable<Period>> GetAsync()
        {
            var Periods = await _context.Periods
                .OrderBy(p => p.PeriodId)
                .ToListAsync();

            return Periods.Select(Build).ToList();
        }

        internal Period Build(Period period)
        {
            return new Period()
            {
                id = period.PeriodId,
                text = period.Name
            };
        }
    }
}
