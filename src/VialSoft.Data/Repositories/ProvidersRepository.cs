﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class ProvidersRepository
    {
        private readonly VialSoftContext _context;

        public ProvidersRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<Provider> GetAsync(int tenantId, int providerId)
        {
            var provider = await _context.Providers
                .Where(p => p.ProviderId == providerId && p.TenantId == tenantId && !p.Deleted)
                .SingleOrDefaultAsync();

            return Build(provider);
        }
        public async Task<IEnumerable<Provider>> GetAsync(int tenantId)
        {
            var providers = await _context.Providers
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .OrderBy(p => p.ProviderId)
                .ToListAsync();

            return providers.Select(Build).ToList();
        }
        public async Task<IEnumerable<Provider>> GetAsync(int tenantId, int pageSize, int pageCount)
        {
            var providers = await _context.Providers
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .OrderBy(p => p.ProviderId)
                .Skip(pageSize * pageCount)
                .Take(pageSize)
                .ToListAsync();

            return providers.Select(Build).ToList();
        }

        public async Task<IEnumerable<Provider>> GetAsync(int tenantId, string name, int count)
        {
            var providers = await _context.Providers
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .Where(p => p.Name.StartsWith(name))
                .OrderBy(p => p.Name)
                .Take(count)
                .ToListAsync();

            return providers.Select(Build).ToList();
        }

        public async Task DeleteAsync(int tenantId, int providerId)
        {
            var provider = await _context.Providers
                .SingleOrDefaultAsync(p => p.ProviderId == providerId && 
                    p.TenantId == tenantId && !p.Deleted);

            if (provider != null)
            {
                provider.Deleted = true;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<int> AddAsync(Provider provider)
        {
            _context.Providers.Add(provider);
            await _context.SaveChangesAsync();
            return provider.ProviderId;
        }

        public async Task UpdateAsync(Provider provider)
        {
            _context.Providers.Update(provider);
            await _context.SaveChangesAsync();
        }

        internal Provider Build(Provider provider)
        {
            return new Provider()
            {
                ProviderId = provider.ProviderId,
                Name = provider.Name,
                City = provider.City,
                State = provider.State,
                Cuit = provider.Cuit,
                Picture = provider.Picture,
                CreatedAt = provider.CreatedAt,
                TenantId = provider.TenantId,
                TypeReceiptId = provider.TypeReceiptId,
                TypeReceipt = GetDescriptionById(provider.TypeReceiptId)
            };
        }

        internal TypeReceipt GetDescriptionById(int id)
        {
            var type = typeof (TypeReceipt);
            var typeSelect = Enum.GetNames(type).Select(
                name => new
                {
                    id = (int) Enum.Parse(type, name),
                    text = name
                }).SingleOrDefault(_ => _.id == id)
                             ?? new {id = 1, text = TypeReceipt.FacturaA.ToString()};

            return (TypeReceipt) Enum.Parse(typeof (TypeReceipt), typeSelect.text);
        }

        public string TypeReceipts()
        {
            var type = typeof(TypeReceipt);
            var enumArray = Enum.GetNames(type)
                .Select(name => new
                {
                    id = (int)Enum.Parse(type, name),
                    text = name
                }).ToArray();

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(enumArray,
                Newtonsoft.Json.Formatting.None,
                new Newtonsoft.Json.JsonSerializerSettings()
                {
                    Converters = new List<Newtonsoft.Json.JsonConverter>
                    {
                        new Newtonsoft.Json.Converters.StringEnumConverter()
                    }
                });

            return data;
        }

    }
}
