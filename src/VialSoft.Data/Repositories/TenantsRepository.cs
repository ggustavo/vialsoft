﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class TenantsRepository
    {
        private readonly VialSoftContext _context;

        public TenantsRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<Tenant> GetAsync(int tenantId)
        {
            var tenant = await _context.Tenants
                                .Where(p => p.TenantId == tenantId)
                .SingleOrDefaultAsync();

            return Build(tenant);
        }

        public async Task<IEnumerable<Tenant>> GetAsync(int pageSize, int pageCount)
        {
            var tenants = await _context.Tenants
                .OrderBy(p => p.TenantId)
                .Skip(pageSize * pageCount)
                .Take(pageSize)
                .ToListAsync();

            return tenants.Select(Build).ToList();
        }

        public async Task<IEnumerable<Tenant>> GetAsync(string name, int count)
        {
            var Tenants = await _context.Tenants
                .Where(p => p.Name.StartsWith(name))
                .OrderBy(p => p.Name)
                .Take(count)
                .ToListAsync();

            return Tenants.Select(p => Build(p)).ToList();
        }

        public async Task DeleteAsync(int tenantId)
        {
            var tenant = await _context.Tenants
                .SingleOrDefaultAsync(p => p.TenantId == tenantId);

            if (tenant != null)
            {
                await _context.SaveChangesAsync();
            }
        }

        public async Task<int> AddAsync(Tenant tenant)
        {
            _context.Tenants.Add(tenant);
            await _context.SaveChangesAsync();
            return tenant.TenantId;
        }

        public async Task UpdateAsync(Tenant tenant)
        {
            _context.Tenants.Update(tenant);
            await _context.SaveChangesAsync();
        }


        internal Tenant Build(Tenant tenant)
        {
            return new Tenant()
            {
                TenantId = tenant.TenantId,
                Name = tenant.Name,
                Address = tenant.Address,
                City = tenant.City,
                Cuit = tenant.Cuit
            };
        }
    }
}
