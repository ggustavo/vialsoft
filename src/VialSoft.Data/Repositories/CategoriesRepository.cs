﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class CategoriesRepository
    {
        private readonly VialSoftContext _context;

        public CategoriesRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<Category> GetAsync(int tenantId, int categoryId)
        {
            var categories = await _context.Categories
                .Where(p => p.CategoryId == categoryId)
                .SingleOrDefaultAsync();

            return Build(categories);
        }

        public async Task<IEnumerable<Category>> GetAsync()
        {
            var categories = await _context.Categories
                .OrderBy(p => p.CategoryId)
                .ToListAsync();

            return categories.Select(Build).ToList();
        }

        internal Category Build(Category category)
        {
            return new Category()
            {
                id = category.CategoryId,
                text = $"{category.CategoryId} - {category.Name}"
            };
        }
    }
}
