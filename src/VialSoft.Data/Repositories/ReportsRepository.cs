﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class ReportsRepository
    {
        private readonly VialSoftContext _context;

        public ReportsRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<AssetSummary> GetAssetsSummaryAsync(int tenantId)
        {
            return await _context.AssetSummaries
                .Where(c => c.TenantId == tenantId)
                .OrderBy(c => c.Date)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<ExpensesSummary>> GetExpensesSummaryAsync(int tenantId, int year)
        {
            return await _context.ExpensesSummaries
                .Where(e => e.TenantId == tenantId & e.Year == year)
                .OrderBy(e => e.Month)
                .ToListAsync();
        }

    }
}
