﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class WorksRepository
    {
        private readonly VialSoftContext _context;

        public WorksRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<IEnumerable<Work>> GetAsync(int tenantId, int pageSize, int pageCount)
        {
            var works = await _context.Works
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .Include(_ => _.Customer)
                .OrderBy(p => p.WorkId)
                .Skip(pageSize * pageCount)
                .Take(pageSize)
                .ToListAsync();

            return works.Select(Build).ToList();
        }

        public async Task<IEnumerable<Work>> GetAsync(int tenantId)
        {
            var works = await _context.Works
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .OrderBy(p => p.WorkId)
                .ToListAsync();

            return works.Select(_ => new Work()
                                { WorkId = _.WorkId, Description = _.Description }).ToList();
        }

        public async Task<Work> GetAsync(int tenantId, int workId)
        {
            var work = await _context.Works
                .Include(_ => _.Customer)
                .Where(p => p.WorkId == workId && p.TenantId == tenantId && !p.Deleted)
                .SingleOrDefaultAsync();

            return Build(work);
        }

        public async Task DeleteAsync(int tenantId, int workId)
        {
            var work = await _context.Works
                .SingleOrDefaultAsync(p => p.WorkId == workId
                                        && p.TenantId == tenantId && !p.Deleted);

            if (work != null)
            {
                work.Deleted = true;
                work.Status = false;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<int> AddAsync(Work work)
        {
            work.Status = true;
            _context.Works.Add(work);
            await _context.SaveChangesAsync();
            return work.WorkId;
        }

        public async Task UpdateAsync(Work work)
        {
            work.Customer = null;
            _context.Works.Update(work);
            await _context.SaveChangesAsync();
        }

        internal Work Build(Work work)
        {
            return new Work()
            {
                City = work.City,
                Country = work.Country,
                ZipCode = work.ZipCode,
                Description = work.Description,
                Status = work.Status,
                CreatedAt = work.CreatedAt,
                TenantId = work.TenantId,
                WorkId = work.WorkId,
                CustomerId = work.CustomerId,
                Customer = work.Customer,
                Id = work.Id
            };
        }
    }
}
