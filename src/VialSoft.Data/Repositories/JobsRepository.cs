﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class JobsRepository
    {
        private readonly VialSoftContext _context;

        public JobsRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<Job> GetAsync(int tenantId, int jobId)
        {
            var Jobs = await _context.Jobs
                .Where(p => p.JobId == jobId)
                .SingleOrDefaultAsync();

            return Build(Jobs);
        }

        public async Task<IEnumerable<Job>> GetAsync()
        {
            var jobs = await _context.Jobs
                .OrderBy(p => p.JobId)
                .ToListAsync();

            return jobs.Select(Build).ToList();
        }

        public async Task<IEnumerable<Job>> GetAsync(int periodId)
        {
            var jobs = await _context.Jobs
                .Where(_ => _.PeriodId == periodId)
                .ToListAsync();

            return jobs.Select(Build).ToList();
        }

        internal Job Build(Job job)
        {
            return new Job()
            {
                id = job.JobId,
                text = job.Name
            };
        }
    }
}
