﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class AssetsRepository
    {
        private readonly VialSoftContext _context;

        public AssetsRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<Asset> GetAsync(int tenantId, int assetId)
        {
            var asset = await _context.Assets
                .Include(_ => _.Category)
                .Include(_ => _.SubCategory)
                .Where(p => p.AssetId == assetId && p.TenantId == tenantId && !p.Deleted)
                .SingleOrDefaultAsync();

            return Build(asset);
        }

        public async Task<IEnumerable<Asset>> GetAsync(int tenantId, int pageSize, int pageCount)
        {
            var assets = await _context.Assets
                .Include(_ => _.Category)
                .Include(_ => _.SubCategory)
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .OrderByDescending(p => p.AssetId)
                .Skip(pageSize * pageCount)
                .Take(pageSize)
                .ToListAsync();

            return assets.Select(Build).ToList();
        }

        public async Task<Asset> GetAsync(int tenantId,string id)
        {
            var asset = await _context.Assets
                .Include(_ => _.Category)
                .Include(_ => _.SubCategory)
                .Include(_ => _.Work)
                .Where(p => p.TenantId == tenantId && !p.Deleted && p.Id == id)
                .OrderByDescending(p => p.AssetId)
                .SingleOrDefaultAsync();

            return Build(asset);
        }

        public async Task DeleteAsync(int tenantId, int assetId)
        {
            var asset = await _context.Assets
                .SingleOrDefaultAsync(p => p.AssetId == assetId && p.TenantId == tenantId && !p.Deleted);

            if (asset != null)
            {
                asset.Deleted = true;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<int> AddAsync(Asset asset)
        {
            _context.Assets.Add(asset);
            await _context.SaveChangesAsync();
            return asset.AssetId;
        }

        public async Task UpdateAsync(Asset asset)
        {
            _context.Assets.Update(asset);
            await _context.SaveChangesAsync();          
        }

        internal Asset Build(Asset asset)
        {
            return new Asset()
            {
                AssetId = asset.AssetId,
                Description = asset.Description,
                Detail = asset.Detail,
                Model = asset.Model,
                TradeMark = asset.TradeMark,
                Domain = asset.Domain,
                Capacity = asset.Capacity,
                Picture = asset.Picture,
                MotorNumber = asset.MotorNumber,
                Category = asset.Category,
                CategoryId = asset.CategoryId,
                SubCategoryId = asset.SubCategoryId,
                SubCategory = asset.SubCategory,
                MotorTradeMark = asset.MotorTradeMark,
                NumberSerie = asset.NumberSerie,
                Power = asset.Power,
                Year = asset.Year,
                Status = asset.Status,
                StatusAsset = asset.StatusAsset,
                StatusNameAsset = RetrieveStatusAsset(asset.StatusAsset),
                CreatedAt = asset.CreatedAt,
                CustomerId = asset.CustomerId,
                Id = asset.Id,
                TenantId = asset.TenantId,
                WorkId = asset.WorkId,
                Work = asset.Work
            };
        }

        internal StatusAsset RetrieveStatusAsset(int statusAsset)
        {
            switch (statusAsset)
            {
                case 2:
                    return StatusAsset.Alquiler;
                case 3:
                    return StatusAsset.Tercero;
                default:
                    return StatusAsset.Propia;
            }
        }

        public string EnumStatusAsset()
        {
            var type = typeof(StatusAsset);
            var enumArray = Enum.GetNames(type)
                .Select(name => new
                {
                    id = (int) Enum.Parse(type, name),
                    text = name
                }).ToArray();

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(enumArray,
                Newtonsoft.Json.Formatting.None,
                new Newtonsoft.Json.JsonSerializerSettings()
                {
                    Converters = new List<Newtonsoft.Json.JsonConverter>
                    {
                        new Newtonsoft.Json.Converters.StringEnumConverter()
                    }
                });

            return data;
        }
    }
}
