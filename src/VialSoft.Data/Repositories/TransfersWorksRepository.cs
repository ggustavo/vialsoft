﻿using System;
using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class TransfersWorksRepository
    {
        private readonly VialSoftContext _context;

        public TransfersWorksRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<TransferWork> GetAsync(int tenantId, int transferworkId)
        {
            var transfersworks = await _context.TransfersWorks
                .Include(_ => _.Details)
                .Where(p => p.TransferWorkId == transferworkId &&
                       p.TenantId == tenantId)
                .SingleOrDefaultAsync();

            return Build(transfersworks);
        }

        public async Task<IEnumerable<TransferWork>> GetAsync(int tenantId, int pageSize, int pageCount)
        {
            var transfersworks = await _context.TransfersWorks
                .Where(p => p.TenantId == tenantId)
                .OrderByDescending(p => p.TransferWorkId)
                .Skip(pageSize * pageCount)
                .Take(pageSize)
                .ToListAsync();

            return transfersworks.Select(Build).ToList();
        }

        public async Task<int> AddAsync(TransferWork transferWork)
        {
            var details = transferWork.Details;
            _context.TransfersWorks.Add(transferWork);
            
            foreach (var detail in details)
            {
                _context.TransfersWorksDetails.Add(detail);
                //update Asset work
                var asset = await _context.Assets
                                .Where(_ => _.AssetId == detail.AssetId
                                       && _.TenantId == transferWork.TenantId)
                                .SingleOrDefaultAsync();
                asset.WorkId = detail.WorkId;
                _context.Assets.Update(asset);
            }
      
            await _context.SaveChangesAsync();

            return transferWork.TransferWorkId;
        }

        public async Task UpdateAsync(TransferWork purchase)
        {
            _context.TransfersWorks.Update(purchase);
            await _context.SaveChangesAsync();
        }

        internal TransferWork Build(TransferWork purchase)
        {
            return new TransferWork()
            {
                TransferWorkId = purchase.TransferWorkId,
                Details = purchase.Details,
                NumberReceipt = purchase.NumberReceipt,
                CreatedAt = purchase.CreatedAt,
                TenantId = purchase.TenantId,
                TypeReceiptId = purchase.TypeReceiptId,
                TypeReceipt = RetrieveTypeReceipt(purchase.TypeReceiptId)
            };
        }

        internal TypeReceipt RetrieveTypeReceipt(int typeReceiptId)
        {
            switch (typeReceiptId)
            {
                case 1:
                    return TypeReceipt.FacturaA;
                case 2:
                    return TypeReceipt.FacturaB;
                case 3:
                    return TypeReceipt.FacturaC;
                case 4:
                    return TypeReceipt.FacturaM;

                case 5:
                    return TypeReceipt.NotaCreditoA;
                case 6:
                    return TypeReceipt.NotaCreditoB;
                case 7:
                    return TypeReceipt.NotaCreditoC;
                case 8:
                    return TypeReceipt.NotaCreditoM;

                case 9:
                    return TypeReceipt.NotaDebitoA;
                case 10:
                    return TypeReceipt.NotaDebitoB;
                case 11:
                    return TypeReceipt.NotaDebitoC;
                case 12:
                    return TypeReceipt.NotaDebitoM;
                default:
                   return TypeReceipt.FacturaA;
            }
        }

        public string TypeReceipts()
        {
            var type = typeof(TypeReceipt);
            var enumArray = Enum.GetNames(type)
                .Select(name => new
                {
                    id = (int)Enum.Parse(type, name),
                    text = name
                }).ToArray();

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(enumArray,
                Newtonsoft.Json.Formatting.None,
                new Newtonsoft.Json.JsonSerializerSettings()
                {
                    Converters = new List<Newtonsoft.Json.JsonConverter>
                    {
                        new Newtonsoft.Json.Converters.StringEnumConverter()
                    }
                });

            return data;
        }
    }
}
