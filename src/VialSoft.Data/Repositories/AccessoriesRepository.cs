﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class AccessoriesRepository
    {
        private readonly VialSoftContext _context;

        public AccessoriesRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<IEnumerable<Accessory>> GetAsync(int tenantId, int assetId)
        {
            var accessories = await _context.Accessories
                .Include(_ => _.Asset).Include(_ => _.Asset.Category)
                .Where(p => p.AssetId == assetId && !p.Deleted)
                .OrderBy(p => p.AccessoryId)
                .ToListAsync();

            return accessories.Select(Build).ToList();
        }

        public async Task<IEnumerable<Accessory>> GetAsync(int tenantId, int pageSize, int pageCount)
        {
            var accessories = await _context.Accessories
                .Include(_ => _.Asset).Include(_ => _.Asset.Category)
                .Where(p => !p.Deleted)
                .OrderBy(p => p.AccessoryId)
                .Skip(pageSize * pageCount)
                .Take(pageSize)
                .ToListAsync();

            return accessories.Select(Build).ToList();
        }

        public async Task DeleteAsync(int tenantId, int accessoryId)
        {
            var accessory = await _context.Accessories
                .SingleOrDefaultAsync(p => p.AccessoryId == accessoryId && !p.Deleted);

            if (accessory != null)
            {
                accessory.Deleted = true;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<int> AddAsync(Accessory accessory)
        {
            _context.Accessories.Add(accessory);
            await _context.SaveChangesAsync();
            return accessory.AccessoryId;
        }

        public async Task UpdateAsync(Accessory accessory)
        {
            _context.Accessories.Update(accessory);
            await _context.SaveChangesAsync();
        }

        internal Accessory Build(Accessory accessory)
        {
            return new Accessory()
            {
                AccessoryId = accessory.AccessoryId,
                Description = accessory.Description,
                AssetId = accessory.AssetId,
                Asset = accessory.Asset,
                Picture = accessory.Picture
            };
        }
    }
}
