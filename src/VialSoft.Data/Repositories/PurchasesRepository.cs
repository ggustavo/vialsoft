﻿using System;
using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class PurchasesRepository
    {
        private readonly VialSoftContext _context;

        public PurchasesRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<Purchase> GetAsync(int tenantId, int purchaseId)
        {
            var purchase = await _context.Purchases
                .Include(_ => _.Provider)
                .Include(_ => _.Details)
                .Where(p => p.PurchaseId == purchaseId &&
                       p.TenantId == tenantId && !p.Deleted)
                .SingleOrDefaultAsync();

            return Build(purchase);
        }

        public async Task<IEnumerable<Purchase>> GetAsync(int tenantId, int pageSize, int pageCount)
        {
            var purchases = await _context.Purchases
                .Include(c => c.Provider)
                .Where(p => p.TenantId == tenantId && !p.Deleted)
                .OrderByDescending(p => p.PurchaseId)
                .Skip(pageSize * pageCount)
                .Take(pageSize)
                .ToListAsync();

            return purchases.Select(Build).ToList();
        }

        public async Task DeleteAsync(int tenantId, int purchaseId)
        {
            var purchase = await _context.Purchases
                .SingleOrDefaultAsync(p => p.PurchaseId == purchaseId 
                                      && p.TenantId == tenantId && !p.Deleted);

            if (purchase != null)
            {
                purchase.Deleted = true;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<int> AddAsync(Purchase purchase)
        {
            var details = purchase.Details;
            _context.Purchases.Add(purchase);

            foreach (var detail in details)
            {
                _context.PurchaseDetails.Add(detail);
            }
            await _context.SaveChangesAsync();
            return purchase.PurchaseId;
        }

        public async Task UpdateAsync(Purchase purchase)
        {
            _context.Purchases.Update(purchase);
            await _context.SaveChangesAsync();
        }

        internal Purchase Build(Purchase purchase)
        {
            return new Purchase()
            {
                PurchaseId = purchase.PurchaseId,
                Details = purchase.Details,
                NumberReceipt = purchase.NumberReceipt,
                CreatedAt = purchase.CreatedAt,
                Provider =  purchase.Provider,
                ProviderId = purchase.ProviderId,
                TenantId = purchase.TenantId
            };
        }

        public string TypeReceipts()
        {
            var type = typeof(TypeReceipt);
            var enumArray = Enum.GetNames(type)
                .Select(name => new
                {
                    id = (int)Enum.Parse(type, name),
                    text = name
                }).ToArray();

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(enumArray,
                Newtonsoft.Json.Formatting.None,
                new Newtonsoft.Json.JsonSerializerSettings()
                {
                    Converters = new List<Newtonsoft.Json.JsonConverter>
                    {
                        new Newtonsoft.Json.Converters.StringEnumConverter()
                    }
                });

            return data;
        }
    }
}
