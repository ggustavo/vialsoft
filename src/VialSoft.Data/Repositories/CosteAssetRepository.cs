﻿using System.Collections.Generic;
using Microsoft.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using VialSoft.Model;

namespace VialSoft.Data.Repositories
{
    public class CosteAssetRepository
    {
        private readonly VialSoftContext _context;

        public CosteAssetRepository(VialSoftContext dbcontext)
        {
            _context = dbcontext;
        }

        public async Task<IEnumerable<CosteAsset>> GetAsync(int tenantId, int pageSize, int pageCount)
        {
            var costeAssets = await _context.CosteAsset
                .Include(_ => _.Asset)
                .Where(p => p.TenantId == tenantId)
                .OrderByDescending(p => p.CosteId)
                .Skip(pageSize * pageCount)
                .Take(pageSize)
                .ToListAsync();

            return costeAssets.Select(Build).ToList();
        }

        public async Task<CosteAsset> GetAsync(int costeAssetId)
        {
            var asset = await _context.CosteAsset
                .Include(_ => _.Asset)
                .Where(_ => _.CosteId == costeAssetId)
                .SingleOrDefaultAsync();

            return Build(asset);
        }

        public async Task<int> AddAsync(CosteAsset costeAsset)
        {
            _context.CosteAsset.Add(costeAsset);
            await _context.SaveChangesAsync();
            return costeAsset.AssetId;
        }

        public async Task UpdateAsync(CosteAsset costeAsset)
        {
            _context.CosteAsset.Update(costeAsset);
            await _context.SaveChangesAsync();          
        }

        internal CosteAsset Build(CosteAsset costeAsset)
        {
            return new CosteAsset
            {
              CosteId = costeAsset.CosteId,
              Amortization = costeAsset.Amortization,
              AnualHorus = costeAsset.AnualHorus,
              Coste = costeAsset.Coste,
              FinancialCost = costeAsset.FinancialCost,
              Performance = costeAsset.Performance,
              UsefulLife = costeAsset.UsefulLife,
              PriceAsset = costeAsset.PriceAsset,
              Repair = costeAsset.Repair,
              FactorHp = costeAsset.FactorHp,
              CreatedAt = costeAsset.CreatedAt,
              AssetId = costeAsset.AssetId,
              Asset = costeAsset.Asset
            };
        }

    }
}
