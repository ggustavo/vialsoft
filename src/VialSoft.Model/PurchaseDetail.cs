﻿using System.ComponentModel.DataAnnotations;

namespace VialSoft.Model
{
    public class PurchaseDetail
    {
        [Key]
        public int PurchaseDetailId { get; set; }

        public int PurchaseId { get; set; }

        public int AssetId { get; set; }

        public Asset Asset { get; set; }

        public double Price { get; set; }

    }
}
