﻿using System;

namespace VialSoft.Model
{
    public class AssetSummary
    {
        public int AssetSummaryId { get; set; }

        public DateTime Date { get; set; }

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public int NewAssets { get; set; }

        public int NewAssetsVariation { get; set; }

        public double MonthProfit { get; set; }

        public double MonthProfitVariation { get; set; }

        public double AnualProfit { get; set; }

        public double AnualProfitVariation { get; set; }
    }
}
