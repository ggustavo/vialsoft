﻿using System.ComponentModel.DataAnnotations;

namespace VialSoft.Model
{
    public class Accessory
    {
        [Key]
        public int AccessoryId { get; set; }

        public int AssetId { get; set; }

        public Asset Asset { get; set; }

        public string Description { get; set; }

        public bool Deleted { get; set; }

        public byte[] Picture { get; set; }
    }
}
