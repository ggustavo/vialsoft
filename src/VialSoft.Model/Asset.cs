﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VialSoft.Model
{
    public class Asset
    {
        [Key]
        public int AssetId { get; set; }

        public string Id { get; set; }

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public int SubCategoryId { get; set; }

        public SubCategory SubCategory { get; set; }

        public int StatusAsset { get; set; }

        [NotMapped]
        [JsonConverter(typeof(StringEnumConverter))]
        public StatusAsset StatusNameAsset { get; set; }

        public string Description { get; set; }

        public string Detail { get; set; }      

        public string Capacity { get; set; }

        public string Model { get; set; }

        public string TradeMark { get; set; }

        public string Domain { get; set; }

        public int Year { get; set; }

        public string NumberSerie { get; set; }

        public string MotorTradeMark { get; set; }

        public string MotorNumber { get; set; }

        public int Power { get; set; }

        public int Lubricant { get; set; }

        public bool Status { get; set; }

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public bool Deleted { get; set; }

        public byte[] Picture { get; set; }

        public DateTime CreatedAt { get; set; }

        public int WorkId { get; set; }

        public Work Work { get; set; }

    }
}
