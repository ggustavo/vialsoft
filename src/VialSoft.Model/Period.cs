﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VialSoft.Model
{
    public class Period
    {
        [Key]
        public int PeriodId { get; set; }

        public string Name { get; set; }

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

        [NotMapped]
        public int id { get; set; }

        [NotMapped]
        public string text { get; set; }
    }
}
