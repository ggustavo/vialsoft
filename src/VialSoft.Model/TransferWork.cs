﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace VialSoft.Model
{
    public class TransferWork
    {
        [Key]
        public int TransferWorkId { get; set; }

        public DateTime CreatedAt { get; set; }

        public int TypeReceiptId { get; set; }

        [NotMapped]
        [JsonConverter(typeof(StringEnumConverter))]
        public TypeReceipt TypeReceipt { get; set; }

        public int NumberReceipt { get; set; }

        public ICollection<TransferWorkDetail> Details { get; set; }

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

    }
}
