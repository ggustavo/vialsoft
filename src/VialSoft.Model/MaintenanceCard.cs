﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VialSoft.Model
{
    public class MaintenanceCard
    {
        [Key]
        public int MaintenanceCardId { get; set; }

        public int AssetId { get; set; }

        public Asset Asset { get; set; }

        public ICollection<MaintenanceCardDetail> MaintenanceCardDetail { get; set; }

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
