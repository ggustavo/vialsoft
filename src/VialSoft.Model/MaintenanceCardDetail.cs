﻿using System.ComponentModel.DataAnnotations;

namespace VialSoft.Model
{
    public class MaintenanceCardDetail
    {
        [Key]
        public int MaintenanceCardDetailId { get; set; }

        public int MaintenanceCardId { get; set; }

        public int PeriodId { get; set; }

        public Period Period { get; set; }

        public int JobId { get; set; }

        public Job Job { get; set; }

        public string Replacement { get; set; }

        public bool Deleted { get; set; }

    }
}
