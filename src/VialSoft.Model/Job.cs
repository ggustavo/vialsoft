﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VialSoft.Model
{
    public class Job
    {
        [Key]
        public int JobId { get; set; }

        public string Name { get; set; }

        public int PeriodId { get; set; }

        public Period Period { get; set; }

        [NotMapped]
        public int id { get; set; }

        [NotMapped]
        public string text { get; set; }
    }
}
