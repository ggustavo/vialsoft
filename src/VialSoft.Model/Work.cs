﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VialSoft.Model
{
    public class Work
    {
        [Key]
        public int WorkId { get; set; }

        public string Description { get; set; }

        public string City { get; set; }

        public int ZipCode { get; set; }

        public string Country { get; set; }

        public DateTimeOffset? CreatedAt { get; set; }

        public bool Status { get; set; }

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public string Id { get; set; }

        public bool Deleted { get; set; }
    }
}
