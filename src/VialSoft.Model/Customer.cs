﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VialSoft.Model
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Address { get; set; }

        public string Country { get; set; }

        public string State { get; set; }

        public int Entity { get; set; }

        [NotMapped]
        [JsonConverter(typeof(StringEnumConverter))]
        public Entity EntityName { get; set; }        

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public bool Deleted { get; set; }

        public byte[] Picture { get; set; }

        public DateTimeOffset? CreatedAt { get; set; }

    }
}
