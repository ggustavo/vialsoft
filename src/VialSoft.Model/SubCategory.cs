﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VialSoft.Model
{
    public class SubCategory
    {
        [Key]
        public int SubCategoryId { get; set; }

        public Category Category { get; set; }

        public int CategoryId { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public int id { get; set; }

        [NotMapped]
        public string text { get; set; }
    }
}
