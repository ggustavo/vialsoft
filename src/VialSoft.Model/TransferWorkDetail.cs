﻿using System.ComponentModel.DataAnnotations;

namespace VialSoft.Model
{
    public class TransferWorkDetail
    {
        [Key]
        public int TransferWorkDetailId { get; set; }

        public int TransferWorkId { get; set; }

        public int AssetId { get; set; }

        public Asset Asset { get; set; }

        public int WorkId { get; set; }

        public Work Work { get; set; }

    }
}
