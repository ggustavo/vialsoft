﻿namespace VialSoft.Model
{
    public enum TypeReceipt
    {
        FacturaA = 1,
        FacturaB = 2,
        FacturaC = 3,
        FacturaM = 4,
        NotaCreditoA = 5,
        NotaCreditoB = 6,
        NotaCreditoC = 7,
        NotaCreditoM = 8,
        NotaDebitoA = 9,
        NotaDebitoB = 10,
        NotaDebitoC = 11,
        NotaDebitoM = 12
    }

}
