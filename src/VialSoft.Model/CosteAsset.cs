﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VialSoft.Model
{
    public class CosteAsset
    {
        [Key]
        public int CosteId { get; set; }

        public int AssetId { get; set; }

        public Asset Asset { get; set; }

        public double PriceAsset { get; set; }

        public int UsefulLife { get; set; }

        public int Amortization { get; set; }

        public int AnualHorus { get; set; }

        public int FinancialCost { get; set; }

        public int Repair { get; set; }

        public int FactorHp { get; set; }

        public decimal Performance { get; set; }

        public double Coste { get; set; }

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
