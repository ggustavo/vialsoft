﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VialSoft.Model
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public int id { get; set; }

        [NotMapped]
        public string text { get; set; }
    }
}
