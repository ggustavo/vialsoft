﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VialSoft.Model
{
    public class Purchase
    {
        [Key]
        public int PurchaseId { get; set; }

        public DateTime CreatedAt { get; set; }

        public int NumberReceipt { get; set; }

        public int ProviderId { get; set; }
        
        public Provider Provider { get; set; }

        public ICollection<PurchaseDetail> Details { get; set; }

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public bool Deleted { get; set; }

    }
}
