﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace VialSoft.Model
{
    public class ApplicationUser : IdentityUser
    {
        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public byte[] Picture { get; set; }
    }
}
