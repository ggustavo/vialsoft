﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace VialSoft.Model
{
    public class Provider
    {
        [Key]
        public int ProviderId { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Cuit { get; set; }

        public int TypeReceiptId { get; set; }

        [NotMapped]
        [JsonConverter(typeof(StringEnumConverter))]
        public TypeReceipt TypeReceipt { get; set; }

        public bool Deleted { get; set; }

        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public DateTimeOffset? CreatedAt { get; set; }

        public byte[] Picture { get; set; }
    }
}
