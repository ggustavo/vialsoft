﻿namespace VialSoft.Model
{
    public enum StatusAsset
    {
        Propia = 1,
        Alquiler = 2,
        Tercero = 3
    }
}
